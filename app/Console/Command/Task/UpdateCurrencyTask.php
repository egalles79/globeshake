<?php
App::uses('ComponentCollection', 'Controller');
App::uses('Controller', 'Controller');
//App::uses('SocialComponent', 'Controller/Component');

class UpdateCurrencyTask extends Shell {

    public $uses = array('Currency');
    public function execute($report = null) {
        $memory_limit = ini_get('memory_limit');
        ini_set('memory_limit', '140M');
        //date_default_timezone_set('Europe/Madrid');
        $this->Currency->recursive = -1;
        $currencies = $this->Currency->find('all');
        $currencies = Hash::extract($currencies,'{n}.Currency.ISO');
        $currenciesString = array();
        foreach ($currencies as $k => $currentCurrency) {
         if ($k != 0) {
             $url = 'http://download.finance.yahoo.com/d/quotes.csv?s='.$currentCurrency.$currencies[0].'=X&f=nl1d1t1';
                $row = 1;

             if (($handle = fopen($url, "r")) !== FALSE) {

               while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                 $row++;
                 $nameField = $valueField = '';
                 $currenciesString[str_replace('/EUR','',$data[0])] = $data[1];
               }
               fclose($handle);
             }
         }
        }
        $currenciesString2 = array();
        foreach ($currenciesString as $k =>$value) {
         $saveValues = $this->Currency->find ('first',array('conditions' => array('ISO' => $k),'fields' => array('id')));
         if (!empty($saveValues)) {
             $currenciesString2[$saveValues['Currency']['id']] = array('id' => $saveValues['Currency']['id'],'conversion_to_euro'=>$value);  
         } else {
             debug($k);
         }
            
        }
        if ($this->Currency->saveMany($currenciesString2)) {
            $this->out('Currencies actualizados '.date('d-m-Y'));
        } else {
            $this->out('Problema en la actualización de currencies '.date('d-m-Y'));
        }
    }
}