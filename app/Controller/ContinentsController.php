<?php
App::uses('AppController', 'Controller');
/**
 * Continents Controller
 *
 * @property Continent $Continent
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class ContinentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	
	public function beforeFilter() {
		$locale = $this->Session->read('Config.language');
		$this->Continent->locale = $locale;
		$this->layout = 'public';
	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Continent->recursive = 0;
		$this->set('continents', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Continent->exists($id)) {
			throw new NotFoundException(__('Invalid continent'));
		}
		$options = array('conditions' => array('Continent.' . $this->Continent->primaryKey => $id));
		$this->set('continent', $this->Continent->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Continent->create();
			if ($this->Continent->save($this->request->data)) {
				$this->Session->setFlash(__('The continent has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The continent could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Continent->exists($id)) {
			throw new NotFoundException(__('Invalid continent'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Continent->save($this->request->data)) {
				$this->Session->setFlash(__('The continent has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The continent could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Continent.' . $this->Continent->primaryKey => $id));
			$this->request->data = $this->Continent->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Continent->id = $id;
		if (!$this->Continent->exists()) {
			throw new NotFoundException(__('Invalid continent'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Continent->delete()) {
			$this->Session->setFlash(__('The continent has been deleted.'));
		} else {
			$this->Session->setFlash(__('The continent could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Continent->recursive = 0;
		$this->set('continents', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Continent->exists($id)) {
			throw new NotFoundException(__('Invalid continent'));
		}
		$options = array('conditions' => array('Continent.' . $this->Continent->primaryKey => $id));
		$this->set('continent', $this->Continent->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Continent->create();
			if ($this->Continent->save($this->request->data)) {
				$this->Session->setFlash(__('The continent has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The continent could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Continent->exists($id)) {
			throw new NotFoundException(__('Invalid continent'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Continent->save($this->request->data)) {
				$this->Session->setFlash(__('The continent has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The continent could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Continent.' . $this->Continent->primaryKey => $id));
			$this->request->data = $this->Continent->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Continent->id = $id;
		if (!$this->Continent->exists()) {
			throw new NotFoundException(__('Invalid continent'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Continent->delete()) {
			$this->Session->setFlash(__('The continent has been deleted.'));
		} else {
			$this->Session->setFlash(__('The continent could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
