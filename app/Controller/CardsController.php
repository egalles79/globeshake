<?php
App::uses('GoogleOauthController', 'Controller');
App::uses('AppController', 'Controller');
/**
 * Cards Controller
 *
 * @property Card $Card
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CardsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->set('breadcrumb', array(
            __('Compra') => array('active' => false, 'url' => '#'),
        ));
        $clientId = Configure::read('Google.clientId');
	    $redirectUri = Router::url(array('action' => 'gmail'), true); 
	    $this->set('clientId', $clientId);
	    $this->set('redirectUri', $redirectUri);

		$this->Card->recursive = 0;
		$this->set('cards', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Card->exists($id)) {
			throw new NotFoundException(__('Invalid card'));
		}
		$options = array('conditions' => array('Card.' . $this->Card->primaryKey => $id));
		$this->set('card', $this->Card->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	private function _setUrlDependence($user) {
	  if ($user['User']['card_completed'] == 0) {
        $this->Session->setFlash(__('Antes de poder realizar cualquier acción debe rellenar su tarjeta de visita'), 'flash_message', array('type' => 'info')); 
        $this->redirect($this->Auth->redirect());
      } else if ($user['User']['formation_completed'] == 0) {
        $this->Session->setFlash(__('Para poder rellenar proyectos debe rellenar su formación'), 'flash_message', array('type' => 'info')); 
        $this->redirect($this->Auth->redirect());
      }
	}
	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->Session->check('UserRegistered')) {
			$user = $this->Session->read('UserRegistered');
			$this->_setUrlDependence($user);
		}
	}
	public function add() {
		if ($this->request->is('post')) {
			$dsCard = $this->Card->getDataSource();
			$dsCard->begin();
			$saveCardData = array();
			$saveCardData['Card']['user_id']   = $this->Session->read('Auth.User.id');
			$saveCardData['Card']['num_cards'] = $this->request->data('num_cards');
			$saveCardData['Card']['action'] = $this->request->data('Cards.opTarjeta');
			$this->Card->create();
			//$saveUSerData['Card']['id_sender'] = //todo para solicitar cambiar este param
			if ($this->Card->save($saveCardData)) {
				//$this->Session->setFlash(__('The card has been saved.'));
				$dsCard->commit();
				$this->Session->setFlash(__('Tarjetas compradas correctamente'), 'flash_message', array('type' => 'success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				//$this->Session->setFlash(__('The card could not be saved. Please, try again.'));
				$dsCard->rollback();
				$this->Session->setFlash(__('Error en la compra de tarjetas'), 'flash_message', array('type' => 'danger'));
			}
		}
		$users = $this->Card->User->find('list');
		$this->set(compact('users'));
	}

	public function gmail() { //you can name the function as you like
	    if (isset($this->request->query['code'])) {
	        $google = new GoogleOauthController; 

	        $authCode = $this->request->query['code'];

	        $clientId = Configure::read('Google.clientId');
	        $clientSecret = Configure::read('Google.clientSecret');
	        $redirectUri = Router::url(array('action' => 'gmail'), true);
	        $result = $google->getContacts($authCode, $clientId, $clientSecret, $redirectUri);
	        if(isset($result['error'])) {
	            $this->Session->setFlash($result['error']);
	        } else {
	            $contacts = $result['contacts']; //here is list of contacts imported
	            $this->loadModel('User');
	            foreach ($contacts as $k => $contact) {
	            	$contacts[$k]['is_globeshake_user'] = false;
	            	if ($this->User->findByEmail($contact['email'])) {
	            		$contacts[$k]['is_globeshake_user'] = true;
	            	}
	            }
	            $this->set('contacts', $contacts); 
	            $this->set('ownerEmail', $result['ownerEmail']); //here is owner email (selected when authenticated)
	        }
	    }
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Card->exists($id)) {
			throw new NotFoundException(__('Invalid card'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Card->save($this->request->data)) {
				$this->Session->setFlash(__('The card has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The card could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Card.' . $this->Card->primaryKey => $id));
			$this->request->data = $this->Card->find('first', $options);
		}
		$users = $this->Card->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Card->id = $id;
		if (!$this->Card->exists()) {
			throw new NotFoundException(__('Invalid card'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Card->delete()) {
			$this->Session->setFlash(__('The card has been deleted.'));
		} else {
			$this->Session->setFlash(__('The card could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Card->recursive = 0;
		$this->set('cards', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Card->exists($id)) {
			throw new NotFoundException(__('Invalid card'));
		}
		$options = array('conditions' => array('Card.' . $this->Card->primaryKey => $id));
		$this->set('card', $this->Card->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Card->create();
			if ($this->Card->save($this->request->data)) {
				$this->Session->setFlash(__('The card has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The card could not be saved. Please, try again.'));
			}
		}
		$users = $this->Card->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Card->exists($id)) {
			throw new NotFoundException(__('Invalid card'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Card->save($this->request->data)) {
				$this->Session->setFlash(__('The card has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The card could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Card.' . $this->Card->primaryKey => $id));
			$this->request->data = $this->Card->find('first', $options);
		}
		$users = $this->Card->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Card->id = $id;
		if (!$this->Card->exists()) {
			throw new NotFoundException(__('Invalid card'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Card->delete()) {
			$this->Session->setFlash(__('The card has been deleted.'));
		} else {
			$this->Session->setFlash(__('The card could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
