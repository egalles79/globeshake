<?php
class IdiomController extends AppController {
    public function beforeFilter() {
        $this->Auth->allow('changeIdiom');
        $this->autoRender = false;
    }
    public function changeIdiom($idiom = null) {
        $this->layout = 'ajax';
        $this->autoRender = false;
        $referente = $_SERVER['HTTP_REFERER'];
        $idioma = (!is_null($idiom)) ? $idiom : 'esp';
        $locale = 'esp';
        if ($idioma == 'gb') {
            $locale = "eng";
        } elseif ($idioma == 'fr') {
            $locale = "fra";
        }
        //$this->Cookie->write('lang', $locale, false, '20 days');
        $this->Session->write('Config.language', $locale);
        $msg = Router::url($referente, true);
        return $msg;
    }
}