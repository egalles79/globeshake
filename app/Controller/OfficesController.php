<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
/**
 * Offices Controller
 *
 * @property Office $Office
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class OfficesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Image');
	public $uses       = array('Office','Geolocalitzation','Company');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Office->recursive = 0;
		$this->set('offices', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Office->exists($id)) {
			throw new NotFoundException(__('Invalid office'));
		}
		$options = array('conditions' => array('Office.' . $this->Office->primaryKey => $id));
		$this->set('office', $this->Office->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->set('breadcrumb', array(
            __('Dar a conocer empresa') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => true, 'url' => '/Companies/ver_cv'),
            __('Añadir oficina') => array('active' => false, 'url' => '#'),
        ));
		if ($this->request->is('post')) {
//guardamos geolocalizacion
				$saveGeoLocalitzacion['longitude'] = $this->request->data['Office']['longitude'];
				$saveGeoLocalitzacion['latitude'] = $this->request->data['Office']['latitude'];
				
				$this->Geolocalitzation->recursive = -1;
				$geolocalitzationId= $this->Geolocalitzation->find('first',array(
					'conditions' => array(
						'longitude' => $saveGeoLocalitzacion['longitude'],
						'latitude' => $saveGeoLocalitzacion['latitude'],
						)
					)
				);
				if (!empty($geolocalitzationId)) {
					$geolocalitzationId = $geolocalitzationId['Geolocalitzation']['id'];
				} else {
					if ($this->Geolocalitzation->save($saveGeoLocalitzacion)) {
						 $geolocalitzationId = $this->Geolocalitzation->getLastInsertID();
					}	
				}
				$this->request->data['Office']['geolocalitzation_id'] = $geolocalitzationId;
				unset($this->request->data['Office']['longitude']);
				unset($this->request->data['Office']['latitude']);
				unset($this->request->data['Office']['country_namex']);
				$image = $this->request->data['Office']['image1'];
				//miramos si tenemos la ciudad sino la guardamos
				if (!empty($this->request->data['Office']['cit_id'])) {
					$cityId = $this->request->data['Office']['cit_id'];
				} else {
					$this->City->recursive = -1;
					$cityId = $this->City->findByName($this->request->data['Office']['cit']);
					if (!empty($cityId)) {
						$cityId = $cityId['City']['id'];
					} else {
						$this->City->create();
						$saveCity['City']['name'] = str_replace('\'','`',$this->request->data['Office']['cit']);
						$saveCity['City']['country_id'] = (int) $this->request->data['Office']['country_id'];
						if ($this->City->save($saveCity)) {
							$cityId = $this->City->getLastInsertID();
						}
					}
				}
				$this->request->data['Office']['city_id'] = $cityId;
				unset($this->request->data['Office']['cit_id']);
				unset($this->request->data['Office']['cit']);
				unset($this->request->data['Office']['addresss']);
				$user = $this->Session->read('UserRegistered');
				$this->request->data['Office']['company_id'] = $user['User']['company_id'];
				$this->Office->create();
				if ($this->Office->save($this->request->data)) {
					$this->Session->setFlash(__('Oficina guardada correctamente'), 'flash_message', array('type' => 'success'));
				} else {
					$this->Session->setFlash(__('No se pudo guardar la oficina, por favor contacte con el administrador'), 'flash_message', array('type' => 'danger'));
				}
				if ($image['error'] == UPLOAD_ERR_OK) {
					$allowedExtensions = array('jpeg', 'jpg', 'gif', 'png');// Extensiones permitidas
					$this->request->data['Office']              = array();
					$this->request->data['Office']['id']        = $this->Office->getLastInsertID();

					$uploads_dir = WWW_ROOT.'files/offices/office_'.$this->request->data['Office']['id'].'/'; // Carpeta destino
					$folder = new Folder($uploads_dir, true, 0777);
					$tmp_name = $image["tmp_name"]; // Ruta al fichero temporal
					$name = time().$image["name"]; // Nombre original del fichero
					$temp_file = new File($tmp_name); // Descriptor de fichero para hacer las comprobaciones
					$mime = explode('/',$temp_file->mime());

					// Comprobamos que la extensión esté permitida
					if(in_array(strtolower($mime[1]), $allowedExtensions)){
					  $temp_file->close();
					  // DESTINO FINAL de la image sanitizando el nombre
					  	$name       = str_replace(' ','_',Inflector::humanize(Inflector::underscore($name)));
					    $image_path = $folder->path.$name;
						$input_file = $tmp_name;
						$output_file = $image_path;
						$this->Image->resize_image( $input_file , 300 , 250 , $output_file );
					  // Movemos el fichero a la carpeta files
					  if(!move_uploaded_file($tmp_name, $image_path)){
					      $this->Session->setFlash(__('Error al subir la imagen.'), 'flash_message', array('type' => 'danger'));
					      $action = array('controller'=>'Companies','action' => 'ver_cv');
					      $this->redirect($action);
					  }
					} else {
					  $this->Session->setFlash(__('Extensión no aceptada'), 'flash_message', array('type' => 'danger'));
					  $action = array('controller'=>'Companies','action' => 'ver_cv');
					  $this->redirect($action);

					}
					//Comprobamos que el tamaño de la imagen sea correcta
					$image_size = getimagesize($image_path);
					$image_dif  = $image_size[0]/$image_size[1];

					//Recogemos los valores validos de tamaño, sera un resultado parecido a la division de ancho i¡y alto
					// $ancho = 100;
					// $alto  = 100;

					// $size = $ancho/$alto;
					$this->request->data['Office']['image']     = $name;
					$this->request->data['Office']['image_dir'] = '/files/offices/office_'.$this->request->data['Office']['id'].'/';
					$this->Office->save($this->request->data, false);
				}
				$action = array('controller'=>'Companies','action' => 'ver_cv');
				$this->redirect($action);
		}
		$companies = $this->Office->Company->find('list');
		$geolocalitzations = $this->Office->Geolocalitzation->find('list');
		$countries = $this->Office->Country->find('list');
		$cities = $this->Office->City->find('list');
		$this->set(compact('companies', 'geolocalitzations', 'countries', 'cities'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Office->exists($id)) {
			throw new NotFoundException(__('Invalid office'));
		}
		if ($this->request->is(array('post', 'put'))) {
				$this->Office->recursive = -1;
				$selectedOffice = $this->Office->findById($this->request->data['Office']['id'],array('fields' => 'principal'));
				if ($selectedOffice['Office']['principal'] == 1) {
					$dataCompany['address'] = $this->request->data['Office']['address'];
					$dataCompany['id']      = $this->request->data['Office']['company_id'];
					$this->Company->save($dataCompany,false);
				}
				//guardamos geolocalizacion
				$saveGeoLocalitzacion['longitude'] = $this->request->data['Office']['longitude'];
				$saveGeoLocalitzacion['latitude'] = $this->request->data['Office']['latitude'];
				
				$this->Geolocalitzation->recursive = -1;
				$geolocalitzationId= $this->Geolocalitzation->find('first',array(
					'conditions' => array(
						'longitude' => $saveGeoLocalitzacion['longitude'],
						'latitude' => $saveGeoLocalitzacion['latitude'],
						)
					)
				);
				if (!empty($geolocalitzationId)) {
					$geolocalitzationId = $geolocalitzationId['Geolocalitzation']['id'];
				} else {
					if ($this->Geolocalitzation->save($saveGeoLocalitzacion)) {
						 $geolocalitzationId = $this->Geolocalitzation->getLastInsertID();
					}	
				}
				$this->request->data['Office']['geolocalitzation_id'] = $geolocalitzationId;
				unset($this->request->data['Office']['longitude']);
				unset($this->request->data['Office']['latitude']);
				unset($this->request->data['Office']['country_namex']);
				
				//miramos si tenemos la ciudad sino la guardamos
				if (!empty($this->request->data['Office']['cit_id'])) {
					$cityId = $this->request->data['Office']['cit_id'];
				} else {
					$this->City->recursive = -1;
					$cityId = $this->City->findByName($this->request->data['Office']['cit']);
					if (!empty($cityId)) {
						$cityId = $cityId['City']['id'];
					} else {
						$this->City->create();
						$saveCity['City']['name'] = str_replace('\'','`',$this->request->data['Office']['cit']);
						$saveCity['City']['country_id'] = (int) $this->request->data['Office']['country_id'];
						if ($this->City->save($saveCity)) {
							$cityId = $this->City->getLastInsertID();
						}
					}
				}
				$this->request->data['Office']['city_id'] = $cityId;
				unset($this->request->data['Office']['cit_id']);
				unset($this->request->data['Office']['cit']);
				unset($this->request->data['Office']['addresss']);
				$image = $this->request->data['Office']['image1'];
				if ($image['error'] == UPLOAD_ERR_OK) {
					$allowedExtensions = array('jpeg', 'jpg', 'gif', 'png');// Extensiones permitidas
					$uploads_dir = WWW_ROOT.'files/offices/office_'.$this->request->data['Office']['id'].'/'; // Carpeta destino
					$folder = new Folder($uploads_dir, true, 0777);
					$tmp_name = $image["tmp_name"]; // Ruta al fichero temporal
					$name = time().$image["name"]; // Nombre original del fichero
					$temp_file = new File($tmp_name); // Descriptor de fichero para hacer las comprobaciones
					$mime = explode('/',$temp_file->mime());

					// Comprobamos que la extensión esté permitida
					if(in_array(strtolower($mime[1]), $allowedExtensions)){
					  $temp_file->close();
					  // DESTINO FINAL de la image sanitizando el nombre
					  	$name       = str_replace(' ','_',Inflector::humanize(Inflector::underscore($name)));
					    $image_path = $folder->path.$name;
						$input_file = $tmp_name;
						$output_file = $image_path;
						$this->Image->resize_image( $input_file , 300 , 250 , $output_file );
					  // Movemos el fichero a la carpeta files
					  if(!move_uploaded_file($tmp_name, $image_path)){
					      $this->Session->setFlash(__('Error al subir la imagen.'), 'flash_message', array('type' => 'danger'));
					      $action = array('controller'=>'Companies','action' => 'ver_cv');
					      $this->redirect($action);
					  }
					} else {
					  $this->Session->setFlash(__('Extensión no aceptada'), 'flash_message', array('type' => 'danger'));
					  $action = array('controller'=>'Companies','action' => 'ver_cv');
					  $this->redirect($action);

					}
					//Comprobamos que el tamaño de la imagen sea correcta
					$image_size = getimagesize($image_path);
					$image_dif  = $image_size[0]/$image_size[1];

					//Recogemos los valores validos de tamaño, sera un resultado parecido a la division de ancho i¡y alto
					// $ancho = 100;
					// $alto  = 100;

					// $size = $ancho/$alto;
					$this->request->data['Office']['image']     = $name;
					$this->request->data['Office']['image_dir'] = '/files/offices/office_'.$this->request->data['Office']['id'];
				}

				unset($this->request->data['Office']['image1']);

			if ($this->Office->save($this->request->data)) {
				$this->Session->setFlash(__('Oficina editada correctamente'), 'flash_message', array('type' => 'success'));
				$this->redirect(array('controller'=>'Companies','action' => 'ver_cv'));
			} else {
				$this->Session->setFlash(__('Error al intentar guardar la oficina, por favor contacte con el administrador'), 'flash_message', array('type' => 'danger'));
			}

		} else {
			$options = array('conditions' => array('Office.' . $this->Office->primaryKey => $id));
			$this->request->data = $this->Office->find('first', $options);
		}
		$this->set('breadcrumb', array(
            __('Dar a conocer empresa') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => true, 'url' => '/Companies/ver_cv'),
            __('Editar oficina %s',array($this->request->data['Office']['name'])) => array('active' => false, 'url' => '#'),
        ));
		$companies = $this->Office->Company->find('list');
		$geolocalitzations = $this->Office->Geolocalitzation->find('list');
		$countries = $this->Office->Country->find('list');
		$cities = $this->Office->City->find('list');
		$this->set('data',$this->request->data);
		$this->set(compact('companies', 'geolocalitzations', 'countries', 'cities'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Office->id = $id;
		if (!$this->Office->exists()) {
			throw new NotFoundException(__('Invalid office'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Office->delete()) {
			$this->Session->setFlash(__('The office has been deleted.'));
		} else {
			$this->Session->setFlash(__('The office could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
