<?php
App::uses('AppController', 'Controller');
/**
 * UserProjects Controller
 *
 * @property UserProject $UserProject
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UserProjectsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
	public $uses = array('CompanyProjectsUser','CompanyProject','CompanyProjectsSector','Company','City','UserProject','Geolocalitzation','Sector','Functionality','Department','UsersUserProject','User');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UserProject->recursive = 0;
		$this->set('userProjects', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UserProject->exists($id)) {
			throw new NotFoundException(__('Invalid user project'));
		}
		$options = array('conditions' => array('UserProject.' . $this->UserProject->primaryKey => $id));
		$this->set('userProject', $this->UserProject->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add($id = null) {
		$user = $this->Session->read('UserRegistered');
		$this->set('user',$user);
		$this->Country->locale = $this->Session->read('Config.language');
		$this->set('breadcrumb', array(
            __('Dar a conocer mi perfil') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => true, 'url' => '/Users/ver_cv'),
            __('Añadir proyecto') => array('active' => false, 'url' => '#'),
        ));
		if ($this->request->is('post')) {
			//cogemos el user para trabajar con el
			//budget correcto
			$this->request->data['UserProject']['budget'] = $this->request->data['UserProject']['budge'];
			unset($this->request->data['UserProject']['budge']);

			//tratamiento de fechas
			$dateInit = explode('/',$this->request->data['UserProject']['init_date']);
			
			if (empty($this->request->data['UserProject']['on_going'])) {
				$dateEnd = explode('/',$this->request->data['UserProject']['end_date']);
				$dateEnd  = new DateTime($dateEnd[1].'-'.$dateEnd[0].'-01');
				$dateEnd = $dateEnd->format('Y-m-d');
			} else {
				$dateEnd = null;
			}
			$dateInit = new DateTime($dateInit[1].'-'.$dateInit[0].'-01');
			$dateInit = $dateInit->format('Y-m-d');
			
			$this->request->data['UserProject']['start_date'] = $dateInit;
			$this->request->data['UserProject']['end_date']   = $dateEnd;
			unset($this->request->data['UserProject']['init_date']);

			//ya tenemos el country_id gracias a google
			unset($this->request->data['UserProject']['country_namex']);

			//miramos si la empresa existe y si no la creamos
			if (empty($this->request->data['UserProject']['CompanyName'])) {
				$this->request->data['UserProject']['CompanyName'] = $this->request->data['UserProject']['CompanyNameHidden'];
			}
			$companyName = str_replace('\'','`',$this->request->data['UserProject']['CompanyName']);
			$this->Company->recursive = -1;
			$companyId = $this->Company->findByName($companyName);
			if (!empty($companyId)) {
				$companyId = $companyId['Company']['id'];
			} else {
				$this->Company->create();
				$companySave['Company']['name'] = $companyName;
				if ($this->Company->save($companySave)) {
					$companyId = $this->Company->getLastInsertID();
				}
			}
			$this->request->data['UserProject']['company_id'] = $companyId;
			unset($this->request->data['UserProject']['CompanyName']);

			//miramos si el customer existe y si no la creamos como empresa
			$companyName = str_replace('\'','`',$this->request->data['UserProject']['custome']);
			
			$companyId = $this->Company->findByName($companyName);
			if (!empty($companyId)) {
				$companyId = $companyId['Company']['id'];
			} else {
				$this->Company->create();
				$companySave['Company']['name'] = $companyName;
				if ($this->Company->save($companySave)) {
					$companyId = $this->Company->getLastInsertID();
				}
			}
			$this->request->data['UserProject']['customer_id'] = $companyId;
			unset($this->request->data['UserProject']['custome']);
			
			//miramos si tenemos la ciudad sino la guardamos
			if (!empty($this->request->data['UserProject']['cit_id']) && is_numeric($this->request->data['UserProject']['cit_id'])) {
				$cityId = $this->request->data['UserProject']['cit_id'];
			} else {
				$this->City->recursive = -1;
				$cityId = $this->City->findByName($this->request->data['UserProject']['cit']);
				if (!empty($cityId)) {
					$cityId = $cityId['City']['id'];
				} else {
					$this->City->create();
					$saveCity['City']['name'] = str_replace('\'','`',$this->request->data['UserProject']['cit']);
					$saveCity['City']['country_id'] = (int) $this->request->data['UserProject']['country_id'];
					if ($this->City->save($saveCity)) {
						$cityId = $this->City->getLastInsertID();
					}
				}
			}
			$this->request->data['UserProject']['city_id'] = $cityId;
			unset($this->request->data['UserProject']['cit_id']);
			unset($this->request->data['UserProject']['cit']);

			//encodeamos tagss
			if (!empty($this->request->data['UserProject']['tags'])) {
				$this->request->data['UserProject']['tags'] = json_encode($this->request->data['UserProject']['tags']);	
			} else {
				$this->request->data['UserProject']['tags'] = null;
			}
			//guardamos geolocalizacion
			$saveGeoLocalitzacion['longitude'] = $this->request->data['UserProject']['longitude'];
			$saveGeoLocalitzacion['latitude'] = $this->request->data['UserProject']['latitude'];
			$this->Geolocalitzation->recursive = -1;
			$geolocalitzationId= $this->Geolocalitzation->find('first',array(
				'conditions' => array(
					'longitude' => $saveGeoLocalitzacion['longitude'],
					'latitude' => $saveGeoLocalitzacion['latitude'],
					)
				)
			);
			if (!empty($geolocalitzationId)) {
				$geolocalitzationId = $geolocalitzationId['Geolocalitzation']['id'];
			} else {
				if ($this->Geolocalitzation->save($saveGeoLocalitzacion)) {
					 $geolocalitzationId = $this->Geolocalitzation->getLastInsertID();
				}	
			}
			$this->request->data['UserProject']['geolocalitzation_id'] = $geolocalitzationId;

			unset($this->request->data['UserProject']['longitude']);
			unset($this->request->data['UserProject']['latitude']);
			
			//miramos si tenemos el sector del proyecto sino la guardamos
			$sectorProjectName = str_replace('\'','`',$this->request->data['UserProject']['sector_proj']);
			$this->Sector->recursive = -1;
			$sectorProjectId = $this->Sector->findByName($sectorProjectName);
			if (!empty($sectorProjectId)) {
				$sectorProjectId = $sectorProjectId['Sector']['id'];
			} else {
				if (!empty($sectorProjectName)) {
					$this->Sector->create();
					$sectorSave['Sector']['name'] = $sectorProjectName;
					if ($this->Sector->save($sectorSave)) {
						$sectorProjectId = $this->Sector->getLastInsertID();
					}
				} else {
					$sectorProjectId = null;
				}
			}
			$this->request->data['UserProject']['sector_project_id'] = $sectorProjectId;
			unset($this->request->data['UserProject']['sector_proj']);

			//miramos si tenemos el sector del servicio sino la guardamos
			$sectorServiceName = str_replace('\'','`',$this->request->data['UserProject']['sector_servic']);
			$this->Sector->recursive = -1;
			$sectorServiceId = $this->Sector->findByName($sectorServiceName);
			if (!empty($sectorServiceId)) {
				$sectorServiceId = $sectorServiceId['Sector']['id'];
			} else {
				if (!empty($sectorServiceName)) {
					$this->Sector->create();
					$sectorSave['Sector']['name'] = $sectorServiceName;
					if ($this->Sector->save($sectorSave)) {
						$sectorServiceId = $this->Sector->getLastInsertID();
					}
				} else {
					$sectorServiceId = null;
				}
			}

			$this->request->data['UserProject']['sector_service_id'] = $sectorServiceId;
			unset($this->request->data['UserProject']['sector_servic']);
			
			//miramos si tenemos la funcionalidad del proyecto sino la guardamos
			$functionalityName = str_replace('\'','`',$this->request->data['UserProject']['functionalit']);
			$this->Functionality->recursive = -1;
			$functionalityId = $this->Functionality->findByName($functionalityName);
			if (!empty($functionalityId)) {
				$functionalityId = $functionalityId['Functionality']['id'];
			} else {
				if (!empty($functionalityName)) {
					$this->Functionality->create();
					$functionSave['Functionality']['name'] = $functionalityName;
					if ($this->Functionality->save($functionSave)) {
						$functionalityId = $this->Functionality->getLastInsertID();
					}
				} else {
					$functionalityId = null;
				}
			}
			$this->request->data['UserProject']['functionality_id'] = $functionalityId;
			unset($this->request->data['UserProject']['functionalit']);

			//miramos si tenemos la departamento (subfuncionalidad) del proyecto sino la guardamos
			$departmentName = str_replace('\'','`',$this->request->data['UserProject']['departmen']);
			$this->Department->recursive = -1;
			$departmentId = $this->Department->findByName($departmentName);
			if (!empty($departmentId)) {
				$departmentId = $departmentId['Department']['id'];
			} else {
				if (!empty($departmentName)) {
					$this->Department->create();
					$departmentSave['Department']['name'] = $departmentName;
					if ($this->Department->save($departmentSave)) {
						$departmentId = $this->Department->getLastInsertID();
					}
				} else {
					$departmentId = null;
				}
			}
			$this->request->data['UserProject']['department_id'] = $departmentId;
			unset($this->request->data['UserProject']['departmen']);

			$this->request->data['UserProject']['duration_month'] = (empty($this->request->data['UserProject']['duration_month'])) ? 0 : $this->request->data['UserProject']['duration_month'];
			$this->request->data['UserProject']['duration_year'] = (empty($this->request->data['UserProject']['duration_year'])) ? 0 : $this->request->data['UserProject']['duration_year'];
			$this->request->data['UserProject']['duration_day'] = (empty($this->request->data['UserProject']['duration_day'])) ? 0 : $this->request->data['UserProject']['duration_day'];
			$this->request->data['UserProject']['duration_hours'] = (empty($this->request->data['UserProject']['duration_hours'])) ? 0 : $this->request->data['UserProject']['duration_hours'];
			
			if (empty($this->request->data['UserProject']['id'])) {
				$this->UserProject->create();
				$this->request->data['UserProject']['is_completed'] = $this->checkTantPerCent($this->request->data, false);
				if ($this->UserProject->save($this->request->data)) {
					$this->UsersUserProject->create();
					$saveUsersUserProject['UsersUserProject']['user_project_id'] = $this->UserProject->getLastInsertID();
					$saveUsersUserProject['UsersUserProject']['user_id']         = $user['User']['id'];
					$this->UsersUserProject->save($saveUsersUserProject);

					//guardamos proyecto de compañía
					unset($this->request->data['UserProject']['budget']);
					unset($this->request->data['UserProject']['budget_currency_id']);
					unset($this->request->data['UserProject']['duration_month']);
					unset($this->request->data['UserProject']['duration_year']);
					unset($this->request->data['UserProject']['duration_day']);
					unset($this->request->data['UserProject']['duration_hours']);
					unset($this->request->data['UserProject']['actual_company']);
					unset($this->request->data['UserProject']['end_date']);
					unset($this->request->data['UserProject']['CompanyNameHidden']);
					unset($this->request->data['UserProject']['volumes']);
					unset($this->request->data['UserProject']['tags']);
					unset($this->request->data['UserProject']['description']);
					unset($this->request->data['UserProject']['start_date']);
					unset($this->request->data['UserProject']['where_to_see']);
					unset($this->request->data['UserProject']['who_can_see']);
					unset($this->request->data['UserProject']['sector_project_id']);
					unset($this->request->data['UserProject']['sector_service_id']);
					unset($this->request->data['UserProject']['functionality_id']);
					unset($this->request->data['UserProject']['department_id']);

					$saveCompanyProject['CompanyProject']                           = $this->request->data['UserProject'];
					//$saveCompanyProject['CompanyProject']['sector_id']              = $saveCompanyProject['CompanyProject']['sector_project_id'];
					$saveCompanyProject['CompanyProject']['contact_responsable_id'] = $user['User']['id'];
					$saveCompanyProject['CompanyProject']['last_reviewer_id']       = $user['User']['id'];
					$this->CompanyProject->create();
					if ($this->CompanyProject->save($saveCompanyProject)) {
						
					} else {
						debug($this->CompanyProject->validationErrors).die;
					}
					
					$saveCompanyProject = array();
					$companyProjectId = $this->CompanyProject->getLastInsertID();
					

					//guardamos sectores de la compañía asociadas a la compañía
					$saveSectorProject['CompanyProjectsSector']['sector_id']          = $sectorServiceId;
					$saveSectorProject['CompanyProjectsSector']['company_project_id'] = $companyProjectId;
					$companySectorExistent = $this->CompanyProjectsSector->find('first', array('conditions' => array('CompanyProjectsSector.sector_id' => $sectorServiceId,'CompanyProjectsSector.company_project_id' => $companyProjectId)));
					if (!empty($companySectorExistent)) {
						$saveSectorProject['CompanyProjectsSector']['id'] = $companySectorExistent['CompanyProjectsSector']['id'];
					} else {
						$this->CompanyProjectsSector->create();
					}
					$this->CompanyProjectsSector->save($saveSectorProject);
					

					//guardamos usuario asociado al proyecto de compañia
					$saveCompanyProjectUser['CompanyProjectsUser']['user_id'] = $user['User']['id'];
					$saveCompanyProjectUser['CompanyProjectsUser']['company_project_id'] = $companyProjectId;

					$saveCompanyProjectUserExistent = $this->CompanyProjectsUser->find('first', array('conditions' => array('user_id' => $user['User']['id'],'company_project_id' => $companyProjectId)));
					if (!empty($saveCompanyProjectUserExistent)) {
						$saveCompanyProject['CompanyProjectsUser']['id'] = $saveCompanyProjectUserExistent['CompanyProjectsUser']['id'];
					} else {
						$this->CompanyProjectsUser->create();
					}

					$this->CompanyProjectsUser->save($saveCompanyProjectUser);
					$this->Session->setFlash(__('Proyecto guardado correctamente'), 'flash_message', array('type' => 'success'));
					return $this->redirect(array('controller' => 'Users','action' => 'ver_cv#pr_'.$this->UserProject->getLastInsertID()));
				} else {
					debug($this->request->data);
					debug($this->UserProject->validationErrors).die;
					$this->Session->setFlash(__('Hubo problemas al intentar guardar el proyecto, inténtelo de nuevo, gracias'), 'flash_message', array('type' => 'success'));
					return $this->redirect(array('controller' => 'UserProjects','action' => 'add'));
				}
			} else {
				$this->request->data['UserProject']['is_completed'] = $this->checkTantPerCent($this->request->data, false);
				if ($this->UserProject->save($this->request->data)) {
					$this->Session->setFlash(__('Proyecto editado correctamente'), 'flash_message', array('type' => 'success'));
					return $this->redirect(array('controller' => 'Users','action' => 'ver_cv#pr_'.$this->request->data['UserProject']['id']));
				} else {
					$this->Session->setFlash(__('Hubo problemas al intentar guardar el proyecto, inténtelo de nuevo, gracias'), 'flash_message', array('type' => 'success'));
					return $this->redirect(array('controller' => 'UserProjects','action' => 'edit'));
				}
			}
		}
		$budgetCurrencies = $this->UserProject->BudgetCurrency->find('list',array('conditions' => array('conversion_to_euro > ' => 0,'conversion_to_euro != ' => null)));
		$countries = $this->UserProject->Country->find('list');
		$sectorProjects = $this->UserProject->SectorProject->find('list');
		$this->City->recursive = -1;
		$cities = $this->City->find('all',array('fields' => array('City.name','City.country_id')));
		$cities = Hash::extract($cities,'{n}.City');
		$functionalities = $this->UserProject->Functionality->find('list');
		$sectorServices = $this->UserProject->SectorService->find('list');
		$departments = $this->UserProject->Department->find('list');
		$companies = $this->UserProject->Company->find('list');
		$customers = $this->UserProject->Customer->find('list');
		$companiesText = '';
		foreach($companies as $company) {
			$companiesText .= '"'.$company.'",';
		}
		$companiesText = substr($companiesText,0,strlen($companiesText)-1);
		$this->set('companiesText',$companiesText);
		$departmentsText = '';
		foreach($departments as $department) {
			$departmentsText .= '"'.$department.'",';
		}
		$departmentsText = substr($departmentsText,0,strlen($departmentsText)-1);
		$this->set('departmentsText',$departmentsText);
		$citiesText = '';
		foreach($cities as $city) {
			$citiesText .= '"'.$city['name'].' ('.$countries[$city['country_id']].')",';
		}
		$citiesText = substr($citiesText,0,strlen($citiesText)-1);
		$this->set('citiesText',$citiesText);
		$sectorProjectText = '';
		foreach($sectorProjects as $sectorProject) {
			$sectorProjectText .= '"'.$sectorProject.'",';
		}
		$sectorProjectText = substr($sectorProjectText,0,strlen($sectorProjectText)-1);
		$this->set('sectorProjectText',$sectorProjectText);

		$functionalitiesText = '';
		foreach($functionalities as $functionality) {
			$functionalitiesText .= '"'.$functionality.'",';
		}
		$functionalitiesText = substr($functionalitiesText,0,strlen($functionalitiesText)-1);
		$this->set('functionalitiesText',$functionalitiesText);

		$this->set(compact('budgetCurrencies', 'billingCurrencies', 'countries', 'sectorProjects', 'cities', 'functionalities', 'sectorServices', 'departments', 'companies', 'customers'));
	}

/**
 * add method
 *
 * @return void
 */
	public function edit($id) {
		$this->set('projectId', $id);
		if (!$this->UserProject->exists($id)) {
			throw new NotFoundException(__('Proyecto no válido'));
		}
		$user = $this->Session->read('UserRegistered');

		$this->UsersUserProject->recursive = 2;
		$projectFromUser = $this->UsersUserProject->find('first',
			array(
				'conditions' => array(
					'user_id' => $user['User']['id'],
					'user_project_id' => $id
				),
			)
		);
		$this->set('user',$user);
		if (empty($projectFromUser)) {
			throw new NotFoundException(__('Éste proyecto no perteneces a éste usuario.Acceda a su panel de control a través del menú principal'));
		}
		if ($projectFromUser['UserProject']['company_id'] == $user['User']['company_id']) {
			$actualCompany = true;
		} else {
			$actualCompany = false;
		}
		$this->set('defaultWhoCanSee',$projectFromUser['UserProject']['who_can_see']);
		$this->set('defaultWhereToSee',$projectFromUser['UserProject']['where_to_see']);
		$this->set('defaultProject',$projectFromUser['UserProject']['id']);
		$this->set('defaultCompany',$actualCompany);
		$this->set('defaultCompanyName',$projectFromUser['UserProject']['Company']['name']);
		$this->set('defaultCountryName',$projectFromUser['UserProject']['Country']['name']);
		$this->set('defaultCountryId',$projectFromUser['UserProject']['Country']['id']);
		$this->set('defaultCityName',$projectFromUser['UserProject']['City']['name']);
		$this->set('defaultCustomerName',$projectFromUser['UserProject']['Customer']['name']);
		$this->set('defaultName',$projectFromUser['UserProject']['name']);
		$this->set('defaultVolumes',$projectFromUser['UserProject']['volumes']);
		$this->set('defaultBudget',$projectFromUser['UserProject']['budget']);
		$this->set('defaultCompletAddress',$projectFromUser['UserProject']['complet_address']);
		$this->set('defaultBudgetCurrencyId',$projectFromUser['UserProject']['budget_currency_id']);
		$this->set('defaultLatitude',$projectFromUser['UserProject']['Geolocalitzation']['latitude']);
		$this->set('defaultLongitude',$projectFromUser['UserProject']['Geolocalitzation']['longitude']);
		$this->set('defaultAddress',$projectFromUser['UserProject']['address']);
		if (!empty($projectFromUser['UserProject']['Functionality'])) {
			$this->set('defaultFunctionality',$projectFromUser['UserProject']['Functionality']['name']);
		} else {
			$this->set('defaultFunctionality',null);	
		}
		if (!empty($projectFromUser['UserProject']['Department'])) {
			$this->set('defaultDepartment',$projectFromUser['UserProject']['Department']['name']);
		} else {
			$this->set('defaultDepartment',null);	
		}
		$this->set('defaultDurationYear',$projectFromUser['UserProject']['duration_year']);
		$this->set('defaultDurationMonth',$projectFromUser['UserProject']['duration_month']);
		$this->set('defaultDurationDay',$projectFromUser['UserProject']['duration_day']);
		$this->set('defaultDurationHour',$projectFromUser['UserProject']['duration_hours']);
		$this->set('defaultDescription',$projectFromUser['UserProject']['description']);
		$this->set('defaultTags',$projectFromUser['UserProject']['tags']);
		$dataInit = new DateTime ($projectFromUser['UserProject']['start_date']);
		$this->set('defaultDataInit',$dataInit->format('m/Y'));
		
		if (!empty($projectFromUser['UserProject']['end_date'])) {
			$dataEnd = new DateTime ($projectFromUser['UserProject']['end_date']);
			$this->set('defaultDataEnd',$dataEnd->format('m/Y'));
		} else {
			$this->set('defaultDataEnd',null);
		}
		
		$this->set('defaultOnGoing',$projectFromUser['UserProject']['on_going']);
		$this->set('defaultSectorProjectId',$projectFromUser['UserProject']['sector_project_id']);
		$this->set('defaultSectorServiceId',$projectFromUser['UserProject']['sector_service_id']);
		$this->set('defaultVolume',$projectFromUser['UserProject']['volumes']);
		
		$this->set('breadcrumb', array(
            __('Dar a conocer mi perfil') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => true, 'url' => '/Users/ver_cv'),
            __('Editar proyecto') => array('active' => false, 'url' => '#'),
        ));
		if ($this->request->is('post')) {

			//budget correcto
			$this->request->data['UserProject']['budget'] = $this->request->data['UserProject']['budge'];
			unset($this->request->data['UserProject']['budge']);

			//tratamiento de fechas
			$dateInit = explode('/',$this->request->data['UserProject']['init_date']);
			//if !empty()
			if (empty($this->request->data['UserProject']['on_going'])) {
				$dateEnd = explode('/',$this->request->data['UserProject']['end_date']);
				$dateEnd  = new DateTime($dateEnd[1].'-'.$dateEnd[0].'-01');
				$dateEnd = $dateEnd->format('Y-m-d');
			} else {
				$dateEnd = null;
			}
			
			$dateInit = new DateTime($dateInit[1].'-'.$dateInit[0].'-01');
			$dateInit = $dateInit->format('Y-m-d');
			
			$this->request->data['UserProject']['start_date'] = $dateInit;
			$this->request->data['UserProject']['end_date']   = $dateEnd;
			unset($this->request->data['UserProject']['init_date']);

			//ya tenemos el country_id gracias a google
			unset($this->request->data['UserProject']['country_name']);

			//miramos si la empresa existe y si no la creamos			
			$companyName = str_replace('\'','`',$this->request->data['UserProject']['CompanyName']);
			$this->Company->recursive = -1;
			$companyId = $this->Company->findByName($companyName);

			if (!empty($companyId)) {
				$companyId = $companyId['Company']['id'];
			} else {
				$this->Company->create();
				//en este caso hacemos el responsable el usuario si no existe
				$companySave['Company']['last_reviewer_id'] 	  = $user['User']['id'];
				$companySave['Company']['contact_responsable_id'] = $user['User']['id'];
				$companySave['Company']['name']                   = $companyName;
				if ($this->Company->save($companySave)) {
					$companyId = $this->Company->getLastInsertID();
				}
			}
			$this->request->data['UserProject']['company_id'] = $companyId;
			unset($this->request->data['UserProject']['CompanyName']);
			//miramos si el customer existe y si no la creamos como empresa
			$customerName = str_replace('\'','`',$this->request->data['UserProject']['custome']);
			$this->Company->recursive = -1;
			$customerId = $this->Company->findByName($customerName);

			if (!empty($customerId)) {
				$customerId = $customerId['Company']['id'];
			} else {
				$this->Company->create();
				$companySave = array();
				$companySave['Company']['name'] = $customerName;
				if ($this->Company->save($companySave)) {
					$customerId = $this->Company->getLastInsertID();
				}
			}

			$this->request->data['UserProject']['customer_id'] = $customerId;
			unset($this->request->data['UserProject']['custome']);
			//miramos si tenemos la ciudad sino la guardamos
			if (!empty($this->request->data['UserProject']['cit_id'])) {
				$cityId = $this->request->data['UserProject']['cit_id'];
			} else {
				$this->City->create();
				$saveCity['City']['name'] = str_replace('\'','`',$this->request->data['UserProject']['cit']);
				$saveCity['City']['country_id'] = (int) $this->request->data['UserProject']['country_id'];
				if ($this->City->save($saveCity)) {
					$cityId = $this->City->getLastInsertID();
				}
			}
			$this->request->data['UserProject']['city_id'] = $cityId;
			unset($this->request->data['UserProject']['cit_id']);
			unset($this->request->data['UserProject']['cit']);

			//encodeamos tagss
			if (!empty($this->request->data['UserProject']['tags'])) {
				$this->request->data['UserProject']['tags'] = json_encode($this->request->data['UserProject']['tags']);	
			}
			//guardamos geolocalizacion
			$saveGeoLocalitzacion['longitude'] = $this->request->data['UserProject']['longitude'];
			$saveGeoLocalitzacion['latitude'] = $this->request->data['UserProject']['latitude'];
			$this->Geolocalitzation->recursive = -1;
			$geolocalitzationId= $this->Geolocalitzation->find('first',array(
				'conditions' => array(
					'longitude' => $saveGeoLocalitzacion['longitude'],
					'latitude' => $saveGeoLocalitzacion['latitude'],
					)
				)
			);
			if (!empty($geolocalitzationId)) {
				$geolocalitzationId = $geolocalitzationId['Geolocalitzation']['id'];
			} else {
				if ($this->Geolocalitzation->save($saveGeoLocalitzacion)) {
					 $geolocalitzationId = $this->Geolocalitzation->getLastInsertID();
				}	
			}
			$this->request->data['UserProject']['geolocalitzation_id'] = $geolocalitzationId;

			unset($this->request->data['UserProject']['longitude']);
			unset($this->request->data['UserProject']['latitude']);
			
			//miramos si tenemos el sector del proyecto sino la guardamos
			$sectorProjectName = str_replace('\'','`',$this->request->data['UserProject']['sector_proj']);
			$this->Sector->recursive = -1;
			$sectorProjectId = $this->Sector->findByName($sectorProjectName);
			if (!empty($sectorProjectId)) {
				$sectorProjectId = $sectorProjectId['Sector']['id'];
			} else {
				$this->Sector->create();
				$sectorSave['Sector']['name'] = $sectorProjectName;
				if ($this->Sector->save($sectorSave)) {
					$sectorProjectId = $this->Sector->getLastInsertID();
				}
			}
			$this->request->data['UserProject']['sector_project_id'] = $sectorProjectId;
			unset($this->request->data['UserProject']['sector_proj']);

			//miramos si tenemos el sector del servicio sino la guardamos
			$sectorServiceName = str_replace('\'','`',$this->request->data['UserProject']['sector_servic']);
			$this->Sector->recursive = -1;
			$sectorServiceId = $this->Sector->findByName($sectorServiceName);
			if (!empty($sectorServiceId)) {
				$sectorServiceId = $sectorServiceId['Sector']['id'];
			} else {
				$this->Sector->create();
				$sectorSave['Sector']['name'] = $sectorServiceName;
				if ($this->Sector->save($sectorSave)) {
					$sectorServiceId = $this->Sector->getLastInsertID();
				}
			}
			$this->request->data['UserProject']['sector_service_id'] = $sectorServiceId;
			unset($this->request->data['UserProject']['sector_servic']);
			
			//miramos si tenemos la funcionalidad del proyecto sino la guardamos
			$functionalityName = str_replace('\'','`',$this->request->data['UserProject']['functionalit']);
			$this->Functionality->recursive = -1;
			$functionalityId = $this->Functionality->findByName($functionalityName);
			if (!empty($functionalityId)) {
				$functionalityId = $functionalityId['Functionality']['id'];
			} else {
				$this->Functionality->create();
				$functionSave['Functionality']['name'] = $functionalityName;
				if ($this->Functionality->save($functionSave)) {
					$functionalityId = $this->Functionality->getLastInsertID();
				}
			}
			$this->request->data['UserProject']['functionality_id'] = $functionalityId;
			unset($this->request->data['UserProject']['functionalit']);

			//miramos si tenemos la departamento (subfuncionalidad) del proyecto sino la guardamos
			$departmentName = str_replace('\'','`',$this->request->data['UserProject']['departmen']);
			$this->Department->recursive = -1;
			$departmentId = $this->Department->findByName($departmentName);
			if (!empty($departmentId)) {
				$departmentId = $departmentId['Department']['id'];
			} else {
				$this->Department->create();
				$departmentSave['Department']['name'] = $departmentName;
				if ($this->Department->save($departmentSave)) {
					$departmentId = $this->Department->getLastInsertID();
				}
			}
			$this->request->data['UserProject']['department_id'] = $departmentId;
			unset($this->request->data['UserProject']['departmen']);
			if ($this->UserProject->save($this->request->data)) {
				//$this->UsersUserProject->create();
				//$saveUsersUserProject['UsersUserProject']['user_project_id'] = $this->request->data['UserProject']['functionality_id'];
				//$saveUsersUserProject['UsersUserProject']['user_id']         = $user['User']['id'];
				//$this->UsersUserProject->save($saveUsersUserProject);
				//$this->User->recursive = -1;
        		$this->Session->write('UserRegistered',$this->User->findByid($user['User']['id']));
				$this->Session->setFlash(__('Proyecto guardado correctamente'), 'flash_message', array('type' => 'success'));
				return $this->redirect(array('controller' => 'Users','action' => 'ver_cv#pr_'.$this->request->data['UserProject']['functionality_id']));
			} else {
				$this->Session->setFlash(__('Hubo problemas al intentar guardar el proyecto, inténtelo de nuevo, gracias'), 'flash_message', array('type' => 'success'));
				return $this->redirect(array('controller' => 'UserProjects','action' => 'add'));
			}
		}
		$budgetCurrencies = $this->UserProject->BudgetCurrency->find('list');
		$countries = $this->UserProject->Country->find('list');
		$sectorProjects = $this->UserProject->SectorProject->find('list');
		$this->City->recursive = 1;
		$cities = $this->City->find('all',array('fields' => array('City.name','City.country_id')));
		$cities = Hash::extract($cities,'{n}.City');
		$functionalities = $this->UserProject->Functionality->find('list');
		$sectorServices = $this->UserProject->SectorService->find('list');
		$departments = $this->UserProject->Department->find('list');
		$companies = $this->UserProject->Company->find('list');
		$customers = $this->UserProject->Customer->find('list');
		$companiesText = '';
		foreach($companies as $company) {
			$companiesText .= '"'.$company.'",';
		}
		$companiesText = substr($companiesText,0,strlen($companiesText)-1);
		$this->set('companiesText',$companiesText);
		$departmentsText = '';
		foreach($departments as $department) {
			$departmentsText .= '"'.$department.'",';
		}
		$departmentsText = substr($departmentsText,0,strlen($departmentsText)-1);
		$this->set('departmentsText',$departmentsText);
		$citiesText = '';
		foreach($cities as $city) {
			$citiesText .= '"'.$city['name'].' ('.$countries[$city['country_id']].')",';
		}
		$citiesText = substr($citiesText,0,strlen($citiesText)-1);
		$this->set('citiesText',$citiesText);
		$sectorProjectText = '';
		foreach($sectorProjects as $sectorProject) {
			$sectorProjectText .= '"'.$sectorProject.'",';
		}
		$sectorProjectText = substr($sectorProjectText,0,strlen($sectorProjectText)-1);
		$this->set('sectorProjectText',$sectorProjectText);

		$functionalitiesText = '';
		foreach($functionalities as $functionality) {
			$functionalitiesText .= '"'.$functionality.'",';
		}
		$functionalitiesText = substr($functionalitiesText,0,strlen($functionalitiesText)-1);
		$this->set('functionalitiesText',$functionalitiesText);

		$this->set(compact('budgetCurrencies', 'billingCurrencies', 'countries', 'sectorProjects', 'cities', 'functionalities', 'sectorServices', 'departments', 'companies', 'customers'));
	}
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserProject->id = $id;
		if (!$this->UserProject->exists()) {
			throw new NotFoundException(__('Invalid user project'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserProject->delete()) {
			$this->Session->setFlash(__('The user project has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user project could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
