<?php
/**
 * Created by PhpStorm.
 * User: Eloi Galles
 * Date: 5/6/15
 * Time: 10:40
 */
App::uses('Component', 'Controller');
/**
 * Class SocialComponent
 */
class EmailTextComponent extends Component {

    public function registration ($user = null) {
        $url = 'http://'.$_SERVER['HTTP_HOST'];
        $link = $url.'/Users/activate/'.$user['User']['id'].'/'.$user['User']['active_key'];
        $htmlToReturn ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
              <title>GlobeShake</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <style type="text/css">td img {display: block;}</style>
            </head>
            <body>

              <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="font-family:Arial, Helvetica, sans-serif">

            <tr>
               <td>
               <img style="margin-bottom:12px" name="'.__('GlobeShake').'"  src="'.$url.'/img/logo_Globeshake.png" border="0" id="'.__('GlobeShake').'" alt="'.__('GlobeShake').'"  />
               
               </td></tr>
               </table>
                
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"style="font-family:Arial, Helvetica, sans-serif; border: #000 1px solid;">


              <tr>
               <td style="font-size: 20px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; background:#47789F; height:30px; color:#FFF; padding:12px; width:100%; ">'.__('Verifica tu dirección de correo electrónico').'</td></tr>
               <tr>
             <td style="font-size: 16px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; height:30px; color:#7A7A7A; padding-left:12px; padding-top:12px; padding-bottom:12px; padding-right:12px;">'.
              __('Hola %s, al verificar tu dirección de correo electrónico obtendrás acceso completo a GlobeShake',$user['User']['firstname'].' '.$user['User']['lastname']).'.</td>
               
              </tr>
              <tr>
             <td style="font-size: 16px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; height:30px; color:#666666; padding:12px; width:100% ">
              <a style="font-size: 14px; background:#6FA4CE; color:#FFF; text-decoration:none; padding:5px; " href="'.$link.'">'.__('Verifica tu dirección de correo electrónico').'</a>
             </td>
                </tr>
               <tr>
             <td style="font-size: 12px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; height:30px; color:#7A7A7A; padding-left:12px; padding-bottom:12px; padding-top:12px;">'.__('Si el botón no funciona, copia y pega este enlace en tu navegador %s',$link).'</td>
               </tr>
              <tr>
               <td style=" margin-bottom: 16px; margin-left: 20px; margin-top: 14px; background:#47789F; height:15px; padding:12px; width:100%;"></td></tr>
               <tr>
            </table>
            </body>
            </html>';
    return $htmlToReturn;
    }

    public function registration_fail ($user = null) {
        $url = 'http://'.$_SERVER['HTTP_HOST'];
        $link = $url.'/Users/activate/'.$user['User']['id'].'/'.$user['User']['active_key'];
        $htmlToReturn ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
              <title>GlobeShake</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <style type="text/css">td img {display: block;}</style>
            </head>
            <body>
              <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="font-family:Arial, Helvetica, sans-serif">
            <tr>
               <td>
               <img style="margin-bottom:12px" name="'.__('GlobeShake').'"  src="'.$url.'/img/logo_Globeshake.png" border="0" id="'.__('GlobeShake').'" alt="'.__('GlobeShake').'"  />
               </td></tr>
               </table>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"style="font-family:Arial, Helvetica, sans-serif; border: #000 1px solid;">
              <tr>
               <td style="font-size: 20px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; background:#47789F; height:30px; color:#FFF; padding:12px; width:100%; ">'.__('Reactivación de cuenta de usuario').'</td></tr>
               <tr>
             <td style="font-size: 16px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; height:30px; color:#7A7A7A; padding-left:12px; padding-top:12px; padding-bottom:12px; padding-right:12px;">'.
              __('Hola %s, ésta correo electrónico ya dispone de un usuario activo y validado su correo electrónico. Acceda a Globeshake des del panel superior izquierdo o bien pulse el enlace "He olvidado mi contraseña" si no recuerda su password ',$user['User']['firstname'].' '.$user['User']['lastname']).'.</td>
              </tr>
              <tr>
               <td style=" margin-bottom: 16px; margin-left: 20px; margin-top: 14px; background:#47789F; height:15px; padding:12px; width:100%;"></td></tr>
               <tr>
            </table>
            </body>
            </html>';
    return $htmlToReturn;
    }
    
    public function forgotten_password ($user = null) {
        $url = 'http://'.$_SERVER['HTTP_HOST'];
        $link = $url.'/Users/forgotten_password/'.$user['User']['id'].'/'.$user['User']['active_key'];
        $htmlToReturn ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
              <title>GlobeShake</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <style type="text/css">td img {display: block;}</style>
            </head>
            <body>
              <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="font-family:Arial, Helvetica, sans-serif">
            <tr>
               <td>
               <img style="margin-bottom:12px" name="'.__('GlobeShake').'"  src="'.$url.'/img/logo_Globeshake.png" border="0" id="'.__('GlobeShake').'" alt="'.__('GlobeShake').'"  />
               
               </td></tr>
               </table>
                
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="600"style="font-family:Arial, Helvetica, sans-serif; border: #000 1px solid;">


              <tr>
               <td style="font-size: 20px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; background:#47789F; height:30px; color:#FFF; padding:12px; width:100%; ">'.__('Verifica tu dirección de correo electrónico').'</td></tr>
               <tr>
             <td style="font-size: 16px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; height:30px; color:#7A7A7A; padding-left:12px; padding-top:12px; padding-bottom:12px; padding-right:12px;">'.
              __('Hola %s, has solicitado reestablecer tu contraseña del portal Globeshake. Por favor pulsa el siguiente enlace para poder reestablecer la conraseña',$user['User']['firstname'].' '.$user['User']['lastname']).'.</td>
               
              </tr>
              <tr>
             <td style="font-size: 16px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; height:30px; color:#666666; padding:12px; width:100% ">
              <a style="font-size: 14px; background:#6FA4CE; color:#FFF; text-decoration:none; padding:5px; " href="'.$link.'">'.__('Reestablecer mi contraseña').'</a>
             </td>
                </tr>
               <tr>
             <td style="font-size: 12px; margin-bottom: 16px; margin-left: 20px; margin-top: 14px; height:30px; color:#7A7A7A; padding-left:12px; padding-bottom:12px; padding-top:12px;">'.__('Si el botón no funciona, copia y pega este enlace en tu navegador %s',$link).'</td>
               </tr>
              <tr>
               <td style=" margin-bottom: 16px; margin-left: 20px; margin-top: 14px; background:#47789F; height:15px; padding:12px; width:100%;"></td></tr>
               <tr>
            </table>
            </body>
            </html>';
    return $htmlToReturn;
    }
}