<?php
App::uses('AppController', 'Controller');
/**
 * Currencies Controller
 *
 * @property Currency $Currency
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CurrenciesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

	public function saveActualCurrencies() {
		//$this->loadModel('Currency');
		$currencies = $this->Project->Currency->find('all');
		$currencies = Hash::extract($currencies,'{n}.Currency.ISO');
		$currenciesString = array();
		foreach ($currencies as $k => $currentCurrency) {
			if ($k != 0) { //excepto el euro
				$url = 'http://download.finance.yahoo.com/d/quotes.csv?s='.$currentCurrency.$currencies[0].'=X&f=nl1d1t1';
				if (($handle = fopen($url, "r")) !== FALSE) {
				  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				    $nameField = $valueField = '';
				    $currenciesString[str_replace('/EUR','',$data[0])] = $data[1];
				  }
				  fclose($handle);
				}
			}
		}
		$currenciesString2 = array();
		foreach ($currenciesString as $k =>$value) {
			$saveValues = $this->Project->Currency->find ('first',array('conditions' => array('ISO' => $k),'fields' => array('id')));
			if (!empty($saveValues)) {
				$currenciesString2[$saveValues['Currency']['id']] = array('id' => $saveValues['Currency']['id'],'conversion_to_euro'=>$value);	
			} else {
				debug($k);
			}
			
		}
		$this->Project->Currency->saveMany($currenciesString2);
	}

}
