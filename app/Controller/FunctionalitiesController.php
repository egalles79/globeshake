<?php
App::uses('AppController', 'Controller');
/**
 * Functionalities Controller
 *
 * @property Functionality $Functionality
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class FunctionalitiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Functionality->recursive = 0;
		$this->set('functionalities', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Functionality->exists($id)) {
			throw new NotFoundException(__('Invalid functionality'));
		}
		$options = array('conditions' => array('Functionality.' . $this->Functionality->primaryKey => $id));
		$this->set('functionality', $this->Functionality->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Functionality->create();
			if ($this->Functionality->save($this->request->data)) {
				$this->Session->setFlash(__('The functionality has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The functionality could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Functionality->exists($id)) {
			throw new NotFoundException(__('Invalid functionality'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Functionality->save($this->request->data)) {
				$this->Session->setFlash(__('The functionality has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The functionality could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Functionality.' . $this->Functionality->primaryKey => $id));
			$this->request->data = $this->Functionality->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Functionality->id = $id;
		if (!$this->Functionality->exists()) {
			throw new NotFoundException(__('Invalid functionality'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Functionality->delete()) {
			$this->Session->setFlash(__('The functionality has been deleted.'));
		} else {
			$this->Session->setFlash(__('The functionality could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Functionality->recursive = 0;
		$this->set('functionalities', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Functionality->exists($id)) {
			throw new NotFoundException(__('Invalid functionality'));
		}
		$options = array('conditions' => array('Functionality.' . $this->Functionality->primaryKey => $id));
		$this->set('functionality', $this->Functionality->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Functionality->create();
			if ($this->Functionality->save($this->request->data)) {
				$this->Session->setFlash(__('The functionality has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The functionality could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Functionality->exists($id)) {
			throw new NotFoundException(__('Invalid functionality'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Functionality->save($this->request->data)) {
				$this->Session->setFlash(__('The functionality has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The functionality could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Functionality.' . $this->Functionality->primaryKey => $id));
			$this->request->data = $this->Functionality->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Functionality->id = $id;
		if (!$this->Functionality->exists()) {
			throw new NotFoundException(__('Invalid functionality'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Functionality->delete()) {
			$this->Session->setFlash(__('The functionality has been deleted.'));
		} else {
			$this->Session->setFlash(__('The functionality could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
