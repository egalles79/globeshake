<?php
App::uses('AppController', 'Controller');
/**
 * Specialities Controller
 *
 * @property Speciality $Speciality
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SpecialitiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

  /**
 * index method
 *  add n specialist to user
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
  public function index() {
    $this->set('breadcrumb', array(
            __('Dar a conocer mi perfil') => array('active' => false, 'url' => '#'),
            __('Soy especialista') => array('active' => false, 'url' => '#'),
        ));
    $this->loadModel('User');
    $this->User->recursive = 0;
    $userRegistered = $this->Session->read('UserRegistered');
    $this->loadModel('SpecialitiesUser');
    $specialitiesUser = $this->SpecialitiesUser->find('all',array('conditions' => array('user_id' => $userRegistered['User']['id'])));
    $this->set('specialitiesUser', $specialitiesUser);
    $this->set('user', $userRegistered);
    $this->set('users', $this->Paginator->paginate('User'));
  }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Speciality->exists($id)) {
			throw new NotFoundException(__('Invalid speciality'));
		}
		$options = array('conditions' => array('Speciality.' . $this->Speciality->primaryKey => $id));
		$this->set('speciality', $this->Speciality->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->layout = 'ajax';
		$this->autoRender = false;
		if ($this->request->is('post')) {
			if (!empty($this->request->data['id'])) {
				$this->request->data['Speciality']['id'] = $this->request->data['id'];
				if ($this->Speciality->save($this->request->data)) {
					$this->Session->setFlash(__('Especialidad guardada correctamente'), 'flash_message', array('type' => 'success'));
					return true;
				} else {
					$this->Session->setFlash(__('No se pudo editar la especialidad, por favor contacte con el administrador'), 'flash_message', array('type' => 'danger'));
					return false;
				}
			} else {
				$this->Speciality->create();
				if ($this->Speciality->save($this->request->data)) {
					$user = $this->Session->read('UserRegistered');
					$this->loadModel('SpecialitiesUser');
					$this->SpecialitiesUser->create();
					$saveSpecialitiesUserArray['user_id'] = $user['User']['id'];
					$saveSpecialitiesUserArray['speciality_id'] = $this->Speciality->getLastInsertID();
					$this->SpecialitiesUser->save($saveSpecialitiesUserArray);
					$this->Session->setFlash(__('Especialidad guardada correctamente'), 'flash_message', array('type' => 'success'));
					return true;
				} else {
					$this->Session->setFlash(__('No se pudo guardar la especialidad, por favor contacte con el administrador'), 'flash_message', array('type' => 'danger'));
					return false;
				}
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Speciality->exists($id)) {
			throw new NotFoundException(__('Invalid speciality'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Speciality->save($this->request->data)) {
				$this->Session->setFlash(__('The speciality has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The speciality could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Speciality.' . $this->Speciality->primaryKey => $id));
			$this->request->data = $this->Speciality->find('first', $options);
		}
		$users = $this->Speciality->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->autoRender = false;
		$this->Speciality->id = $id;
		if (!$this->Speciality->exists()) {
			throw new NotFoundException(__('Invalid speciality'));
		}
		if ($this->Speciality->delete()) {
			$this->Session->setFlash(__('Especialidad eliminada correctamente'), 'flash_message', array('type' => 'success'));
		} else {
			$this->Session->setFlash(__('No se pudo eliminar la especialidad, por favor contacte con el administrador'), 'flash_message', array('type' => 'danger'));
		}
		$this->redirect(array('controller' => 'Specialities','action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Speciality->recursive = 0;
		$this->set('specialities', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Speciality->exists($id)) {
			throw new NotFoundException(__('Invalid speciality'));
		}
		$options = array('conditions' => array('Speciality.' . $this->Speciality->primaryKey => $id));
		$this->set('speciality', $this->Speciality->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Speciality->create();
			if ($this->Speciality->save($this->request->data)) {
				$this->Session->setFlash(__('The speciality has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The speciality could not be saved. Please, try again.'));
			}
		}
		$users = $this->Speciality->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Speciality->exists($id)) {
			throw new NotFoundException(__('Invalid speciality'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Speciality->save($this->request->data)) {
				$this->Session->setFlash(__('The speciality has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The speciality could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Speciality.' . $this->Speciality->primaryKey => $id));
			$this->request->data = $this->Speciality->find('first', $options);
		}
		$users = $this->Speciality->User->find('list');
		$this->set(compact('users'));
	}
	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->Session->check('UserRegistered')) {
			$user = $this->Session->read('UserRegistered');
			$this->_setUrlDependence($user);
		}
	}
	private function _setUrlDependence($user) {
	  if ($user['User']['card_completed'] == 0) {
        $this->Session->setFlash(__('Para poder rellenar proyectos debe rellenar su tarjeta de visita'), 'flash_message', array('type' => 'info')); 
        $this->redirect($this->Auth->redirect());
      } else if ($user['User']['formation_completed'] == 0) {
        $this->Session->setFlash(__('Para poder rellenar proyectos debe rellenar su formación'), 'flash_message', array('type' => 'info')); 
        $this->redirect($this->Auth->redirect());
      }
      
	}
/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Speciality->id = $id;
		if (!$this->Speciality->exists()) {
			throw new NotFoundException(__('Invalid speciality'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Speciality->delete()) {
			$this->Session->setFlash(__('The speciality has been deleted.'));
		} else {
			$this->Session->setFlash(__('The speciality could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
