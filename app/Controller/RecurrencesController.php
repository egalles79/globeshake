<?php
App::uses('AppController', 'Controller');
/**
 * Recurrences Controller
 *
 * @property Recurrence $Recurrence
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class RecurrencesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

}
