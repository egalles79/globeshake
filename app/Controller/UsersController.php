<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array('Country','User','Functionality','Company','Sector','City','Department','Language','Currency','UsersUserProject');
	public $components = array('Paginator', 'Session','EmailText','Image');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = -1;
		$this->set('users', $this->Paginator->paginate());
	}
  private function _checkTantPerCentCardComplete($user) {
    $tantPerCent  = 0;
    if ($user['User']['card_completed']) {
      $tantPerCent = 100;
    } else {
      if ($user['User']['firstname']) {
        $tantPerCent += 15;
      }
      if ($user['User']['lastname']) {
        $tantPerCent += 15;
      }
      if ($user['User']['department_id']) {
        $tantPerCent += 15;
      }
      if ($user['User']['functionality_id']) {
        $tantPerCent += 20;
      }
      if ($user['User']['sector_id']) {
        $tantPerCent += 20;
      }
      if ($user['User']['sector_id']) {
        $tantPerCent += 15;
      }
    }
    return $tantPerCent;
  }

  public function update_visit_card() {
    $user    = $this->Session->read('UserRegistered');
    $percent = $this->_checkTantPerCentCardComplete($user);
    $this->set('user',$user);
    $this->set('percent',$percent);
    $this->layout = 'none';
    $this->User->recursive = -1;
    $this->set('users', $this->Paginator->paginate());
  }
  /*linkefin connect */
  // This route will redirect to LinkedIn's login page, collect a request token and
    // then redirect back to the route provided
    public function connect() {
        $this->LinkedIn->connect(['action' => 'authorize']);
    }

    // Here we convert the request token into a usable access token and redirect
    public function authorize() {
        $this->LinkedIn->authorize(['action' => 'index']);
    }
    /*end linkedin connect*/
	public function cvmap() {
		// if (100 no esta completat) {
		// 	$this->Session->setFlash(__('El informe se ha eliminado correctamente'), 'flash_message', array('type' => 'info'));	
		// 	$this->element('tarjeta_bc')
		// }
    $user = $this->Session->read('UserRegistered');
    $this->set('companyId',$user['User']['company_id']);
		$this->set('breadcrumb', array(
        ));
		//$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}
	

	public function target_add() {
		if ($this->request->data) {
			return true;
		}
	}
/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->data) {
			
			$dataRequested = $this->request->data;
			$this->Country->locale = $this->Session->read('Config.language');
			$country_id = $this->Country->findByCode_2($dataRequested['User']['country_id']);
			$dataRequested['User']['country_id'] = (int) $country_id['Country']['id'];
			$dataRequested['User']['username'] = $dataRequested['User']['email'];
			$dataRequested['User']['group_id'] = '4';
			$dataRequested['User']['active_key'] = md5($dataRequested['User']['username'].date('Y-m-d H:i:s'));
			if ($dataRequested['User']['repeat_password'] != $dataRequested['User']['password']) {
				$this->Session->setFlash(__('Las contraseñas que ha rellenado son diferentes. Por favor repita la contraseña correctamente'));
				return $this->redirect($this->Auth->logoutRedirect);
			}
			$this->User->recursive = -1;
			$existsUser = $this->User->findByUsername($dataRequested['User']['username']);
			if (!empty($existsUser)) {
				if ($existsUser['User']['active']) {
					$this->Session->setFlash(__('Éste usuario ya existe y tiene cuenta activa. Si quiere recuperar la contraseña pulse en "Entrar" y "Recuperar contraseña"'));
					return $this->redirect($this->Auth->logoutRedirect);
				} else {
					$this->Session->setFlash(__('Éste usuario ya existe pero no tiene cuenta activa. Si quiere que le reenviemos los datos de acceso por favor pulse en "Entrar" y luego "Reenviar email para activar la cuenta"'));
					return $this->redirect($this->Auth->logoutRedirect);
				}
			}
			

			$this->User->create();
			if ($this->User->save($dataRequested)) {
				$dataRequested['User']['id'] = $this->User->id;
				$Email = new CakeEmail('gmail');
		        $Email->emailFormat('html');
		        $Email->from(array('noreply.globeshake@gmail.com' => 'noreply.globeshake@gmail.com'));
		        $Email->subject(__('GlobeShake  - Verificación de la cuenta'));
		        $Email->to($dataRequested['User']['email']);
		        $Email->send($this->EmailText->registration($dataRequested));
				$this->Session->setFlash(__('Usuario guardado correctamente.Revise su correo para poder activar la cuenta'));
			} else {
				$this->Session->setFlash(__('Usuario no guardado, error en la entrada de datos, usuario existente'));
			}
			return $this->redirect($this->Auth->logoutRedirect);
		}
		$countries = $this->User->Country->find('list');
		$cities = $this->User->City->find('list');
		$functionalities = $this->User->Functionality->find('list');
		$sectors = $this->User->Sector->find('list');
		$groups = $this->User->Group->find('list');
		$this->set(compact('countries', 'cities', 'functionalities', 'sectors', 'groups'));
	}

  public function get_data_card($id) {
    $this->layout = 'ajax';
    $this->autoRender = false;
    $this->loadModel('User');
    $user = $this->User->findById($id);
    $specialitiesFromUser = count($this->_hasSpecialities($user));
    $user['User']['has_speciality'] = ($specialitiesFromUser > 0) ? true : false;
    return json_encode($user);
  }
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$countries = $this->User->Country->find('list');
		$cities = $this->User->City->find('list');
		$functions = $this->User->Functionality->find('list');
		$sectors = $this->User->Sector->find('list');
		$groups = $this->User->Group->find('list');
		$this->set(compact('countries', 'cities', 'functions', 'sectors', 'groups'));
	}
/**
 * edit_formation method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
  public function edit_formation() {
    $this->set('breadcrumb', array(
            __('Dar a conocer mi perfil') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => true, 'url' => '/Users/ver_cv'),
            __('Editar formación') => array('active' => false, 'url' => '#'),
        ));
    if ($this->request->is(array('post', 'put'))) {
      $sBirthdate = explode('/',$this->request->data['User']['birthdate']);
      $user = $this->Session->read('UserRegistered');
      $this->request->data['User']['id']        = $user['User']['id'];
      $this->request->data['User']['birthdate'] = $sBirthdate[2].'-'.$sBirthdate[1].'-'.$sBirthdate[0];
      $this->request->data['User']['idioms']    = json_encode($this->request->data['User']['idioms']);
      $this->request->data['User']['formation_completed'] = true;
      if ($this->User->save($this->request->data,false)) {
        $this->Session->setFlash(__('Formación actualizada correctamente'), 'flash_message', array('type' => 'success'));
        $this->User->recursive = -1;
        $this->Session->write('UserRegistered',$this->User->findByid($user['User']['id']));
        $this->Session->setFlash(__('Tarjeta de usuario editada correctamente'), 'flash_message', array('type' => 'success'));  
        $this->redirect('/Users/ver_cv#formation');
      } else {
        $this->Session->setFlash(__('Error al intentar guardar la información de formación. Por favor inténtelo de nuevo'), 'flash_message', array('type' => 'success'));
      }      
    }

    $locale = $this->Session->read('Config.language');
    $this->set('project_from_url', null);

    $user = $this->Session->read('UserRegistered');
    
    $this->Functionality->recursive = -1;
    $functionalities = $this->Functionality->find('list');
    
    $this->City->recursive = -1;
    $cities = $this->City->find('list');
    
    $departments = $this->User->Department->find('list');
    
    $this->Company->recursive = -1;
    $companies = $this->Company->find('list');
    
    $this->Sector->recursive = -1;
    $sectors = $this->Sector->find('list');
    
    $this->set('functionalities',$functionalities);
    $this->set('cities',$cities);
    $this->set('departments',$departments);
    $this->set('sectors',$sectors);
    $this->set('companies',$companies);
    $id = $user['User']['id'];
    if (!$this->User->exists($id)) {
      throw new NotFoundException(__('Usuario no válido'));
    }
    $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
    $this->request->data = $this->User->find('first', $options);

    $countries = $this->User->Country->find('list');
    $cities = $this->User->City->find('list');
    $sectors = $this->User->Sector->find('list');
    $groups = $this->User->Group->find('list');
    $this->set('user',$user);
    $this->set(compact('countries', 'cities', 'functionalities', 'sectors', 'groups'));
  }

  /**
 * resumen_cv method
 *
 * @throws NotFoundException
 * @return void
 */
  public function resumen_cv()
  {
      $user = $this->Session->read('UserRegistered');
      $this->_setUrlDependence($user);
      $this->set('breadcrumb', array(
            __('Dar a conocer mi perfil') => array('active' => false, 'url' => '#'),
            __('Resumen CV') => array('active' => false, 'url' => '#'),
        ));
      $this->set('user',$user);

      $this->Sector->recursive = -1;
      $sectorNames = $this->Sector->find('list');
      $this->Currency->recursive = -1;
      $currentCurrency = $this->Currency->find('all',array('fields' => array('id','conversion_to_euro')));
      $currentCurrency = Hash::extract($currentCurrency,'{n}.Currency');
      $currenciesText = array();
      foreach ($currentCurrency as $current) {
        $currenciesText[$current['id']] = $current['conversion_to_euro'];
      }
      $this->Company->recursive = -1;
      $companyNames = $this->Company->find('all',array('fields' => array('id','name')));
      $companyNames = Hash::extract($companyNames,'{n}.Company');
      $companyString = array();
      foreach($companyNames as $companyName) {
        $companyString[$companyName['id']] = $companyName['name'];
      }
      $this->UsersUserProject->recursive = 2;
      $projects = $this->UsersUserProject->find('all',array('conditions' => array('UsersUserProject.user_id' => $user['User']['id'])));
        
        $customerIds =$companyNames = $projectsNames = $countryProjectIds = $customerNames = $sectorsProject = $sectorsProjectNames = array();
        $sectorServiceId = $functionalitiesNames = $departmentNames = array();
        $projectsSumBudget = 0;
        $counter = $counter2 = $durationYears = $durationMonths = $durationDays = $durationHours = 0;
        foreach ($projects as $project) {
          if (!in_array($project['UserProject']['Company']['name'],$companyNames)) {
            $companyNames[] = $project['UserProject']['Company']['name'];
          }
          if (!in_array($project['UserProject']['id'],$projectsNames)) {
            $projectsNames[] = $project['UserProject']['name'];
          }
          if (!in_array($project['UserProject']['country_id'],$countryProjectIds)) {
            $countryProjectIds[] = $project['UserProject']['country_id'];
          }

          if (!in_array($project['UserProject']['customer_id'],$customerIds)) {
            $customerIds[]   = $project['UserProject']['customer_id'];
            $customerNames[] = $companyString[$project['UserProject']['customer_id']];
          }
          if (!in_array($project['UserProject']['sector_project_id'],$sectorsProject)) {
            $sectorsProject[] = $project['UserProject']['sector_project_id'];
            $sectorsProjectNames[] = $sectorNames[$project['UserProject']['sector_project_id']];
          }
          if (!in_array($project['UserProject']['sector_service_id'],$sectorServiceId)) {
            if (!empty($project['UserProject']['sector_service_id'])) {
              $sectorServiceId[] = $project['UserProject']['sector_service_id'];  
            }
            
          }
          if (!empty($project['UserProject']['Functionality'])) {
            if (!in_array($project['UserProject']['Functionality']['name'],$functionalitiesNames)) {
              $functionalitiesNames[] = $project['UserProject']['Functionality']['name'];
            }
          }
          if (!empty($project['UserProject']['Department'])) {
            if (!in_array($project['UserProject']['Department']['name'],$departmentNames)) {
              $departmentNames[] = $project['UserProject']['Department']['name'];
            }
          }
          $durationYears += $project['UserProject']['duration_year'];
          $durationMonths += $project['UserProject']['duration_month'];
          $durationDays += $project['UserProject']['duration_day'];
          $durationHours += $project['UserProject']['duration_hours'];
          $budget = (float) str_replace(array('.',','),array('','.'),$project['UserProject']['budget']);
          $realCurrency = (float) $currenciesText[$project['UserProject']['budget_currency_id']]; 
          $projectsSumBudget += $budget*$realCurrency;
        }
        $duration = '';
        $duration .= ($durationYears > 0) ? $durationYears.' '.__('años') : '';
        $duration .= ($durationMonths > 0) ? ' '.$durationMonths.' '.__('meses') : '';
        $duration .= ($durationDays > 0) ? ' '.$durationDays.' '.__('días') : '';
        $duration .= ($durationHours > 0) ? ' '.$durationHours.' '.__('horas') : '';

        $specialitiesFromUser = $this->_hasSpecialities($user);
        $this->set('specialitiesFromUser',$specialitiesFromUser);
        $this->set('duration',$duration);
        $this->set('sectorServices',$sectorServiceId);
        $this->set('departmentNames',$departmentNames);
        $this->set('functionalitiesNames',$functionalitiesNames);
        $this->set('companyNames',$companyNames);
        $this->set('projectsNames',$projectsNames);
        $this->set('countryProjectIds',$countryProjectIds);
        $this->set('customerNames',$customerNames);
        $this->set('sectorsProjectNames',$sectorsProjectNames);
        $this->set('projectsSumBudget',$projectsSumBudget);

  }
  private function _hasSpecialities($user) {
    $this->loadModel('SpecialitiesUser');
    $this->SpecialitiesUser->recursive = -1;
    $specialitiesUser = $this->SpecialitiesUser->find('all',array('conditions' => array('user_id' => $user['User']['id'])));  
    $specialitiesUser = Hash::extract($specialitiesUser,'{n}.SpecialitiesUser.speciality_id');
    $specialitiesFromUser = array();
    if (!empty($specialitiesUser)) {
      $this->loadModel('Speciality');
      foreach($specialitiesUser as $specialiti) {
        $this->Speciality->recursive = -1;
        $specialityFromUser = $this->Speciality->findById($specialiti);
        $specialitiesFromUser[] = Hash::extract($specialityFromUser,'Speciality');
      }  
    }
    return $specialitiesFromUser;
  }
/**
 * view_cv method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
  public function view_cv($id = null) {
    $this->layout = 'fancybox';
    
  }
/**
 * ver_cv method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function ver_cv() {
    $this->Company->recursive = -1;
    $companyNames = $this->Company->find('list');
    $this->set('companyNames',$companyNames);
		$this->set('breadcrumb', array(
            __('Dar a conocer mi perfil') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => false, 'url' => '#'),
        ));

        if ($this->request->is(array('post', 'put'))) {
        	//preparamos datos para guardar
        	$dsUser = $this->User->getDataSource();
      		$dsUser->begin();
      		$aImages = array();
          $aImages[] = $this->request->data['User']['image1'];
        	foreach ($aImages as $kindImage => $image) {
                // Comprobamos si ha habido algun error al subir el fichero
              if ($image['error'] == UPLOAD_ERR_OK) {
                  $allowedExtensions = array('jpeg', 'jpg', 'gif', 'png');// Extensiones permitidas
                  $uploads_dir = WWW_ROOT.'files/users/user_'.$this->request->data['User']['id'].'/'; // Carpeta destino
                  $folder = new Folder($uploads_dir,true,0777);
                  
                  $tmp_name = $image["tmp_name"]; // Ruta al fichero temporal
                  $name = time().$image["name"]; // Nombre original del fichero
                  $temp_file = new File($tmp_name); // Descriptor de fichero para hacer las comprobaciones
                  $mime = explode('/',$temp_file->mime());

                  // Comprobamos que la extensión esté permitida
                  if(in_array(strtolower($mime[1]), $allowedExtensions)){
                      $temp_file->close();
                      // DESTINO FINAL de la image sanitizando el nombre
                      $name = str_replace(' ','_',Inflector::humanize(Inflector::underscore($name)));
                      $image_path = $folder->path.str_replace(' ','_',$name);
          						$input_file = $tmp_name;
          						$output_file = $image_path;
          						$this->Image->resize_image( $input_file , 100 , 100 , $output_file );
                      // Movemos el fichero a la carpeta files
                      if(!move_uploaded_file($tmp_name, $image_path)){
                          $this->Session->setFlash(__('Error al subir la imagen.'), 'flash_message', array('type' => 'danger'));
                          $action = array('controller'=>'Users','action' => 'ver_cv');
                          $dsUser->rollback();
                          $this->redirect($action);
                      }
                  } else {
                      $this->Session->setFlash(__('Extensión no aceptada'), 'flash_message', array('type' => 'danger'));
                      $action = array('controller'=>'Users','action' => 'ver_cv');
                      $dsUser->rollback();
                      $this->redirect($action);

                  }
                  //Comprobamos que el tamaño de la imagen sea correcta
                  $image_size = getimagesize($image_path);
                  $image_dif  = $image_size[0]/$image_size[1];

                  //Recogemos los valores validos de tamaño, sera un resultado parecido a la division de ancho i¡y alto
                  // $ancho = 100;
                  // $alto  = 100;

                  // $size = $ancho/$alto;
                  
      				  $this->request->data['User']['photo'] = $name;
      				  $this->request->data['User']['photo_dir'] = 'files/users/user_'.$this->request->data['User']['id'].'/'; // Carpeta destino;
              } 
          }
          foreach($this->request->data['User'] as $k => $value) { //quitamos ' para posibles intrusiones mysql'
          	if (!is_array($value)) {
          		$this->request->data['User'][$k] = str_replace('\'','&#96;',$value);
          	}
          }
          unset($this->request->data['User']['image1']);
        	if ($this->request->data['User']['hasCompany'] == 'A') {
        		$this->request->data['User']['company_id'] = null;
        	} else {
        		$this->Company->recursive = -1;
        		$companyId = $this->Company->findByName(trim($this->request->data['User']['company']));

        		if (!empty($companyId)) {
        			$this->request->data['User']['company_id'] = $companyId['Company']['id'];
        		} else {
        			$this->Company->create();
        			$saveCompanyData['Company']['name'] = $this->request->data['User']['company'];
              $saveCompanyData['Company']['last_reviewer_id'] = $this->request->data['User']['id'];
              $saveCompanyData['Company']['contact_responsable_id']   = $this->request->data['User']['id'];
              $saveCompanyData['Company']['percent_complete'] = 0;
        			$this->Company->save($saveCompanyData);
        			$this->request->data['User']['company_id'] = $this->Company->getLastInsertID();
        		}
        		unset($this->request->data['User']['company']);
        	}
        	if (!empty($this->request->data['User']['country'])) {
        		$countryId = $this->Country->findByName(trim($this->request->data['User']['country']));
        		if (!empty($countryId)) {
        			$this->request->data['User']['country_id'] = $countryId['Country']['id'];
        		}
        	}
        	unset($this->request->data['User']['country']);

        	if (!empty($this->request->data['User']['city'])) {
        		$cityId = $this->City->findByName($this->request->data['User']['city']);
        		if (!empty($cityId)) {
        			$this->request->data['User']['city_id'] = $cityId['City']['id'];
        		} else {
        			$this->City->create();
        			$saveCityData['City']['name']       = $this->request->data['User']['city'];
        			$saveCityData['City']['country_id'] = $this->request->data['User']['country_id'];
        			$this->City->save($saveCityData);
        			$this->request->data['User']['city_id'] = $this->City->getLastInsertID();
        		}
        	} else {
        		$this->request->data['User']['city_id'] = null;
        	}
        	unset($this->request->data['User']['city']);

        	if (!empty($this->request->data['User']['department'])) {
        		$departmentId = $this->Department->findByName(trim($this->request->data['User']['department']));
        		if (!empty($departmentId)) {
        			$this->request->data['User']['department_id'] = $departmentId['Department']['id'];
        		} else {
        			$this->Department->create();
        			$savedDepartmentData['Department']['name'] = $this->request->data['User']['department'];
        			$this->Department->save($savedDepartmentData);
        			$this->request->data['User']['department_id'] = $this->Department->getLastInsertID();
        		}
        	} else {
        		$this->request->data['User']['department_id'] = null;
        	}
        	unset($this->request->data['User']['department']);
        	if (!empty($this->request->data['User']['sector'])) {
        		$sectorId = $this->Sector->findByName(trim($this->request->data['User']['sector']));
        		if (!empty($sectorId)) {
        			$this->request->data['User']['sector_id'] = $sectorId['Sector']['id'];
        		} else {
        			$this->Sector->create();
        			$saveSectorData['Sector']['name'] = $this->request->data['User']['sector'];
        			$this->Sector->save($saveSectorData);
        			$this->request->data['User']['sector_id'] = $this->Sector->getLastInsertID();
        		}
        	} else {
        		$this->request->data['User']['sector_id'] = null;
        	}
        	unset($this->request->data['User']['sector']);
        	if (!empty($this->request->data['User']['functionality'])) {
        		$functionalityId = $this->Functionality->findByName(trim($this->request->data['User']['functionality']));
        		if (!empty($functionalityId)) {
        			$this->request->data['User']['functionality_id'] = $functionalityId['Functionality']['id'];
        		} else {
        			$this->Functionality->create();
        			$saveFunctionalityData['Functionality']['name'] = $this->request->data['User']['functionality'];
        			$this->Functionality->save($saveFunctionalityData);
        			$this->request->data['User']['functionality_id'] = $this->Functionality->getLastInsertID();
        		}
        	} else {
        		$this->request->data['User']['functionality_id'] = null;
        	}
          $this->request->data['User']['card_completed'] = 1;
        	unset($this->request->data['User']['functionality']);
          
        	$isEdited = $this->User->save($this->request->data);
        	if ($isEdited) {
        		$dsUser->commit();
				    $this->User->recursive = -1;
	        	$this->Session->write('UserRegistered',$this->User->findByid($this->request->data['User']['id']));
	        	$this->Session->setFlash(__('Tarjeta de usuario editada correctamente'), 'flash_message', array('type' => 'success'));	
	            return $this->redirect('/Users/ver_cv');
        	}
        }
        $locale = $this->Session->read('Config.language');
        $this->set('project_from_url', null);
        $user = $this->Session->read('UserRegistered');
        $this->Functionality->recursive = -1;
        $functionalities = $this->Functionality->find('list');
        $this->set('functionalities',$functionalities);
        $this->City->recursive = -1;
        $cities = $this->City->find('list');
        $this->set('cities',$cities);
        $departments = $this->User->Department->find('list');
        $this->set('departments',$departments);
        $this->Company->recursive = -1;
        $companies = $this->Company->find('list');
        $this->set('companies',$companies);
        $this->Sector->recursive = -1;
        $sectors = $this->Sector->find('list');
        $this->set('sectors',$sectors);
        $company   = $this->Company->find('first',array('conditions' => array('id' => $user['User']['company_id'])));
        $this->set('company',$company);
        $this->Currency->recursive = -1;
        $currencies = $this->Currency->find('all',array('conditions' => array('conversion_to_euro' > 0)));
        $currencies = Hash::extract($currencies,'{n}.Currency');
        $this->set('currencies',$currencies);
        $id = $user['User']['id'];

    		if (!$this->User->exists($id)) {
    			throw new NotFoundException(__('Usuario no válido'));
    		}
    		//quan pulsin editar / guardar
    		if ($this->request->is(array('post', 'put'))) {
    			if ($this->User->save($this->request->data)) {
    				$this->Session->setFlash(__('Usuario editado correctamente'), 'flash_message', array('type' => 'success'));	
            $user = $this->User->findById($this->Company->getLastInsertID());
            $this->Session->write('UserRegistered',$user);
    				return $this->redirect(array('action' => 'index'));
    			} else {
    				$this->Session->setFlash(__('El usuarion no se guardó. Faltan datos necesarios'), 'flash_message', array('type' => 'danger'));	
    			}
    		} else {
    			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
    			$this->request->data = $this->User->find('first', $options);
    		}
    $specialitiesFromUser = $this->_hasSpecialities($user);
    $this->set('specialitiesFromUser',$specialitiesFromUser);
		$countries = $this->User->Country->find('list');
		$cities = $this->User->City->find('list');
		$sectors = $this->User->Sector->find('list');
		$groups = $this->User->Group->find('list');
		$this->set('user',$user);
    $this->UsersUserProject->recursive = 2;
    $projects = $this->UsersUserProject->find('all',array('conditions' => array('UsersUserProject.user_id' => $user['User']['id'])));
    $this->set(compact('countries', 'cities', 'functionalities', 'sectors', 'groups','projects'));
	}


/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
		$countries = $this->User->Country->find('list');
		$cities = $this->User->City->find('list');
		$functions = $this->User->Function->find('list');
		$sectors = $this->User->Sector->find('list');
		$groups = $this->User->Group->find('list');
		$this->set(compact('countries', 'cities', 'functions', 'sectors', 'groups'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		$countries = $this->User->Country->find('list');
		$cities = $this->User->City->find('list');
		$functions = $this->User->Function->find('list');
		$sectors = $this->User->Sector->find('list');
		$groups = $this->User->Group->find('list');
		$this->set(compact('countries', 'cities', 'functions', 'sectors', 'groups'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('Se eliminó correctamente el usuario'));
		} else {
			$this->Session->setFlash(__('No se pudo eliminar el usuario. Por favor inténtelo más tarde'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	public function register($codeKey = null) {
		$userToRegister = $this->User->findByCodeKey($codeKey);
		if (!empty($userToRegister)) {
			$userData = $userToRegister;
			$userData['User']['codeKey'] = '';
			$userData['User']['active'] = 1;
			$this->User->save($userData);
		}
	}
  private function _setUrlDependence($user) {

      if ($user['User']['card_completed'] == 0) {
        $this->Session->setFlash(__('Antes de poder realizar cualquier acción debe rellenar su tarjeta de visita'), 'flash_message', array('type' => 'info'));
        return $this->redirect($this->Auth->redirect());
      } else if ($user['User']['formation_completed'] == 0) {
        $this->Session->setFlash(__('Para poder rellenar proyectos debe rellenar su formación'), 'flash_message', array('type' => 'info')); 
        return $this->redirect($this->Auth->redirect());
      }
      
  }
	public function login() {
		$this->layout = 'public';
		$this->set('bootstrap_message','');
	    if ($this->request->data) {
        $isActive = true;
	    	if (!empty($this->request->data['User']['username'])) {
	    		$username = $this->request->data['User']['username'];
	    		$this->User->recursive = -1;
	    		$isActive = $this->User->findByUsername($username);
	    		if (!empty($isActive)) {
	    			$isActive = $isActive['User']['active'];	
	    		}
	    	}
	    	if ($isActive) {
          if ($this->Auth->login()) {
            $isLogged = true;
            $this->User->recursive = -1;
            $user = $this->User->findByUsername($this->request->data['User']['username']);
            $this->Session->write('UserRegistered',$user);
            $this->_setUrlDependence($user);
          } else {
            $this->set('bootstrap_message',__('Nombre de usuario y/o contraseña incorrecto'));
            $isLogged = false;
          }
	    	} else {
	    		$this->set('bootstrap_message',__('Usuario no existente o no activo'));
          $isLogged = false;
	    	}
        if ($isLogged) {
          $this->redirect($this->Auth->redirect());
        } else {
          $this->Auth->redirect();
        }
	    } else {
        //debug($this->Session->read('UserRegistered')).die;
        $sessionRegistered = $this->Session->read('UserRegistered');
        if (!empty($sessionRegistered)) {
            $user = $this->Session->read('UserRegistered');
            $this->_setUrlDependence($user);
            $this->redirect($this->Auth->redirect());
        }
      }
	}
    public function contactsmap() {
      $this->set('breadcrumb', array(
        ));
    }
    public function projectmap() {
    // if (100 no esta completat) {
    //  $this->Session->setFlash(__('El informe se ha eliminado correctamente'), 'flash_message', array('type' => 'info')); 
    //  $this->element('tarjeta_bc')
    // }
        // if (100 no esta completat) {
    //  $this->Session->setFlash(__('El informe se ha eliminado correctamente'), 'flash_message', array('type' => 'info')); 
    //  $this->element('tarjeta_bc')
    // }
    $user = $this->Session->read('UserRegistered');
    $user = $user['User'];
    $this->UsersUserProject->recursive = -1;
    $conditions['conditions'] = array('UsersUserProject.user_id' => $user['id']);
    $projects = $this->UsersUserProject->find('all',$conditions);
    if (!empty($projects)) {
      $this->UserProject->recursive = 1;
      $projects = Hash::extract($projects,'{n}.UsersUserProject.user_project_id');
      if (count($projects) > 1) {
        $projects = $this->UserProject->find('all', array('conditions' => array('UserProject.id IN' => $projects)));
      } else {
        $projects = $this->UserProject->find('all', array('conditions' => array('UserProject.id' => $projects)));
      }  
    }

    $countriesNames = $this->Session->read('Static.Countries');
    $this->set('countriesNames',$countriesNames);
    $this->set('cprojects',$projects);
    $this->set('breadcrumb', array(
        ));
    //$this->User->recursive = 0;
  }

  /**
 * forgotten_password method  // show the window to set the new password to the requested user_id
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
  public function forgotten_password($userId,$secretKey) {
    $this->layout = 'public';
    $this->User->recursive = -1;
    $user = $this->User->findByIdAndActiveKey($userId,$secretKey);
    if (!empty($user)) {
      if ($this->request->is('post')) {
        $userToSave = $user;
        $userToSave['User']['password']   = AuthComponent::password($this->request->data['User']['mine']);
        $userToSave['User']['active_key'] = '';
        if ($this->User->save($userToSave)) {
          $this->Session->setFlash(__('Contraseña modificada correctamente. Acceda a través del panel superior a la derecha con su nombre de usuario y la nueva contraseña'));
        };
      }
    } else {
      throw new NotFoundException(__('Usuario no válido'));
    }
  }
  /**
 * forgot_password method  // show window to send the email requested password and send email if user email is checked in bbdd
 *
 * @throws NotFoundException
 * @return void
 */
	public function forgot_password() {
    $this->layout = 'public';
    if ($this->request->is('post')) {
      $this->User->recursive = -1;
      $userFogotten = $this->User->findByEmail($this->request->data['forgot_password_identity']);
      if (!empty($userFogotten)) {
        $dataToSave['User']               = $userFogotten['User'];
        $dataToSave['User']['active_key'] = md5($dataRequested['User']['username'].date('Y-m-d H:i:s'));
        $this->User->save($dataToSave);
        $dataRequested = $dataToSave;

        $Email = new CakeEmail('gmail');
        $Email->emailFormat('html');
        $Email->from(array('noreply.globeshake@gmail.com' => 'noreply.globeshake@gmail.com'));
        $Email->subject(__('GlobeShake  - Recuperar contraseña'));
        $Email->to($dataRequested['User']['email']);
        if ($Email->send($this->EmailText->forgotten_password($dataRequested))) {
          $this->Session->setFlash(__('Se ha enviado un correo electrónico a su cuenta para reestablecer su contraseña'));
        } else {
          $this->Session->setFlash(__('Ha habido algún tipo de error al enviar el correo electrónico, por favor contacte con el administrador'));  
        }
      } else {
        $this->Session->setFlash(__('Éste usuario no existe en la base de datos. Por favor, introduzca un email válido'));
      }
      
    }
	}
	public function resend_token() {
    $this->layout = 'public';
    if ($this->request->is('post')) {
      $this->User->recursive = -1;
      $user = $this->User->findByEmail($this->request->data['User']['forgot_password_identity']);
      if (!empty($user)) {
        $Email = new CakeEmail('gmail');
        $Email->emailFormat('html');
        $Email->from(array('noreply.globeshake@gmail.com' => 'noreply.globeshake@gmail.com'));
        $Email->subject(__('GlobeShake  - Reenviar código de activación'));
        $Email->to($user['User']['email']);
        

        if (!empty($user['User']['active_key'])) {
          $Email->send($this->EmailText->registration($user));
          $this->Session->setFlash(__('Se ha reenviado el código de activación a la cuenta indicada'));
        } elseif (($user['User']['active'] == 1)) {
          //no tiene active key pero está activo
          $Email->send($this->EmailText->registration_fail($user));
          $this->Session->setFlash(__('El usuario asociado a éste email ya está activo. No tiene código de activación. Si no recuerda su contraseña pulse en "He olvidado la contraseña"'));
        } else {
          $this->Session->setFlash(__('El usuario que está asociado a éste email no está activo ni tiene token de activación.'));
        }
      } else {
        $this->Session->setFlash(__('No existe ningún usuario en GlobeShake asociado a éste correo electrónico'));
      }
    }
	}
	public function activate() {
		$this->layout = 'public';
		$this->User->recursive = -1;
		$id         =  $this->request->params['pass'][0];
		$userKey    = $this->request->params['pass'][1];
		$activeUser = $this->User->find('first',array('conditions' => array('id' => $id,'active_key' => $userKey)));
		if (empty($activeUser)) {
			$this->Session->setFlash(__('El nombre de usuario no coincide con los parámetros pasados, por favor compruebe que puede acceder a Globeshake.com o reenvie su código de autentificación'));
			$this->redirect($this->Auth->logout());
		} else {
			$dataToSave = $activeUser;
			$dataToSave['User']['active'] = 1;
			$dataToSave['User']['active_key'] = '';
			if ($this->User->save($dataToSave,false)) {
	    		$this->Session->setFlash(__('Cuenta activada correctamente. Acceda a través del panel superior a la derecha con su nombre de usuario y la contraseña que nos facilitó'));
	    		$this->redirect('/Users/login');
			} else {
				$this->Session->setFlash(__('La cuenta no se ha podido activar'));
				$this->redirect($this->Auth->logout());
			}
		}
	}
	public function beforeFilter() {
		parent::beforeFilter();
	}
	public function logout() {
	  $this->Session->setFlash(__('Usuario deslogueado correctamente. Gracias por participar en GlobeShake.com'));
		$this->Session->delete('UserRegistered');
    $this->redirect($this->Auth->logout());
	}
	public function initDB() {
    $group = $this->User->Group;

    // Allow admins to everything
    $group->id = 3;
    $this->Acl->allow($group, 'controllers');

    // allow managers to posts and widgets
    $group->id = 4;
    $this->Acl->deny($group, 'controllers');
    $this->Acl->allow($group, 'controllers/Users');
    
    // allow basic users to log out
    $this->Acl->allow($group, 'controllers/users/logout');
    $this->Acl->allow($group, 'controllers/users/login');
    $this->Acl->allow($group, 'controllers/users/add');

    // we add an exit to avoid an ugly "missing views" error message
    echo "all done";
    exit;
}
}
