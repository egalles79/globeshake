<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
/**
 * CompanyProjects Controller
 *
 * @property CompanyProject $CompanyProject
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CompanyProjectsController extends AppController {

/**
 * Components
 *
 * @var arraytr
 */
	public $components = array('Paginator', 'Session','Image');
	public $uses = array('Office','CompanyProject','CompanyProjectsUser','Department','Geolocalitzation','Functionality','User','Company','CompanyProjectsImage','CompanyProject','Currency','CompanyProjectsSector','City','Sector');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CompanyProject->recursive = 0;
		$this->set('companyProjects', $this->Paginator->paginate());
	}

/**
 * index method
 *
 * @return void
 */
	public function has_participed($hasParticiped, $companyProjectId, $countryId) {
		$user = $this->Session->read('UserRegistered');
		$conditions['company_project_id'] = $companyProjectId;
		$conditions['user_id']            = $user['User']['id'];
		$cProject = $this->CompanyProjectsUser->find('first', array('conditions' => $conditions));
		if ($hasParticiped) {
			$this->loadModel('CompanyProjectsUser');
			$this->CompanyProjectsUser->recursive = -1;
			if (!empty($cProject)) {
				$message = __('El usuario ya estaba añadido al proyecto');
			} else {
				$this->CompanyProjectsUser->create();
				$dataSave['CompanyProjectsUser']['user_id']            = $conditions['user_id'];
				$dataSave['CompanyProjectsUser']['company_project_id'] = $conditions['company_project_id'];
				if ($this->CompanyProjectsUser->save($dataSave)) {
					$message = __('Usuario añadido al proyecto.');	
				} else {
					$message = __('Que passa terrassa');
				}
				
			}
			
		} else {
			if (!empty($cProject)) {
				$this->CompanyProjectsUser->id = $cProject['CompanyProjectsUser']['id'];
				if ($this->CompanyProjectsUser->delete()) {
					$message = __('Usuario eliminado del proyecto.');
				} else {
					$message = debug($cProject);
				}
			} else {
				$message = __('Usuario no asociado al proyecto');
			}
		}
		$this->Session->setFlash($message, 'flash_message', array('type' => 'success'));	
		$this->redirect(array('controller' => 'Companies', 'action' => 'proyectos_pais',$countryId));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CompanyProject->exists($id)) {
			throw new NotFoundException(__('Invalid company project'));
		}
		$user = $this->Session->read('UserRegistered');
		$this->loadModel('CompanyProjectsUsers');
		$conditions = array('user_id' => $user['User']['id'],'company_project_id' => $id);
		$userInProject = $this->CompanyProjectsUsers->find('first',array('conditions' => $conditions));
		$bUserInProject = (!empty($userInProject)) ? true : false;
		$this->set('bUserInProject', $bUserInProject);
		$options = array('conditions' => array('CompanyProject.' . $this->CompanyProject->primaryKey => $id));
		$this->CompanyProject->recursive = 1;
		$project = $this->CompanyProject->find('first', $options);
		//debug($project).die;
		$countriesStatic = $this->getStatic('Country');
		$this->set('breadcrumb', array(
            __('Dar a conocer empresa') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => true, 'url' => '/Companies/ver_cv'),
            __('Proyectos en %s',array($countriesStatic[$project['Country']['id']])) => array('active' => false, 'url' => '/Companies/proyectos_pais/'.$project['Country']['id']),
            __('Ver proyecto %s',array($project['CompanyProject']['name'])) => array('active' => false, 'url' => '#'),
        ));
		$dateInit = new DateTime($companyProject['CompanyProject']['start_date']);
		$conditions = array('conditions' => array('CompanyProjectsUsers.company_project_id' => $id));
		$usersProject = $this->CompanyProjectsUsers->find('all', $conditions);
		$usersProject = Hash::extract($usersProject,'{n}.CompanyProjectsUsers.user_id');
		$this->set('usersProjects', $usersProject);
		$dateInit = $dateInit->format('m/Y');
		$this->set('dateInit', $dateInit);
		$this->loadModel('CompanyProjectsSectors');
		$sectorsProject = $this->CompanyProjectsSectors->find('all',array('conditions' => array('company_project_id' => $project['CompanyProject']['id'])));
		$sectorsProject = Hash::extract($sectorsProject,'{n}.CompanyProjectsSectors.sector_id');	
		$this->set('sectors_companyproject',$sectorsProject);
		if ($project['CompanyProject']['on_going']) {
			$this->set('dateEnd', null);
		} else {
			$dateEnd = new DateTime($project['CompanyProject']['end_date']);
			$dateEnd = $dateEnd->format('m/Y');
			$this->set('dateEnd', $dateEnd);
		}
		$this->set('project', $project);
	}

/**
 * add method
 *
 * @return void
 */
/**
 * add method
 *
 * @return void
 */
	public function add($id = null) {
		$user = $this->Session->read('UserRegistered');
		$this->set('user',$user);
		$this->Country->locale = $this->Session->read('Config.language');
		$contactResponsableList = $this->_getPossibleResponsables($user);
		$images = array();
		if ($id != null) {
			$this->CompanyProjectImage->recursive = -1;
			$images = $this->CompanyProjectImage->find('all',array('conditions' => array('company_project_id' => $id)));
			$images = Hash::extract($images,'{n}.CompanyProjectImage');
		}
		$this->set('images',$images);
		
		$this->set('contactResponsableList',$contactResponsableList);
		$this->set('breadcrumb', array(
            __('Dar a conocer empresa') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => true, 'url' => '/Companies/ver_cv'),
            __('Añadir proyecto') => array('active' => false, 'url' => '#'),
        ));
		if ($this->request->is('post')) {
			$images = array();
			$images[] = $this->request->data['CompanyProject']['image1'];
			$images[] = $this->request->data['CompanyProject']['image2'];
			$images[] = $this->request->data['CompanyProject']['image3'];
			$images[] = $this->request->data['CompanyProject']['image4'];
			unset($this->request->data['CompanyProject']['image1']);
			unset($this->request->data['CompanyProject']['image2']);
			unset($this->request->data['CompanyProject']['image3']);
			unset($this->request->data['CompanyProject']['image4']);
			unset($this->request->data['CompanyProject']['address']);
			$sectorsArray = $this->request->data['CompanyProject']['Sectors'];
			unset($this->request->data['CompanyProject']['Sectors']);
			$hasParticiped = $this->request->data['CompanyProject']['has_participed'];
			unset($this->request->data['CompanyProject']['has_participed']);
			//cogemos el user para trabajar con el
			
			//budget correcto
			$this->request->data['CompanyProject']['budget'] = $this->request->data['CompanyProject']['budge'];
			unset($this->request->data['CompanyProject']['budge']);

			//tratamiento de fechas
			$dateInit = explode('/',$this->request->data['CompanyProject']['init_date']);
			
			if (empty($this->request->data['CompanyProject']['on_going'])) {
				$dateEnd = explode('/',$this->request->data['CompanyProject']['end_date']);
				$dateEnd  = new DateTime($dateEnd[1].'-'.$dateEnd[0].'-01');
				$dateEnd = $dateEnd->format('Y-m-d');
			} else {
				$dateEnd = null;
			}
			$dateInit = new DateTime($dateInit[1].'-'.$dateInit[0].'-01');
			$dateInit = $dateInit->format('Y-m-d');
			
			$this->request->data['CompanyProject']['start_date'] = $dateInit;
			$this->request->data['CompanyProject']['end_date']   = $dateEnd;
			unset($this->request->data['CompanyProject']['init_date']);
			//ya tenemos el country_id gracias a google
			unset($this->request->data['CompanyProject']['country_namex']);

			$this->request->data['CompanyProject']['company_id'] = $user['User']['company_id'];

			//customer add or getId
			$customerId = $this->Company->check(str_replace('\'','`',$this->request->data['CompanyProject']['custome']));
			$this->request->data['CompanyProject']['customer_id'] = $customerId;
			unset($this->request->data['CompanyProject']['custome']);
			
			//miramos si el customer existe y si no la creamos como empresa
			$this->request->data['CompanyProject']['company_id'] = $user['User']['company_id'];
			
			//miramos si tenemos la ciudad sino la guardamos
			if (!empty($this->request->data['CompanyProject']['cit_id'])) {
				$cityId = $this->request->data['CompanyProject']['cit_id'];
			} else {
				$cityId = $this->City->check(str_replace('\'','`',$this->request->data['CompanyProject']['cit']), (int) $this->request->data['CompanyProject']['country_id']);
			}
			$this->request->data['CompanyProject']['city_id'] = $cityId;

			unset($this->request->data['CompanyProject']['cit_id']);
			unset($this->request->data['CompanyProject']['cit']);
			
			$geolocalitzationId = $this->Geolocalitzation->check($this->request->data['CompanyProject']['latitude'],$this->request->data['CompanyProject']['longitude']);
			$this->request->data['CompanyProject']['geolocalitzation_id'] = $geolocalitzationId;
			unset($this->request->data['CompanyProject']['longitude']);
			unset($this->request->data['CompanyProject']['latitude']);
			
			//miramos si tenemos el sector del proyecto sino la guardamos
			$sectorProjectId = $this->Sector->check(str_replace('\'','`',$this->request->data['CompanyProject']['sector_servic']));
			$this->request->data['CompanyProject']['sector_id'] = $sectorProjectId;
			unset($this->request->data['CompanyProject']['sector_servic']);
			
			//miramos si tenemos la funcionalidad del proyecto sino la guardamos
			$functionalityId = $this->Functionality->check(str_replace('\'','`',$this->request->data['CompanyProject']['functionalit']));
			$this->request->data['CompanyProject']['functionality_id'] = $functionalityId;
			unset($this->request->data['CompanyProject']['functionalit']);
			
			//miramos si tenemos la departamento (subfuncionalidad) del proyecto sino la guardamos
			$departmentId = $this->Department->check(str_replace('\'','`',$this->request->data['CompanyProject']['departmen']));
			$this->request->data['CompanyProject']['department_id'] = $functionalityId;
			unset($this->request->data['CompanyProject']['departmen']);

			$this->request->data['CompanyProject']['duration_month'] = (empty($this->request->data['CompanyProject']['duration_month'])) ? 0 : $this->request->data['CompanyProject']['duration_month'];
			$this->request->data['CompanyProject']['duration_year'] = (empty($this->request->data['CompanyProject']['duration_year'])) ? 0 : $this->request->data['CompanyProject']['duration_year'];
			$this->request->data['CompanyProject']['duration_day'] = (empty($this->request->data['CompanyProject']['duration_day'])) ? 0 : $this->request->data['CompanyProject']['duration_day'];
			$this->request->data['CompanyProject']['duration_hours'] = (empty($this->request->data['CompanyProject']['duration_hours'])) ? 0 : $this->request->data['CompanyProject']['duration_hours'];
			
			$this->request->data['CompanyProject']['is_completed'] = $this->checkTantPerCent($this->request->data, true);

			if (empty($this->request->data['CompanyProject']['id'])) {
				$this->request->data['CompanyProject']['last_reviewer_id'] = $user['User']['id'];
				$this->CompanyProject->create();
				$dsCompanyProject = $this->CompanyProject->getDataSource();
				if ($this->CompanyProject->save($this->request->data['CompanyProject'])) {
					// despues de guardar company_project
					$companyProjectId = $this->CompanyProject->getLastInsertID();
					if (!empty($sectorsArray)) {
						foreach($sectorsArray as $nameSector) {
							$sectorName = $this->Sector->findByName($nameSector);
							if (empty($sectorName)) {
								$sectorSave['Sector']['name'] = $nameSector;
								$this->Sector->save($sectorSave);
								$sectorId = $this->Sector->getLastInsertID();
							} else {
								$sectorId = $sectorName['Sector']['id'];
							}
							$saveSectorsCompany['company_project_id'] = $companyProjectId;
							$saveSectorsCompany['sector_id'] = $sectorId;
							$this->CompanyProjectsSector->create();
							$this->CompanyProjectsSector->save($saveSectorsCompany);
						}
					} else {
						unset($this->request->data['CompanyProject']['Sectors']);
					}
					$k = 0;

					foreach ($images as $kindImage => $image) {
                // Comprobamos si ha habido algun error al subir el fichero
		              if ($image['error'] == UPLOAD_ERR_OK) {
		                  $allowedExtensions = array('jpeg', 'jpg', 'gif', 'png');// Extensiones permitidas
		                  $uploads_dir = WWW_ROOT.'files/companies/company_'.$user['User']['company_id'].'/company_project_'.$companyProjectId.'/'; // Carpeta destino
		                  $folder = new Folder($uploads_dir,true,0777);

		                  $tmp_name = $image["tmp_name"]; // Ruta al fichero temporal
		                  $name = str_replace(' ','_',strtolower(Inflector::humanize(Inflector::underscore(time().'_'.$image['name'])))); // Nombre original del fichero
		                  $temp_file = new File($image["tmp_name"]); // Descriptor de fichero para hacer las comprobaciones
		                  $mime = explode('/',$temp_file->mime());
		                  // Comprobamos que la extensión esté permitida
		                  
		                  if(in_array(strtolower($mime[1]), $allowedExtensions)){
		                      $temp_file->close();
		                      // DESTINO FINAL de la image sanitizando el nombre
		                      $image_path = $uploads_dir.$name;
							  $input_file = $tmp_name;
							  $output_file = $image_path;
							  $this->Image->resize_image( $input_file , 200 , 150 , $output_file );
		                      // Movemos el fichero a la carpeta files

		                      if(!move_uploaded_file($tmp_name, $image_path)){
		                          $this->Session->setFlash(__('Error al subir la imagen.'), 'flash_message', array('type' => 'danger'));
		                          $action = array('controller'=>'CompanyProject','action' => 'add');
		                          $dsCompanyProject->rollback();
		                          $this->redirect($action);
		                      } 
		                  } else {
		                      $this->Session->setFlash(__('Extensión no aceptada'), 'flash_message', array('type' => 'danger'));
		                      $action = array('controller'=>'CompanyProject','action' => 'add');
		                      $$dsCompanyProject->rollback();
		                      $this->redirect($action);

		                  }
		                  //Comprobamos que el tamaño de la imagen sea correcta
		                  $image_size = getimagesize($image_path);
		                  $image_dif  = $image_size[0]/$image_size[1];

		                  //Recogemos los valores validos de tamaño, sera un resultado parecido a la division de ancho i¡y alto
		                  // $ancho = 100;
		                  // $alto  = 100;

		                  // $size = $ancho/$alto;
		                  $dataImageSave = array();
						  $dataImageSave['CompanyProjectsImage']['name'] = $name;
						  $dataImageSave['CompanyProjectsImage']['company_project_id'] = $companyProjectId;

						  $dataImageSave['CompanyProjectsImage']['default'] =  ($k == 0) ? 1 : 0;

						  try {
						  	$this->CompanyProjectsImage->create();
						  	$this->CompanyProjectsImage->save($dataImageSave);
						  } catch (Exception $e) {
								debug($e).die;
					      }
						  $k++;
		              } 
          			}
          			if ($hasParticiped) {
          				$saveCompanyProjectusers['CompanyProjectsUser']['user_id'] = $user['User']['id'];
          				$saveCompanyProjectusers['CompanyProjectsUser']['company_project_id'] = $companyProjectId;
						$this->CompanyProjectsUser->create();
          				$this->CompanyProjectsUser->save($saveCompanyProjectusers);
          			} else {
          				$userToDelete = $this->CompanyProjectsUser->find('all');
          				$this->CompanyProjectsUser->delete($userToDelete);
          			}
					$this->User->recursive = -1;
	        		$this->Session->write('UserRegistered',$this->User->findByid($user['User']['id']));
					$this->Session->setFlash(__('Proyecto guardado correctamente'), 'flash_message', array('type' => 'success'));
					return $this->redirect(array('controller' => 'Companies','action' => 'ver_cv'));
				} else {
					$this->Session->setFlash(__('Hubo problemas al intentar guardar el proyecto, inténtelo de nuevo, gracias'), 'flash_message', array('type' => 'success'));
					return $this->redirect(array('controller' => 'UserProjects','action' => 'add'));
				}
			} else {
				if ($this->UserProject->save($this->request->data)) {
					$this->User->recursive = -1;
	        		$this->Session->write('UserRegistered',$this->User->findByid($user['User']['id']));
					$this->Session->setFlash(__('Proyecto editado correctamente'), 'flash_message', array('type' => 'success'));
					return $this->redirect(array('controller' => 'Companies','action' => 'ver_cv#pr_'.$this->request->data['UserProject']['id']));
				} else {
					$this->Session->setFlash(__('Hubo problemas al intentar guardar el proyecto, inténtelo de nuevo, gracias'), 'flash_message', array('type' => 'success'));
					return $this->redirect(array('controller' => 'UserProjects','action' => 'edit'));
				}
			}
		}
		$budgetCurrencies = $this->Currency->find('list',array('conditions' => array('conversion_to_euro > ' => 0,'conversion_to_euro != ' => null)));
		$billingCurrencies = $this->Currency->find('list',array('conditions' => array('conversion_to_euro > ' => 0,'conversion_to_euro !=' => NULL)));
		
		$countries = $this->getStatic('Country',false);
		$sectorProjects = $this->Session->read('Static.Sectors');
		$this->City->recursive = -1;
		$cities = $this->City->find('all',array('fields' => array('City.name','City.country_id')));
		$cities = Hash::extract($cities,'{n}.City');
		$functionalities = $this->Session->read('Static.Functionalities');
		$sectorServices = $this->Session->read('Static.Sectors');
		$departments = $this->Session->read('Static.Departments');
		$companies = $this->Session->read('Static.Companies');
		$customers = $this->Session->read('Static.Companies');

		$companiesText = '';
		foreach($companies as $company) {
			$companiesText .= '"'.$company.'",';
		}
		//para jscript
		$companiesText = substr($companiesText,0,strlen($companiesText)-1);
		$this->set('companiesText',$companiesText);
		$departmentsText = '';
		foreach($departments as $department) {
			$departmentsText .= '"'.$department.'",';
		}
		$departmentsText = substr($departmentsText,0,strlen($departmentsText)-1);
		$this->set('departmentsText',$departmentsText);
		$citiesText = '';
		foreach($cities as $city) {
			$citiesText .= '"'.$city['name'].' ('.$countries[$city['country_id']].')",';
		}
		$citiesText = substr($citiesText,0,strlen($citiesText)-1);
		$this->set('citiesText',$citiesText);
		$sectorProjectText = '';
		foreach($sectorProjects as $sectorProject) {
			$sectorProjectText .= '"'.$sectorProject.'",';
		}
		$sectorProjectText = substr($sectorProjectText,0,strlen($sectorProjectText)-1);
		$this->set('sectorProjectText',$sectorProjectText);

		$functionalitiesText = '';
		foreach($functionalities as $functionality) {
			$functionalitiesText .= '"'.$functionality.'",';
		}
		$functionalitiesText = substr($functionalitiesText,0,strlen($functionalitiesText)-1);
		$this->set('functionalitiesText',$functionalitiesText);

		$this->set(compact('budgetCurrencies', 'billingCurrencies', 'countries', 'sectorProjects', 'cities', 'functionalities', 'sectorServices', 'departments', 'companies', 'customers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function edit($id = null) {
		if (!$this->CompanyProject->exists($id)) {
			throw new NotFoundException(__('Invalid company project'));
		}
		$this->CompanyProject->recursive = 1;
		$companyProject = $this->CompanyProject->findById($id);

		$dateInit = new DateTime($companyProject['CompanyProject']['start_date']);

		$dateInit = $dateInit->format('m/Y');
		$this->set('dateInit', $dateInit);
		if ($companyProject['CompanyProject']['on_going']) {
			$this->set('dateEnd', null);
		} else {
			$dateEnd = new DateTime($companyProject['CompanyProject']['end_date']);
			$dateEnd = $dateEnd->format('m/Y');
			$this->set('dateEnd', $dateEnd);
		}
		$companyProjectsSectors = $this->CompanyProjectsSector->find('all',array('conditions' => array('company_project_id' => $companyProject['CompanyProject']['id'])));
		$companyProjectsSectors = Hash::extract($companyProjectsSectors,'{n}.CompanyProjectsSector.sector_id');
		$this->set('companyprojectsectors',$companyProjectsSectors);
		$this->set('project',$companyProject);
		$user = $this->Session->read('UserRegistered');
		$this->set('user',$user);
		$this->Country->locale = $this->Session->read('Config.language');
		$contactResponsableList = $this->_getPossibleResponsables($user);
		$this->set('contactResponsableList',$contactResponsableList);
		$this->set('breadcrumb', array(
            __('Dar a conocer empresa') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => true, 'url' => '/Companies/ver_cv'),
            __('Editar proyecto') => array('active' => false, 'url' => '#'),
        ));
		if ($this->request->is('post')) {
			$images = array();
			$images[] = $this->request->data['CompanyProject']['image1'];
			$images[] = $this->request->data['CompanyProject']['image2'];
			$images[] = $this->request->data['CompanyProject']['image3'];
			$images[] = $this->request->data['CompanyProject']['image4'];
			unset($this->request->data['CompanyProject']['image1']);
			unset($this->request->data['CompanyProject']['image2']);
			unset($this->request->data['CompanyProject']['image3']);
			unset($this->request->data['CompanyProject']['image4']);
			unset($this->request->data['CompanyProject']['address']);
			$sectorsArray = $this->request->data['CompanyProject']['Sectors'];
			unset($this->request->data['CompanyProject']['Sectors']);
			$hasParticiped = $this->request->data['CompanyProject']['has_participed'];
			unset($this->request->data['CompanyProject']['has_participed']);
			//cogemos el user para trabajar con el
			
			//budget correcto
			$this->request->data['CompanyProject']['budget'] = $this->request->data['CompanyProject']['budge'];
			unset($this->request->data['CompanyProject']['budge']);

			//tratamiento de fechas
			$dateInit = explode('/',$this->request->data['CompanyProject']['init_date']);
			
			if (empty($this->request->data['CompanyProject']['on_going'])) {
				$dateEnd = explode('/',$this->request->data['CompanyProject']['end_date']);
				$dateEnd  = new DateTime($dateEnd[1].'-'.$dateEnd[0].'-01');
				$dateEnd = $dateEnd->format('Y-m-d');
			} else {
				$dateEnd = null;
			}
			$dateInit = new DateTime($dateInit[1].'-'.$dateInit[0].'-01');
			$dateInit = $dateInit->format('Y-m-d');
			
			$this->request->data['CompanyProject']['start_date'] = $dateInit;
			$this->request->data['CompanyProject']['end_date']   = $dateEnd;
			unset($this->request->data['CompanyProject']['init_date']);
			//ya tenemos el country_id gracias a google
			unset($this->request->data['CompanyProject']['country_namex']);

			$this->request->data['CompanyProject']['company_id'] = $user['User']['company_id'];

			//customer add or getId
			$customerId = $this->Company->check(str_replace('\'','`',$this->request->data['CompanyProject']['custome']));
			$this->request->data['CompanyProject']['customer_id'] = $customerId;
			unset($this->request->data['CompanyProject']['custome']);
			
			//miramos si el customer existe y si no la creamos como empresa
			$this->request->data['CompanyProject']['company_id'] = $user['User']['company_id'];
			
			//miramos si tenemos la ciudad sino la guardamos
			// if (!empty($this->request->data['CompanyProject']['cit_id'])) {
			// 	$cityId = $this->request->data['CompanyProject']['cit_id'];
			// } else {
				
			// }
			$cityId = $this->City->check(str_replace('\'','`',$this->request->data['CompanyProject']['cit']), (int) $this->request->data['CompanyProject']['country_id']);
			
			$this->request->data['CompanyProject']['city_id'] = $cityId;
			unset($this->request->data['CompanyProject']['cit_id']);
			unset($this->request->data['CompanyProject']['cit']);
			
			$geolocalitzationId = $this->Geolocalitzation->check($this->request->data['CompanyProject']['latitude'],$this->request->data['CompanyProject']['longitude']);

			$this->request->data['CompanyProject']['geolocalitzation_id'] = $geolocalitzationId;
			unset($this->request->data['CompanyProject']['longitude']);
			unset($this->request->data['CompanyProject']['latitude']);
			
			//miramos si tenemos el sector del proyecto sino la guardamos
			$sectorProjectId = $this->Sector->check(str_replace('\'','`',$this->request->data['CompanyProject']['sector_servic']));
			$this->request->data['CompanyProject']['sector_id'] = $sectorProjectId;
			unset($this->request->data['CompanyProject']['sector_servic']);
			
			//miramos si tenemos la funcionalidad del proyecto sino la guardamos
			$functionalityId = $this->Functionality->check(str_replace('\'','`',$this->request->data['CompanyProject']['functionalit']));
			$this->request->data['CompanyProject']['functionality_id'] = $functionalityId;
			unset($this->request->data['CompanyProject']['functionalit']);
			
			//miramos si tenemos la departamento (subfuncionalidad) del proyecto sino la guardamos
			$departmentId = $this->Department->check(str_replace('\'','`',$this->request->data['CompanyProject']['departmen']));
			$this->request->data['CompanyProject']['department_id'] = $functionalityId;
			unset($this->request->data['CompanyProject']['departmen']);

			$this->request->data['CompanyProject']['duration_month'] = (empty($this->request->data['CompanyProject']['duration_month'])) ? 0 : $this->request->data['CompanyProject']['duration_month'];
			$this->request->data['CompanyProject']['duration_year'] = (empty($this->request->data['CompanyProject']['duration_year'])) ? 0 : $this->request->data['CompanyProject']['duration_year'];
			$this->request->data['CompanyProject']['duration_day'] = (empty($this->request->data['CompanyProject']['duration_day'])) ? 0 : $this->request->data['CompanyProject']['duration_day'];
			$this->request->data['CompanyProject']['duration_hours'] = (empty($this->request->data['CompanyProject']['duration_hours'])) ? 0 : $this->request->data['CompanyProject']['duration_hours'];
			
			$this->request->data['CompanyProject']['last_reviewer_id'] = $user['User']['id'];
			$dsCompanyProject = $this->CompanyProject->getDataSource();
			$this->request->data['CompanyProject']['is_completed'] = $this->checkTantPerCent($this->request->data['CompanyProject'], true);
			
			if ($this->CompanyProject->save($this->request->data['CompanyProject'])) {
				// despues de guardar company_project
				if (!empty($sectorsArray)) {
					$companyProjectId = $id;
					$this->CompanyProjectsSector->deleteAll(array('company_project_id' => $id));
					foreach($sectorsArray as $nameSector) {
						$sectorName = $this->Sector->findByName($nameSector);
						if (empty($sectorName)) {
							$sectorSave['Sector']['name'] = $nameSector;
							$this->Sector->save($sectorSave);
							$sectorId = $this->Sector->getLastInsertID();
						} else {
							$sectorId = $sectorName['Sector']['id'];
						}
						
						$saveSectorsCompany['company_project_id'] = $id;
						$saveSectorsCompany['sector_id'] = $sectorId;
						$this->CompanyProjectsSector->create();
						$this->CompanyProjectsSector->save($saveSectorsCompany);
					}
				} else {
					unset($this->request->data['CompanyProject']['Sectors']);
				}
				$k = 0;
				foreach ($images as $kindImage => $image) {
            		// Comprobamos si ha habido algun error al subir el fichero
					if ($image['error'] == UPLOAD_ERR_OK) {
		                  $allowedExtensions = array('jpeg', 'jpg', 'gif', 'png');// Extensiones permitidas
		                  $uploads_dir = WWW_ROOT.'files/companies/company_'.$user['User']['company_id'].'/company_project_'.$id.'/'; // Carpeta destino
		                  $folder = new Folder($uploads_dir,true,0777);

		                  $tmp_name = $image["tmp_name"]; // Ruta al fichero temporal
		                  $name = str_replace(' ','_',strtolower(Inflector::humanize(Inflector::underscore(time().'_'.$image['name'])))); // Nombre original del fichero
		                  $temp_file = new File($image["tmp_name"]); // Descriptor de fichero para hacer las comprobaciones
		                  $mime = explode('/',$temp_file->mime());
		                  // Comprobamos que la extensión esté permitida
		                  
		                  if(in_array(strtolower($mime[1]), $allowedExtensions)){
		                      $temp_file->close();
		                      // DESTINO FINAL de la image sanitizando el nombre
		                      $image_path = $uploads_dir.$name;
							  $input_file = $tmp_name;
							  $output_file = $image_path;
							  $this->Image->resize_image( $input_file , 200 , 150 , $output_file );
		                      // Movemos el fichero a la carpeta files

		                      if(!move_uploaded_file($tmp_name, $image_path)){
		                          $this->Session->setFlash(__('Error al subir la imagen.'), 'flash_message', array('type' => 'danger'));
		                          $action = array('controller'=>'CompanyProject','action' => 'add');
		                          $dsCompanyProject->rollback();
		                          $this->redirect($action);
		                      } 
		                  } else {
		                      $this->Session->setFlash(__('Extensión no aceptada'), 'flash_message', array('type' => 'danger'));
		                      $action = array('controller'=>'CompanyProject','action' => 'add');
		                      $$dsCompanyProject->rollback();
		                      $this->redirect($action);

		                  }
		                  //Comprobamos que el tamaño de la imagen sea correcta
		                  $image_size = getimagesize($image_path);
		                  $image_dif  = $image_size[0]/$image_size[1];

		                  //Recogemos los valores validos de tamaño, sera un resultado parecido a la division de ancho i¡y alto
		                  // $ancho = 100;
		                  // $alto  = 100;

		                  // $size = $ancho/$alto;
		                  $dataImageSave = array();
						  $dataImageSave['CompanyProjectsImage']['name'] = $name;
						  $dataImageSave['CompanyProjectsImage']['company_project_id'] = $id;

						  $dataImageSave['CompanyProjectsImage']['default'] =  ($kindImage == 0) ? 1 : 0;
						  try {
						  	if ($kindImage == 0) {
								$this->CompanyProjectsImage->deleteAll(array('company_project_id' => $id,'default' => 1));
						  	} else {
						  		$this->CompanyProjectsImage->recursive = -1;
						  		$imagesBbdd = $this->CompanyProjectsImage->find('all',array('conditions' => array('company_project_id' => $id,'default' => 0)));
						  		foreach($imagesBbdd as $k => $imageBbdd) {
						  			if ($k == $kindImage-1) {
						  				$this->CompanyProjectsImage->delete($imageBbdd['CompanyProjectsImage']['id']);
						  			}
						  		}
						  	}
						  	$this->CompanyProjectsImage->create();
						  	$this->CompanyProjectsImage->save($dataImageSave);
						  } catch (Exception $e) {
								debug($e).die;
					      }
						  $k++;
		              }
      			}
      			$this->CompanyProjectsUser->deleteAll(array('company_project_id' => $id,'user_id' =>$user['User']['id']));
      			if ($hasParticiped != 'N') {
      				$saveCompanyProjectusers['CompanyProjectsUser']['user_id'] = $user['User']['id'];
      				$saveCompanyProjectusers['CompanyProjectsUser']['company_project_id'] = $id;
					$this->CompanyProjectsUser->create();
      				$this->CompanyProjectsUser->save($saveCompanyProjectusers);
      			}
				$this->User->recursive = -1;
        		$this->Session->write('UserRegistered',$this->User->findByid($user['User']['id']));
				$this->Session->setFlash(__('Proyecto guardado correctamente'), 'flash_message', array('type' => 'success'));
				return $this->redirect(array('controller' => 'Companies','action' => 'ver_cv'));
			} else {
				$this->Session->setFlash(__('Hubo problemas al intentar guardar el proyecto, inténtelo de nuevo, gracias'), 'flash_message', array('type' => 'success'));
				return $this->redirect(array('controller' => 'UserProjects','action' => 'add'));
			}			
		}
		$budgetCurrencies = $this->Currency->find('list',array('conditions' => array('conversion_to_euro > ' => 0,'conversion_to_euro != ' => null)));
		$billingCurrencies = $this->Currency->find('list',array('conditions' => array('conversion_to_euro > ' => 0,'conversion_to_euro !=' => NULL)));
		
		$countries = $this->getStatic('Country',false);
		$sectorProjects = $this->Session->read('Static.Sectors');
		$this->City->recursive = -1;
		$cities = $this->City->find('all',array('fields' => array('City.id','City.name','City.country_id')));
		$cities = Hash::extract($cities,'{n}.City');
		$arrCity = array();
		foreach ($cities as $cit) {
			$arrCity[$cit['id']]['name'] = $cit['name'];
			$arrCity[$cit['id']]['country_id'] = $cit['country_id']; 
		}
		$cities = $arrCity;
		$functionalities = $this->Session->read('Static.Functionalities');
		$sectorServices  = $this->Session->read('Static.Sectors');
		$departments     = $this->Session->read('Static.Departments');
		$companies       = $this->Session->read('Static.Companies');
		$customers       = $companies;

		$companiesText = '';
		foreach($companies as $company) {
			$companiesText .= '"'.$company.'",';
		}
		//para jscript
		$companiesText = substr($companiesText,0,strlen($companiesText)-1);
		$this->set('companiesText',$companiesText);
		$departmentsText = '';
		foreach($departments as $department) {
			$departmentsText .= '"'.$department.'",';
		}
		$departmentsText = substr($departmentsText,0,strlen($departmentsText)-1);
		$this->set('departmentsText',$departmentsText);
		$citiesText = '';
		foreach($cities as $city) {
			$citiesText .= '"'.$city['name'].' ('.$countries[$city['country_id']].')",';
		}
		$citiesText = substr($citiesText,0,strlen($citiesText)-1);
		$this->set('citiesText',$citiesText);
		$sectorProjectText = '';
		foreach($sectorProjects as $sectorProject) {
			$sectorProjectText .= '"'.$sectorProject.'",';
		}
		$sectorProjectText = substr($sectorProjectText,0,strlen($sectorProjectText)-1);
		$this->set('sectorProjectText',$sectorProjectText);

		$functionalitiesText = '';
		foreach($functionalities as $functionality) {
			$functionalitiesText .= '"'.$functionality.'",';
		}
		$functionalitiesText = substr($functionalitiesText,0,strlen($functionalitiesText)-1);
		$this->set('functionalitiesText',$functionalitiesText);

		$this->set(compact('budgetCurrencies', 'billingCurrencies', 'countries', 'sectorProjects', 'cities', 'functionalities', 'sectorServices', 'departments', 'companies', 'customers'));
	}

	private function _getPossibleResponsables($user) {
		$conditions = array('fields' => array('firstname','lastname','id'),'conditions' => array('company_id' => $user['User']['company_id']));
		$this->User->recursive = -1;
		$contactresponsables = $this->User->find('all',$conditions);
		$contactresponsables = Hash::extract($contactresponsables,'{n}.User');
		$contactResponsableList = array();
		foreach ($contactresponsables as $contactresponsable) {
			$contactResponsableList[$contactresponsable['id']] = $contactresponsable['firstname'].' '.$contactresponsable['lastname'];
		}
		$contactResponsableList[0]  = __('Escoja la persona responsable del proyecto');
		return $contactResponsableList;
	}
/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CompanyProject->id = $id;
		if (!$this->CompanyProject->exists()) {
			throw new NotFoundException(__('Invalid company project'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CompanyProject->delete()) {
			$this->Session->setFlash(__('The company project has been deleted.'));
		} else {
			$this->Session->setFlash(__('The company project could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CompanyProject->recursive = 0;
		$this->set('companyProjects', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CompanyProject->exists($id)) {
			throw new NotFoundException(__('Invalid company project'));
		}
		$options = array('conditions' => array('CompanyProject.' . $this->CompanyProject->primaryKey => $id));
		$this->set('companyProject', $this->CompanyProject->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CompanyProject->create();
			if ($this->CompanyProject->save($this->request->data)) {
				$this->Session->setFlash(__('The company project has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The company project could not be saved. Please, try again.'));
			}
		}
		$businessLines = $this->CompanyProject->BusinessLine->find('list');
		$companies = $this->CompanyProject->Company->find('list');
		$geolocalitzations = $this->CompanyProject->Geolocalitzation->find('list');
		$countries = $this->CompanyProject->Country->find('list');
		$cities = $this->CompanyProject->City->find('list');
		$currencies = $this->CompanyProject->Currency->find('list');
		$departments = $this->CompanyProject->Department->find('list');
		$users = $this->CompanyProject->User->find('list');
		$this->set(compact('businessLines', 'companies', 'geolocalitzations', 'countries', 'cities', 'currencies', 'departments', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CompanyProject->exists($id)) {
			throw new NotFoundException(__('Invalid company project'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CompanyProject->save($this->request->data)) {
				$this->Session->setFlash(__('The company project has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The company project could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CompanyProject.' . $this->CompanyProject->primaryKey => $id));
			$this->request->data = $this->CompanyProject->find('first', $options);
		}
		$businessLines = $this->CompanyProject->BusinessLine->find('list');
		$companies = $this->CompanyProject->Company->find('list');
		$geolocalitzations = $this->CompanyProject->Geolocalitzation->find('list');
		$countries = $this->CompanyProject->Country->find('list');
		$cities = $this->CompanyProject->City->find('list');
		$currencies = $this->CompanyProject->Currency->find('list');
		$departments = $this->CompanyProject->Department->find('list');
		$users = $this->CompanyProject->User->find('list');
		$this->set(compact('businessLines', 'companies', 'geolocalitzations', 'countries', 'cities', 'currencies', 'departments', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CompanyProject->id = $id;
		if (!$this->CompanyProject->exists()) {
			throw new NotFoundException(__('Invalid company project'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CompanyProject->delete()) {
			$this->Session->setFlash(__('The company project has been deleted.'));
		} else {
			$this->Session->setFlash(__('The company project could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
