<?php
App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
/**
 * Companies Controller
 *
 * @property Company $Company
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CompaniesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session','Image');
	public $uses = array('Geolocalitzation','Continents','Company','CompanyProject','CompanyProjectsSector','CompanyProjectsUser','Country','Currency','Office','Sector','User','City');
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Company->recursive = 0;
		$this->set('companies', $this->Paginator->paginate());
	}
/**
 * index method
 *
 * @return void
 */
	public function proyectos_pais($idCountry = null, $filter = null) {
		$user = $this->Session->read('UserRegistered');
		$countryName = $this->Country->findById($idCountry);
		$this->CompanyProject->recursive = 1;
		$conditions = array('CompanyProject.company_id' => $user['User']['company_id'],'CompanyProject.country_id' => $idCountry);
		switch ($filter) {
			case 'ongoing':
				$conditions['CompanyProject.on_going'] = true;
				break;
			case 'finished':
				$conditions['CompanyProject.on_going'] = 0;
				break;
		}
		

        $projects = $this->CompanyProject->find('all',array('conditions' => $conditions));
		$projectsIds = array();
		foreach ($projects as $project) {
			$projectsIds[] = $project['CompanyProject']['id'];
		}
		if (count($projectsIds) > 1) {
			$companyProjectsUsers = $this->CompanyProjectsUser->find('all',array('conditions' => array('CompanyProjectsUser.user_id' => $user['User']['id'],'CompanyProjectsUser.company_project_id IN' => $projectsIds)));	
		} else {
			$companyProjectsUsers = $this->CompanyProjectsUser->find('all',array('conditions' => array('CompanyProjectsUser.user_id' => $user['User']['id'],'CompanyProjectsUser.company_project_id' => $projectsIds)));	
		}
		$this->set('companyProjectUser',$companyProjectsUsers);
		if (empty($projects)) {
			throw new NotFoundException(__('Éste usuario no tiene proyectos en éste país o bien ha seleccionado unos filtros en los que no aparece ningún proyecto'));
		}
		$this->CompanyProjectsSector->recursive = -1;
		foreach ($projects as $project) {
			$sectorsProject[] = $project['CompanyProject']['id'];
		}
		
		if (count($sectorsProject) == 1) {
			$sectorsProjects = $this->CompanyProjectsSector->find('all',array('conditions' => array('company_project_id' => $sectorsProject)));	
		} else {
			$sectorsProjects = $this->CompanyProjectsSector->find('all',array('conditions' => array('company_project_id IN' => $sectorsProject)));
		}
		
		$sectorsProjects = Hash::extract($sectorsProjects,'{n}.CompanyProjectsSector');
		$sectorsShow = array();
		foreach ($sectorsProjects as $sectorsProject) {
			$this->Sector->recursive = -1;
			$sector = $this->Sector->findById($sectorsProject['sector_id']);
			$sector = Hash::extract($sector,'Sector');
			if (!in_array($sector,$sectorsShow)) {
				$sectorsShow[] = $sector;	
			}
		}
		$this->set('sectorsShow',$sectorsShow);
		$this->set('breadcrumb', array(
            __('Dar a conocer empresa') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => true, 'url' => '/Companies/ver_cv'),
            __('Proyectos en %s',$countryName['Country']['name']) => array('active' => false, 'url' => '#'),
        ));
		$this->set('countryName',$countryName);
        $user = $this->Session->read('UserRegistered');
        //$projects = Hash::extract($projects,'{n}.CompanyProject');
        $this->set('projects',$projects);
		$this->Company->recursive = -1;
		$this->set('companies', $this->Paginator->paginate());
	}
/**
 * index method
 *
 * @return void
 */
	public function ver_cv() {
		$this->set('breadcrumb', array(
            __('Dar a conocer empresa') => array('active' => false, 'url' => '#'),
            __('Ver CV') => array('active' => false, 'url' => '#'),
        ));

        $user = $this->Session->read('UserRegistered');
        $this->CompanyProject->recursive = 1;
        $projects = $this->CompanyProject->find('all',array('conditions' => array('CompanyProject.company_id' => $user['User']['company_id'],'CompanyProject.company_id' => $user['User']['company_id'])));
        if (!empty($projects)) {
        	$projects = Hash::extract($projects,'{n}.CompanyProject');	
        }
        

        $countryCounts = $continentsProjects = $incompleteProjects = array();
        $translateNameSpanish = array('North America' => 'América del Norte','South America' => 'América del Sur','Europe' => 'Europa','Africa' => 'África','Asia' => 'Ásia','Oceania' => 'Oceanía');
        
        foreach ($projects as $project) {
        	if (!in_array($project['country_id'],$countryCounts)) {
        		$countryCounts[] = $project['country_id'];
        		$continent = $this->Country->recursive = 1;
        		$continent = $this->Country->findById($project['country_id']);
        		$continentsProjects[$continent['Continent']['id']]['name'] = $translateNameSpanish[$continent['Continent']['name']];
        		$continentsProjects[$continent['Continent']['id']]['countries'][$continent['Country']['id']] = $continent['Country']['name'];
        	}
        	$percentCompleteProject = $this->porcentajeProyecto($project);
        	if ($percentCompleteProject != 100) {
        		$incompleteProjects[] = $project;
        	}
        }
		
        $this->set('continents',$continentsProjects);
        
        $this->CompanyProject->recursive = -1;
        $company = $this->CompanyProject->find('all',array('conditions' => array('CompanyProject.company_id' => $user['User']['company_id']),'fields' => array('id')));
        $companyProjectIds = Hash::extract($company,'{n}.CompanyProject.id');

        $this->CompanyProjectsSector->recursive = -1;
        $sectors = array();
        if (!empty($companyProjectIds)) {
        	$sectorSearch = (count($companyProjectIds) == 1) ? 'id' : 'id IN';
	        $sectors = $this->CompanyProjectsSector->find('all',array('conditions' => array($sectorSearch => $companyProjectIds)));
	        $sectors = Hash::extract($sectors,'{n}.CompanyProjectsSector');
        }
        
        $arrSector = array();
        foreach($sectors as $sector) {
        	if (!in_array($sector['sector_id'],$arrSector)) {
        		$arrSector[] = $sector['sector_id'];
        	}
        }
        if (!empty($arrSector)) {
        	$this->Sector->recursive = -1;
        	if (count($arrSector) == 1) {
				$sectorsShow = $this->Sector->find('all',array('conditions' => array('Sector.id'=>$arrSector[0])));
        	} else {
        		$sectorsShow = $this->Sector->find('all',array('conditions' => array('Sector.id IN '=>$arrSector)));	
        	}
	        
	        $sectorsShow = Hash::extract($sectorsShow,'{n}.Sector');
        } else {
        	$sectorsShow = array();
        }
        $this->set('sectorsShow',$sectorsShow);
        
        $this->User->recursive = -1;
        $percentComplete = $this->porcentajeEmpresa($user['User']['company_id']);
        $this->CompanyProject->recursive = -1;
        $companyProjects = $this->CompanyProject->find('all',array('conditions' => array('CompanyProject.company_id' => $user['User']['company_id']),'fields'=>array('id')));
        $companyProjects = Hash::extract($companyProjects,'{n}.CompanyProject.id');
        $usersCompany = array();
        if (!empty($companyProjects)) {
        	if (count($companyProjects) == 1) {
        		$companyProjectsUser   = $this->CompanyProjectsUser->find('all',array('conditions' => array('company_project_id' => $companyProjects),'fields' => array('DISTINCT user_id')));
        	} else {
        		$companyProjectsUser   = $this->CompanyProjectsUser->find('all',array('conditions' => array('company_project_id IN' => $companyProjects),'fields' => array('DISTINCT user_id')));
        	}
			if (!empty($companyProjectsUser)) {
				$companyProjectsUsers  = Hash::extract($companyProjectsUser,'{n}.CompanyProjectsUser.user_id');
		        $this->User->recursive = -1;
		        $conditions = (count($companyProjectsUsers) > 1) ? array('id IN' => $companyProjectsUsers) : array('id' => $companyProjectsUsers);
		        $usersCompany = $this->User->find('all',array(
		    		'conditions' => $conditions,
		    		'fields' => array('id')
		    		)
		    	);
		    	$usersCompany = Hash::extract($usersCompany,'{n}.User');

			}
			foreach ($usersCompany as $k => $userCompany) {
				$idUser = $userCompany['id'];
				$userPersonalProjects  = $this->UsersUserProject->find('all',array('conditions' => array('user_id' => $idUser)));
				$userPersonalProjects  = Hash::extract($userPersonalProjects,'{n}.UsersUserProject.user_project_id');
				foreach ($userPersonalProjects as $personalproject) {
					$usersCompany[$k]['personalprojects'][] = $personalproject;
				}
				if (empty($usersCompany[$k]['personalprojects'])) {
					$conditions = array('conditions' => array('id IN' => $companyProjects));
					$personalprojects = $this->CompanyProject->find('all',$conditions);
					$personalprojects = Hash::extract($personalprojects,'{n}.CompanyProject.id');
					$usersCompany[$k]['companyprojects'] = $personalprojects;
				}
			}
        } else {
        	
        }
        $numempleados = $this->User->find('count',array('conditions' => array('company_id' => $user['User']['company_id'])));
        $offices = $this->Office->find('all',array('conditions' => array('company_id' => $user['User']['company_id'])));
        $this->set('numempleados',$numempleados);
        $this->set('companyProjects',$companyProjects);
        $this->set('usersCompany',$usersCompany);
        $this->set('usersInCompany',count($usersCompany));
        $this->set('percentComplete',$percentComplete);
        $this->set('incompleteProjects',count($incompleteProjects));
        $this->set('numCountries',count($countryCounts));
        $this->set('numProjects',count($projects));
        $this->set('offices',$offices);
        $this->set('dataUser',$user);
        $this->Company->recursive = -1;
        $companyInfo = $this->Company->find('first',array('conditions' => array('Company.id' => $user['User']['company_id'])));
        $this->set('dataCompany',$companyInfo);
        //$this->Company->find('first',array('conditions' => ))
        
        $userReviewer = $this->Company->findById($user['User']['company_id']);
        $userReviewer = Hash::extract($userReviewer,'Company.last_reviewer_id');

        $this->set('last_reviewer',$userReviewer[0]);
		$this->Company->recursive = -1;
		$this->set('companies', $this->Paginator->paginate());
	}
	public function projectmap() {
		// if (100 no esta completat) {
		// 	$this->Session->setFlash(__('El informe se ha eliminado correctamente'), 'flash_message', array('type' => 'info'));	
		// 	$this->element('tarjeta_bc')
		// }
				// if (100 no esta completat) {
		// 	$this->Session->setFlash(__('El informe se ha eliminado correctamente'), 'flash_message', array('type' => 'info'));	
		// 	$this->element('tarjeta_bc')
		// }
		$user = $this->Session->read('UserRegistered');
		$user = $user['User'];
		$this->CompanyProject->recursive = 0;
		$cProjects = $this->CompanyProject->find('all',array('conditions' => array('CompanyProject.company_id' => $user['company_id'])));
		$countriesNames = $this->Session->read('Static.Countries');
		$this->set('countriesNames',$countriesNames);
		$this->set('cprojects',$cProjects);
		$this->set('breadcrumb', array(
        ));
		//$this->User->recursive = 0;
	}
	public function officemap() {
		// if (100 no esta completat) {
		// 	$this->Session->setFlash(__('El informe se ha eliminado correctamente'), 'flash_message', array('type' => 'info'));	
		// 	$this->element('tarjeta_bc')
		// }
		$user = $this->Session->read('UserRegistered');
		$user = $user['User'];
		$userCompanyId = $user['company_id'];
		$offices  = $this->Office->find('all',array('conditions' => array('company_id' => $userCompanyId)));
		$officeShow = array();
		foreach ($offices as $office) {
			$officeShow['id']           = $office['Office']['id'];
			$officeShow['company_name'] = strtoupper($office['Company']['name']);
			$officeShow['latitude']     = $office['Geolocalitzation']['latitude'];
			$officeShow['longitude']    = $office['Geolocalitzation']['longitude'];
			$officeShow['name']         = $office['Office']['name'];
			$officeShow['phone']         = $office['Office']['phone'];
			$officeShow['email']         = $office['Office']['email'];
			$officeShow['description']  = $office['Office']['description'];
			$officeShow['principal']  = $office['Office']['principal'];
			$officeShow['address']      = $office['Office']['address'];
			$officesShow[] = $officeShow;
		}
		$this->set('offices',$officesShow);
		$this->set('breadcrumb', array(
        ));
		//$this->User->recursive = 0;
	}
	private function _setUrlDependence($user) {
	  if ($user['User']['card_completed'] == 0) {
        $this->Session->setFlash(__('Antes de poder realizar cualquier acción debe rellenar su tarjeta de visita'), 'flash_message', array('type' => 'info')); 
        $this->redirect($this->Auth->redirect());
      } else if ($user['User']['formation_completed'] == 0) {
        $this->Session->setFlash(__('Para poder rellenar proyectos debe rellenar su formación'), 'flash_message', array('type' => 'info')); 
        $this->redirect($this->Auth->redirect());
      }
      
	}
	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->Session->check('UserRegistered')) {
			$user = $this->Session->read('UserRegistered');
			$this->_setUrlDependence($user);
		}
	}
	public function cvmap() {
		$this->set('breadcrumb', array(
        ));
		//$this->User->recursive = 0;
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Invalid company'));
		}
		$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
		$this->set('company', $this->Company->find('first', $options));
	}
	private function porcentajeEmpresa($company_id)
	  {
	  	
	    /* EMPRESA
	    4 camps
	    address  25%
	    description     25%
	    logo 25%
	    differences   25%
	    */

	    $hasOffice = $this->Office->find('all',array('conditions' => array('company_id' => $company_id)));
	    $this->Company->recursive = -1;
	    $company = $this->Company->find('first',array('conditions' => array('id'=>$company_id)));
	    $company = Hash::extract($company,'Company');	
	    $percent = 0;

	    $percent += (!empty($company['name'])) ? 20 : 0;
	    $percent += (!empty($company['address'])) ? 20 : 0;
	    $percent += (!empty($company['description'])) ? 20 : 0;
	    $percent += (!empty($company['logo'])) ? 20 : 0;
	    $percent += (!empty($company['differences'])) ? 15 : 0;
	    $percent += (!empty($hasOffice)) ? 5 : 0;
	    return $percent;
	  }
	  private function porcentajeProyecto($project)
	  {
	  	
	  	
	    /* PROYECTO
	    4 camps
	    customer_id 
	    volumes   
	    sector_id  
	    functionality_id   
	    department_id
	    duration_year month day hours
	    geolocalitzation_id
	    billing
	    budget
	    description
	    contact_responsable_id
	    last_reviewer_id
	    */
	    $time_service = $project['duration_year']+$project['duration_month']+$project['duration_day']+$project['duration_hours'];
	    $path = WWW_ROOT.'files'.DS.'companies'.DS.'company_'.$project['company_id'];
		$folder = new Folder($path, true, 0777);

		$hasImages = (!is_null($folder->path)) ? true : false;
		
	    $percent = 0;
	    $percent += (!empty($project['customer_id'])) ? 10 : 0;
	    $percent += (!empty($project['volumes'])) ? 10 : 0;
	    $percent += (!empty($project['sector_id'])) ? 10 : 0;
	    $percent += (!empty($project['functionality_id'])) ? 10 : 0;
	    $percent += (!empty($project['department_id'])) ? 10 : 0;
	    $percent += (!empty($time_service)) ? 10 : 0;
	    $percent += (!empty($hasImages)) ? 10 : 0;
	    $percent += (!empty($project['geolocalitzation_id'])) ? 5 : 0;
	    $percent += (!empty($project['billing'])) ? 5 : 0;
	    $percent += (!empty($project['budget'])) ? 5 : 0;
	    $percent += (!empty($project['description'])) ? 5 : 0;
	    $percent += (!empty($project['contact_responsable_id'])) ? 5 : 0;
	    $percent += (!empty($project['last_reviewer_id'])) ? 5 : 0;
	    return $percent;
	  }
/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Company->create();
			if ($this->Company->save($this->request->data)) {
				$this->Session->setFlash(__('The company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The company could not be saved. Please, try again.'));
			}
		}
		$sectors = $this->Company->Sector->find('list');
		$this->set(compact('sectors'));
	}


  /**
 * resumen_cv method
 *
 * @throws NotFoundException
 * @return void
 */
  public function resumen_cv()
  {
      $this->set('breadcrumb', array(
            __('Dar a conocer empresa') => array('active' => false, 'url' => '#'),
            __('Resumen CV') => array('active' => false, 'url' => '#'),
        ));

      $user = $this->Session->read('UserRegistered');
      $this->set('user',$user);

      $this->Sector->recursive = -1;
      $sectorNames = $this->Sector->find('list');
      $this->Currency->recursive = -1;
      $currentCurrency = $this->Currency->find('all',array('fields' => array('id','conversion_to_euro')));
      $currentCurrency = Hash::extract($currentCurrency,'{n}.Currency');
      $currenciesText = array();
      foreach ($currentCurrency as $current) {
        $currenciesText[$current['id']] = $current['conversion_to_euro'];
      }
      $companyString = $this->Session->read('Static.Companies');
      $this->CompanyProject->recursive = 1;
      $projects = $this->CompanyProject->find('all',array('conditions' => array('CompanyProject.company_id' => $user['User']['company_id'])));
      //$projects = Hash::extract($projects,'{n}.');

      $this->CompanyProjectsUser->recursive = 1;
		// if (count($projects) ==1) {
		// 	$projects = $this->CompanyProjectsUser->find('all',array('conditions' => array('CompanyProjectsUser.company_project_id' => $projects,'CompanyProjectsUser.user_id' => $user['User']['id'])));
		// } else {
		// 	$projects = $this->CompanyProjectsUser->find('all',array('conditions' => array('CompanyProjectsUser.company_project_id IN' => $projects,'CompanyProjectsUser.user_id' => $user['User']['id'])));
		// }

        $customerIds =$companyNames = $projectsNames = $countryProjectIds = $customerNames = $sectorsProject = $sectorsProjectNames = array();
        $sectorServiceId = $functionalitiesNames = $departmentNames = array();
        $projectsSumBudget = 0;
        $counter = $counter2 = $durationYears = $durationMonths = $durationDays = $durationHours = 0;

        foreach ($projects as $project) {
          if (!in_array($companyString[$project['CompanyProject']['company_id']],$companyNames)) {
            $companyNames[] = $companyString[$project['CompanyProject']['company_id']];
          }
          if (!in_array($project['CompanyProject']['id'],$projectsNames)) {
            $projectsNames[] = $project['CompanyProject']['name'];
          }
          if (!in_array($project['CompanyProject']['country_id'],$countryProjectIds)) {
            $countryProjectIds[] = $project['CompanyProject']['country_id'];
          }

          if (!in_array($project['CompanyProject']['customer_id'],$customerIds)) {
            $customerIds[]   = $project['CompanyProject']['customer_id'];
            $customerNames[] = $companyString[$project['CompanyProject']['customer_id']];
          }
          if (!empty($project['CompanyProject']['sector_id'])) {
				if (!in_array($project['CompanyProject']['sector_id'],$sectorsProject)) {
		            $sectorsProject[] = $project['CompanyProject']['sector_id'];
		            $name = $this->Sector->findByName($sectorNames[$project['CompanyProject']['sector_id']]);
		            $name = Hash::extract($name,'Sector');
		            $sectorsProjectNames[] = $name;
	          	}
          }
          
          if (!in_array($project['CompanyProject']['sector_service_id'],$sectorServiceId)) {
            if (!empty($project['CompanyProject']['sector_service_id'])) {
              $sectorServiceId[] = $project['CompanyProject']['sector_service_id'];  
            }
            
          }
          if (!empty($project['CompanyProject']['Functionality'])) {
            if (!in_array($project['CompanyProject']['Functionality']['name'],$functionalitiesNames)) {
              $functionalitiesNames[] = $project['CompanyProject']['Functionality']['name'];
            }
          }
          if (!empty($project['CompanyProject']['Department'])) {
            if (!in_array($project['CompanyProject']['Department']['name'],$departmentNames)) {
              $departmentNames[] = $project['CompanyProject']['Department']['name'];
            }
          }
          $durationYears  += $project['CompanyProject']['duration_year'];
          $durationMonths += $project['CompanyProject']['duration_month'];
          $durationDays   += $project['CompanyProject']['duration_day'];
          $durationHours  += $project['CompanyProject']['duration_hours'];
          $budget       = (float) str_replace(array('.',','),array('','.'),$project['CompanyProject']['budget']);
          $realCurrency = (float) $currenciesText[$project['CompanyProject']['budget_currency_id']]; 
          $projectsSumBudget += $budget*$realCurrency;
        }
        $duration = '';
        $duration .= ($durationYears > 0) ? $durationYears.' '.__('años') : '';
        $duration .= ($durationMonths > 0) ? ' '.$durationMonths.' '.__('meses') : '';
        $duration .= ($durationDays > 0) ? ' '.$durationDays.' '.__('días') : '';
        $duration .= ($durationHours > 0) ? ' '.$durationHours.' '.__('horas') : '';
		
        $this->set('duration',$duration);
        $this->set('sectorServices',$sectorServiceId);
        $this->set('departmentNames',$departmentNames);
        $this->set('functionalitiesNames',$functionalitiesNames);
        $this->set('companyNames',$companyNames);
        $this->set('projectsNames',$projectsNames);
        $this->set('countryProjectIds',$countryProjectIds);
        $this->set('customerNames',$customerNames);
        $this->set('sectorsProjectNames',$sectorsProjectNames);
        $this->set('projectsSumBudget',$projectsSumBudget);

  }
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null,$onlyDifferences = false) {
		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Ésta compañía no existe, por favor contacte con el administrador'));
		}
		$user = $this->Session->read('UserRegistered');
		if ($user['User']['company_id'] != $id) {
			throw new NotFoundException(__('No tiene permisos para editar ésta compañía, por favor, contacte con el administrador'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$onlyDifferences = $this->request->data['Company']['onlyDifferences'];
		}
		$this->set('onlyDifferences',$onlyDifferences);
		if (!$onlyDifferences) {
			$companyUsers = array('0' => __('Seleccione persona responsable de contacto de la compañía'),'1' => 'Eloi','2' => 'Enrique');
			$this->set('companyUsers',$companyUsers);
			if ($this->request->is(array('post', 'put'))) {
				//guardamos geolocalizacion
				//$saveGeoLocalitzacion['longitude'] = $this->request->data['Company']['longitude'];
				//$saveGeoLocalitzacion['latitude']  = $this->request->data['Company']['latitude'];
				//unset($this->request->data['Company']['longitude']);
				//unset($this->request->data['Company']['latitude']);
				//$this->Geolocalitzation->recursive = -1;
				//$geolocalitzationId= $this->Geolocalitzation->find('first',array(
				//	'conditions' => array(
				//		'longitude' => $saveGeoLocalitzacion['longitude'],
				//		'latitude' => $saveGeoLocalitzacion['latitude'],
				//		)
				//	)
				//);
				/*if (!empty($geolocalitzationId)) {
					$geolocalitzationId = $geolocalitzationId['Geolocalitzation']['id'];
				} else {
					if ($this->Geolocalitzation->save($saveGeoLocalitzacion)) {
						 $geolocalitzationId = $this->Geolocalitzation->getLastInsertID();
					}	
				}*/
				$image = $this->request->data['Company']['image1'];

				if ($image['error'] == UPLOAD_ERR_OK) {
					$allowedExtensions = array('jpeg', 'jpg', 'gif', 'png');// Extensiones permitidas
					$uploads_dir = WWW_ROOT.'files/companies/company_'.$this->request->data['Company']['id'].'/'; // Carpeta destino
					$folder = new Folder($uploads_dir, true, 0777);
					$tmp_name = $image["tmp_name"]; // Ruta al fichero temporal
					
					$temp_file = new File($tmp_name); // Descriptor de fichero para hacer las comprobaciones
					$mime = explode('/',$temp_file->mime());
					
					// Comprobamos que la extensión esté permitida
					if(in_array(strtolower($mime[1]), $allowedExtensions)){
					  $temp_file->close();
					  // DESTINO FINAL de la image sanitizando el nombre
					  	$name       = str_replace(' ','_',Inflector::humanize(Inflector::underscore($temp_file->name())));
					    $image_path = $folder->path.$name;
						$input_file = $tmp_name;
						$output_file = $image_path;
						$this->Image->resize_image( $input_file , 300 , 250 , $output_file );
					  // Movemos el fichero a la carpeta files
					  if(!move_uploaded_file($tmp_name, $image_path)){
					      $this->Session->setFlash(__('Error al subir la imagen.'), 'flash_message', array('type' => 'danger'));
					      $action = array('controller'=>'Companies','action' => 'ver_cv');
					      $this->redirect($action);
					  }
					} else {
					  $this->Session->setFlash(__('Extensión no aceptada'), 'flash_message', array('type' => 'danger'));
					  $action = array('controller'=>'Companies','action' => 'ver_cv');
					  $this->redirect($action);

					}
					//Comprobamos que el tamaño de la imagen sea correcta
					$image_size = getimagesize($image_path);
					$image_dif  = $image_size[0]/$image_size[1];

					//Recogemos los valores validos de tamaño, sera un resultado parecido a la division de ancho i¡y alto
					// $ancho = 100;
					// $alto  = 100;

					// $size = $ancho/$alto;
					$this->request->data['Company']['logo'] = $name;
					$this->request->data['Company']['logo_dir'] = '/files/companies/company_'.$this->request->data['Company']['id'].'/';
				}

				unset($this->request->data['Company']['image1']);
				//miramos si tenemos la ciudad sino la guardamos
				if (!empty($this->request->data['Company']['cit_id'])) {
					$cityId = $this->request->data['Company']['cit_id'];
				} else {
					$this->City->recursive = -1;
					$cityId = $this->City->findByName($this->request->data['Company']['cit']);
					if (!empty($cityId)) {
						$cityId = $cityId['City']['id'];
					} else {
						$this->City->create();
						$saveCity['City']['name'] = str_replace('\'','`',$this->request->data['Company']['cit']);
						$saveCity['City']['country_id'] = (int) $this->request->data['Company']['country_id'];
						if ($this->City->save($saveCity)) {
							$cityId = $this->City->getLastInsertID();
						}
					}
				}

				$this->request->data['Company']['city_id'] = $cityId;
				unset($this->request->data['Company']['cit_id']);
				unset($this->request->data['Company']['cit']);
				
				$this->request->data['Office']['address']             = $this->request->data['Company']['complet_address'];
				if (!empty($geolocalitzationId)) {
					$this->request->data['Office']['geolocalitzation_id'] = $geolocalitzationId;	
				}
				$this->request->data['Office']['company_id']          = $this->request->data['Company']['id'];
				$this->request->data['Office']['principal']           = 1;
				$this->request->data['Office']['country_id']          = $this->request->data['Company']['country_id'];
				$this->request->data['Office']['city_id']             = $this->request->data['Company']['city_id'];
				if (empty($this->request->data['Office']['id'])) {
					$this->request->data['Office']['name']                = __('Sede');
					$this->request->data['Office']['description']         = __('Sede principal de %s',$this->request->data['Company']['name']);	
				}
				$this->request->data['Company']['last_reviewer_id']   = $user['User']['id'];
				$this->request->data['Company']['address']            = $this->request->data['Company']['complet_address'];
				$this->Office->save($this->request->data['Office']);
				
				if ($this->Company->save($this->request->data)) {
					$this->Session->setFlash(__('Compañía editada correctamente'), 'flash_message', array('type' => 'success'));
				} else {
					$this->Session->setFlash(__('No se pudo editar la compañía, por favor contacte con el administrador'), 'flash_message', array('type' => 'success'));
				}
				return $this->redirect(array('action' => 'ver_cv'));
			} else {
				$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
				$options2 = array('conditions' => array('Office.principal' => 1,'Office.company_id' => $id));
				$this->Company->recursive = -1;
				$this->request->data = $this->Company->find('first', $options);
				$this->request->data['Office'] = $this->Office->find('first', $options2);
			}
			$this->set('breadcrumb', array(
	            __('Dar a conocer empresa') => array('active' => false, 'url' => '#'),
	            __('Ver CV %s',$this->request->data['Company']['name']) => array('active' => true, 'url' => '/Companies/ver_cv'),
	            __('Editar %s',$this->request->data['Company']['name']) => array('active' => false, 'url' => '#'),
	        ));
			$this->User->recursive = -1;
			$usersList = $this->User->find('all',array('conditions' => array('User.company_id' => $user['User']['company_id'])));
			$usersList = Hash::extract($usersList,'{n}.User.id');
			$countryList = $this->Country->find('list');
			$userNamesStatic = $this->Session->read('Static.Users');
			$userNames[0] = __('Añada a una persona responsable de contacto');
			foreach ($usersList as $userId) {
				$userNames[] = $userNamesStatic[$userId];
			}
			$this->set('userNames',$userNames);
			$this->set('countryList',$countryList);
			$this->set('data',$this->request->data);
		} else {
			if ($this->request->data) {
				unset($this->request->data['Company']['onlyDifferences']);
				$this->request->data['Company']['last_reviewer_id'] = $user['User']['id'];
				$this->Session->setFlash(__('Diferencias de compañia editadas correctamente'), 'flash_message', array('type' => 'success'));
				$this->Company->save($this->request->data, false);
				return $this->redirect(array('action' => 'ver_cv'));
			} else {
				$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
				$this->Company->recursive = -1;
				$this->request->data = $this->Company->find('first', $options);
				$this->set('data',$this->request->data);
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Company->id = $id;
		if (!$this->Company->exists()) {
			throw new NotFoundException(__('Invalid company'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Company->delete()) {
			$this->Session->setFlash(__('The company has been deleted.'));
		} else {
			$this->Session->setFlash(__('The company could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Company->recursive = 0;
		$this->set('companies', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Invalid company'));
		}
		$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
		$this->set('company', $this->Company->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Company->create();
			if ($this->Company->save($this->request->data)) {
				$this->Session->setFlash(__('The company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The company could not be saved. Please, try again.'));
			}
		}
		$sectors = $this->Company->Sector->find('list');
		$this->set(compact('sectors'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Company->exists($id)) {
			throw new NotFoundException(__('Invalid company'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Company->save($this->request->data)) {
				$this->Session->setFlash(__('The company has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The company could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Company.' . $this->Company->primaryKey => $id));
			$this->request->data = $this->Company->find('first', $options);
		}
		$sectors = $this->Company->Sector->find('list');
		$this->set(compact('sectors'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Company->id = $id;
		if (!$this->Company->exists()) {
			throw new NotFoundException(__('Invalid company'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Company->delete()) {
			$this->Session->setFlash(__('The company has been deleted.'));
		} else {
			$this->Session->setFlash(__('The company could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
