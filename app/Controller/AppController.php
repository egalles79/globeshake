<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package     app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array(
        'Acl',
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            ),
            /*'flash' => array(
                'element' => 'alert',
                'key' => 'auth',
                'params' => array(
                    'plugin' => 'BoostCake',
                    'class' => 'alert-danger'
                )
            )*/
        ),
        'Session',
        //'LinkedIn.LinkedIn'
    );
    public $helpers = array(
        'Session',
        'Html',// => array('className' => 'BoostCake.BoostCakeHtml'),
        'Form',// => array('className' => 'BoostCake.BoostCakeForm'),
        'Paginator',// => array('className' => 'BoostCake.BoostCakePaginator')
         'Fancybox.Fancybox'
    );
    public $uses = array('Card','City','Department','Functionality','User','Company','Country','Group','Language','Sector','UsersUserProject','UserProject','CompanyProjectsUser','CompanyProject','CountryPoint');

    private function _setLanguage($lang = 'esp') {
        if ($this->Session->read('Config.language') != Configure::read('Config.language')) {
            $lang = $this->Session->read('Config.language');
        }
        $sLanguage = (!empty($lang)) ? $lang : 'esp';
        $this->Session->write('Config.language', $sLanguage);
        Configure::write('Config.language',$lang);
        //$this->Cookie->write('lang', $lang, false, '20 days');

    }

    public function getStatic($Model = null,$withExtras = null) {
        $this->loadModel($Model);
        if ($Model == 'Country') {
            $locale = $this->Session->read('Config.language');
            $this->$Model->locale = $locale;
        }
        $resources = $this->$Model->find('all',array('recursive' => -1));
        $resources = Hash::extract($resources,'{n}.'.$Model);
        $staticNames = array();
        foreach($resources as $resource) {
            switch($Model) {
                case 'Company':
                    $staticNames[$resource['id']] = strtoupper($resource['name']);
                    break;
                case 'Country':
                    if (!empty($withExtras)) {
                        $staticNames[$resource['id']] = $resource['name']. ' ('.$resource['code_2'].')';    
                    } else {
                        $staticNames[$resource['id']] = $resource['name'];
                    }
                    break;
                case 'User':
                    $staticNames[$resource['id']] = $resource['firstname'].' '.$resource['lastname'];
                    break;
                default:
                    $staticNames[$resource['id']] = $resource['name'];
                    break;
            }
        }
        return $staticNames;
    }

    public function checkTantPerCent($project, $isProjectCompany = true) {
        
        $tantPercent = 0;
        if ($isProjectCompany) {
            $fieldsToCheck = array('name','start_date','end_date','');

            $tantPercent += (empty($project['name'])) ? 0 : 10;
            $tantPercent += (empty($project['start_date'])) ? 0 : 10;
            $tantPercent += (empty($project['end_date']) && empty($project['on_going'])) ? 0 : 10;
            $tantPercent += (empty($project['customer_id'])) ? 0 : 10;
            $tantPercent += (empty($project['company_id'])) ? 0 : 10;
            $tantPercent += (empty($project['sector_id'])) ? 0 : 10;
            $tantPercent += (empty($project['functionality_id'])) ? 0 : 5;
            $tantPercent += (empty($project['department_id'])) ? 0 : 5;
            $tantPercent += (empty($project['duration_year']) && empty($project['duration_month'])
                && empty($project['duration_day']) && empty($project['duration_hours'])) ? 0 : 5;
            $tantPercent += (empty($project['billing'])) ? 0 : 5;
            $tantPercent += (empty($project['budget'])) ? 0 : 5;
            $tantPercent += (empty($project['description'])) ? 0 : 5;
            $tantPercent += (empty($project['contact_responsable_id'])) ? 0 : 5;
            $tantPercent += (empty($project['last_reviewer_id'])) ? 0 : 5;
        } else {
            $tantPercent += (empty($project['name'])) ? 0 : 10;
            $tantPercent += (empty($project['start_date'])) ? 0 : 10;
            $tantPercent += (empty($project['end_date']) && empty($project['on_going'])) ? 0 : 10;
            $tantPercent += (empty($project['customer_id'])) ? 0 : 10;
            $tantPercent += (empty($project['tags'])) ? 0 : 10;
            $tantPercent += (empty($project['functionality_id'])) ? 0 : 10;
            $tantPercent += (empty($project['department_id'])) ? 0 : 10;
            $tantPercent += (empty($project['duration_year']) && empty($project['duration_month'])
                && empty($project['duration_day']) && empty($project['duration_hours'])) ? 0 : 10;
            $tantPercent += (empty($project['billing'])) ? 0 : 5;
            $tantPercent += (empty($project['budget'])) ? 0 : 5;
            
            $tantPercent += (empty($project['description'])) ? 0 : 10;
        }
        $isCompleted = ($tantPercent == 100) ? true : false;
        return $isCompleted;
    }
    public function beforeFilter() {
        $validUserActions = array('add','forgot_password','forgotten_password','resend_token','activate');
        if ((strtoupper($this->request->controller) == 'USERS') &&
            (in_array($this->request->action,$validUserActions))) {
            $this->Auth->allow();
        }
        $this->_setLanguage(Configure::read('Config.language'));
        //$this->Cookie->write('lang', $this->params['language'], false, '20 days');
        $locale = $this->Session->read('Config.language');
        setlocale(LC_ALL, $locale);
        // if ($locale && file_exists(APP.'View'.DS.$this->viewPath.DS.$locale)) {
        //     $this->viewPath = $this->viewPath. DS . $locale;
        // }
        $this->set('locale',$locale);
        //Configure AuthComponent
        $this->Auth->loginAction = array(
          'controller' => 'users',
          'action' => 'login'
        );
        $this->Auth->logoutRedirect = array(
          'controller' => 'users',
          'action' => 'login'
        );
      
        //get static names for each table
        $companiesStatic       = $this->getStatic('Company');
        $departmentsStatic     = $this->getStatic('Department');
        $functionalitiesStatic = $this->getStatic('Functionality');
        $countriesStatic       = $this->getStatic('Country', true);
        $citiesStatic          = $this->getStatic('City');
        $sectorStatic          = $this->getStatic('Sector');
        $userStatic            = $this->getStatic('User');
        $companyProjectStatic  = $this->getStatic('CompanyProject');
        $userProjectStatic     = $this->getStatic('UserProject');
        $usersStatic           = $this->getStatic('User');

        $arrayStaticVars = array(
            'staticUserNames'            => $userStatic,
            'staticProjectCompanyNames'  => $companyProjectStatic,
            'staticUserProjectNames'     => $userProjectStatic,
            'staticCompanyNames'         => $companiesStatic,
            'staticDepartmentsNames'     => $departmentsStatic,
            'staticFunctionalitiesNames' => $functionalitiesStatic,
            'staticCountriesNames'       => $countriesStatic,
            'staticCitiesNames'          => $citiesStatic,
            'staticSectorsNames'         => $sectorStatic,
            'staticUserNames'            => $userStatic,
        );
        
        $arraySessionVars = array(
            'Static.CompanyProjects' => $companyProjectStatic,
            'Static.UserProjects'    => $userProjectStatic,
            'Static.Companies'       => $companiesStatic,
            'Static.Departments'     => $departmentsStatic,
            'Static.Functionalities' => $functionalitiesStatic,
            'Static.Countries'       => $countriesStatic,
            'Static.Cities'          => $citiesStatic,
            'Static.Sectors'         => $sectorStatic,
            'Static.Users'           => $userStatic
        );

        //set staticValues session.static
        $this->_setStaticVars($arrayStaticVars);
        
        //save session.static
        $this->_saveStaticSession($arraySessionVars);
        

        $this->Country->locale = $locale;
        $this->Country->recursive = -1;
        $countries = $this->Country->find('all',array('contain' => array('Countrypoint.countrypoint')));
        
        $countryPoints = Hash::extract($countries,'{n}.Countrypoint.{n}');
        
        $countryNames = $this->getStatic('Country', false);

        $sectors = $this->Sector->find('list');
        $this->set('sectorNames',$sectors);

        $this->Language->recursive = -1;
        $languages = $this->Language->find('list');

        $lastCountryPoints = array();
        $countryWithProjects = array();
        $countryToSet = array();

        $controllerCondition = 'Companies';
        $actionCondition     = 'cvmap';
        
        if ($this->Session->check('UserRegistered')) {
            $user = $this->Session->read('UserRegistered');
            if ($user['User']['card_completed'] == 0) {
                $controllerCondition = 'Users';
                $actionCondition     = 'ver_cv';
                //$this->redirect('/'.$controllerConditioned.'/'.$actionConditioned);
            } else if ($user['User']['formation_completed'] == 0) {
                $controllerCondition = 'Users';
                $actionCondition     = 'ver_cv#formation';
                //$this->redirect('/'.$controllerConditioned.'/'.$actionConditioned);
            }
            $this->UsersUserProject->recursive = -1;
            $userProjects    = $this->_getUserProjects($user);
            if (count($userProjects) > 0) {
                $randNumProjects = (count($userProjects) < 5) ? count($userProjects) : 5;
                $userProjectsFiltered = array_rand($userProjects, $randNumProjects);    
            } else {
                $userProjectsFiltered = null;
            }
            $this->set('userprojects',$userProjectsFiltered);
            
            $companyProjects = $this->_getCompanyProjects($user);

            if (strtoupper($this->request->controller) == 'USERS') {
                $this->UsersUserProject->recursive = -1;
                $projects = $userProjects;
            } else {
                $this->CompanyProjectsUser->recursive = -1;
                $projects = $companyProjects;
            }

            $countryNameString = $this->getStatic('Country', false);

            $countryToSet = array();
            foreach ($projects as $project) {
                $countryToSet[$project['country_id']]['id']                     = $project['country_id'];
                $countryToSet[$project['country_id']]['name']                   = $countryNameString[$project['country_id']];
                $countryToSet[$project['country_id']]['company_id']             = $project['company_id'];
                $countryToSet[$project['country_id']]['projects_in_country']    = count($countryToSet[$project['country_id']]['projects_in_country']['count'])+1;
                if (strtoupper($this->request->controller) != 'USERS') {
                    $numUsers = $this->CompanyProjectsUser->find('count',array('conditions' => array('company_project_id' => $project['id'])));
                    $isCompleted = $this->checkTantPerCent($project,true);
                } else {
                    $numUsers = $this->UsersUserProject->find('count',array('conditions' => array('user_project_id' => $project['id'])));
                    $isCompleted = $this->checkTantPerCent($project,false);
                }
                $stringComplet = ($isCompleted) ? 'complete' : 'incomplete';
                $countryToSet[$project['country_id']][$stringComplet] = $countryToSet[$project['country_id']][$stringComplet]+1;
                $countryToSet[$project['country_id']]['users_in_project'] = $countryToSet[$project['country_id']]['users_in_project']+$numUsers;
                
                $textIsOnGoing = ($project['on_going']) ? 'on_going' : 'finished';
                $countryToSet[$project['country_id']][$textIsOnGoing] = $countryToSet[$project['country_id']][$textIsOnGoing]+1;
            }
            $totalCompanyProjects = count($projects);
            foreach ($projects as $project) {
                $countryToSet[$project['country_id']]['total_company_projects'] = count($companyProjects);
                $countryToSet[$project['country_id']]['company_name']           = $companiesStatic[$project['company_id']];
            }
            $this->set('totalCompanyProjects', $totalCompanyProjects);
        } 
        $this->Auth->loginRedirect = array(
          'controller' => $controllerCondition,
          'action' => $actionCondition
        );

        foreach($countryPoints as $countyPointLayout) {
            $id = $countyPointLayout['country_id'];
            //$countryToSet[$id]['name'] = Hash::extract($countryToSet[$id]['name'],'{n}');
            if (isset($countryToSet[$id])) {
                if (!in_array($name,$countryWithProjects)) {
                    $countryWithProjects[$id]['id']                     = $id;
                    $countryWithProjects[$id]['name']                   = $countryToSet[$id]['name'];
                    $countryWithProjects[$id]['company_name']           = $countryToSet[$id]['company_name'];
                    $countryWithProjects[$id]['company_id']             = $countryToSet[$id]['company_id'];
                    $countryWithProjects[$id]['projects_in_country']    = $countryToSet[$id]['projects_in_country'];
                    $countryWithProjects[$id]['total_company_projects'] = $countryToSet[$id]['total_company_projects'];
                    $countryWithProjects[$id]['projectsongoing']        = (!empty($countryToSet[$id]['on_going'])) ? $countryToSet[$id]['on_going'] : 0;
                    $countryWithProjects[$id]['projectsfinished']       = (!empty($countryToSet[$id]['finished'])) ? $countryToSet[$id]['finished'] : 0;
                    $countryWithProjects[$id]['projectsincomplete']     = (!empty($countryToSet[$id]['incomplete'])) ? $countryToSet[$id]['incomplete'] : 0;
                    $countryWithProjects[$id]['projectscomplete']       = (!empty($countryToSet[$id]['complete'])) ? $countryToSet[$id]['complete'] : 0;
                    $countryWithProjects[$id]['users_in_project']       = $countryToSet[$id]['users_in_project'];
                }
            }
            $lastCountryPoints[$id]['countrypoints'][] = $countyPointLayout['countrypoint'];
            $lastCountryPoints[$id]['id']              = $id;
        }
        $conditons = array();
        $this->Card->recursive = -1;
        $conditions = array('conditions' => array('Cards.user_id' => $user['User']['id'],'OR' => array('Cards.action' => array('C','D'))),'fields' => array('num_cards'));
        $aNumCards = $this->Card->find('all',$condiions);
        $aNumCards = Hash::extract($aNumCards,'{n}.Card.num_cards');
        $totalCards = 0;
        foreach($aNumCards as $numCards) {
            $totalCards += $numCards;
        }
        //debug($countryWithProjects).die;
        $this->set('totalCards',$totalCards);
        //para el mapa
        $this->set('countryWithProjects',json_encode($countryWithProjects));
        $this->set('countryPoints',json_encode($lastCountryPoints));
        $this->set('countryNames',$countryNameString);

        //para la pagina de inicio
        $this->Country->recursive = -1;
        $listGroups = $this->Group->find('list');
        $this->set('optionsGroups',$listGroups);
        $listCountries = $this->Country->find('all',array('fields' => array('code_2','name')));
        $listCountries = Hash::extract($listCountries,'{n}.Country');
        $optionsCountries = array();
        foreach($listCountries as $nameCountry) {
            $optionsCountries[$nameCountry['code_2']] = $nameCountry['name'].' ('.$nameCountry['code_2'].')';
        }
        asort($optionsCountries);
        $namesForMap = $this->Country->find('list');

        $this->set('namesForMap',json_encode($namesForMap));
        //$optionsCountries[0] = __('Seleccione país');
        $this->set('optionsCountries',$optionsCountries);
        $this->set('countryListCompanies',$countries);
        $this->set('languageName',$languages);
        $this->set('selectedCountry','ES');
        $this->set(compact('page', 'subpage', 'title_for_layout'));
    }

    private function _setStaticVars($arrayStatics) {
        foreach ($arrayStatics as $k => $arrayStatic) {
            $this->set($k,$arrayStatic);
        }
    }
    private function _saveStaticSession($arraySessions) {
        foreach ($arraySessions as $k => $arraySession) {
            $this->Session->write($k, $arraySession);
        }
    }
    private function _getCompanyProjects($user) {
        $this->CompanyProject->recursive = -1;
        $companyProjects = $this->CompanyProject->find('all',array('conditions' => array('CompanyProject.company_id' => $user['User']['company_id'])));
        $companyProjects = Hash::extract($companyProjects,'{n}.CompanyProject');
        return $companyProjects;
    }
    private function _getUserProjects($user) {
        $userProjects = $this->UsersUserProject->find('all',array('conditions' => array('user_id' => $user['User']['id']),'fields' =>'user_project_id'));
        $userProjects = Hash::extract($userProjects,'{n}.UsersUserProject.user_project_id');

        if (!empty($userProjects)) {
            $this->UserProject->recursive = -1;
            $searchString = (count($userProjects) == 1) ? array('UserProject.id' =>$userProjects) : array('UserProject.id IN' =>$userProjects);
            $userProjects = $this->UserProject->find('all',array('conditions' => $searchString));
            $userProjects = Hash::extract($userProjects,'{n}.UserProject');    
        }
        return $userProjects;
    }
}
