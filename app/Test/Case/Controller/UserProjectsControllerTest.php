<?php
App::uses('UserProjectsController', 'Controller');

/**
 * UserProjectsController Test Case
 *
 */
class UserProjectsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.user_project',
		'app.budget_currency',
		'app.company_project',
		'app.customer',
		'app.company',
		'app.office',
		'app.user',
		'app.country',
		'app.continent',
		'app.geolocalitzation',
		'app.city',
		'app.countrypoint',
		'app.group',
		'app.functionality',
		'app.sector',
		'app.company_projects_sector',
		'app.department',
		'app.card',
		'app.projects_user_project',
		'app.company_projects_user',
		'app.speciality',
		'app.specialities_user',
		'app.billing_currency',
		'app.contact_responsable',
		'app.last_reviewer',
		'app.sector_project',
		'app.sector_service'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
