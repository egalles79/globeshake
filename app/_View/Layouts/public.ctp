<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'Globesake: la mejor versión de la industria');
$cakeVersion = __d('cake_dev', 'GlobeShake %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.css" />
	<script src="http://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>	
	<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.js'></script>
  	<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.css' rel='stylesheet' />
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->Html->css('public');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('icons');
		echo $this->Html->css('jquery_ui');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->Html->script(array('i18n_js/js/i18n_js'));
		echo $this->fetch('script');
	?>
</head>
<body>
	<div id="wrapper">
		<div id="container">
			<div id="header">
				<div class="headdown">
					<div id="window_area">
						<a id="show_header_window">
							<div class="login">
								<?php echo __('Entrar');?>		
							</div>
						</a>
						<div class="header_window" align="left" style="display: none;">
							<?php 
								echo $this->Form->create('User',array('action' => 'login','class' => 'parallel','accept-charset'=>'utf-8'));
							?>
							<?php echo $this->Form->input('username',array('type' => 'text','label' => __('Correo electrónico'),'class' => 'rsitya_contras'));?>
							<?php echo $this->Form->input('password',array('label' => __('Contraseña'),'class' => 'rsitya_con'));?>
							<div class="rsitya_holv">
								<label for="remember_me">&nbsp;<?php echo __('Recordar la contraseña');?>
									<input type="checkbox" id="remember_me" style="float:left" name="remember_me" value="1">
								</label>
							</div>
							<div class="rsitya_holv">
								<?php
								echo $this->Html->link(
								     __('He olvidado la contraseña'),
								     array('controller' => 'users',
        							 'action' => 'forgot_password')
								);
								?>
							</div>
							<div class="rsitya_holv">
								<?php
								echo $this->Html->link(
								     __('Reenviar email para activar la cuenta'),
								     array('controller' => 'users',
        							 'action' => 'resend_token')
								);
								?>
							</div>
							<?php echo $this->Form->end(__('Iniciar sesión'));?>
						</div>			
					</div>
				</div>
				<?php echo $this->Element('social-icons');?>
			</div><!-- end header-->
			
			<div id="content">
				<?php echo $this->fetch('content'); ?>
			</div><!-- #content -->
			
			<div id="footer">
				<div class="footer-last row mtf clearfix">
					<div class="foot-menu">
						<ul>
							<li><a href="#"><?php echo __('GlobeShake BETA Copyright @').date('Y');?></a></li>
							<li><a href="#"><?php echo __('Copyright');?></a></li>
							<li><a href="#"><?php echo __('Condiciones de uso');?></a></li>
							<li><a href="#"><?php echo __('Política de Privacidad');?></a></li>
							<li><a href="#"><?php echo __('Directrices comunitarias');?></a></li>
							<li><a href="#"><?php echo __('Política de Copyright');?></a></li>
							<li><a href="#"><?php echo __('Blog');?></a></li>
						</ul><!-- end links -->
					</div><!-- end foot menu -->
				</div>
			</div><!--footer-->
		</div>
	</div><!-- #wrapper -->	
</body>
</html>

