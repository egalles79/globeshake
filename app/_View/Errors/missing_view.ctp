<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'jquery.mockjax.min',
  'bootstrap-editable.min',
  'moment.min',
  'typeahead.min',
  'typeaheadjs.min',
  'address.min',
  'select2.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));?>
<h2><?php echo __d('cake_dev', 'Vista no encontrada, por favor contacte con el administrador'); ?></h2>
<p class="error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', 'The view for %1$s%2$s was not found.', '<em>' . h(Inflector::camelize($this->request->controller)) . 'Controller::</em>', '<em>' . h($this->request->action) . '()</em>'); ?>
</p>

<p>
	<?php echo __d('cake_dev', 'Confirm you have created the file: %s', h($file)); ?>
	in one of the following paths:
</p>
<ul>
<?php
	$paths = $this->_paths($this->plugin);
	foreach ($paths as $path):
		if (strpos($path, CORE_PATH) !== false) {
			continue;
		}
		echo sprintf('<li>%s%s</li>', h($path), h($file));
	endforeach;
?>
</ul>

<p class="notice">
	<strong><?php echo __d('cake_dev', 'Notice'); ?>: </strong>
	<?php echo __d('cake_dev', 'If you want to customize this error message, create %s', APP_DIR . DS . 'View' . DS . 'Errors' . DS . 'missing_view.ctp'); ?>
</p>

<?php echo $this->element('exception_stack_trace'); ?>
