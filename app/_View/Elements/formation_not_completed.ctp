<div class="abso_form">
	<div class="inside">
		<a class="pp_closedos" href="#"><?php echo __('Close');?></a>
		<div class="wait"><?php echo __('Antes de continuar, rellene la formación');?>.</div>
		<a href="/Users/edit_formation" class="accept" style="font-size:17px!important;"><?php echo __('Aceptar');?></a>
	</div>
</div>
<style>

.abso_form .inside {
  margin: auto;
  width: 400px;
  border: 2px solid #4778a0;
  padding: 20px;
  text-align: center;
  background: white;
  border-radius: 10px;
  font-size: 22px;
  line-height: 22px;
  margin-top: 37px;
  position: relative;
}
.inside .pp_closedos {
  position: absolute;
  right: 4px;
  top: 0;
}
a.pp_closedos {
  background: url("../img/sprite.png") no-repeat scroll -77px 0 rgba(0, 0, 0, 0);
  cursor: pointer;
  display: block;
  float: right;
  line-height: 22px;
  text-indent: -10000px;
  width: 4%;
}
.accept {
  text-shadow: 0 1px 1px rgba(0,0,0,.3);
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  box-shadow: 0 1px 2px rgba(0,0,0,.2);
  background: #f78d1d;
  background: -webkit-gradient(linear, left top, left bottom, from(#c5a653), to(#ddc079));
  background: -moz-linear-gradient(top, #c5a653, #ddc079);
  background-color: #C2A34E;
  color: white!important;
  /* margin-top: 20px; */
  background-color: #C5A653;
  text-shadow: 0 1px 1px rgba(0,0,0,.3);
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  box-shadow: 0 1px 2px rgba(0,0,0,.2);
  background: #f78d1d;
  background: -webkit-gradient(linear, left top, left bottom, from(#ddc079), to(#c5a653));
  background: -moz-linear-gradient(top, #ddc079, #c5a653);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ddc079', endColorstr='#c5a653');
  background-color: #C2A34E;
  border: medium none;
  width: 150px !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  font-size: 1.4em !important;
  padding: 6px;
  -webkit-box-shadow: 5px 6px 7px -3px #adadad;
  -moz-box-shadow: 5px 6px 7px -3px #adadad;
  box-shadow: 5px 6px 7px -3px #adadad;
  margin: 20px auto 0;
  display: block;
}
</style>