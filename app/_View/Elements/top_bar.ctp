<?php
/**
 * User: Eloi Gallés Villaplana
 * Date: 30/04/14
 * Time: 18:03
 * @file top_bar.ctp
 */

echo $this->Html->script('loadHotspotsListOnTopBar', array('inline' => false));
?>

<!-- start: Header -->
<header class="navbar">
  <div class="container">
    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".sidebar-nav.nav-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a id="main-menu-toggle" class="hidden-xs open"><i class="fa fa-bars"></i></a>
    <a class="navbar-brand col-md-2 col-sm-1 col-xs-2" href="/Companies/cvmap"><span>&nbsp;&nbsp;&nbsp;<?php echo $this->Html->image('logo.png').' '.__('Globeshake');?></span></a>
      <nav>
          <ul class="menu">
           <li><a id="menu1" href="/Companies/cvmap"><i class="fa fa-building-o"></i>&nbsp;&nbsp;<?php echo __('EMPRESA');?></a>
           <ul class="sub-menu">
             <li>
                <a href="#"><i class="fa fa-globe">&nbsp;<?php echo __('Dar a conocer empresa');?></i></a>
                <ul class="sub-menu">
                  <li><a href="/Companies/ver_cv"><i class="fa fa-columns">&nbsp;<?php echo __('Ver CV');?></i></a></li>
                  <li><a href="/Companies/resumen_cv"><i class="fa fa-dashboard">&nbsp;<?php echo __('Cifras totales de empresa');?></i></a></li>
                  <li><a href="#"><i class="fa fa-external-link">&nbsp;<?php echo __('Crea portal empresa');?></i></a></li>
                </ul>
             </li>
             <li>
                <a href="#"><i class="fa fa-users">&nbsp;<?php echo __('Colaboradores');?></i></a>
                <ul class="sub-menu">
                  <li><a href="#"><i class="fa fa-list">&nbsp;<?php echo __('Listas internas');?></i></a></li>
                  <li><a href="#"><i class="fa fa-edit">&nbsp;<?php echo __('Crear lista');?></i></a></li>
                  <li><a href="#"><i class="fa fa-list-ul">&nbsp;<?php echo __('Mis listas');?></i></a></li>
                </ul>
             </li>
             <li>
                <a href="#"><i class="fa fa-paperclip">&nbsp;<?php echo __('Licitaciones');?></i></a>
                <ul class="sub-menu">
                  <li><a href="#"><i class="fa fa-edit">&nbsp;<?php echo __("Publicar licitación") ?></i></a></li>
                  <li><a href="#"><i class="fa fa-file-text-o">&nbsp;<?php echo __("Petición de oferta") ?></i></a></li>
                  <li><a href="#"><i class="fa fa-list">&nbsp;<?php echo __("Listas internas") ?></i></a></li>
                  <li><a href="#"><i class="fa fa-search">&nbsp;<?php echo __("Market Survey") ?></i></a></li>
                </ul>
             </li>
           </ul>
         </li>
          <li><a id="menu2"  href="/Users/cvmap"><i class="fa fa-briefcase"></i>&nbsp;&nbsp;<?php echo __('PERFIL PROFESIONAL');?></a>
          <ul class="sub-menu">
           <li>
              <a href="#"><i class="fa fa-info">&nbsp;<?php echo __("Dar a conocer mi perfil") ?></i></a>
              <ul>
              <li><a href="/Users/ver_cv"><i class="fa fa-columns">&nbsp;<?php echo __('Ver CV');?></i></a></li>
              <li><a href="/Users/resumen_cv"><i class="fa fa-file-text">&nbsp;<?php echo __('Resumen CV');?></i></a></li>
              <li><a href="/Specialities/"><i class="fa fa-star">&nbsp;<?php echo __('Soy especialista');?></i></a></li>
              <li><a href="#"><i class="fa fa-sun-o">&nbsp;<?php echo __('Sé visible en la empresa');?></i></a></li>
            </ul>
           </li>
           <li><a href="#"><i class="fa fa-search">&nbsp;<?php echo __('Buscar (general)');?></i></a>
            <ul>
              <li><a href="#"><i class="fa fa-users">&nbsp;<?php echo __('Personas');?></i></a></li>
              <li><a href="#"><i class="fa fa-book">&nbsp;<?php echo __('Contactos');?></i></a></li>
              <li><a href="#"><i class="fa fa-briefcase">&nbsp;<?php echo __('Empresas');?></i></a></li>
            </ul>
          </li>
         </ul>

        </li>
        </ul>
        </nav>
    <!-- start: Header Menu -->
    <div class="nav-no-collapse header-nav">
      <ul class="nav navbar-nav pull-right">
          <?php echo $this->Element('idiom',array('visible' => true));?>

        <li class="dropdown hidden-xs">
          <!--<a class="btn dropdown-toggle"  href="#">
            <i class="glyphicons wifi_alt" style="color:#fff"></i>
          </a>-->
          <ul class="dropdown-menu notifications" id="hotspotslist">
            <li class="dropdown-menu-title" style="text-align: center;">
              <span><?= __('Puntos de acceso') ?></span>
            </li>
          </ul>
        </li>

        <!-- start: User Dropdown -->
        <li class="dropdown">
          <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
            <div class="avatar"></div>
            <div class="user">
              <span class="hello"><?= __('Bienvenido') ?></span>
              <span class="name"><?php echo $this->Session->read('Auth.User.username')?></span>
            </div>
          </a>
          <ul class="dropdown-menu menumine" style="text-align: left">
            <li><a href="/Users/my"><i class="fa fa-user"></i><?= __('Perfil') ?></a></li>
            <!-- <li><a href="/Users/my"><i class="fa fa-cog"></i><?= __('Configuration') ?></a></li> -->
            <li><a href="/Users/logout"><i class="fa fa-off"></i><?= __('Salir') ?></a></li>
          </ul>
        </li>
        <!-- end: User Dropdown -->
      </ul>
    </div>
    <div style="float: right;
  margin-right: 20px;
  margin-top: 5px;">
        <?php echo $this->Element('social-icons');?>
    </div>
    <!-- end: Header Menu -->

  </div>
</header>
<style>
/* ---------- Google Font ---------- */
@import url(http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,700,800);

/* RESET STYLES */
*, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td { margin:0; padding:0 }
table { border-collapse:collapse; border-spacing:0 }
fieldset, img { border:0 }
address, caption, cite, code, dfn, em, strong, th, var { font-style:normal; font-weight:normal }
ol, ul, li { list-style:none }
caption, th { text-align:left }
h1, h2, h3, h4, h5, h6 {font-weight:normal;}
q:before, q:after { content:''}
strong { font-weight: bold }
em { font-style: italic }
.italic { font-style: italic }
.aligncenter { display:block; margin:0 auto; }
.alignleft { float:left; margin:10px; }
.alignright { float:right;margin:10px; }
.no-margin{margin:0px;}
.no-bottom{margin-bottom:0px;}
.no-padding{padding:0px;}
.margin-t{margin-top:22px;}

a{text-decoration:none;}
a:hover{text-decoration:underline;}
a:active,a:focus{outline: none;}

img.alignleft, img.alignright, img.aligncenter {
    margin-bottom: 11px;
}

.alignleft, img.alignleft{
display: inline;
    float: left;
    margin-right: 22px;
}

.alignright, img.alignright {
    display: inline;
    float: right;
    margin-left: 22px;
}

.aligncenter, img.aligncenter {
    clear: both;
    display: block;
    margin-left: auto;
    margin-right: auto;
}

article, aside, figure, footer, header, hgroup, nav, section {display: block;}


*{ 
  -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
  -moz-box-sizing: border-box;    /* Firefox, other Gecko */
  box-sizing: border-box;         /* Opera/IE 8+ */
}


p{padding-bottom:11px;}
p,div, span{line-height:1.5em;}
.column-clear{clear:both;}
.clear{overflow: hidden;}


nav{display:block;
margin-left:48px;
float:left;
}

.menu{display:block;}

.menu li{
display: inline-block;
position: relative;
z-index:100;}

.menu li:first-child{margin-left:0;}

.menu li a {font-weight:600;
text-decoration:none;
padding:11px;
display:block;
color:#ffffff;

-webkit-transition: all 0.2s ease-in-out 0s;
   -moz-transition: all 0.2s ease-in-out 0s;
   -o-transition: all 0.2s ease-in-out 0s;
   -ms-transition: all 0.2s ease-in-out 0s;
   transition: all 0.2s ease-in-out 0s;
}
.active2 {
  font-weight:600;
  text-decoration:none;
  padding:11px;
  display:block;
  color:#47789f !important;
  background:#e9ebec;
  border-radius: 5px 5px 0px 0px;
  -moz-border-radius: 5px 5px 0px 0px;
  -webkit-border-radius: 5px 5px 0px 0px;
}
.menu li a:hover, .menu li:hover > a{
  color:#47789f;
  background:#e9ebec;
  border-radius: 5px 5px 0px 0px;
  -moz-border-radius: 5px 5px 0px 0px;
  -webkit-border-radius: 5px 5px 0px 0px;
}

.menu ul {display: none;
margin: 0;
padding: 0;
width: 150px;
position: absolute;
top: 43px;
left: 0px;
background: #ffffff;
}

.menu ul li {display:block;
float: none;
background:none;
margin:0;
padding:0;
}
.menu ul li a {font-size:12px;
font-weight:normal;
display:block;
color:#47789f;
border-left:3px solid #ffffff;
background:#ffffff;}

.menu ul li a:hover, .menu ul li:hover > a{
background:#47789f;
border-left:3px solid #fff;
color:#fff;
}

.menu li:hover > ul{ display: block;}
.menu ul ul {left: 149px;
  top: 0px;
}

.mobile-menu{display:none;
width:100%;
padding:11px;
background:#3E4156;
color:#ffffff;
text-transform:uppercase;
font-weight:600;
}
.mobile-menu:hover{background:#3E4156;
color:#ffffff;
text-decoration:none;
}


@media (min-width: 768px) and (max-width: 979px) {
.menu ul {top:37px;}
.menu li a{font-size:12px;
padding:8px;}
}

@media (max-width: 767px) {

.mainWrap{width:auto;padding:50px 20px;}

.menu{display:none;}

.mobile-menu{display:block;
margin-top:100px;}

nav{margin:0;
background:none;}

.menu li{display:block;
margin:0;}

.menu li a {background:#ffffff;
color:#797979;
border-top:1px solid #e0e0e0;
border-left:3px solid #ffffff;}

.menu li a:hover, .menu li:hover > a{
background:#f0f0f0;
color:#797979;
border-left:3px solid #9CA3DA;}

.menu ul {display:block;
position:relative;
top:0;
left:0;
width:100%;}

.menu ul ul {left:0;}

}

@media (max-width: 480px) {

}


@media (max-width: 320px) {
}
</style>
<!-- end: Header -->