<?php
echo $this->Html->script('msdropdown/jquery.dd.min', array('inline' => false));
echo $this->Html->script('/I18nJs/js/i18n_js', array('inline' => false));
echo $this->Html->script('/js/Locale/i18n_js.' . $this->Session->read('Config.language'), array('inline' => false));

echo $this->Html->css('msdropdown/dd');
echo $this->Html->css('msdropdown/flags');


$this->Html->scriptStart(array('inline' => false));
?>

$(document).ready(function() {
$("#countries").msDropdown();
$( "#countries" ).change(function(e) {
var al = $( "#countries option:selected" ).val();
var url = "/Idiom/changeidiom/"+al; // the script where you handle the form input.
e.preventDefault();
$.ajax({
type: "POST",
url: url,
data: al, // serializes the form's elements.
success: function(data)
{
console.log(data);
window.location.href = data;

},
error : function() {
console.log(e);
}
});
});
});

<?php $this->Html->scriptEnd(); ?>
<!-- <select name="countries" id="countries" style="cursor:pointer;width:150px;">-->
<?php
    $locale    = $this->Session->read('Config.language');
    $languages = Configure::read('Config.languages');
    if ($visible) {
        echo '<select id="countries" style="width:150px;cursor:pointer;margin-top:4px">';
        foreach ($languages as $language) {
            $idiom = $this->Language->selectIdiom($language);
            echo '<option value="' . $idiom['locale'] . '"';
            $idioms[$idiom['locale']] = $idiom['name'];
    //    $idioms['data-image'][$idiom['locale']] = "/img/msdropdown/icons/blank.gif";
    //    $idioms['data-imagecss'][$idiom['locale']] = 'flag '.$idiom['speciallocale'];
    //    $idioms['data-title'][$idiom['locale']] = $idiom['name'];
            if (($locale == $language) || ($locale == '')) {
                echo 'selected = "' . $idiom['locale'] . '"';
            };
            //echo 'data-image="/img/msdropdown/icons/blank.gif" data-imagecss="flag ' . $idiom['speciallocale'] . '" data-title="' . $idiom['name'] . '">' . $idiom['name'] . '</option>';
            echo 'data-title="' . $idiom['name'] . '">' . $idiom['name'] . '</option>';
        }
        echo '</select>';
    }
?>
<!--</select>-->
<?php //echo $this->Form->input('countries',array('label' => false,'options' => $idioms,'selected' => $selected,'style' => 'width:150px'));?>