<!-- start: Main Menu -->
<div id="sidebar-left" class="col-lg-2 col-sm-1 ">

    <div class="sidebar-nav nav-collapse collapse navbar-collapse">
        <ul class="nav main-menu">
            <li>
                <?php echo $this->Html->link('<i class="fa fa-home"></i><span class="hidden-sm text"> ' . __('Inicio') . '</span>',
                    array('controller' => 'Companies', 'action' => 'projectmap', ),
                    array('escape' => false)); ?>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="fa fa-shopping-cart"></i><span class="hidden-sm text submenu"> ' . __('Comprar tarjeta de visita') . '</span>',
                    array('controller' => 'Cards', 'action' => 'index', ),
                    array('escape' => false)); ?>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="fa fa-shopping-cart"></i><span class="hidden-sm text submenu"> ' . __('Tiene %s tarjetas de visita',$totalCards) . '</span>',
                    '#',
                    array('escape' => false)); ?>
            </li>
            <li>
                <a class="dropmenu hidden2" href="#"><i class="fa fa-building-o"></i><span class="hidden-sm text"> <?php echo __('Empresa');?></span> <span class="chevron closed"></span></a>
                <ul class="hidden2">
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-globe"></i><span class="hidden-sm text"> <?php echo __("Dar a conocer empresa") ?></span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-info"></i><span class="hidden-sm text"> <?php echo __("Presenta a tu empresa") ?></span></a>
                            </li>
                            <li>
                                <?php echo $this->Html->link('<i class="fa fa-book"></i><span class="hidden-sm text submenu"> ' . __('Ver CV') . '</span>',
                                    array('controller' => 'Companies', 'action' => 'ver_cv', ),
                                    array('escape' => false)); ?>
                            </li>
                             <li>
                                <a class="submenu" href="#"><i class="fa fa-dashboard"></i><span class="hidden-sm text"> <?php echo __("Cifras totales empresa") ?></span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-external-link"></i><span class="hidden-sm text"> <?php echo __("Crea portal empresa") ?></span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-users"></i><span class="hidden-sm text"> <?php echo __("Colaboradores") ?></span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-list"></i><span class="hidden-sm text"> <?php echo __("Listas internas") ?></span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-edit"></i><span class="hidden-sm text"> <?php echo __("Crear lista") ?></span></a>
                            </li>
                             <li>
                                <a class="submenu" href="#"><i class="fa fa-list-ul"></i><span class="hidden-sm text"> <?php echo __("Mis listas") ?></span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-paperclip"></i><span class="hidden-sm text"> <?php echo __("Licitaciones") ?></span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-edit"></i><span class="hidden-sm text"> <?php echo __("Publicar licitación") ?></span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-file-text-o"></i><span class="hidden-sm text"> <?php echo __("Petición oferta") ?></span></a>
                            </li>
                             <li>
                                <a class="submenu" href="#"><i class="fa fa-list"></i><span class="hidden-sm text"> <?php echo __("Listas internas") ?></span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-search"></i><span class="hidden-sm text"> <?php echo __("Market Survey") ?></span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a class="dropmenu hidden2" href="#"><i class="fa fa-briefcase"></i><span class="hidden-sm text"> <?php echo __("Perfil profesional") ?></span><span class="chevron closed"></span></a>
                <ul class="hidden2">
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-info"></i><span class="hidden-sm text"> <?php echo __("Dar a conocer mi perfil") ?></span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-info"></i><span class="hidden-sm text"> <?php echo __("Preséntate profesionalmente") ?></span></a>
                            </li>
                            <li>
                                <?php echo $this->Html->link('<i class="fa fa-columns"></i><span class="hidden-sm text submenu"> ' . __('Ver CV') . '</span>',
                                    array('controller' => 'Users', 'action' => 'ver_cv', ),
                                    array('escape' => false)); ?>
                            </li>
                            <li>

                                <?php echo $this->Html->link('<i class="fa fa-file-text"></i><span class="hidden-sm text submenu"> ' . __('Resumen CV') . '</span>',
                                    array('controller' => 'Users', 'action' => 'resumen_cv', ),
                                    array('escape' => false)); ?>


                                
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-star"></i><span class="hidden-sm text"> <?php echo __("Soy especialista") ?></span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-sun-o"></i><span class="hidden-sm text"> <?php echo __("Sé visible en la empresa") ?></span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-search"></i><span class="hidden-sm text"> <?php echo __("Buscar (general)") ?></span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-users"></i><span class="hidden-sm text"> <?php echo __("Personas") ?></span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-book"></i><span class="hidden-sm text"> <?php echo __("Contactos") ?></span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-building-o"></i><span class="hidden-sm text"> <?php echo __("Empresas") ?></span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <?php echo $this->Html->link('<i class="fa fa-user"></i><span class="hidden-sm text submenu"> ' . __('Ver perfil') . '</span>',
                    array('controller' => 'Users', 'action' => 'ver_cv', ),
                    array('escape' => false)); ?>
            </li>
            <li>
            <a href="#" id="main-menu-min" class="visible-md visible-lg full" style="backgorund-color:#fff !important"><i class="fa fa-angle-double-left"></i></a>

            <!-- ACL User Secction -->
            <!--            <?php /*if ($Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.Group.id')), 'WifiClients/index')): */?>
                <li>
                    <?php /*echo
                    $this->Html->link('<i class="fa fa-group"></i><span class="hidden-sm text"> ' . __('Wifi Users') . '</span>', array('controller' => 'WifiClients', 'action' => 'index'), array('escape' => FALSE)
                    );
                    */?>
                </li>
            <?php /*endif; */?>

            <?php /*if ($Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.Group.id')), 'Communications/index')): */?>
                <li>
                    <?php /*echo
                    $this->Html->link('<i class="fa fa-bullhorn"></i><span class="hidden-sm text">' . __('Communication') . '</span>', array('controller' => 'Communications', 'action' => 'index'), array('escape' => false)
                    );
                    */?>
                </li>
            <?php /*endif; */?>

            <?php /*if ($Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.Group.id')), 'Gamifications')): */?>
                <li>
                    <?php /*echo
                    $this->Html->link('<i class="fa fa-gears"></i><span class="hidden-sm text">' . __('Gamificating') . '</span>', array('controller' => 'Gamifications', 'action' => 'index'), array('escape' => false)
                    );
                    */?>
                </li>
            <?php /*endif; */?>

            <?php /*if ($Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.Group.id')), 'AccessPoints/index')): */?>
                <li>
                    <?php /*echo
                    $this->Html->link('<i class="fa fa-laptop"></i><span class="hidden-sm text"> ' . __('Captive') . '</span>', array('controller' => 'AccessPoints', 'action' => 'index'), array('escape' => FALSE)
                    );
                    */?>
                </li>
            <?php /*endif; */?>

            <?php /*if ($Acl->check(array('model' => 'Group', 'foreign_key' => $this->Session->read('Auth.User.Group.id')), 'Statistics/index')): */?>
                <li>
                    <?php /*echo
                    $this->Html->link('<i class="fa fa-bar-chart-o"></i><span class="hidden-sm text">' . __('Statistics') . '</span>', array('controller' => 'Statistics', 'action' => 'index'), array('escape' => false)
                    );
                    */?>
                </li>
            <?php /*endif; */?>
            <?php
            /*            $is_admin = $this->Session->read('Auth.IsAdmin');
                        if ($is_admin):
                            */?>
                <li>
                    <?php /*echo
                    $this->Html->link('<i class="fa fa-user"></i><span class="hidden-sm text">' . __('Vendors') . '</span>', array('controller' => 'Vendors', 'action' => 'index'), array('escape' => false)
                    );
                    */?>
                </li>
            --><?php /*endif; */?>
        </ul>
    </div>
    <?php // echo $this->Element('idiom',array('visible' => true));?>
</div>
<!-- end: Main Menu -->
