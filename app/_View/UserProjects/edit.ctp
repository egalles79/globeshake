<?php

$this->extend('/Common/panel');
echo $this->Html->script(array(
    'jquery-ui-1.10.3.custom.min',
    'jquery.sparkline.min',
    'jquery.chosen.min',
    'jquery.autosize.min',
    'jquery.placeholder.min',
    'daterangepicker.min',
    'moment.min',
    'jquery.steps.min',
    'custom.min',
    'bootbox.min',
    'core.min',
    'jquery.tooltipster.min',
    'jquery.mask.min',
    'tag-it'
), array('inline' => false));

echo $this->Html->css('tagit');

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#menu1').removeClass('active2');
  $('#menu2').addClass('active2');
  var defaultOnGoing = <?php echo $defaultOnGoing;?>;
  if (defaultOnGoing) {
    $('#UserProjectOnGoing').click();
    $('#UserProjectEndDate').parent().parent().hide();
  }
  $('#UserProjectCompanyName').on('change',function() {
    if ($('#UserProjectCompanyName').val() != $('#UserProjectCompanyNameHidden').val()) {
      $('#actualCompanyNo').prop('checked',true);
    } else {
      $('#actualCompanySi').prop('checked',true);
    }
  });
  $('#UserProjectOnGoing').on('click', function() {
    if ($('#UserProjectOnGoing').prop('checked')) {
      $('#UserProjectEndDate').val('');
      $('#UserProjectEndDate').parent().parent().hide();
    } else {
      $('#UserProjectEndDate').parent().parent().show();
    }
  });
  $('#actualCompanySi').prop('checked',true);
  $('#actualCompanyNo').click(function() {
    $('#UserProjectCompanyName').val('');
  });
  $('#UserProjectCompanyName').on('change',function() {
    if ($('#UserProjectCompanyName').val() != $('#UserProjectCompanyNameHidden').val()) {
      $('#actualCompanyNo').prop('checked',true);
    } else {
      $('#actualCompanySi').prop('checked',true);
    }
  });
  
  $('#actualCompanySi').click(function() {
    $('#UserProjectCompanyName').val($('#UserProjectCompanyNameHidden').val());
  });
  $('#wheretosee1').click(function() {
    $('#whocansee1').prop('disabled',false);
    $('#whocansee2').prop('disabled',false);
    $('#whocansee3').prop('disabled',false);
    $('#whocansee4').prop('disabled',false);
    $('#whocansee1').prop('checked', true);
  });
  $('#wheretosee2').click(function() {
    $('#whocansee1').prop('disabled',false);
    $('#whocansee2').prop('disabled',false);
    $('#whocansee3').prop('disabled',false);
    $('#whocansee4').prop('disabled',false);
    $('#whocansee1').prop('checked', true);
  });
  $('#wheretosee3').click(function() {
    $('#whocansee2').prop('checked', true);
    $('#whocansee1').prop('disabled',true);
    $('#whocansee3').prop('disabled',true);
    $('#whocansee4').prop('disabled',true);
  });

  $('.money').mask('000.000.000.000.000,00', {reverse: true});
  var departments   = [ <?php echo $departmentsText;?> ];
  var functionalities = [ <?php echo $functionalitiesText;?> ];
  var companies = [ <?php echo $companiesText;?> ];
  var sectors = [ <?php echo $sectorProjectText;?> ];
  var cities = [ <?php echo $citiesText;?> ];
    var accentMap = {
      "á": "a",
      "ö": "o",
      "é": "e",
      "í": "i",
    };
    var normalize = function( term ) {
      var ret = "";
      for ( var i = 0; i < term.length; i++ ) {
        ret += accentMap[ term.charAt(i) ] || term.charAt(i);
      }
      return ret;
    };
 
    $( "#UserProjectDepartmen" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( departments, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectFunctionalit" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( functionalities, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectSectorServic" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectSectorProj" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
     $( "#UserProjectCit" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( cities, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectCustome" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( companies, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectCompanyName" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( companies, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    /*$( "#lineaNegocio" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#lineaNegocio_personal" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#ciudad" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( cities, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });*/
  $( ".datepicker" ).datepicker({
    dateFormat: "mm/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: '1925:2015',
    showButtonPanel: true,
    onClose: function(dateText, inst) {


        function isDonePressed(){
            return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
        }

        if (isDonePressed()){
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
            
             $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
        }
    },
    beforeShow : function(input, inst) {

        inst.dpDiv.addClass('month_year_datepicker')

        if ((datestr = $(this).val()).length > 0) {
            year = datestr.substring(datestr.length-4, datestr.length);
            month = datestr.substring(0, 2);
            $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
            $(this).datepicker('setDate', new Date(year, month-1, 1));
            $(".ui-datepicker-calendar").hide();
        }
    }

  });

  $.datepicker.regional['es'] = {
      closeText: I18nJs.t('Escoger fecha'),
      prevText: '< Anterior',
      nextText: 'Siguiente >',
      currentText: 'Hoy',
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
      dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
      weekHeader: 'Sm',
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''
  };
  $.datepicker.setDefaults($.datepicker.regional['es']);

  

  /* ---------- FuelUX Wizard ---------- */
    var wizard = $('#MyWizard');

    wizard.on('finished', function(e, data) {
        $('#UserProjectAddForm').submit();
    });
    $('#but1').click(function(){
      alertError = '<ul>';
      if (($('#UserProjectInitDate').val() == '')) {
          alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar una fecha incio/fin de proyecto para poder editar')+'</li>';
      }
      if (($('#UserProjectSectorProj').val() == '')) {
          alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar un sector del proyecto para poder editar')+'</li>';
      }
      
      if (($('#UserProjectCompanyName').val() == '')) {
          alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar un nombre de la compañía para poder editar')+'</li>';
      }
      if (($('#UserProjectName').val() == '')) {
          alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar un nombre para el proyecto para poder editar')+'</li>';
      }
      if (($('#UserProjectEndDate').val() == '') && (!$('#UserProjectOnGoing').prop('checked'))) {
          alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar una fecha final para el proyecto o pulsar en curso')+'</li>';
      }
      alertError = alertError+'</ul>';
      valorLocalizacion = $('#UserProjectLatitude').val()
      if (valorLocalizacion.length == 0) {
        bootbox.alert(I18nJs.t('Debe tener una localización exacta para poder editar. En caso de no tenerla arrastre el puntero hacia el lugar que le parezca más cercano'));
      }
      if (alertError != '<ul></ul>') {
        bootbox.alert(alertError);
        return false;
      }
      $('#UserProjectAddForm').submit();
    });

    adddress = $('#UserProjectCompletAddress').val();
    $('#pac-input').val(adddress);
    google.maps.event.trigger(map, 'resize');
    
    //control errores
    /*
    
    */

    $('#UserProjectCountryId').change(function() {
      value = $("#UserProjectCountryId option:selected").text();
      $('#UserProjectCountryNamex').val(value);
    });
    wizard.on('change', function(e, data) {
      
    });
    wizard.wizard();

  /* ---------- Datapicker ---------- */
  $('.datepicker').datepicker();

  /* ---------- Choosen ---------- */
  $('[data-rel="chosen"],[rel="chosen"]').chosen();

  /* ---------- Placeholder Fix for IE ---------- */
  $('input, textarea').placeholder();

  /* ---------- Auto Height texarea ---------- */
  $('textarea').autosize();   
});
var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 18,
    center: new google.maps.LatLng($('#UserProjectLatitude').val(), $('#UserProjectLongitude').val()),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng($('#UserProjectLatitude').val(), $('#UserProjectLongitude').val()),
    draggable: true
});

var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

    google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    $('#UserProjectAddress').val('');
    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+evt.latLng.lat().toFixed(3)+","+evt.latLng.lng().toFixed(3)+"&sensor=true";
    locc = evt.latLng;
    map.setCenter(locc);
    
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(locc);
    latitudeLongitude = JSON.stringify(bounds);
    
    latitudeLongitude = latitudeLongitude.split(',');
    latitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
    console.log(latitudeLongitude);
    longitude = latitudeLongitude[2].substring(9,latitudeLongitude[2].length);
    
    $('#UserProjectLongitude').val(longitude);
    $('#UserProjectLatitude').val(latitude);

    map.fitBounds(bounds);
    map.setZoom(20);
    $.ajax({
      url: url,
    }).success(function(result) {
      if (result['status'] == 'OK') {
        if (typeof result != 'undefined') {
          $('#pac-input').val(result['results'][0]['formatted_address']);
          var haslocality = false;
          var hasroute = false;
          var hasadministrativearea = false;
          $.each(result['results'][0]['address_components'], function(index, resultType) {
            if (resultType['types'][0] == 'country') {
                $.ajax({
                  url:  '/Countries/get_country_from_code_2/'+resultType['short_name'],
                  type: 'get',
                  success:  function (response) {
                      if (response != false) {
                        $('#UserProjectCountryId').val(response);
                      } else {
                        $('#UserProjectCountryId').val(0);
                      }
                      $('#UserProjectCountryId').change();
                  }
                });
            }
            
            if (resultType['types'][0] == 'street_number') {
                $('#UserProjectAddress').val(','+resultType['short_name']);
            }
            if (resultType['types'][0] == 'postal_code') {
                $('#UserProjectAddress').val($('#UserProjectAddress').val()+'('+resultType['short_name']+')');
            }
            if (resultType['types'][0] == 'locality') {
                haslocality = true;
                searchplace = searchplace = resultType['long_name'].trim();
            }
            if (!haslocality) {
              if (resultType['types'][0] == 'administrative_area_level_1') {
                  hasadministrativearea = true;
                  searchplace = resultType['long_name'].trim();
              }
            }
            if ((!haslocality)&&(!hasadministrativearea)) {
              if (resultType['types'][0] == 'route') {
                  hasroute = true;
                  searchplace = resultType['long_name'].trim();
                  $('#UserProjectAddress').val(searchplace+$('#UserProjectAddress').val());
              }
            }
          });
          $('#UserProjectCompletAddress').val($('#pac-input').val());
          console.log(searchplace);
          $.ajax({
            url:  '/Cities/get_city_from_name/'+searchplace,
            type: 'get',
            success:  function (response) {
                if (response != false) {
                  response = JSON.parse(response);
                  $('#UserProjectCit').val(response['name']);
                  $('#UserProjectCitId').val(response['id']);
                } else {
                  $('#UserProjectCit').val(searchplace);
                }
            }
          });
          
        } 
      }
    });
});




google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
    $('#pac-input').val('Asignando punto');
});
google.maps.event.addListener(map, 'bounds_changed', function() {
  var bounds = map.getBounds();
  searchBox.setBounds(bounds);
});

map.setCenter(myMarker.position);
myMarker.setMap(map);
markers = [];

google.maps.event.addListener(searchBox, 'places_changed', function() {
      $('#UserProjectAddress').val('');
      $('#UserProjectCountryNamex').val('');
      $('#UserProjectCit').val('');
      $('#UserProjectCompletAddres').val($('#pac-input').val());
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }
      for (var i = 0, marker; marker = markers[i]; i++) {
        marker.setMap(null);
      }

      // For each place, get the icon, place name, and location.
      markers = [];
      var bounds = new google.maps.LatLngBounds();
      var myPlaces = [];
      var results  = [];
      for (var i = 0, place; place = places[i]; i++) {
        var image = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        myMarker.setPosition(place.geometry.location);

        map.setCenter(place.geometry.location);
        //markers.push(marker);

        bounds.extend(place.geometry.location);
        var hasCompleteAddress = false;
        if (place.types[0] == 'street_address') {
          console.log(place.geometry.location);
          //latitudeLongitude = JSON.stringify(place.geometry.location);
          //latitudeLongitude = latitudeLongitude.split(',');
          //latitude = latitudeLongitude[0].substring(5);
          //longitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
          
          latitudeLongitude = JSON.stringify(bounds);
    
          latitudeLongitude = latitudeLongitude.split(',');
          latitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
          console.log(latitudeLongitude);
          longitude = latitudeLongitude[2].substring(9,latitudeLongitude[2].length);
          $('#UserProjectLongitude').val(longitude);
          $('#UserProjectLatitude').val(latitude);
          var hasCompleteAddress = true;
        } else {
          $('#UserProjectLongitude').val('');
          $('#UserProjectLatitude').val('');
        }
        results.push(place['address_components']);
        myPlaces.push(place.types[0]);
      }
      map.fitBounds(bounds);
      var listener = google.maps.event.addListener(map, "idle", function() { 
        val = map.getZoom();
        switch (myPlaces[0]) {
          case 'continent' :
            val = 4;
            break;
          case 'country' :
            val = 6;
            break;
          case 'administrative_area_level_1':
          case 'locality' :
            val = 13;
            break;
          case 'lodging' :
          case 'route' :
            val = 17;
            break;
          case 'neighborhood' :
            val = 16;
            break;
        }
       $.each(results[0], function(index, resultType) {
          if (resultType['types'][0] == 'country') {
            var url = '/Countries/get_country_from_code_2/'+resultType['short_name'];

              $.ajax({
                url:  url,
                type: 'get',
                success:  function (response) {
                    if (response != false) {
                      $('#UserProjectCountryId').val(response);
                    } else {
                      $('#UserProjectCountryId').val(0);
                    }
                    $('#UserProjectCountryId').change();
                }
              });
          }
        });
        if (hasCompleteAddress) {
          $('#UserProjectCompletAddress').val($('#pac-input').val());
          $('#UserProjectAddress').val('');
          var haslocality = false;
          var hasroute = false;
          var hasadministrativearea = false;
          
          $.each(results[0], function(index, resultType) {
            if (resultType['types'][0] == 'postal_code') {
                $('#UserProjectAddress').val($('#UserProjectAddress').val()+'('+resultType['short_name']+')');
            }
            if (resultType['types'][0] == 'street_number') {
                $('#UserProjectAddress').val(','+resultType['short_name'].trim());
            }
            if (resultType['types'][0] == 'locality') {
                haslocality = true;
                searchplace = resultType['short_name'].trim();
            }
            if (!haslocality) {
              if (resultType['types'][0] == 'administrative_area_level_1') {
                  hasadministrativearea = true;
                  searchplace = resultType['long_name'].trim();
              }
            }
            if ((!haslocality)&&(!hasadministrativearea)) {
              if (resultType['types'] == 'route') {
                  hasroute = true;
                  searchplace = resultType['long_name'].trim();
                  $('#UserProjectAddress').val(searchplace+$('#UserProjectAddress').val());
              }
            }
          });
          $.ajax({
            url:  '/Cities/get_city_from_name/'+searchplace,
            type: 'get',
            success:  function (response) {
                if (response != false) {
                  response = JSON.parse(response);
                  $('#UserProjectCit').val(response['name']);
                  $('#UserProjectCitId').val(response['id']);
                } else {
                  $('#UserProjectCit').val(searchplace);
                }
            }
          });
        } 
        map.setZoom(val);
        google.maps.event.removeListener(listener); 
      });
});

$("#myTags").tagit({

    // Options
    fieldName: "data[UserProject][tags][]",
    availableTags: ["Obra Pública", "Servicio","Industria", "Licitaciones Internacionales", "Construcción", "Contratistas", "UTES", "Ingeniería", "Arquitectura",  "Proyectos internacionales", "Ofertas", "Empresas"],
    autocomplete: {delay: 0, minLength: 2},
    showAutocompleteOnFocus: false,
    removeConfirmation: false,
    caseSensitive: true,
    allowDuplicates: false,
    allowSpaces: false,
    readOnly: false,
    tagLimit: null,
    singleField: false,
    singleFieldDelimiter: ',',
    singleFieldNode: null,
    tabIndex: null,
    placeholderText: null,

    // Events
    beforeTagAdded: function(event, ui) {
        console.log(ui.tag);
    },
    afterTagAdded: function(event, ui) {
        console.log(ui.tag);
    },
    beforeTagRemoved: function(event, ui) {
        console.log(ui.tag);
    },
    onTagExists: function(event, ui) {
        console.log(ui.tag);
    },
    onTagClicked: function(event, ui) {
        console.log(ui.tag);
    },
    onTagLimitExceeded: function(event, ui) {
        console.log(ui.tag);
    }

});

<?php $this->Html->scriptEnd();
?>

<div class="box">
    <div class="box-header">
      <h2><i class="fa fa-edit"></i><?php echo __('Editar proyecto').' '.$defaultName;?></h2>
    </div>
    <?= $this->Form->create('UserProject', array('action' => 'add', 'class' => 'form-horizontal rellenar-campos', 'type' => 'file')); ?>
    <div class="box-content">
      <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('El proyecto pertenece a la compañía actual');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <input type="radio" id="actualCompanySi" name="UserProject[actual_company]" <?php if ($defaultCompany) {echo "checked";}?> value="S" style="margin-right:6px;"><?php echo __('Si');?>
                  <input type="radio" id="actualCompanyNo" name="UserProject[actual_company]" <?php if (!$defaultCompany) {echo "checked";}?> value="N" style="  margin-left: 6px;margin-right:6px;"><?php echo __('No');?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Nombre de la empresa*:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php echo $this->Form->input('CompanyName',array('label' => false,'class' => 'ui-autocomplete-input span8','value' => $defaultCompanyName));?>
                  <?php echo $this->Form->input('CompanyNameHidden',array('value' => $user['Company']['name'],'type' => 'hidden'));?>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Nombre del proyecto*:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php echo $this->Form->input('name',array('value' =>$defaultName,'label' => false,'class' => 'ui-autocomplete-input span8'));?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Fecha Inicio/ Fin - Desde*:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <input type="text" id="UserProjectInitDate" value="<?php echo $defaultDataInit?>" name="UserProject[init_date]" class="datepicker" placeholder="mm/yyyy" />
                </div>
              </div>
              
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Hasta*:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <input type="text" id="UserProjectEndDate" value="<?php echo $defaultDataEnd?>" name="UserProject[end_date]" class="datepicker" placeholder="mm/yyyy" />
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php
                    echo $this->Form->input('on_going',array('type' => 'checkbox','label' => __('Proyecto en curso')));
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Nombre del cliente:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php echo $this->Form->input('custome',array('value' => $defaultCustomerName,'label' => false,'class' => 'ui-autocomplete-input span8'));?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Sector del proyecto:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php echo $this->Form->input('sector_proj',array('value' => $sectorProjects[$defaultSectorProjectId],'label' => false,'class' => 'ui-autocomplete-input span8'));?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Presupuesto del proyecto*:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php 
                  echo $this->Form->input('budge',array('label' => false,'class' => 'money','value' => $defaultBudget));
                  echo $this->Form->input('budget_currency_id',array('selected' => $defaultBudgetCurrencyId,'label' => false,'div' => false,'style' => 'width:200px;height:26px;'));
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Volumen del proyecto:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php echo $this->Form->input('volumes',array('type' => 'textarea','value' => $defaultVolumes,'label' => false));?>
                </div>
              </div>
        </div>
        <div class="col-md-12">
          <div class="row">
              <input id="pac-input" class="controls" type="text" placeholder="<?php echo __('Busca la ubicación de tu proyecto');?>">
              <div class="col-sm-12 col-md-12 col-xs-12">
                <p><?php echo __('Haga una búsqueda en el mapa y en caso de no encontrar dirección exacta arrastre el puntero hasta la zona dónde está su proyecto para poder continuar')?></p>
                <div id="map-canvas" class="col-md-9 col-xs-9 -col-sm-9"></div>
                <div class="col-md-3 col-sm-3 col-xs-3">
                  <?php 

                  echo $this->Form->input('country_namex', array('value' => $defaultCountryName,'label'=>__('País de ejecución:'),'readonly' => 'readonly'));
                  echo $this->Form->input('country_id', array('value'=>$defaultCountryId,'label'=>false, 'class'=>'hidden'));
                  echo $this->Form->input('latitude', array('value' => $defaultLatitude,'label'=>false, 'class'=>'hidden'));
                  echo $this->Form->input('longitude', array('value' => $defaultLongitude,'label'=>false, 'class'=>'hidden'));
                  echo $this->Form->input('cit',array('label'=>__('Ciudad de ejecución:'),'value' => $defaultCityName,'class' => 'ui-autocomplete-input ok_input span8','readonly' => 'readonly'));
                  echo $this->Form->input('cit_id', array('label'=>false, 'type'=>'hidden'));
                  echo $this->Form->input('address',array('value' => $defaultAddress,'label'=>__('Dirección:'),'class' => 'hidden', 'label' => false));
                  echo $this->Form->input('complet_address', array('value' => $defaultCompletAddress,'label'=>false,'type' => 'hidden'));
                  echo $this->Form->input('id',array('value' => $defaultProject,'class' => 'hidden','label' => false));
                  ?>
                </div>
              </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Sector del servicio:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php 
                if (!empty($defaultSectorServiceId)) {
                echo $this->Form->input('sector_servic',array('value' => $sectorServices[$defaultSectorServiceId],'label'=>false,'class' => 'ui-autocomplete-input ok_input span8'));
              } else {
                echo $this->Form->input('sector_servic',array('label'=>false,'class' => 'ui-autocomplete-input ok_input span8'));
              }
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Función durante su servicio:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php 
                echo $this->Form->input('functionalit',array('label'=>false,'value' => $defaultFunctionality, 'class' => 'ui-autocomplete-input ok_input span8'));
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Subfunción:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php 
              echo $this->Form->input('departmen',array('value' => $defaultDepartment,'label'=>false,'class' => 'ui-autocomplete-input ok_input span8'));
              ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Duración de su servicio:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <input type="text" name="data[UserProject][duration_year]" value="<?php echo $defaultDurationYear;?>" class="tiempo" placeholder="<?=__('Años');?>"><?php echo __('Años');?>
              <input type="text" name="data[UserProject][duration_month]" value="<?php echo $defaultDurationMonth;?>" class="tiempo" placeholder="<?=__('Meses');?>"><?php echo __('Meses');?>
              <input type="text" name="data[UserProject][duration_day]" value="<?php echo $defaultDurationDay;?>" class="tiempo" placeholder="<?=__('Días');?>"><?php echo __('Días');?>
              <input type="text" name="data[UserProject][duration_hours]" value="<?php echo $defaultDurationHour;?>" class="tiempo" placeholder="<?=__('Horas');?>"><?php echo __('Horas');?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Palabras clave:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <ul id="myTags">
                <?php
                  if (!empty($defaultTags) && (json_decode($defaultTags) != false))
                  {
                    $tags = json_decode($defaultTags);
                    foreach ($tags as $tag) {
                      echo "<li>".$tag."</li>";
                    }

                }
                ?>
                  <!-- Existing list items will be pre-added to the tags -->
              </ul>
            </div>
          </div> 
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Descripción:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php echo $this->Form->textarea('description',array('value' => $defaultDescription,'id'=>'area1','style'=>'width:450px;'));?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Dónde se verá su CV:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <input type="radio" id="wheretosee1" name="data[UserProject][where_to_see]" <?php if ($defaultWhereToSee == 'M') echo "checked";?>  value="M" style="margin-right:6px;"><?php echo __('Mundial');?>
              <input type="radio" id="wheretosee2" <?php if ($defaultWhereToSee == 'N') echo "checked";?>  name="data[UserProject][where_to_see]" value="N" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Nacional');?>
              <input type="radio" id="wheretosee3" <?php if ($defaultWhereToSee == 'I') echo "checked";?>  name="data[UserProject][where_to_see]" value="I" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Interno');?>
            </div>
          </div>   
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Quién lo verá:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <input type="radio" id="whocansee1" name="data[UserProject][who_can_see]" <?php if ($defaultWhoCanSee == 'T') echo "checked";?> value="T" style="margin-right:6px;"><?php echo __('Todos');?>
              <input type="radio" id="whocansee2"  <?php if ($defaultWhoCanSee == 'C') echo "checked";?> 
              name="data[UserProject][who_can_see]" value="C" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Colegas');?>
              <input type="radio" id="whocansee3" <?php if ($defaultWhoCanSee == 'U') echo "checked";?> name="data[UserProject][who_can_see]" value="U" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Usuarios');?>
              <input type="radio" id="whocansee4"  <?php if ($defaultWhoCanSee == 'E') echo "checked";?>  name="data[UserProject][who_can_see]" value="E" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Empresas');?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
           <div class="actions">
                <center>
                  <?php $button = '<button type="button" id="but1" class="btn">'.__('Guardar').'</button>';
                  echo $button;?>
                </center>
            </div>
        </div>
      </div>
    </div>
    </form>
</div>
<style>
.rellenar-campos input, textarea, select {
  margin: 3px;
  padding: 2px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border: 1px solid #999999;
  border-color: #47789f;
}
#map-canvas {
  height: 400px;
  width: 70%;
  margin: 0px;
  border-color: #47789f;
}
#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.ui-datepicker-calendar {
    display: none;
    }

.pac-container {
  font-family: Roboto;
  font-color: #47789f
}
.tiempo {
  width:60px;
}
.btn-success2 {
  background-color: #47789f;
  color:#fff !important;
}

.myTags {
  border: 1px solid #799cb9;
  border-radius: 5px;
  margin-bottom: 5px;
}
</style>