<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */
echo $this->Html->script(array(
  'jquery-1.11.3.min'
), array('inline' => false));

if (!Configure::read('debug')):
  throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>
<!-- <h2>h2</h2> -->
<div class="row containgener">
<div id='map'></div>
        <script>
        // Provide your access token
        L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
        // Create a map in the div #map
        // Create array of lat,lon points.
        
        // Define polyline options
        // http://leafletjs.com/reference.html#polyline
        var map = L.mapbox.map('map', 'egalles79.m2k17jkm')
          .setView([40.731, -6.438],2);
        // Defining a polygon here instead of a polyline will connect the
        // endpoints and fill the path.
        // http://leafletjs.com/reference.html#polygon
        //map.addLayer(line_points);
        </script>
  <section class="decontornno">
    <!-- Inicio columna de la izda la del logo --> 
    <div class="grid_2 logo globes">
      <a href="http://www.globeshake.com/test/"><img src="http://www.globeshake.com/test/images/logo_Globeshake.png" title="GlobeShake" alt="GlobeShake"></a>   
    </div>
  </section>
  
  <section>
    <div class="row grid_10 hregistr_com">
      <div class="hregistr_tit"><?php echo __('Una oportunidad de poner a su empresa en el Mapa');?></div>
      <span class="hregistr_sub">Construcción, ingeniería, arquitectura e industria asociada</span>
    </div>
  </section>   <!-- Fin de la parte de la cabecera... lo de dar una oportunidad y la siguiente línea--> 
  <div class="hregistr_subtit">
    <ul class="sf-menu colorsubconttootorre sf-js-enabled sf-shadow">
      <div class="mine">
        <li class="current selectedLava"><a class="colorsubconttoo hregistr_enl" href="index.html">Concepto</a>
        </li>
        <li class="current selectedLava"><a class="colorsubconttoo hregistr_enl" href="index.html">Paso a paso</a>
        </li>
        <li class="current selectedLava"><a class="colorsubconttoo hregistr_enl" href="index.html">Servicios</a>
        </li>
      </div>
      
    <li class="back" style="left: 0px; width: 115px; height: 18px;"><div class="left"></div></li></ul><!-- end menu -->
  </div><!-- Fin de la botonera dar a conocer, control de inteligencia y control licitaciones--> 
  <section>
    <?php echo $this->Session->flash(); ?>
    <div class="grid_4 hregistr_dosgrup">
      <div class="grid_12 hregistr_subtitemp">Empiece obteniendo las primeras tarjetas de visita gratuitas ya</div>
      <div class="hregistr_flech"><img src="http://www.globeshake.com/test/images/flecha_regist.png" alt="Flecha Registro"></div>
    </div><!-- Fin de los div... empieze y flecha-->

    <div class="grid_5 hregistr_conttarjet"><!-- Comienzo de la tarjeta de registro-->
      <div class="grid_5 hregistr_tarjetavis"></div>
      <div class="grid_5 hregistr_tarjetavisdos"></div>
      <div class="grid_5 hregistr_tarjetavistres">
        <div class="grid_12 hregistr_lgris">
          <img src="http://www.globeshake.com/test/images/logoglobeshake_gris.png" alt="GlobeShake logo gris">
        </div>
          
          <?php echo $this->Form->create('User',array('action' => 'add','class' => 'form-horizontal'));?>
          
            <div class="col-md-6">
              <?php echo $this->Form->input('firstname',array('label' => false,'required','placeholder' => __('Nombre')));?>
            </div>
            <div class="col-md-6">
              <?php echo $this->Form->input('lastname',array('label' => false,'required','placeholder' =>  __('Apellidos')));?>
            </div>
            <div class="col-md-6" style="margin-top:10px">
              <?php echo $this->Form->input('email',array('label' => false,'required','placeholder' => __('Correo electrónico')));?>
            </div>
            <div class="col-md-6" style="margin-top:10px">
              <?php echo $this->Form->input('country_id',array('label' => false,'style' => ('width:150px;padding:10px'),'options' => $optionsCountries,'class' => false,'selected' => $selectedCountry));?>
            </div>
            <div class="col-md-12"></div>
            <div class="col-md-6" style="margin-top:10px">
              <?php echo $this->Form->input('password',array('class' => 'false','required','label' => false,'placeholder' => __('Contraseña')));?>
            </div>
            <div class="col-md-6" style="margin-top:10px">
              <?php echo $this->Form->input('repeat_password',array('label' => false,'required','placeholder' => __('Repita Contraseña')));?>
            </div>
            <?php /*
            echo $this->Form->input('group_id',array('label' => false,'style' => ('width:150px;padding:10px'),'options' => $optionsGroups,'class' => false)); */?>

            
        </form>
        <p style="margin-top:4px;margin-left:15px">Al registrarme acepto la política de privacidad y recuerdo la contraseña.</p>
      </div>
    </div><!-- Fin de la tarjeta de registro-->

    <!-- Aquí se meterá en un futuro información, dejo ya el div vacio-->
    <div class="grid_5">
    </div>
    <!-- Fin de este div vacio-->
    <div class="grid_4 hregistr_mapamunduno">
      
    </div><!-- Fin de la línea al registrarme.....-->
</section><!-- Fin del bloque horizontal central.....-->
</div>

<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#window_area .login').click(function(){
    if($(this).hasClass('active')) $('.header_window').hide();
    else $('.header_window').show(100);
    $(this).toggleClass('active');
  });
});
<?php $this->Html->scriptEnd();
?>