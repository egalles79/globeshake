<?php
/**
 * User: Eloi Gallés Villaplana
 * Date: 30/04/14
 * Time: 01:49
 * @file panel.ctp
 */
if(!isset($breadcrumb)){
    $cName = Inflector::humanize($this->request->params['controller']);
    $aName = Inflector::humanize($this->request->params['action']);
    $breadcrumb = array(
        __($cName) => array(
            'url' => '/'.$this->request->params['controller'],
            'active' => false
        ),
        __($aName) => array(
            'url' => '/'.$this->request->params['controller'].'/'.$this->request->params['action'],
            'active' => true
        )
    );
    $this->set('breadcrum',$breadcrumb);
}
echo $this->Html->css(array('glyphicons', 'estilo'));
?>
<?= $this->element('top_bar');?>
<div class="container">
    <div class="row">
        <?php
        echo $this->element('menu');
        ?>
        <div id="content" class="col-lg-10 col-md-10 col-sm-11">
            <?= $this->element('breadcrumb', $breadcrumb);?>
            <?php echo $this->Session->flash(); ?>
            <?= $this->fetch('content');?>
        </div>
    </div>
</div>

<?= $this->fetch('modal'); ?>

<div class="clearfix"></div>
<footer>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-3">
                &copy; <?php echo __('GlobeShake BETA Copyright @ %s',date('Y'));?>
            </div>
            <div class="col-sm-2">
                <?php echo $this->Html->link(__('Copyright'),'#');?>
            </div>
            <div class="col-sm-2">
                <?php echo $this->Html->link(__('Condiciones de uso'),'#');?>
            </div>
            <div class="col-sm-2">
                <?php echo $this->Html->link(__('Política de Privacidad'),'#');?>
            </div>
            <div class="col-sm-2">
                <?php echo $this->Html->link(__('Directrices comunitarias'),'#');?>
            </div>
            <div class="col-sm-1">
                <?php echo $this->Html->link(__('Blog'),'#');?>
            </div>
        </div><!--/.col-->
    </div><!--/.row-->
</footer>
