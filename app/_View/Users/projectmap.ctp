  <?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));
?>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#menu1').removeClass('active2');
  $('#menu2').addClass('active2');

  //bootbox.alert('Bon dia');
});
<?php $this->Html->scriptEnd();
?>
<?php // echo $this->Element('card');

?>
<script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet' />
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet' />

<div id="mobile">
<div class='custom-popup' id="map" style="position:absolute;"></div>
  <div id="buttons">
      <div class="tbuttons"><a href="/Users/cvmap"><?php echo __('CV personal GS');?></a></div>
      <div class="tbuttons active"><a href="/Users/projectmap"><?php echo __('Proyectos');?></div>
      <div class="tbuttons"><a href="/Users/contactsmap"><?php echo __('Contactos');?></a></div>
    </div>
  </div>
</div>
<script>
L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
// Here we don't use the second argument to map, since that would automatically
// load in non-clustered markers from the layer. Instead we add just the
// backing tileLayer, and then use the featureLayer only for its data.
var map = L.mapbox.map('map')
    .setView([40.731, -6.438],3)
    .addLayer(L.mapbox.tileLayer('mapbox.streets'));

var testMarkers = new L.markerClusterGroup();
var projects = JSON.parse(JSON.stringify(<?php echo json_encode($cprojects)?>));
var countryNames = JSON.parse(JSON.stringify(<?php echo json_encode($staticCountriesNames)?>));

var coords = new Array();
$.each(projects, function(index, value) {
  coords.push(value);
});
urldomain = "http://<?php echo $_SERVER["SERVER_NAME"]?>";
for (var i = 0; i < coords.length; i++) {
    var marker = 
      L.mapbox.featureLayer({
        type: 'Feature',
        properties: {
              'title': '<div style="float:left"><a href="'+urldomain+'/Users/ver_cv"><img src="'+urldomain+'/img/lupaicon.png"></a></div><div style="text-align:center"><strong>'+coords[i]['UserProject']['name'].toUpperCase()+'</strong></div>',
              'marker-color': '#f86767',
              'description': '<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-4"><i class="fa fa-home">&nbsp;&nbsp;<?php echo __("Ciudad")?>: </i></div><div class="col-md-8"><div class="new_font">'+coords[i]['City']['name']+'</div></div><div class="col-md-12"><div class="row"><div class="col-md-4"><i class="fa fa-phone">&nbsp;&nbsp;<?php echo __("País")?>: </i></div><div class="col-md-8"><div class="new_font">'+countryNames[coords[i]['Country']['id']]+'</div></div></div>'
        },
        geometry: {
            type: 'Point',
            coordinates: [coords[i]['Geolocalitzation']['longitude'], coords[i]['Geolocalitzation']['latitude']]
        },
    });
    testMarkers.addLayer(marker);
}

testMarkers.eachLayer(function(layer) {
    // here you call `bindPopup` with a string of HTML you create - the feature
    // properties declared above are available under `layer.feature.properties`
    var title = layer.feature.properties.title;
    var content = title+layer.feature.properties.description;
    
    layer.bindPopup(content);
});

var bounds = testMarkers.getBounds();
//map.fitBounds(bounds);

map.addLayer(testMarkers);
</script>
<style>
#buttons {
  z-index: 100;
  position: relative;
  float: right;
  margin-right: 20px;
}
.tbuttons {
    background-color: #47789F;
    font-size: 0.9em;    
    color: #fff;
    text-align: center;
    -webkit-box-shadow: 7px 4px 6px -3px #333333;
    -moz-box-shadow: 7px 4px 6px -3px #333333;
    box-shadow: 7px 4px 6px -3px #333333;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    float: left;
    margin-left: 10px;
    padding: 5px;
    padding-left: 20px;
    padding-right: 20px;
}
.tbuttons a {
  text-decoration: none;
  color: #fff;
}
.active {
  background-color: #66A0C6
}

.custom-popup .leaflet-popup-content-wrapper {
  background:#e6ebf1;
  color:#47789f;
  font-size: 13px;
  line-height:24px;
  width:300px;
  -webkit-box-shadow: 14px 8px 12px -6px #333333;
    -moz-box-shadow: 14px 8px 12px -6px #333333;
    box-shadow: 14px 8px 12px -6px #333333;
  }

.custom-popup .leaflet-popup-tip-container {
  width:10px;
  height:15px;
  }
.custom-popup .leaflet-popup-tip {
  border-left:15px solid transparent;
  border-right:15px solid transparent;
  border-top:15px solid #2c3e50;
  }
  .custom-popup .brumdos {
    position: absolute;
    top: 2px;
    left: 20px;
  }
  .custom-popup .marker-title {
    text-align: center;
    margin-bottom: 0px;
  }
  .new_font {
      padding: 3px;
      border: 1px solid #517fa4;
      min-height: 20px; 
      text-align: center; 
      margin-bottom: 8px; 
    font-size: 13px;
        background: white;
    border-radius: 4px;
  }
  .marker-description {
    padding: 20px;
  }
  .buttonadd {
    background-color: #47789f;
    color:#fff;
    text-align:center;
    margin-top: 10px;
        border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
  }
  .buttonadd a {
    text-decoration: none;
    color: #fff;
  }
  .closes {
    cursor: hand;
    float: right;
  }


</style>