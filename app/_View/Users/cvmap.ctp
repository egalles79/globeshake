<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'jquery.mockjax.min',
  'bootstrap-editable.min',
  'moment.min',
  'typeahead.min',
  'typeaheadjs.min',
  'address.min',
  'select2.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));
?>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#menu2').addClass('active2');
  $('#menu1').removeClass('active2');

  //bootbox.alert('Bon dia');
});
<?php $this->Html->scriptEnd();
?>
<?php // echo $this->Element('card');

?>

<div id="mobile">
  <div id="map" style="position:absolute;"></div>
  <div id="buttons">
      <div class="tbuttons active"><?php echo __('CV personal GS');?></a></div>
      <div class="tbuttons"><a href="/Users/projectmap"><?php echo __('Proyectos');?></a></div>
      <div class="tbuttons"><a href="/Users/contactsmap"><?php echo __('Contactos');?></a></div>
    </div>
  </div>
  <input type="hidden" id="staticCompanyName" value="<?php echo $staticCompanyNames[$companyId];?>" />
<script>
// Provide your access token
L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
// Create a map in the div #map
// Create array of lat,lon points.

// Define polyline options
// http://leafletjs.com/reference.html#polyline
var map = L.mapbox.map('map', 'egalles79.mcf2id2p')
  .setView([40.731, -6.438],3);
        var countryNames = JSON.parse(JSON.stringify(<?php echo $namesForMap?>));

        var countries = JSON.parse(JSON.stringify(<?php echo $countryPoints?>));
        var countryWithProjects = JSON.parse(JSON.stringify(<?php echo $countryWithProjects?>));

        // Define polyline options
        // http://leafletjs.com/reference.html#polyline
        
        // Defining a polygon here instead of a polyline will connect the
        // endpoints and fill the path.
        // http://leafletjs.com/reference.html#polygon
        var layerPoints = [];
        var myPoints = [];
        var clickpoints = [];
        map.featureLayer.on('ready', function(e) {
          var company_name = $('#staticCompanyName').val();
          var i = 0;
          $.each(countries, function(index, country) {
            hasProjects = false;
            myCountryProjects = [];
            
            if (countryWithProjects[index] != undefined) {
              myCountryProjects.push(index);
              var hasProjects = true;
            } else {
              var hasProjects = false;
            }
            
            var valueToPush = {};
            valueToPush.hasprojects = hasProjects;
            if (hasProjects) {
              myColor = '#'+Math.floor(Math.random()*16777215).toString(16);
              myOpacity = 0.6;
            } else {
              myColor = '#ddd';
              myOpacity = 0;

            }
            
            var polyline_options = {
              color: myColor,
              fill: myColor,
              fillOpacity: myOpacity,
              stroke: 0
            };
            myPoints = [];
            cprepeated = [];
            $.each(country.countrypoints, function( index2, countrypointline ) {
              var a = cprepeated.indexOf(countrypointline);
              if (a == -1) {
                if (typeof(countrypointline) == 'string') {
                  linepointsCountry = "["+countrypointline+"]";
                  linepointsCountry = JSON.parse(linepointsCountry);
                  myPoints.push(linepointsCountry);
                }
              }
              
            });
            layerPoints = [];
            valuesLayerPoints = [];
            $.each(myPoints, function( index, value ) {
              layerPoints.push(L.polyline(value, polyline_options).addTo(map));
            });
            valueToPush.name = countryNames[index];
            valueToPush.points = layerPoints;
            if (countryWithProjects[index] != undefined) {
              valueToPush.general                = countryWithProjects[index];
              valueToPush.projectsongoing        = countryWithProjects[index]['projectsongoing'];
              valueToPush.projectsincomplete     = countryWithProjects[index]['projectsincomplete'];
              valueToPush.projects_in_country    = countryWithProjects[index]['projects_in_country'];
              valueToPush.total_company_projects = countryWithProjects[index]['total_company_projects'];
            }
            valueToPush.company_name = company_name;
            clickpoints.push(valueToPush);
            layerPoints = [];
          }); 
          $.each(clickpoints, function( indexes, values ) {
             btn = values.points;
             $.each(btn, function (index2,buttons) {
                buttons.on('click',function() {
                  if (values.hasprojects) {
                    text = '<div class="pop-up-map" style=""><div class="row-fluid"><div class="span12" style="  min-height: 10px;"><a href="#"><p class="title">'+values.name+'</p></a></div><div class="row-fluid"><div class="span12"><a href="#"><p class="title">Posicionamiento perfil <span>30%</span></p></a></div></div><div class="row-fluid"><div class="span12"><div class="span6 icon2">'+"<?php echo __('Tus contactos');?>"+' :</div><a class="span6" href="#"><div class="new_font">'+values.projects_in_country+'</div></a></div><div class="row-fluid"><div class="span12"><div class="span6 icon03p">'+"<?php echo __('CV visto');?>"+':</div><a class="span6" href=""><div class="new_font">'+values.projectsincomplete+' vez/ces</div></a></div></div><div class="row-fluid"><div class="span12"></div></div><div class="row-fluid"><div class="span12"><button id="forta" class="accept">'+"<?php echo __('Fortalezca potencial');?>"+'</button></div></div></div><div class="row-fluid"><div class="span12"><button id="forta" class="accept">'+"<?php echo __('No especialista / Regístrate');?>"+'</button><div class="row-fluid nope_seen" style=""><div class="span12"><form action="/UserProjects/add"><button type="submit" id="" class="accept">'+"<?php echo __('10 nuevos contactes / Añade');?>"+'</button></form></div><div class="row-fluid nope_seen" style=""><div class="span12"><form action="/Cards"><button type="submit" id="" class="accept">'+"<?php echo __('Comprar tarjetas');?>"+'</button></form></div></div></div></div>';
                  } else {
                    
                      text = '<div class="pop-up-map" style=""><div class="row-fluid"><div class="span12"><a href="#"><p class="title">'+values.name+'</p></a></div><div class="row-fluid"><div class="span12"><div class="span6 icon2">'+I18nJs.t('Posicionamiento')+' '+values.company_name+'</div><a class="span6" href=""><div class=" new_font">0%</div></a></div><div class="row-fluid"><div class="span12"><div class="span6 icon2">'+I18nJs.t('Proyectos totales')+' '+values.company_name+'</div><a class="span6" href=""><div class=" new_font">0</div></a></div></div></div><div class="row-fluid " style=""><div class="span12"><a class="accept" href="/UserProjects/add">'+I18nJs.t('Añadir proyecto')+'</a><a></a></div></div></div></div>';
                  }
                  bootbox.alert(text);
                }); 
             });
          });
        });
// Defining a polygon here instead of a polyline will connect the
// endpoints and fill the path.
// http://leafletjs.com/reference.html#polygon
//map.addLayer(line_points);
</script>
<style>
.modal-dialog{
	width: 300px;
	height: 100%;
}
	p.title{
		padding-bottom: 0px;
		    text-align: center;
    margin-bottom: 0px;
    font-size: 16px;
    text-transform: uppercase;
	}
#buttons {
  z-index: 100;
  position: relative;
  float: right;
  margin-right: 20px;
}
.tbuttons {
    background-color: #47789F;
    font-size: 0.9em;    
    color: #fff;
    text-align: center;
    -webkit-box-shadow: 7px 4px 6px -3px #333333;
    -moz-box-shadow: 7px 4px 6px -3px #333333;
    box-shadow: 7px 4px 6px -3px #333333;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    float: left;
    margin-left: 10px;
    padding: 5px;
    padding-left: 20px;
    padding-right: 20px;
}
.accept{
text-shadow: 0 1px 1px rgba(0,0,0,.3);
-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
box-shadow: 0 1px 2px rgba(0,0,0,.2);
background: #f78d1d;
background: -webkit-gradient(linear, left top, left bottom, from(#c5a653), to(#ddc079));
background: -moz-linear-gradient(top, #c5a653, #ddc079);
background-color: #C2A34E;
color: white!important;
background-color: #C5A653;
text-shadow: 0 1px 1px rgba(0,0,0,.3);
-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
box-shadow: 0 1px 2px rgba(0,0,0,.2);
background: #f78d1d;
background: -webkit-gradient(linear, left top, left bottom, from(#ddc079), to(#c5a653));
background: -moz-linear-gradient(top, #ddc079, #c5a653);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ddc079', endColorstr='#c5a653');
background-color: #C2A34E;
border: medium none;
width: 150px !important;
border-radius: 4px;
-moz-border-radius: 4px;
-webkit-border-radius: 4px;
font-size: 1.4em !important;
padding: 6px;
-webkit-box-shadow: 5px 6px 7px -3px #adadad;
-moz-box-shadow: 5px 6px 7px -3px #adadad;
box-shadow: 5px 6px 7px -3px #adadad;
margin: 20px auto 0;
display: block;
}
.accept:hover{
	text-shadow: 0 1px 1px rgba(0,0,0,.3);
-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
-moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
box-shadow: 0 1px 2px rgba(0,0,0,.2);
background: #f78d1d;
background: -webkit-gradient(linear, left top, left bottom, from(#c5a653), to(#ddc079));
background: -moz-linear-gradient(top, #c5a653, #ddc079);
background-color: #C2A34E;
color: white!important;
}
.tbuttons a {
  text-decoration: none;
  color: #fff;
}
.active {
  background-color: #66A0C6
}
.modal-content{
	top: 20%;
	    background: #e6ebf1;
    color: #47789f;
    font-size: 13px;
    line-height: 24px;
    width: 300px;
    -webkit-box-shadow: 14px 8px 12px -6px #333333;
    -moz-box-shadow: 14px 8px 12px -6px #333333;
    box-shadow: 14px 8px 12px -6px #333333;
        border-radius: 3px;
}
.modal-footer{
	display: none;
}
.pop-up-map{
	width: 400px;
	max-width: 100%;
}
.pop-up-map small{
	    font-size: 10px;
    line-height: 12px;
    margin: 5px 0px;
    display: block;
}
.pop-up-map .icon2,.pop-up-map .iconcasc,.pop-up-map .iconsol,.pop-up-map .iconteam, .pop-up-map .icon03p {
	  background-repeat: no-repeat;
  padding-left: 36px;
  line-height: 12px;
  background-size: 28px;
}
.pop-up-map .new_font{
	padding: 3px;
    border: 1px solid #517fa4;
    min-height: 20px;
    text-align: center;
    margin-bottom: 8px;
    font-size: 13px;
    background: white;
    border-radius: 4px;
}
.pop-up-map .span12 button,a.accept{
	font-size: 12px!important;
  padding: 5px!important;
  width: 100%!important;
  margin-top: 10px;
      text-align: center;
}
.pop-up-map .title{
	text-align: center;
	margin-bottom: 0px;
}
.pop-up-map .brum{
	    position: absolute;
    top: 35px;
    right: 16px;
    width: 15px;
}
.pop-up-map .brumdos{
	  position: absolute;
  top: 2px;
  left: 20px;
  width: 28px;
}
.row-fluid {
  width: 100%;
  *zoom: 1;
}

.row-fluid:before,
.row-fluid:after {
  display: table;
  line-height: 0;
  content: "";
}

.row-fluid:after {
  clear: both;
}

.row-fluid [class*="span"] {
  display: block;
  float: left;
  width: 100%;
  min-height: 30px;
  margin-left: 2.127659574468085%;
  *margin-left: 2.074468085106383%;
  -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
}

.row-fluid [class*="span"]:first-child {
  margin-left: 0;
}

.row-fluid .controls-row [class*="span"] + [class*="span"] {
  margin-left: 2.127659574468085%;
}

.row-fluid .span12 {
  width: 100%;
  *width: 99.94680851063829%;
}

.row-fluid .span6 {
  width: 48.93617021276595%;
  *width: 48.88297872340425%;
}


.icon{
	background-image: url(../img/icon_03.png);
}
.icon2{
	background-image: url(../img/icon_07.png);
}
.icon03p{
  background-image: url(../img/icon_03p.png);
}
.iconsol{
	background-image: url(../img/logo_hombre_casco.png);
}
.iconteam{
	background-image: url(../img/icon_07p.png);
}
.iconcasc{
	background-image: url(../img/logo_hombre_casco.png);
	background-repeat: no-repeat;
}
.icon3{
	background-image: url(../img/icon_10.png);
}
.icon4{
	background-image: url(../img/icon_14.png);
}
.icon5{
	background-image: url(../img/icon_17.png);
}
.icon6{
	background-image: url(../img/icon_20.png);
}
</style>