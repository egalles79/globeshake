<?php
$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'jquery.mockjax.min',
  'bootstrap-editable.min',
  'moment.min',
  'typeahead.bundle',
  'form-x-editable-demo',
  'custom.min',
  'core.min',
  'bootbox.min',
  'jquery.flip.min'
), array('inline' => false));
/*
  'typeaheadjs.10x.js',
  'form-x-editable',
  'form-x-editable-demo',
  */
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){

  $("#flipPad a:not(.revert)").bind("click",function(){
    var $this = $(this);
    $(".tarjvitr_contres").flip({
      direction: $this.attr("rel"),
      content: '<a href="#" class="revert"><?php echo $this->Html->image('ven.png');?></a><div class="tarjvitr_concuatro"><div class="tarjvitr_mcv">MI CV</div><h6 class="sub_title">Su CV on-line, proyecto a proyecto le hará visible.</h6><a href="#">+ Anadir proyecto</a><div class="tarjvitr_concuatrocont"><div class="tarjvitr_concuatroactual"><img src="<?php echo IMAGES_URL;?>industri_verde.png" alt="Industria proyecto actual" title="Industria proyecto actual">PROYECTO 1:<span class="tarjvitr_concuatroactualdef"> Lorem ipsum dolor sit amet ipsum.</span></div><div class="tarjvitr_concuatroactualdos"><img src="<?php echo IMAGES_URL;?>industri_gris.png" alt="Industria proyecto actual" title="Industria proyecto actual">PROYECTO 2:<span class="tarjvitr_concuatroactualdefdos"> Lorem ipsum dolor sit amet ipsum.</span></div><div class="tarjvitr_concuatroactualtres"><img src="<?php echo IMAGES_URL;?>industri_gris.png" alt="Industria proyecto actual" title="Industria proyecto actual">PROYECTO 3:<span class="tarjvitr_concuatroactualdeftres"> Lorem ipsum dolor sit amet ipsum.</span></div><div class="tarjvitr_concuatroactualcuatr"><img src="<?php echo IMAGES_URL;?>industri_gris.png" alt="Industria proyecto actual" title="Industria proyecto actual">PROYECTO 4:<span class="tarjvitr_concuatroactualdefcuatr"> Lorem ipsum dolor sit amet ipsum.</span></div></div><div class="tarjvitr_cvempresdos"><a href="#" class="tarjvitr_en">RESUMEN MI CV</a></div><div class="tarjvitr_accredes"><p class="tarjvitr_ress">Acceso a mis redes sociales</p><input class="tarjvitr_eplauttres" type="radio" name="autonomosi" value="si"><span class="tarjvitr_eplauttressi">Si</span><input class="tarjvitr_eplautcuatr" type="radio" name="autonomosno" value="no"><span class="tarjvitr_eplautdosspa">No</span></div><div class="socialb social-headb"><a href="#" title="Facebook"><i class="icon-facebook"></i></a><a href="#" title="Twitter"><i class="icon-twitter"></i></a><a href="#" title="Rss"><i class="icon-rss"></i></a></div></div>',
      onBefore: function(){
        $(".revert").show();
        $('a.right').hide()
      },
    })
    return false;
  });

  $(".revert").bind("click",function(){
    $(".tarjvitr_contres").revertFlip();
    return false;
  });

  var mk = 0;
  var test = $('#pane-target')[0].scrollHeight;
  var limiting = test - 150;

  console.log(test);

  $('.num_pad_down').click(function() {
    if(test <= mk){
       mk += 31;
       console.log(mk);
      $('#pane-target').animate({ scrollTop: mk }, 600);
    } else{};
  });
  $('.num_pad_up').click(function() {
    if(mk >= 31){
      mk -= 31;
      console.log(mk);
      $('#pane-target').animate({ scrollTop: mk }, 600);
    } else{};
  });
  $(".continuar").bind("click",function(){
    $(".abso_tarjeta").flip({
      direction: 'tb',
      onEnd: function(){
        $('.abso_tarjeta').css("display","none");
        $('.tarjvitr_contres').css("display","block");
        $('#flipPad').css("display","block");
        $('#tarjvitr_aceptar').css("display","inline-block");
      },
    })
    return false;
  });
  
  $(function() {
    $( "#slider" ).slider({
      range: "min",
      value: "0",
      min:1,
      max: 100,
    });
    var percent = $( "#slider" ).slider( "value" );
    if (percent != 1) {
      $('.amount').append(percent);  
    }
    
  });

});
<?php $this->Html->scriptEnd();
  if (!empty($user['image'])) {
    $urlImage = IMAGES_URL.'uploads/users/'.$user['image'];
  }
  else {
    $urlImage = '/../img/camarafot.png';
  }
?>
  <div class="tarjvitr_container">
    <div class="tarjvitr_containertext"><?php echo __('Tarjeta de visita');?></div>
    <div class="col-sm-6 col-md-6">
      <div class="abso_tarjeta tarjeta">
        <div style="padding:20px;">
          <p>
            <span class="amount"><?php echo __('Ha rellenado un %d% de la tarjeta de visita',array($porcentaje));?>.<br>
          <?php echo __('Rellene todos los campos obligatorios');?>.
        </span></p>
           <div id="slider" style="pointer-events: none;"></div>
          <br><br><br>
          <div class="continuar"><?php echo __('Continuar');?></div> 
        </div>
      </div>
      <div class="tarjeta tarjvitr_con"></div>
      <div class="tarjeta tarjvitr_condos"></div>
      <div class="tarjeta tarjvitr_contres" style="display:none;">

      <div class="interior1">
        <div class="col-sm-12 col-md-12">
            <div class="row star">
              <?php 
              if (!empty($specialitiesFromUser)) { 
                echo $this->Html->image('star_03.png');
              } ?>
            </div>
          <!-- Tarjeta -->
          <?php
          echo $this->Form->create('User', array(
            'inputDefaults' => array(
                'div' => 'form-group',
                'class' => 'form-control',
                'type' => 'file'
            ),
            'type'=>'file'
          ));?>
          <input type="hidden" id="User.id" name="User[id]" value='<?php echo $user['User']['id']?>'>
          
            <div class="row" style="color: #333333;font-size: 16px;margin:auto;">
              <div class="col-md-12">
              <?php 
                    $sectorName = (!empty($user['User']['sector_id'])) ? $staticSectorsNames[$user['User']['sector_id']] : __('Línea de negocio'); ?>
                    <a href="#" style="border-bottom: 0px" id="sector" data-type="typeaheadjs" data-pk="1" data-original-title="<?php echo $sectorName;?>" class="editable editable-click" data-value=""><?php echo $sectorName;?></a>
                <div class="row">
                  <div class="col-md-3">
                    <?php 
                    $urlImage = (!empty($user['User']['photo_dir'])) ? $user['User']['photo'] : 'camarafot.png';
                    if ($urlImage == 'camarafot.png') {
                      $nameImage = $this->webroot.'img'.DS.$urlImage;  
                    } else {
                      $nameImage = $this->webroot.'files'.DS.'users'.DS.'user_'.$user['User']['id'].DS.$urlImage;
                    }
                    ?>
                    <img src="<?php echo  $nameImage?>" width="100px" heigth="100px" id="image1" type ='file' alt="<?php echo __('Imágen de perfil');?>" > 
                    <?php
                        echo $this->Form->input('User.image1',array('id'=> 'imgInp1','label' => false,'class'=>'hidden'));
                    ?>
                  </div>

                  <div class="col-md-9">
                    <div class="form-inline">
                      <label class="radio" style="cursor:pointer" >
                          <input type="radio" name="User[hasCompany]" id="hasCompanyA" value="A" style="cursor:pointer" 
                          <?php  if(empty($user['User']['company_id'])) {echo 'checked';}?>
                          />
                          <?php echo __('Soy autónomo').'&nbsp;&nbsp;&nbsp;&nbsp;'?>
                      </label>
                      <label class="radio" style="cursor:pointer">
                          <input type="radio" name="User[hasCompany]" id="hasCompanyE" value="E" style="cursor:pointer" 
                          <?php  if (!empty($user['User']['company_id'])) {echo 'checked';}?>
                          />
                          <?php echo __('Empleado');?>
                       </label>
                    </div>
                    <div class="row">
                      <?php
                        $companyName = (empty($user['User']['company_id'])) ? __('Sin empresa / Autónomo') : $staticCompanyNames[$user['User']['company_id']];
                        $companyNameValue = ($companyName !=__('Sin empresa / Autónomo')) ? $companyName : '';
                        ?>
                        <a href="#" id="company" data-type="typeaheadjs" data-pk="1" data-original-title="<?php echo __('Empresa');?>" class="editable editable-click" data-value="<?php echo $companyNameValue?>"><?php echo $companyName;?></a>
                    </div>

                    <div class="row">
                      <a href="#" id="firstname" data-type="text" data-pk="1" data-original-title="Nombre" class="editable editable-click">
                        <?=$user['User']['firstname'];?>
                      </a>&nbsp;&nbsp;
                      <a href="#" id="lastname" data-type="text" data-pk="1" data-original-title="Apellidos" class="editable editable-click">
                        <?=$user['User']['lastname'];?>
                      </a>
                    </div>

                    <div class="row">
                      <?php 
                      $function_name = (empty($user['User']['functionality_id'])) ? __('FUNCIÓN DENTRO DE LA EMPRESA') : $staticFunctionalitiesNames[$user['User']['functionality_id']];
                      $value_function_name = ($function_name != __('FUNCIÓN DENTRO DE LA EMPRESA')) ? $function_name : '';?>
                        <a href="#" id="functionality" data-type="typeaheadjs" data-pk="1" 
                         class="editable editable-click" data-value="<?php echo $value_function_name?>"><?php echo $function_name?></a>
                    </div>

                    <div class="row">
                      <?php 
                      $department_name = (!empty($user['User']['department_id'])) ? $staticDepartmentsNames[$user['User']['department_id']] : __('DEPARTAMENTO DENTRO DE LA EMPRESA');
                      $value_department_name = ($department_name != __('DEPARTAMENTO DENTRO DE LA EMPRESA')) ? $department_name : '';?>
                      <a href="#" id="department" data-type="typeaheadjs" data-pk="1" class="editable editable-click" data-value="<?php echo $value_department_name?>"><?php echo $department_name?></a>
                    </div>

                    <div class="row">
                      <?php 
                        $countryName = (!empty($user['User']['country_id'])) ? $staticCountriesNames[$user['User']['country_id']] : __('Seleccione país');
                        $countryNameValue = ($countryName != __('Seleccione país')) ? $countryName : '';
                      ?>
                      <a title="" data-original-title="" class="editable editable-click" href="#" id="country" data-type="select2" data-pk="1" data-value="<?php echo $countryNameValue?>" data-title="<?php echo __('Seleccione país');?>"><?php echo $countryName?></a>
                      <?php 
                        $cityName = (!empty($user['User']['city_id'])) ? $staticCitiesNames[$user['User']['city_id']] : __('Seleccione ciudad');
                        $cityNameValue = ($cityName != __('Seleccione ciudad')) ? $cityName : '';
                      ?>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <a href="#" id="city" data-type="typeaheadjs" data-pk="1" class="editable editable-click" data-value="<?php echo $cityNameValue?>"><?php echo $cityName?></a>
                    </div>
                  </div>      
                </div>
              </div>
              <div class="row-fluid bot">
                <div class="span8 edis">
                    <?php
                    $mail = $user['User']['email'];
                    $nameShort = (strlen($mail) > 25) ? substr($mail,0,25).'...' : $mail; 
                    echo $nameShort;
                    ?>
                    <input type="hidden" name="User[email]" value='<?php echo $mail?>'>
                </div>
                <div class="span8 edis">
                  <span class="address"><?php echo $company['Company']['address'];?></span>
                </div>
              </div>
              <div class="row-fluid" style="margin-top: 0px;font-size:12px;">
                <!-- <div class="span4"><a href="#">CV EMPRESA</a></div>-->
                <div class="text-center" style="margin-top:10px">
                <?php echo $this->Form->submit(__('GUARDAR'),array('class' => 'in_back tarj_front','style' => 'display:none;padding: 3px 107px;','id' => 'tarjvitr_aceptar'));
                echo $this->Form->end(); ?>
                </div>
              </div>
            </div>
          <!-- Fin tarjeta -->
        </div>
        <a class="pp_closedos" href="<?php echo $urlBase;?>/auth_public/buy_visit_card">Close</a>
      </div>
      <div id="flipPad" style="display:none;">
  				<a href="#" class="right" rel="lr" rev="#fff"><?php echo $this->Html->image('ven.png');?></a>
  				<a href="#" class="revert" style="display:none;"><?php echo $this->Html->image('ven.png');?></a>
      </div>
      <div class="tarjvitr_concuatro" style="display:none;">
        <a class="pp_closedos" href="<?php echo $urlBase;?>/auth_public/buy_visit_card">Close</a>
        <div class="tarjvitr_mcv">
          <a href="<?php echo $urlBase?>auth_public/edit_cv/">MI CV</a>
        </div>
        <h6>Su CV on-line, proyecto a proyecto le hara visible</h6>
        <div class="tarjvitr_concuatrocont" style="border-bottom:none;">
          <a href="#">
            <?php $numProj = (isset($projects)) ? count($projects)+1 : 1;?>
            <div class="anadir" id="<?php echo $numProj?>"><span>+</span>PROYECTOS</div>
          </a>
          <div class="num_pad_up" style="position:absolute;right:10px;">Up</div>
          <div class="num_pad_down" style="position:absolute;right:10px;bottom: 33px;">Down</div>
          <div class="tarjvitr_concuatroactualdos" id="pane-target">
          <?php if (isset($projects)) { 
            if (count($projects) > 0) {
              foreach ($projects as $project) {
                echo $project;?>
                <!-- <div>
                  <img src="<?php echo IMAGES_URL;?>industri_gris.png" alt="Industria proyecto actual" title="Industria proyecto actual">
                  <a style="text-decoration: underline !important" href="<?php echo $urlBase?>auth_public/edit_cv?id=<?php echo base64_encode($project['id']);?>"><?php echo trim($project['name']).'</a> : '; ?>
                  <span class="tarjvitr_concuatroactualdefdos"><?php echo trim(substr($project['description'],0,15)).' ...' ?></span>
                </div> -->
            <?php }
            } else {
              echo 'No ha añadido ningún proyecto aún';
            }
          } else {
            echo 'No ha añadido ningún proyecto aún';
          }
          ?>
          </div>
        </div>
        <div class="tarjvitr_cvempresdos">
          <a href="<?php echo $urlBase;?>auth_public/resumen_cv_profesional" class="tarjvitr_en">RESUMEN MI CV</a>
        </div>
        <div class="socialb social-headb">
          <a href="#" title="Facebook"><i class="icon-facebook"></i></a>
          <a href="#" title="Twitter"><i class="icon-twitter"></i></a>
          <a href="#" title="Rss"><i class="icon-rss"></i></a>
        </div>
      </div>              
    </div>
    </div>
  </div>
<style>
.tarjeta {
    margin: auto;
    height: 314px;
    width: 100%;
    background-color: white;
}
.tarjvitr_containertext {
    float: left;
    font-size: 3em;
    color: black;
    margin-bottom: 0 !important;
    text-align: center !important;
    width: 100%;
    font-family: sourcesansprobold;
    position: absolute;
    z-index: 2;
}
.tarjvitr_con {
    border: 2mm solid #9F8B57;
    left: 167px;
    position: absolute;
    top: 34px;
    z-index: 100;
}
.tarjvitr_condos {
    border: 2mm solid #C0A669;
    left: 178px;
    position: absolute;
    top: 45px;
    z-index: 101;
}
.tarjvitr_contres {
    font-family: "sourcesansproreg",Helvetica,Arial,sans-serif !important;
    color: #63605E !important;
    font-size: 1.3em;
    border: 2mm solid #DDC079;
    left: 190px;
    position: absolute;
    top: 56px;
    z-index: 102;
}
.tarjvitr_foto {
    border: 1px solid #ADADAD;
    -webkit-box-shadow: -1px 0 10px 2px #ADADAD;
    -moz-box-shadow: -1px 0 10px 2px #ADADAD;
    box-shadow: -1px 0 10px 2px #ADADAD;
    width: 24%;
    height: 37%;
    margin-left: 10px;
    margin-top: 50px;
    float: left;
    position: absolute;
}
a.pp_closedos {
    background: url("/../img/prettyPhoto/dark_rounded/sprite.png") no-repeat scroll -77px 0 rgba(0, 0, 0, 0);
    cursor: pointer;
    display: block;
    float: right;
    line-height: 22px;
    text-indent: -10000px;
    width: 4%;
}
form#tarjeta_visita {
    color: #2A4051 !important;
}
.abso_tarjeta {
    left: 190px;
    top: 56px;
    font-family: "sourcesansproreg",Helvetica,Arial,sans-serif !important;
    color: #63605E !important;
    font-size: 1.3em;
    border: 2mm solid #DDC079;
    position: absolute;
    z-index: 102;
}
.tarjvitr_containerbut {
    font-family: sourcesansprobold;
    font-size: 1.7em;
    position: absolute;
    z-index: 9;
    width: 100%;
    text-align: center;
    top: 350px;
}
.continuar {
    font-size: 22px!important;
    margin: auto;
    cursor: pointer;
    text-align: center;
}
.continuar, .tarj_front {
    margin-top: 1%;
    background-color: #C5A653;
    text-shadow: 0 1px 1px rgba(0,0,0,.3);
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);
    background: #f78d1d;
    background: -webkit-gradient(linear, left top, left bottom, from(#ddc079), to(#c5a653));
    background: -moz-linear-gradient(top, #ddc079, #c5a653);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ddc079', endColorstr='#c5a653');
    background-color: #C2A34E;
    border: medium none;
    color: #FFFFFF;
    width: 37% !important;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    font-size: 0.7em !important;
    padding: 7px;
    -webkit-box-shadow: 5px 6px 7px -3px #adadad;
    -moz-box-shadow: 5px 6px 7px -3px #adadad;
    box-shadow: 5px 6px 7px -3px #adadad;
}
#flipPad {
    position: absolute;
    z-index: 999;
    left: -7px;
    top: -15px;
}
</style>