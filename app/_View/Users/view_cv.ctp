<?php
echo $this->Html->script(array(
'jquery-ui-1.10.3.custom.min',
'jquery.mockjax.min',
'bootstrap-editable.min',
'moment.min',
'typeahead.bundle',
'typeaheadjs.10x.js',
'address.min',
'select2.min',
'custom.min',
'form-x-editable',
'form-x-editable-demo',
'core.min',
'jquery.mockjax.min',
'bootbox.min'
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));

?>
$(document).ready(function(){
  $('#menu1').removeClass('active2');
  $('#menu2').addClass('active2');
  var hashTag = '';
  if (window.location.hash != '') {
    hashTag = window.location.hash.substr(1, window.location.hash.length);
  } else if ($('#card_completed').val() == 0) {
    hashTag = 'target';
  } else if ($('#formation_completed').val() == 0) {
    hashTag = 'formation';
  }
  if (hashTag.length == 0) {
    console.log('sinhash');
  } else {
    var hashTagsPassed = [];
    $.each($('.btn-minimize'), function(index) {
      if (hashTag == 'target' || hashTag == 'formation') {
        if (!($(this).attr('class').indexOf(hashTag) > 0)) {
           if ($.inArray( $(this).attr('class'), hashTagsPassed) == -1) {
              hashTagsPassed.push($(this).attr('class'));
              $(this).click();
           }
        }
      } else {
        if (!($(this).attr('class').indexOf(hashTag) > 0) && 
           (!($(this).attr('class').indexOf('project') > 0))) {
           if ($.inArray( $(this).attr('class'), hashTagsPassed) == -1) {
              hashTagsPassed.push($(this).attr('class'));
              $(this).click();
           }
        }
      }
    });
  }
  console.log(hashTagsPassed);
  $("#hasCompanyA").click(function(){
    $('#company').html('');
    $('.address').html('');
  });
  $("#hasCompanyE").click(function(){
    if ($('#lastcompany').val() != '') {
      $('#company').html($('#lastcompany').val());
    } else {
      $('#company').html('Introduzca su empresa');
    }
  });

  $('#UserVerCvForm').submit(function(){
      nombreempresa = $('#company').html().trim();
      radiochecked = $("#UserVerCvForm input[type='radio']:checked").val();
      if ((radiochecked == 'E')&&(nombreempresa == I18nJs.t('Seleccione empresa')))  {
        bootbox.alert('Por favor seleccione una empresa para poder guardar el registro de la tarjeta');
        return false;
      }
      var department_value = '';
      if ($('#department').html().trim() != I18nJs.t('DEPARTAMENTO DENTRO DE LA EMPRESA')) {
        department_value = $('#department').html();
     }
      $('<input>').attr({
        type: 'hidden',
        id: 'company',
        name: 'User[department]',
        value: department_value,
      }).appendTo($('#UserVerCvForm'));
      
      var functionality_value = '';
      if ($('#functionality').html().trim() != I18nJs.t('FUNCIÓN DENTRO DE LA EMPRESA')) {
        functionality_value = $('#functionality').html();
     }
      $('<input>').attr({
        type: 'hidden',
        id: 'functionality',
        name: 'User[functionality]',
        value: functionality_value,
      }).appendTo($('#UserVerCvForm'));

      $('<input>').attr({
        type: 'hidden',
        id: 'company',
        name: 'User[firstname]',
        value: $('#firstname').html().trim(),
      }).appendTo($('#UserVerCvForm'));

      $('<input>').attr({
        type: 'hidden',
        id: 'company',
        name: 'User[lastname]',
        value: $('#lastname').html().trim(),
      }).appendTo($('#UserVerCvForm'));


      var countrys = $('#country').html();
      countrysPos = countrys.indexOf('(');
      countrys = countrys.substr(0, countrysPos-1);
      $('<input>').attr({
        type: 'hidden',
        id: 'country',
        name: 'User[country]',
        value: countrys,
      }).appendTo($('#UserVerCvForm'));

      $('<input>').attr({
        type: 'hidden',
        id: 'company',
        name: 'User[company]',
        value: $('#company').html(),
      }).appendTo($('#UserVerCvForm'));

      var citys = $('#city').html();
      if (citys == I18nJs.t('Seleccione ciudad')) {
        citys = '';
      }
      $('<input>').attr({
        type: 'hidden',
        id: 'city',
        name: 'User[city]',
        value: citys,
      }).appendTo($('#UserVerCvForm'));

      var sectorValue = $('#sector').html();
      if (sectorValue == I18nJs.t('Linea de negocio')) {
        sectorValue = '';
      }
      $('<input>').attr({
        type: 'hidden',
        id: 'sector',
        name: 'User[sector]',
        value: sectorValue,
      }).appendTo($('#UserVerCvForm'));

  });
   $('[id^="image"]').click(function(){
        var str = this.id;
        var index = str.replace("image", "");
        $('#imgInp'+index).click();
    });
    $('[id^="imgInp"]').click(function(){
        var str = this.id;
        var index = str.replace("imgInp", "");
        $('#imgInp'+index).change(function() {
            readURL(this, index);
        });
    });

    function readURL(input,value) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                isImage = $.inArray( input.files[0].type, [ "image/gif", "image/jpeg", "image/png","image/vnd.microsoft.icon"] );
                if (isImage != -1) {
                    var img = new Image;
                    img.onload = function() {
                        $('#image'+value).attr('src', e.target.result);
                          var img = new Image;
                          img.onload = function() {
                            max_ancho = 100;
                            max_alto = 100;
                            x_ratio = max_ancho / img.width;
                            y_ratio = max_alto / img.height;
                            if ((img.width <= max_ancho) && (img.height <= max_alto)) {
                            //Si es más pequeña que el máximo no redimensionamos
                            ancho_final = img.width;
                            alto_final = img.height;
                            } else if ((x_ratio * img.height) < max_alto) {
                              alto_final = x_ratio * img.height;
                              ancho_final = max_ancho;
                            } else{
                              ancho_final = y_ratio * img.width;
                              alto_final = max_alto;
                            }
                            $('#image'+value).attr('style', 'width:'+ancho_final+'px;height:'+alto_final+'px');
                          };
                        img.src = reader.result;
                    };
                img.src = reader.result;
                } else {
                    bootbox.alert(I18nJs.t('Sólo se aceptan formatos gif / jpg / png o ico'));
                    return false;
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


});
<?php 
$this->Html->scriptEnd();
?><!-- start: Content -->
<div class="box">
<div class="box-content">
<div clas="row">
<div class="text-center">
<?php if ($user['User']['card_completed'] && $user['User']['formation_completed']) { ?>
<a href="/UserProjects/add"><i class="fa fa-plus-circle green"></i> <?php echo __('PROYECTOS');?></a>
<?php } ?>
</div>
<input type="hidden" id="lastcompany" value="<?php echo $staticCompanyNames[$user['User']['company_id']]?>">
<input type="hidden" id="optionsCountries" value='<?php echo json_encode($optionsCountries)?>'>
<input type="hidden" id="functionalities" value='<?php echo json_encode($functionalities)?>'>
<input type="hidden" id="departments" value='<?php echo json_encode($departments)?>'>
<input type="hidden" id="companies" value='<?php echo json_encode($companies)?>'>
<input type="hidden" id="sectors" value='<?php echo json_encode($sectors)?>'>
<input type="hidden" id="cities" value='<?php echo json_encode($cities)?>'>
<input type="hidden" id="formation_completed" value='<?php echo $user['User']['formation_completed']?>'>
<input type="hidden" id="card_completed" value='<?php echo $user['User']['card_completed']?>'>

<div class="box">
<div class="box-content">
<div class="row">
<div class="col-sm-offset-3 col-sm-6 col-sm-offset-3">

<div class="row star">
<?php 
if (!empty($specialitiesFromUser)) { 
  echo $this->Html->image('star_03.png');
} ?>
</div>
<!-- Tarjeta -->
<?php
echo $this->Form->create('User', array(
'inputDefaults' => array(
  'div' => 'form-group',
  'class' => 'form-control',
  'type' => 'file'
),
'type'=>'file'
));?>
<input type="hidden" id="User.id" name="User[id]" value='<?php echo $user['User']['id']?>'>
<div class="row tarjeta" style="color: #333333;font-size: 16px;">
<div class="col-md-12">
<?php 
    $sectorName = (!empty($user['User']['sector_id'])) ? $staticSectorsNames[$user['User']['sector_id']] : __('Línea de negocio'); ?>
    <a href="#" style="border-bottom: 0px" id="sector" data-type="typeaheadjs" data-pk="1" data-original-title="<?php echo $sectorName;?>" class="editable editable-click" data-value=""><?php echo $sectorName;?></a>
<div class="row">
  <div class="col-md-3">
    <?php 
    $urlImage = (!empty($user['User']['photo_dir'])) ? $user['User']['photo'] : 'camarafot.png';
    if ($urlImage == 'camarafot.png') {
      $nameImage = $this->webroot.'img'.DS.$urlImage;  
    } else {
      $nameImage = $this->webroot.'files'.DS.'users'.DS.'user_'.$user['User']['id'].DS.$urlImage;
    }
    ?>
    <img src="<?php echo  $nameImage?>" width="100px" heigth="100px" id="image1" type ='file' alt="<?php echo __('Imágen de perfil');?>" > 
    <?php
        echo $this->Form->input('User.image1',array('id'=> 'imgInp1','label' => false,'class'=>'hidden'));
    ?>
  </div>

  <div class="col-md-9">
    <div class="form-inline">
      <label class="radio" style="cursor:pointer" >
          <input type="radio" name="User[hasCompany]" id="hasCompanyA" value="A" style="cursor:pointer" 
          <?php  if($user['User']['is_autonomous']) {echo 'checked';}?>
          />
          <?php echo __('Soy autónomo').'&nbsp;&nbsp;&nbsp;&nbsp;'?>
      </label>
      <label class="radio" style="cursor:pointer">
          <input type="radio" name="User[hasCompany]" id="hasCompanyE" value="E" style="cursor:pointer" 
          <?php  if (!empty($user['User']['company_id'])) {echo 'checked';}?>
          />
          <?php echo __('Empleado');?>
       </label>
    </div>
    <div class="row">
      <?php
        $companyName = (empty($user['User']['company_id'])) ? __('Sin empresa / Autónomo') : $staticCompanyNames[$user['User']['company_id']];
        $companyNameValue = ($companyName !=__('Sin empresa / Autónomo')) ? $companyName : '';
        ?>
        <a href="#" id="company" data-type="typeaheadjs" data-pk="1" data-original-title="<?php echo __('Empresa');?>" class="editable editable-click" data-value="<?php echo $companyNameValue?>"><?php echo $companyName;?></a>
    </div>

    <div class="row">
      <a href="#" id="firstname" data-type="text" data-pk="1" data-original-title="Nombre" class="editable editable-click">
        <?=$user['User']['firstname'];?>
      </a>&nbsp;&nbsp;
      <a href="#" id="lastname" data-type="text" data-pk="1" data-original-title="Apellidos" class="editable editable-click">
        <?=$user['User']['lastname'];?>
      </a>
    </div>

    <div class="row">
      <?php 
      $function_name = (empty($user['User']['functionality_id'])) ? __('FUNCIÓN DENTRO DE LA EMPRESA') : $staticFunctionalitiesNames[$user['User']['functionality_id']];
      $value_function_name = ($function_name != __('FUNCIÓN DENTRO DE LA EMPRESA')) ? $function_name : '';?>
        <a href="#" id="functionality" data-type="typeaheadjs" data-pk="1" 
         class="editable editable-click" data-value="<?php echo $value_function_name?>"><?php echo $function_name?></a>
    </div>

    <div class="row">
      <?php 
      $department_name = (!empty($user['User']['department_id'])) ? $staticDepartmentsNames[$user['User']['department_id']] : __('DEPARTAMENTO DENTRO DE LA EMPRESA');
      $value_department_name = ($department_name != __('DEPARTAMENTO DENTRO DE LA EMPRESA')) ? $department_name : '';?>
      <a href="#" id="department" data-type="typeaheadjs" data-pk="1" class="editable editable-click" data-value="<?php echo $value_department_name?>"><?php echo $department_name?></a>
    </div>

    <div class="row">
      <?php 
        $countryName = (!empty($user['User']['country_id'])) ? $staticCountriesNames[$user['User']['country_id']] : __('Seleccione país');
        $countryNameValue = ($countryName != __('Seleccione país')) ? $countryName : '';
      ?>
      <a title="" data-original-title="" class="editable editable-click" href="#" id="country" data-type="select2" data-pk="1" data-value="<?php echo $countryNameValue?>" data-title="<?php echo __('Seleccione país');?>"><?php echo $countryName?></a>
      <?php 
        $cityName = (!empty($user['User']['city_id'])) ? $staticCitiesNames[$user['User']['city_id']] : __('Seleccione ciudad');
        $cityNameValue = ($cityName != __('Seleccione ciudad')) ? $cityName : '';
      ?>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <a href="#" id="city" data-type="typeaheadjs" data-pk="1" class="editable editable-click" data-value="<?php echo $cityNameValue?>"><?php echo $cityName?></a>
    </div>
  </div>      
</div>
</div>
<div class="row-fluid bot">
				<div class="span8 edis">
				    <?php
				    $mail = $user['User']['email'];
				    $nameShort = (strlen($mail) > 25) ? substr($mail,0,25).'...' : $mail; 
				    echo $nameShort;
				    ?>
				    <input type="hidden" name="User[email]" value='<?php echo $mail?>'>
				</div>
				<div class="span8 edis">
				  <span class="address"><?php echo $company['Company']['address'];?></span>
				</div>
				</div>
				<div class="row-fluid" style="margin-top: 0px;font-size:12px;">
<!-- <div class="span4"><a href="#">CV EMPRESA</a></div>-->
					<div class="text-center" style="margin-top:10px">
						<?php echo $this->Form->submit(__('GUARDAR'),array('class' => 'submittarjet'));
						echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
		<!-- Fin tarjeta -->
		</div>
    </div>
  </div>
</div>
