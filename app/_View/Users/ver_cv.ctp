<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
'jquery-ui-1.10.3.custom.min',
'jquery.mockjax.min',
'bootstrap-editable.min',
'moment.min',
'typeahead.bundle',
'typeaheadjs.10x.js',
'address.min',
'select2.min',
'custom.min',
'form-x-editable',
'form-x-editable-demo',
'core.min',
'jquery.mockjax.min',
'bootbox.min'
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));

?>
$(document).ready(function(){
  $('#menu1').removeClass('active2');
  $('#menu2').addClass('active2');
  var hashTag = '';
  if (window.location.hash != '') {
    hashTag = window.location.hash.substr(1, window.location.hash.length);
  } else if ($('#card_completed').val() == 0) {
    hashTag = 'target';
  } else if ($('#formation_completed').val() == 0) {
    hashTag = 'formation';
  }
  if (hashTag.length == 0) {
    console.log('sinhash');
  } else {
    var hashTagsPassed = [];
    $.each($('.btn-minimize'), function(index) {
      if (hashTag == 'target' || hashTag == 'formation') {
        if (!($(this).attr('class').indexOf(hashTag) > 0)) {
           if ($.inArray( $(this).attr('class'), hashTagsPassed) == -1) {
              hashTagsPassed.push($(this).attr('class'));
              $(this).click();
           }
        }
      } else {
        if (!($(this).attr('class').indexOf(hashTag) > 0) && 
           (!($(this).attr('class').indexOf('project') > 0))) {
           if ($.inArray( $(this).attr('class'), hashTagsPassed) == -1) {
              hashTagsPassed.push($(this).attr('class'));
              $(this).click();
           }
        }
      }
    });
  }
  console.log(hashTagsPassed);
  $("#hasCompanyA").click(function(){
    $('#company').html('');
    $('.address').html('');
  });
  $("#hasCompanyE").click(function(){
    if ($('#lastcompany').val() != '') {
      $('#company').html($('#lastcompany').val());
    } else {
      $('#company').html('Introduzca su empresa');
    }
  });

  $('#UserVerCvForm').submit(function(){
      nombreempresa = $('#company').html().trim();
      radiochecked = $("#UserVerCvForm input[type='radio']:checked").val();
      if ((radiochecked == 'E')&&(nombreempresa == I18nJs.t('Seleccione empresa')))  {
        bootbox.alert('Por favor seleccione una empresa para poder guardar el registro de la tarjeta');
        return false;
      }
      var department_value = '';
      if ($('#department').html().trim() != I18nJs.t('DEPARTAMENTO DENTRO DE LA EMPRESA')) {
        department_value = $('#department').html();
     }
      $('<input>').attr({
        type: 'hidden',
        id: 'company',
        name: 'User[department]',
        value: department_value,
      }).appendTo($('#UserVerCvForm'));
      
      var functionality_value = '';
      if ($('#functionality').html().trim() != I18nJs.t('FUNCIÓN DENTRO DE LA EMPRESA')) {
        functionality_value = $('#functionality').html();
     }
      $('<input>').attr({
        type: 'hidden',
        id: 'functionality',
        name: 'User[functionality]',
        value: functionality_value,
      }).appendTo($('#UserVerCvForm'));

      $('<input>').attr({
        type: 'hidden',
        id: 'company',
        name: 'User[firstname]',
        value: $('#firstname').html().trim(),
      }).appendTo($('#UserVerCvForm'));

      $('<input>').attr({
        type: 'hidden',
        id: 'company',
        name: 'User[lastname]',
        value: $('#lastname').html().trim(),
      }).appendTo($('#UserVerCvForm'));


      var countrys = $('#country').html();
      countrysPos = countrys.indexOf('(');
      countrys = countrys.substr(0, countrysPos-1);
      $('<input>').attr({
        type: 'hidden',
        id: 'country',
        name: 'User[country]',
        value: countrys,
      }).appendTo($('#UserVerCvForm'));

      $('<input>').attr({
        type: 'hidden',
        id: 'company',
        name: 'User[company]',
        value: $('#company').html(),
      }).appendTo($('#UserVerCvForm'));

      var citys = $('#city').html();
      if (citys == I18nJs.t('Seleccione ciudad')) {
        citys = '';
      }
      $('<input>').attr({
        type: 'hidden',
        id: 'city',
        name: 'User[city]',
        value: citys,
      }).appendTo($('#UserVerCvForm'));

      var sectorValue = $('#sector').html();
      if (sectorValue == I18nJs.t('Linea de negocio')) {
        sectorValue = '';
      }
      $('<input>').attr({
        type: 'hidden',
        id: 'sector',
        name: 'User[sector]',
        value: sectorValue,
      }).appendTo($('#UserVerCvForm'));

  });
   $('[id^="image"]').click(function(){
        var str = this.id;
        var index = str.replace("image", "");
        $('#imgInp'+index).click();
    });
    $('[id^="imgInp"]').click(function(){
        var str = this.id;
        var index = str.replace("imgInp", "");
        $('#imgInp'+index).change(function() {
            readURL(this, index);
        });
    });

    function readURL(input,value) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                isImage = $.inArray( input.files[0].type, [ "image/gif", "image/jpeg", "image/png","image/vnd.microsoft.icon"] );
                if (isImage != -1) {
                    var img = new Image;
                    img.onload = function() {
                        $('#image'+value).attr('src', e.target.result);
                          var img = new Image;
                          img.onload = function() {
                            max_ancho = 100;
                            max_alto = 100;
                            x_ratio = max_ancho / img.width;
                            y_ratio = max_alto / img.height;
                            if ((img.width <= max_ancho) && (img.height <= max_alto)) {
                            //Si es más pequeña que el máximo no redimensionamos
                            ancho_final = img.width;
                            alto_final = img.height;
                            } else if ((x_ratio * img.height) < max_alto) {
                              alto_final = x_ratio * img.height;
                              ancho_final = max_ancho;
                            } else{
                              ancho_final = y_ratio * img.width;
                              alto_final = max_alto;
                            }
                            $('#image'+value).attr('style', 'width:'+ancho_final+'px;height:'+alto_final+'px');
                          };
                        img.src = reader.result;
                    };
                img.src = reader.result;
                } else {
                    bootbox.alert(I18nJs.t('Sólo se aceptan formatos gif / jpg / png o ico'));
                    return false;
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


});
<?php 
$this->Html->scriptEnd();
?><!-- start: Content -->
  
  <div class="box">
	  
    <div class="box-header">
      <h2><i class="fa fa-edit"></i><?php echo __('Ver CV');?></h2>
       <div class="box-icon">
      <a href="/Users/resumen_cv" class=" pull-left" style="margin-right:25px;">
        <?php echo $this->Html->image('resumen_cv.png');?>
        <?php echo __('Ir a resumen CV');?>
      </a>
      <a href="/Users/cvmap" class=" pull-left" style="margin-right:25px;">
        <?php echo $this->Html->image('mapamundi_mini.png');?>
        <?php echo __('Ver CV en mapa');?>
      </a>
      </div>
    </div>
    <div class="box-content">
      <div clas="row">
        <p><h2 class="title "><?php echo __('CV PERSONAL GS');?></h2></p>
        <hr />
        <div class="text-center">
          <?php if ($user['User']['card_completed'] && $user['User']['formation_completed']) { ?>
            <a href="/UserProjects/add"><i class="fa fa-plus-circle green"></i> <?php echo __('PROYECTOS');?></a>
          <?php } ?>
        </div>
        <input type="hidden" id="lastcompany" value="<?php echo $staticCompanyNames[$user['User']['company_id']]?>">
        <input type="hidden" id="optionsCountries" value='<?php echo json_encode($optionsCountries)?>'>
        <input type="hidden" id="functionalities" value='<?php echo json_encode($functionalities)?>'>
        <input type="hidden" id="departments" value='<?php echo json_encode($departments)?>'>
        <input type="hidden" id="companies" value='<?php echo json_encode($companies)?>'>
        <input type="hidden" id="sectors" value='<?php echo json_encode($sectors)?>'>
        <input type="hidden" id="cities" value='<?php echo json_encode($cities)?>'>
        <input type="hidden" id="formation_completed" value='<?php echo $user['User']['formation_completed']?>'>
        <input type="hidden" id="card_completed" value='<?php echo $user['User']['card_completed']?>'>
        <div class="box-content">
	        <div class="row">
        <div class="box">
          <div class="box-header" style="background-color:#4778a0">
	          <h2 class="title " style="color:#fff">
                  <?php echo $this->Html->image('mini_tarjeta.png');?>
                  <a href="#" class="btn-minimize" style="text-decoration:none;color:#fff"><?php echo __('TARJETA DE VISITA');?></a>
                </h2>

            <div class="box-icon">
              <a href="#" class="btn-minimize target"><i class="fa fa-chevron-up"></i></a>
            </div>
          </div>
          <div class="box-content">
            <div class="row">
              <div class="col-sm-offset-3 col-sm-6 col-sm-offset-3">

                  <div class="row star">
                    <?php 
                    if (!empty($specialitiesFromUser)) { 
                      echo $this->Html->image('star_03.png');
                    } ?>
                  </div>
                <!-- Tarjeta -->
                <?php
                echo $this->Form->create('User', array(
                  'inputDefaults' => array(
                      'div' => 'form-group',
                      'class' => 'form-control',
                      'type' => 'file'
                  ),
                  'type'=>'file'
                ));?>
                <input type="hidden" id="User.id" name="User[id]" value='<?php echo $user['User']['id']?>'>
                <div class="row tarjeta" style="color: #333333;font-size: 16px;">
                  <div class="col-md-12">
                  <?php 
                        $sectorName = (!empty($user['User']['sector_id'])) ? $staticSectorsNames[$user['User']['sector_id']] : __('Línea de negocio'); ?>
                        <a href="#" style="border-bottom: 0px" id="sector" data-type="typeaheadjs" data-pk="1" data-original-title="<?php echo $sectorName;?>" class="editable editable-click" data-value=""><?php echo $sectorName;?></a>
                    <div class="row">
                      <div class="col-md-3">
                        <?php 
                        $urlImage = (!empty($user['User']['photo_dir'])) ? $user['User']['photo'] : 'camarafot.png';
                        if ($urlImage == 'camarafot.png') {
                          $nameImage = $this->webroot.'img'.DS.$urlImage;  
                        } else {
                          $nameImage = $this->webroot.'files'.DS.'users'.DS.'user_'.$user['User']['id'].DS.$urlImage;
                        }
                        ?>
                        <img src="<?php echo  $nameImage?>" width="100px" heigth="100px" id="image1" type ='file' alt="<?php echo __('Imágen de perfil');?>" > 
                        <?php
                            echo $this->Form->input('User.image1',array('id'=> 'imgInp1','label' => false,'class'=>'hidden'));
                        ?>
                      </div>

                      <div class="col-md-9">
                        <div class="form-inline">
                          <label class="radio" style="cursor:pointer" >
                              <input type="radio" name="User[hasCompany]" id="hasCompanyA" value="A" style="cursor:pointer" 
                              <?php  if($user['User']['is_autonomous']) {echo 'checked';}?>
                              />
                              <?php echo __('Soy autónomo').'&nbsp;&nbsp;&nbsp;&nbsp;'?>
                          </label>
                          <label class="radio" style="cursor:pointer">
                              <input type="radio" name="User[hasCompany]" id="hasCompanyE" value="E" style="cursor:pointer" 
                              <?php  if (!empty($user['User']['company_id'])) {echo 'checked';}?>
                              />
                              <?php echo __('Empleado');?>
                           </label>
                        </div>
                        <div class="row">
                          <?php
                            $companyName = (empty($user['User']['company_id'])) ? __('Sin empresa / Autónomo') : $staticCompanyNames[$user['User']['company_id']];
                            $companyNameValue = ($companyName !=__('Sin empresa / Autónomo')) ? $companyName : '';
                            ?>
                            <a href="#" id="company" data-type="typeaheadjs" data-pk="1" data-original-title="<?php echo __('Empresa');?>" class="editable editable-click" data-value="<?php echo $companyNameValue?>"><?php echo $companyName;?></a>
                        </div>

                        <div class="row">
                          <a href="#" id="firstname" data-type="text" data-pk="1" data-original-title="Nombre" class="editable editable-click">
                            <?=$user['User']['firstname'];?>
                          </a>&nbsp;&nbsp;
                          <a href="#" id="lastname" data-type="text" data-pk="1" data-original-title="Apellidos" class="editable editable-click">
                            <?=$user['User']['lastname'];?>
                          </a>
                        </div>

                        <div class="row">
                          <?php 
                          $function_name = (empty($user['User']['functionality_id'])) ? __('FUNCIÓN DENTRO DE LA EMPRESA') : $staticFunctionalitiesNames[$user['User']['functionality_id']];
                          $value_function_name = ($function_name != __('FUNCIÓN DENTRO DE LA EMPRESA')) ? $function_name : '';?>
                            <a href="#" id="functionality" data-type="typeaheadjs" data-pk="1" 
                             class="editable editable-click" data-value="<?php echo $value_function_name?>"><?php echo $function_name?></a>
                        </div>

                        <div class="row">
                          <?php 
                          $department_name = (!empty($user['User']['department_id'])) ? $staticDepartmentsNames[$user['User']['department_id']] : __('DEPARTAMENTO DENTRO DE LA EMPRESA');
                          $value_department_name = ($department_name != __('DEPARTAMENTO DENTRO DE LA EMPRESA')) ? $department_name : '';?>
                          <a href="#" id="department" data-type="typeaheadjs" data-pk="1" class="editable editable-click" data-value="<?php echo $value_department_name?>"><?php echo $department_name?></a>
                        </div>

                        <div class="row">
                          <?php 
                            $countryName = (!empty($user['User']['country_id'])) ? $staticCountriesNames[$user['User']['country_id']] : __('Seleccione país');
                            $countryNameValue = ($countryName != __('Seleccione país')) ? $countryName : '';
                          ?>
                          <a title="" data-original-title="" class="editable editable-click" href="#" id="country" data-type="select2" data-pk="1" data-value="<?php echo $countryNameValue?>" data-title="<?php echo __('Seleccione país');?>"><?php echo $countryName?></a>
                          <?php 
                            $cityName = (!empty($user['User']['city_id'])) ? $staticCitiesNames[$user['User']['city_id']] : __('Seleccione ciudad');
                            $cityNameValue = ($cityName != __('Seleccione ciudad')) ? $cityName : '';
                          ?>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <a href="#" id="city" data-type="typeaheadjs" data-pk="1" class="editable editable-click" data-value="<?php echo $cityNameValue?>"><?php echo $cityName?></a>
                        </div>
                      </div>      
                    </div>
                  </div>
                  <div class="row-fluid bot">
                    <div class="span8 edis">
                        <?php
                        $mail = $user['User']['email'];
                        $nameShort = (strlen($mail) > 25) ? substr($mail,0,25).'...' : $mail; 
                        echo $nameShort;
                        ?>
                        <input type="hidden" name="User[email]" value='<?php echo $mail?>'>
                    </div>
                    <div class="span8 edis">
                      <span class="address"><?php echo $company['Company']['address'];?></span>
                    </div>
                  </div>
                  <div class="row-fluid" style="margin-top: 0px;font-size:12px;">
                    <!-- <div class="span4"><a href="#">CV EMPRESA</a></div>-->
                    <div class="text-center" style="margin-top:10px">
                    <?php echo $this->Form->submit(__('GUARDAR'),array('class' => 'submittarjet'));
                    echo $this->Form->end(); ?>
                    </div>
                  </div>
                </div>
                <!-- Fin tarjeta -->
              </div>
            </div>
          </div>
        </div>
        <div class="box">
          <div class="box-header" style="background-color:#4778a0">
            <h2 class="title " style="color:#fff"><?php echo $this->Html->image('document_formacion.png');?>
              <a href="#" class="btn-minimize formation" style="text-decoration:none;color:#fff"><?php echo __('FORMACIÓN');?></a></h2>
              <a name="formation"></a>
            <div class="box-icon">
              <a href="/Users/edit_formation" class="" style="color:#fff"><?php echo $this->Html->image('edit_pencil.png').' '.__('EDITAR');?>&nbsp;&nbsp;</a>
              <a href="#" class="btn-minimize formation"><i class="fa fa-chevron-up"></i></a>
            </div>
          </div>
          <div class="box-content">
            <div class="row">
                <?php 
                if (!empty($user['User']['formation_completed'])) { ?>
                  <div class="col-md-12">
                    <?php
                    $fecha    = empty($user['User']['birthdate']) ? time() : date($user['User']['birthdate']) ;
                    if (is_string($fecha)) {
                        $fecha_de_nacimiento = strtotime($fecha);
                        $diferencia_de_fechas = time() - $fecha_de_nacimiento;
                        $edad = round($diferencia_de_fechas / (60 * 60 * 24 * 365));
                    } else {
                      $edad = __('Fecha de nacimiento no asignada');
                    }
                    
                    $languages =  json_decode($user['User']['idioms']);
                    $languagesText = '';
                        if (!empty($languages)) {
                          foreach ($languages as $language) {
                            $languagesText .= $languageName[$language].' - ';
                          }
                        } else {
                          $languagesText = __('No ha añadido idiomas');
                        }
                        $otherInterests = (!empty($user['User']['other_interests'])) ? $user['User']['other_interests'] : __('Sin otros intereses añadidos');
                    switch ($user['User']['gender']) {
                      case 0: $sSex = __('Hombre'); break;
                      case 1: $sSex = __('Mujer'); break;
                      default: $sSex = __('Sin sexo asignado');
                    }
                    $courses = (!empty($user['User']['courses'])) ? $user['User']['courses'] : __('Sin cursos añadidos');
                      $formation = (!empty($user['User']['formation'])) ? $user['User']['formation'] : __('Sin formación añadida');
                    ?>
                    <table class="table table-responsive">
                      <thead></thead>
                      <tbody>
                        <tr>
                          <td class="col-md-2">
                            <div class="text-right" style="color:#3fa9f5;padding:10px">
                              <?php echo __('Edad').':';?>
                            </div>
                          </td>
                          <td class="col-md-9">
                            <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;border-radius: 10px 10px 0 0;"><?php echo $edad;?>
                          </td>
                          <td class="col-md-1">
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <div class="text-right" style="color:#3fa9f5;padding:10px">
                              <?php echo __('Sexo').':';?>
                            </div>
                          </td>
                          <td>
                            <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;"><?php echo $sSex;?>
                          </td>
                          <td>
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td class="col-md-2">
                            <div class="text-right" style="color:#3fa9f5;padding:10px">
                              <?php echo __('Formación').':';?>
                            </div>
                          </td>
                          <td class="col-md-9">
                            <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;"><?php echo $formation;?>
                          </td>
                          <td class="col-md-1">
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td class="col-md-2">
                            <div class="text-right" style="color:#3fa9f5;padding:10px">
                              <?php echo __('Idiomas').':';?>
                            </div>
                          </td>
                          <td class="col-md-9">
                            <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;"><?php echo $languagesText;?>
                          </td>
                          <td class="col-md-1">
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td class="col-md-2">
                            <div class="text-right" style="color:#3fa9f5;padding:10px">
                              <?php echo __('Cursos').':';?>
                            </div>
                          </td>
                          <td class="col-md-9">
                            <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;"><?php echo $courses;?>
                          </td>
                          <td class="col-md-1">
                            &nbsp;
                          </td>
                        </tr>
                        <tr>
                          <td class="col-md-2">
                            <div class="text-right" style="color:#3fa9f5;padding:10px">
                              <?php echo __('Cursos').':';?>
                            </div>
                          </td>
                          <td class="col-md-9">
                            <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;border-radius: 0px 0px 10px 10px;"><?php echo $otherInterests;?>
                          </td>
                          <td class="col-md-1">
                            &nbsp;
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                <?php } else {?>
                  <div class="col-md-12">
                    <div class="row">
                      <?php echo $this->Element('formation_not_completed');?>
                    </div>
                  </div>
                <?php }?>
              
            </div>
          </div>
        </div>
        <div class="box">
          <div class="box-header" style="background-color:#4778a0">
            <h2 class="title " style="color:#fff"><?php echo $this->Html->image('grua_mini.png');?>
              <a href="#" class="btn-minimize projects" style="text-decoration:none;color:#fff"><?php echo __('PROYECTOS');?></a></h2>
            <div class="box-icon">
              <a href="#" class="btn-minimize projects"><i class="fa fa-chevron-up"></i></a>
            </div>
          </div>
          <div class="box-content">
            <div class="row">
              <div class="col-md-12">
                <?php 
                  if (empty($projects)) { 
                    ?>
                    <table class="table table-responsive">
                      <thead></thead>
                      <tbody>
                        <tr>
                          <td class="col-md-1">
                            <div class="text-right" style="color:#3fa9f5;padding:10px">
                              &nbsp;
                            </div>
                          </td>
                          <td class="col-md-10">
                              <div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 text-left">
                  <?php echo __('No tiene proyectos añadidos.Pulse arriba en (+) Proyectos para añadir nuevos proyectos a su cv');?>
                          </td>
                          <td class="col-md-1">
                            &nbsp;
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  <div class="col-xs-2 col-sm-2 col-md-2 text-right">
                    &nbsp;
                  </div>
                  <?php
                  } else { ?>
                  <?php
                      foreach($projects as $project) {
                    ?>
                    <div class="box">
                      <a name="pr_<?php echo $project['id']?>"></a>
                      <div class="box-header">
                        <h2 class="title ">
                          <a href="#" class="btn-minimize pr_<?php echo $project['UserProject']['id']?>" style="text-decoration:none;"><?php echo strtoupper($project['UserProject']['name']);?></a></h2>
                        <div class="box-icon">
                          <a href="/UserProjects/edit/<?php echo $project['UserProject']['id']?>" class=""><?php echo $this->Html->image('edit_pencil.png').' '.__('EDITAR');?>&nbsp;&nbsp;</a>
                          <a href="#" class="btn-minimize <?php echo 'pr_'.$project['UserProject']['id']?>">
                            <i class="fa fa-chevron-up"></i>
                          </a>
                        </div>
                      </div>
                      <div class="box-content">
                        <div class="row">
                          <div class="col-sm-12 col-md-12 col-xs-12">
                            <table class="table table-responsive">
                              <thead></thead>
                              <tbody>
                                <tr style="background-color:#e6ebf1">
                                  <td class="col-md-2 col-xs-2 col-sm-2" style="background-color:#fff">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Nombre de la empresa').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 xol-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;border-radius: 10px 10px 0 0;">
                                      <?php echo $project['UserProject']['Company']['name'];?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('País de ejecución').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php echo $countries[$project['UserProject']['Country']['id']];?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Fecha de inicio / final').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php 
                                        $dateInit = new DateTime($project['UserProject']['start_date']);
                                        $dateInit = $dateInit->format('m/Y');
                                        if (!$project['on_going']) {
                                          $dateEnd = new DateTime($project['UserProject']['end_date']);
                                          $dateEnd = $dateEnd->format('m/Y');
                                        } else {
                                          $dateEnd = __('En curso');
                                        }
                                        $stringDates = $dateInit.' - '.$dateEnd;
                                        echo $stringDates;
                                        ?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Nombre del cliente').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-sm-10 col-xs-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php
                                       echo $companyNames[$project['UserProject']['customer_id']];?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Sector del proyecto').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php echo $sectors[$project['UserProject']['sector_project_id']];?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Presupuesto').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php echo $project['UserProject']['budget'].' ';
                                      foreach ($currencies as $currentCurrency) {
                                        if ($currentCurrency['id'] == $project['UserProject']['budget_currency_id']) {
                                          echo $currentCurrency['symbol'].' ('.$currentCurrency['name'].')';
                                          break;
                                        }
                                      }
                                      ?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Sector del servicio').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php 

                                        $sectorService = (!empty($project['UserProject']['sector_service_id'])) ? $sectors[$project['UserProject']['sector_service_id']] : __('No ha añadido sector del servicio'); 
                                        echo $sectorService;
                                        ?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Función').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php $functionName = (!empty($project['UserProject']['Functionality']['name'])) ? $project['UserProject']['Functionality']['name'] : __('No ha añadido función del servicio'); echo $functionName;?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Subfunción').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php $departmentName = (!empty($project['UserProject']['Department']['name'])) ? $project['UserProject']['Department']['name'] : __('No ha añadido subfunción del servicio'); echo $departmentName;?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Duración de su servicio').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php 
                                        $timeDuration = '';
                                        if (!empty($project['UserProject']['duration_year'])) {
                                          $timeDuration .= ' '.$project['UserProject']['duration_year'].' '.__('años');
                                        }
                                        if (!empty($project['UserProject']['duration_month'])) {
                                          $timeDuration .= ' '.$project['UserProject']['duration_month'].' '.__('meses');
                                        }
                                        if (!empty($project['UserProject']['duration_day'])) {
                                          $timeDuration .= ' '.$project['UserProject']['duration_day'].' '.__('días');
                                        }
                                        if (!empty($project['UserProject']['duration_hours'])) {
                                          $timeDuration .= ' '.$project['UserProject']['duration_hours'].' '.__('horas');
                                        }
                                        $timeDuration = (!empty($timeDuration)) ? $timeDuration : __('No ha añadido duración');
                                        echo $timeDuration;
                                      ?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Palabras clave').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
                                      <?php 
                                      if (!empty($project['UserProject']['tags'])) {
                                        $tags = json_decode($project['UserProject']['tags']);
                                        $tagString = '';
                                        foreach ($tags as $tag) {
                                          $tagString .=  $tag.',';
                                        };
                                        $tagString = (!empty($tagString)) ? substr($tagString,0,strlen($tagString)-1) : __('Sin palabras clave');  
                                      } else {
                                        $tagString = __('Sin etiquetas');
                                      }
                                      
                                      echo $tagString;
                                      ?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                                <tr>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    <div class="text-right" style="color:#3fa9f5;padding:10px">
                                      <?php echo __('Descripción').':';?>
                                    </div>
                                  </td>
                                  <td class="col-md-10 col-xs-10 col-sm-10">
                                    <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;border-radius: 0px 0px 10px 10px;">
                                      <?php $descriptionText = (!empty($project['UserProject']['description'])) ? $project['UserProject']['description'] : __('Sin descripción') ; echo $descriptionText;?>
                                    </div>
                                  </td>
                                  <td class="col-md-2 col-xs-2 col-sm-2">
                                    &nbsp;
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                <?php }
                }?>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<style>
.abso_tito{
  background: #4778a0;
width: 560px;
display: inline-block;
height: 20px;
cursor: pointer;
vertical-align: top;
}
.abso_titos h3{
  color: #4778a0;
background-color: white;
padding: 0px 30px 0 8px;
font-family: sourcesansprobold!important;
background-image: url(../../images/close_03.png);
background-repeat: no-repeat;
background-position: 98% 2px;
background-size: 20px;
}
.collapsed h3{
  background-image: url(../../images/down_03.png);
}
.cv_personal{
padding: 20px 0 20px 20px;
border-top: 2px solid #d9d9d9;
}
.bot .span8,.tarjeta .span8 .span11{
  border-bottom: 1px dashed #82c7f8;
  padding-top: 10px;
  margin-left: 0px;
  font-size: 16px;
}
.tarjeta{
  border: 2mm solid #DDC079;
  padding: 10px;
  font-family: sourcesansprobold;
font-size: 10pt;
}
.abso_titos{
  cursor: pointer;
}

.back{
  background-image: url(../../images/fondo_tarjeta_03.png);
background-color: white;
background-position: right 80px;
position: relative;
background-repeat: no-repeat;
background-size: 165px;
}
.forma{
  background-color: #e6ebf1;
  background-image: url(../../images/grua_mini.png);
  background-repeat: no-repeat;
  background-position: right 10px;
  padding: 10px;
  font-size: 14px;
  border-radius: 10px;
  margin-bottom: 20px;
}
.forma.fon{
  background-image: url(../../images/doument_formacion.png);
}
.sans{
  font-family: sourcesansprobold;
  font-size: 11pt;
  color: #3fa9f5;
  text-align: right;
}
.edit{
  margin-right: 10px;
font-size: 14px;
margin-top: 1px;
cursor: pointer;
}
.edit img{
  display: inline-block;
  position: relative;
top: 2px;
  
}

#cv_personal .row-fluid .span11:first-child,.forma .span9:first-child{
  margin-left: 2.127659574468085%!important;
}
.open_new{
  background-image: url(../../images/close_03.png);
  width: 20px;
  height: 17px;
  background-repeat: no-repeat;
  float: left;
  margin: 6px 0 0 6px;
}
.edis{
min-height: 26px!important;
padding-top: 7px!important; 
}
.tit_proj.collapsed{
margin-bottom: 20px;
}
.table th, .table td { 
     border-top: none !important;
     margin: 0px !important;
     padding: 0px !important;
 }
.submittarjet {
    text-shadow: 0 1px 1px rgba(0,0,0,.3);
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  box-shadow: 0 1px 2px rgba(0,0,0,.2);
  background: #f78d1d;
  background: -webkit-gradient(linear, left top, left bottom, from(#ddc079), to(#c5a653));
  background: -moz-linear-gradient(top, #ddc079, #c5a653);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ddc079', endColorstr='#c5a653');
  background-color: #C2A34E;
  border: medium none;
  color: #FFFFFF;
  margin-top: 2% !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  font-size: 1.5em !important;
  padding: 5px;
}
}
</style>