<?php

$this->extend('/Common/panel');
echo $this->Html->script(array(
    'jquery-ui-1.10.3.custom.min',
    'jquery.sparkline.min',
    'jquery.chosen.min',
    'jquery.autosize.min',
    'jquery.placeholder.min',
    'daterangepicker.min',
    'moment.min',
    'jquery.steps.min',
    'wizard.min',
    'custom.min',
    'bootbox.min',
    'core.min',
    'jquery.tooltipster.min',
    'jquery.mask.min',
    'tag-it'
), array('inline' => false));

echo $this->Html->css('tagit');

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
    $('#menu1').addClass('active2');
    $('#menu2').removeClass('active2');
    $('[id^="image"]').click(function(){
        var str = this.id;
        var index = str.replace("image", "");
        console.log(index);
        $('#imgInp'+index).click();
    });
    $('[id^="imgInp"]').click(function(){
        var str = this.id;
        var index = str.replace("imgInp", "");
        $('#imgInp'+index).change(function() {
            readURL(this, index);
        });
    });

    function readURL(input,value) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              isImage = $.inArray( input.files[0].type, [ "image/gif", "image/jpeg", "image/png","image/vnd.microsoft.icon"] );
              if (isImage != -1) {
                  var img = new Image;
                  img.onload = function() {
                      $('#image'+value).attr('src', e.target.result);
                        var img = new Image;
                        img.onload = function() {
                          
                        };
                      img.src = reader.result;
                  };
              img.src = reader.result;
              } else {
                  bootbox.alert(I18nJs.t('Sólo se aceptan formatos gif / jpg / png o ico'));
                  return false;
              }
          }
          reader.readAsDataURL(input.files[0]);
      }
  }


  $('#CompanyProjectOnGoing').on('click', function() {
    if ($('#CompanyProjectOnGoing').prop('checked')) {
      $('#CompanyProjectEndDate').val('');
      $('#CompanyProjectEndDate').parent().parent().hide();
    } else {
      $('#CompanyProjectEndDate').parent().parent().show();
    }
  });
  $('#wheretosee1').prop('checked',true);
  $('#whocansee1').prop('checked',true);
  $('#actualCompanySi').prop('checked',true);
  $('#actualCompanyNo').click(function() {
    $('#CompanyProjectCompanyName').val('');
    $('#CompanyProjectCompanyName').prop('disabled',false);
  });
  
  $('#actualCompanySi').click(function() {
    $('#CompanyProjectCompanyName').val($('#CompanyProjectCompanyNameHidden').val());
    $('#CompanyProjectCompanyName').prop('disabled','disabled');
  });
  $('#wheretosee1').click(function() {
    $('#whocansee1').prop('disabled',false);
    $('#whocansee2').prop('disabled',false);
    $('#whocansee3').prop('disabled',false);
    $('#whocansee4').prop('disabled',false);
    $('#whocansee1').prop('checked', true);
  });
  $('#wheretosee2').click(function() {
    $('#whocansee1').prop('disabled',false);
    $('#whocansee2').prop('disabled',false);
    $('#whocansee3').prop('disabled',false);
    $('#whocansee4').prop('disabled',false);
    $('#whocansee1').prop('checked', true);
  });
  $('#wheretosee3').click(function() {
    $('#whocansee2').prop('checked', true);
    $('#whocansee1').prop('disabled',true);
    $('#whocansee3').prop('disabled',true);
    $('#whocansee4').prop('disabled',true);
  });

  $('.money').mask('000.000.000.000.000,00', {reverse: true});
  var departments   = [ <?php echo $departmentsText;?> ];
  var functionalities = [ <?php echo $functionalitiesText;?> ];
  var companies = [ <?php echo $companiesText;?> ];
  var sectors = [ <?php echo $sectorProjectText;?> ];
  var cities = [ <?php echo $citiesText;?> ];
    var accentMap = {
      "á": "a",
      "ö": "o",
      "é": "e",
      "í": "i",
    };
    var normalize = function( term ) {
      var ret = "";
      for ( var i = 0; i < term.length; i++ ) {
        ret += accentMap[ term.charAt(i) ] || term.charAt(i);
      }
      return ret;
    };
 
    $( "#CompanyProjectDepartmen" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( departments, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#CompanyProjectFunctionalit" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( functionalities, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#CompanyProjectSectorServic" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#CompanyProjectSectorProj" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
     $( "#CompanyProjectCit" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( cities, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#CompanyProjectCustome" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( companies, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#CompanyProjectCompanyName" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( companies, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
  $( ".datepicker" ).datepicker({
    dateFormat: "mm/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: '1925:<?php echo date('Y');?>',
    showButtonPanel: true,
    onClose: function(dateText, inst) {


        function isDonePressed(){
            return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
        }

        if (isDonePressed()){
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
            
             $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
        }
    },
    beforeShow : function(input, inst) {

        inst.dpDiv.addClass('month_year_datepicker')

        if ((datestr = $(this).val()).length > 0) {
            year = datestr.substring(datestr.length-4, datestr.length);
            month = datestr.substring(0, 2);
            $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
            $(this).datepicker('setDate', new Date(year, month-1, 1));
            $(".ui-datepicker-calendar").hide();
        }
    }

  });

  $.datepicker.regional['es'] = {
      closeText: I18nJs.t('Escoger fecha'),
      prevText: '< Anterior',
      nextText: 'Siguiente >',
      currentText: 'Hoy',
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
      dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
      weekHeader: 'Sm',
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''
  };
  $.datepicker.setDefaults($.datepicker.regional['es']);

  

  /* ---------- FuelUX Wizard ---------- */
    var wizard = $('#MyWizard');

    wizard.on('finished', function(e, data) {
        $('#CompanyProjectAddForm').submit();
    });
    function volverAtras() {
        $('#but1').click();
    }
    $('#but1').click(function(){
        $('#butant').click();
        if ($('#step1').hasClass('active')) {
            $('#but1').removeClass('btn-success2');
        }
    });
    $('#but2').click(function(){
        $('#butnext').click();
    });
    $('#CompanyProjectInitDate').on('change', function() {
      $('#CompanyProjectEndDate').val($('#CompanyProjectInitDate').val());
    });
    
    $('.btn-next').on('click', function() {
        //logica de los errores paso a paso

        if ($('#step2').hasClass('active')) {
            valorLocalizacion = $('#CompanyProjectAddress').val();
            if (valorLocalizacion.length == 0) {
              google.maps.event.trigger(map, 'resize');
              var pt = new google.maps.LatLng(42, 21);
              map.setCenter(pt);
            }
            alertError = '<ul>';
            if (($('#CompanyProjectInitDate').val() == '')) {
                alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar una fecha incio/fin de proyecto para continuar')+'</li>';
            }
            if (($('#CompanyProjectCompanyName').val() == '')) {
                alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar un nombre de la compañía para continuar')+'</li>';
            }
            if (($('#CompanyProjectName').val() == '')) {
                alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar un nombre para el proyecto para continuar')+'</li>';
            }
            if (($('#CompanyProjectEndDate').val() == '') && (!$('#CompanyProjectOnGoing').prop('checked'))) {
                alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar una fecha final para el proyecto o pulsar en curso')+'</li>';
            }
            fechaInicio = $('#CompanyProjectInitDate').val();
            if ($('#CompanyProjectOnGoing').prop('checked')) {
              fechaFin = new Date();
              fechaFin = fechaFin.format('mm/yyyy');
            } else {
              fechaFin = $('#CompanyProjectEndDate').val();
            }
            compareFechasInit = fechaInicio.split('/');
            compareFechasEnd  = fechaFin.split('/');
            
            dateNow  = new Date();
            maxYear  = parseInt(dateNow.format('yyyy'));
            maxMonth = parseInt(dateNow.format('mm'));

            
            if ((parseInt(compareFechasEnd[1]) > maxYear) || (parseInt(compareFechasInit[1]) > maxYear)) {
              alertError = alertError + '<li>'+I18nJs.t('El año de fin de proyecto o de inicio de proyecto no puede superar a la fecha actual')+'</li>';
            }
            if ((parseInt(compareFechasEnd[1]) == maxYear) && (parseInt(compareFechasEnd[0]) > maxMonth)) {
              alertError = alertError + '<li>'+I18nJs.t('La fecha de fin de proyecto no puede superar a la fecha actual')+'</li>';
            }
            if ((parseInt(compareFechasInit[1]) == maxYear) && (parseInt(compareFechasInit[0]) > maxMonth)) {
              alertError = alertError + '<li>'+I18nJs.t('La fecha de inicio de proyecto no puede superar a la fecha actual')+'</li>';
            }

            if (($('#CompanyProjectSectorProj').val() == '')) {
              alertError = alertError + '<li>'+I18nJs.t('El sector del proyecto es obligatorio. Añada uno por favor')+'</li>';
            }

            if (parseInt(compareFechasInit[1]) > parseInt(compareFechasEnd[1])) {
              alertError = alertError + '<li>'+I18nJs.t('El año de fin de proyecto no puede ser inferior a la fecha actual')+'</li>';
            } else if (parseInt(compareFechasInit[1]) == parseInt(compareFechasEnd[1])) {
              console.log(compareFechasInit[0]);
              console.log(compareFechasEnd  [0]);
              if (parseInt(compareFechasInit[0]) > parseInt(compareFechasEnd[0])) {
                alertError = alertError + '<li>'+I18nJs.t('La fecha de final de proyecto no puede ser inferior a la de inicio')+'</li>';
              }
            }

            alertError = alertError+'</ul>';
            if (alertError != '<ul></ul>') {
              bootbox.alert(alertError,volverAtras);
            }
        }
        if ($('#step3').hasClass('active')) {
            valorLocalizacion = $('#CompanyProjectLatitude').val()
            if (valorLocalizacion.length == 0) {
              bootbox.alert(I18nJs.t('Para poder pasar al siguiente paso arrastre el puntero hasta un punto exacto del mapa para poder obtener la localización exacta'),volverAtras);
            }
        }
        if ($('#step4').hasClass('active')) {
            alertError = '<ul>';
            if (($('#CompanyProjectBudge').val() == '')) {
              alertError = alertError + '<li>'+I18nJs.t('El presupuesto es obligatorio, por favor añada el presupuesto del proyecto actual')+'</li>';
            }
            if (($('#CompanyProjectBilling').val() == '')) {
              alertError = alertError + '<li>'+I18nJs.t('La facturación final es obligatoria, por favor añada la facturación del proyecto actual')+'</li>';
            }
            alertError = alertError+'</ul>';
            if (alertError != '<ul></ul>') {
              bootbox.alert(alertError,volverAtras);
            }
        }

    });
    $('#CompanyProjectCountryId').change(function() {
      value = $("#CompanyProjectCountryId option:selected").text();
      $('#CompanyProjectCountryNamex').val(value);
    });
    wizard.on('change', function(e, data) {
      
    });
    wizard.wizard();

    var dateFormat = function () {
        var    token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
            timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
            timezoneClip = /[^-+\dA-Z]/g,
            pad = function (val, len) {
                val = String(val);
                len = len || 2;
                while (val.length < len) val = "0" + val;
                return val;
            };
    
        // Regexes and supporting functions are cached through closure
        return function (date, mask, utc) {
            var dF = dateFormat;
    
            // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
            if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
                mask = date;
                date = undefined;
            }
    
            // Passing date through Date applies Date.parse, if necessary
            date = date ? new Date(date) : new Date;
            if (isNaN(date)) throw SyntaxError("invalid date");
    
            mask = String(dF.masks[mask] || mask || dF.masks["default"]);
    
            // Allow setting the utc argument via the mask
            if (mask.slice(0, 4) == "UTC:") {
                mask = mask.slice(4);
                utc = true;
            }
    
            var    _ = utc ? "getUTC" : "get",
                d = date[_ + "Date"](),
                D = date[_ + "Day"](),
                m = date[_ + "Month"](),
                y = date[_ + "FullYear"](),
                H = date[_ + "Hours"](),
                M = date[_ + "Minutes"](),
                s = date[_ + "Seconds"](),
                L = date[_ + "Milliseconds"](),
                o = utc ? 0 : date.getTimezoneOffset(),
                flags = {
                    d:    d,
                    dd:   pad(d),
                    ddd:  dF.i18n.dayNames[D],
                    dddd: dF.i18n.dayNames[D + 7],
                    m:    m + 1,
                    mm:   pad(m + 1),
                    mmm:  dF.i18n.monthNames[m],
                    mmmm: dF.i18n.monthNames[m + 12],
                    yy:   String(y).slice(2),
                    yyyy: y,
                    h:    H % 12 || 12,
                    hh:   pad(H % 12 || 12),
                    H:    H,
                    HH:   pad(H),
                    M:    M,
                    MM:   pad(M),
                    s:    s,
                    ss:   pad(s),
                    l:    pad(L, 3),
                    L:    pad(L > 99 ? Math.round(L / 10) : L),
                    t:    H < 12 ? "a"  : "p",
                    tt:   H < 12 ? "am" : "pm",
                    T:    H < 12 ? "A"  : "P",
                    TT:   H < 12 ? "AM" : "PM",
                    Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                    o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                    S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
                };
    
            return mask.replace(token, function ($0) {
                return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
            });
        };
    }();
    
    // Some common format strings
    dateFormat.masks = {
        "default":      "ddd mmm dd yyyy HH:MM:ss",
        shortDate:      "m/d/yy",
        mediumDate:     "mmm d, yyyy",
        longDate:       "mmmm d, yyyy",
        fullDate:       "dddd, mmmm d, yyyy",
        shortTime:      "h:MM TT",
        mediumTime:     "h:MM:ss TT",
        longTime:       "h:MM:ss TT Z",
        isoDate:        "yyyy-mm-dd",
        isoTime:        "HH:MM:ss",
        isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
        isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
    };
    
    // Internationalization strings
    dateFormat.i18n = {
        dayNames: [
            "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
            "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
        ],
        monthNames: [
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        ]
    };
    
    // For convenience...
    Date.prototype.format = function (mask, utc) {
        return dateFormat(this, mask, utc);
    };

  /* ---------- Datapicker ---------- */
  $('.datepicker').datepicker();

  /* ---------- Choosen ---------- */
  $('[data-rel="chosen"],[rel="chosen"]').chosen();

  /* ---------- Placeholder Fix for IE ---------- */
  $('input, textarea').placeholder();

  /* ---------- Auto Height texarea ---------- */
  $('textarea').autosize();   
});
var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 1,
    center: new google.maps.LatLng(35.137879, -82.836914),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng(47.651968, 9.478485),
    draggable: true
});

var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    $('#CompanyProjectAddress').val('');
    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+evt.latLng.lat().toFixed(3)+","+evt.latLng.lng().toFixed(3)+"&sensor=true";
    locc = evt.latLng;
    map.setCenter(locc);
    
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(locc);
    //latitudeLongitude = JSON.stringify(locc);
    //latitudeLongitude = latitudeLongitude.split(',');
    //latitude = latitudeLongitude[0].substring(5);
    //longitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
    
    latitudeLongitude = JSON.stringify(bounds);
    latitudeLongitude = latitudeLongitude.split(',');
    latitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
    longitude = latitudeLongitude[2].substring(9,latitudeLongitude[2].length);

    $('#CompanyProjectLongitude').val(longitude);
    $('#CompanyProjectLatitude').val(latitude);
    map.fitBounds(bounds);
    map.setZoom(20);
    $.ajax({
      url: url,
    }).success(function(result) {
      console.log(result);
      if (result['status'] == 'OK') {
        if (typeof result != 'undefined') {
          $('#pac-input').val(result['results'][0]['formatted_address']);
          $('#CompanyProjectCompletAddress').val($('#pac-input').val());
          var haslocality = false;
          var hasroute = false;
          var hasadministrativearea = false;
          $.each(result['results'][0]['address_components'], function(index, resultType) {
            if (resultType['types'][0] == 'country') {
                $.ajax({
                  url:  '/Countries/get_country_from_code_2/'+resultType['short_name'],
                  type: 'get',
                  success:  function (response) {
                      if (response != false) {
                        $('#CompanyProjectCountryId').val(response);
                      } else {
                        $('#CompanyProjectCountryId').val(0);
                      }
                      $('#CompanyProjectCountryId').change();
                  }
                });
            }
            if (resultType['types'][0] == 'street_number') {
                $('#CompanyProjectAddress').val(','+resultType['short_name']);
            }
            if (resultType['types'][0] == 'postal_code') {
                $('#CompanyProjectAddress').val($('#CompanyProjectAddress').val()+'('+resultType['short_name']+')');
            }
            if (resultType['types'][0] == 'locality') {
                haslocality = true;
                searchplace = searchplace = resultType['long_name'].trim();
            }
            if (!haslocality) {
              if (resultType['types'][0] == 'administrative_area_level_1') {
                  hasadministrativearea = true;
                  searchplace = resultType['long_name'].trim();
              }
            }
            if ((!haslocality)&&(!hasadministrativearea)) {
              if (resultType['types'][0] == 'route') {
                  hasroute = true;
                  searchplace = resultType['long_name'].trim();
                  $('#CompanyProjectAddress').val(searchplace+$('#CompanyProjectAddress').val());
              }
            }
          });
          console.log(searchplace);
          $.ajax({
            url:  '/Cities/get_city_from_name/'+searchplace,
            type: 'get',
            success:  function (response) {
                if (response != false) {
                  response = JSON.parse(response);
                  $('#CompanyProjectCit').val(response['name']);
                  $('#CompanyProjectCitId').val(response['id']);
                } else {
                  $('#CompanyProjectCit').val(searchplace);
                }
            }
          });
          
        } 
      }
    });
});




google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
    $('#pac-input').val('Asignando punto');
});
google.maps.event.addListener(map, 'bounds_changed', function() {
  var bounds = map.getBounds();
  searchBox.setBounds(bounds);
});




map.setCenter(myMarker.position);
myMarker.setMap(map);
markers = [];
google.maps.event.addListener(searchBox, 'places_changed', function() {
      $('#CompanyProjectAddress').val('');
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }
      for (var i = 0, marker; marker = markers[i]; i++) {
        marker.setMap(null);
      }

      // For each place, get the icon, place name, and location.
      markers = [];
      var bounds = new google.maps.LatLngBounds();
      var myPlaces = [];
      var results  = [];
      for (var i = 0, place; place = places[i]; i++) {
        var image = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        myMarker.setPosition(place.geometry.location);

        map.setCenter(place.geometry.location);
        //markers.push(marker);

        bounds.extend(place.geometry.location);
        var hasCompleteAddress = false;
        if (place.types[0] == 'street_address') {
          //latitudeLongitude = JSON.stringify(place.geometry.location);
          //latitudeLongitude = latitudeLongitude.split(',');
          //latitude = latitudeLongitude[0].substring(5);
          //longitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);

          latitudeLongitude = JSON.stringify(bounds);
          latitudeLongitude = latitudeLongitude.split(',');
          latitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
          longitude = latitudeLongitude[2].substring(9,latitudeLongitude[2].length);
          
          $('#CompanyProjectLongitude').val(longitude);
          $('#CompanyProjectLatitude').val(latitude);
          var hasCompleteAddress = true;
        } else if (place.types[0] == 'route') {
          var hasCompleteAddress = true;
        } else {
          $('#CompanyProjectLongitude').val('');
          $('#CompanyProjectLatitude').val('');
        }
        results.push(place['address_components']);
        myPlaces.push(place.types[0]);
      }
      map.fitBounds(bounds);
      var listener = google.maps.event.addListener(map, "idle", function() { 
        val = map.getZoom();
        switch (myPlaces[0]) {
          case 'continent' :
            val = 4;
            break;
          case 'country' :
            val = 6;
            break;
          case 'administrative_area_level_1':
          case 'locality' :
            val = 13;
            break;
          case 'lodging' :
          case 'route' :
            val = 17;
            break;
          case 'neighborhood' :
            val = 16;
            break;
        }
          if (hasCompleteAddress) {
          $('#CompanyProjectAddress').val('');
          $('#CompanyProjectCompletAddress').val($('#pac-input').val());
          var haslocality = false;
          var hasroute = false;
          var hasadministrativearea = false;
          var i = 0;
          $.each(results[0], function(index, resultType) {
            if (resultType['types'][0] == 'country') {
              var url = '/Countries/get_country_from_code_2/'+resultType['short_name'];

                $.ajax({
                  url:  url,
                  type: 'get',
                  success:  function (response) {
                      if (response != false) {
                        $('#CompanyProjectCountryId').val(response);
                      } else {
                        $('#CompanyProjectCountryId').val(0);
                      }
                      $('#CompanyProjectCountryId').change();
                  }
                });
            }
            if (resultType['types'][0] == 'postal_code') {
                $('#CompanyProjectAddress').val($('#CompanyProjectAddress').val()+'('+resultType['short_name']+')');
            }
            if (resultType['types'][0] == 'street_number') {
                $('#CompanyProjectAddress').val(','+resultType['short_name'].trim());
            }
            if (resultType['types'][0] == 'locality') {
                haslocality = true;
                searchplace = resultType['short_name'].trim();
            }
            if (!haslocality) {
              if (resultType['types'][0] == 'administrative_area_level_1') {
                  hasadministrativearea = true;
                  searchplace = resultType['long_name'].trim();
              }
            }
            if ((!haslocality)&&(!hasadministrativearea)) {
              if (resultType['types'] == 'route') {
                  hasroute = true;
                  searchplace = resultType['long_name'].trim();
                  $('#CompanyProjectAddress').val(searchplace+$('#CompanyProjectAddress').val());
              }
            }
            i++;
          });
          $.ajax({
            url:  '/Cities/get_city_from_name/'+searchplace,
            type: 'get',
            success:  function (response) {
                if (response != false) {
                  response = JSON.parse(response);
                  $('#CompanyProjectCit').val(response['name']);
                  $('#CompanyProjectCitId').val(response['id']);
                } else {
                  $('#CompanyProjectCit').val(searchplace);
                }
            }
          });
        }
        map.setZoom(val);
        google.maps.event.removeListener(listener); 
      });
});

var sectors = [ <?php echo $sectorProjectText;?> ];
console.log(sectors);
$("#mySectors").tagit({
    
    // Options
    fieldName: "data[CompanyProject][Sectors][]",
    availableTags: sectors,
    autocomplete: {delay: 0, minLength: 0},
    showAutocompleteOnFocus: false,
    removeConfirmation: false,
    caseSensitive: true,
    allowDuplicates: false,
    allowSpaces: false,
    readOnly: false,
    tagLimit: null,
    singleField: false,
    singleFieldDelimiter: ',',
    singleFieldNode: null,
    tabIndex: null,
    placeholderText: null,

    // Events
    beforeTagAdded: function(event, ui) {
        console.log(ui.tag);
    },
    afterTagAdded: function(event, ui) {
        console.log(ui.tag);
    },
    beforeTagRemoved: function(event, ui) {
        console.log(ui.tag);
    },
    onTagExists: function(event, ui) {
        console.log(ui.tag);
    },
    onTagClicked: function(event, ui) {
        console.log(ui.tag);
    },
    onTagLimitExceeded: function(event, ui) {
        console.log(ui.tag);
    }

});
<?php $this->Html->scriptEnd();
?>

<div class="box">
    <div class="box-header">
      <h2><i class="fa fa-edit"></i><?php echo __('Añadir proyecto');?></h2>
    </div>
    <div class="box-content">
        <div id="MyWizard" class="wizard">
          <ul class="steps">
              <li data-target="#step1" class="active"><span class="badge badge-info">1</span><span class="chevron"></span><?= __('Información general') ?></li>
              <li data-target="#step2"><span class="badge">2</span><span class="chevron"></span><?= __('Localización') ?></li>
              <li data-target="#step3"><span class="badge ">3</span><span class="chevron"></span><?= __('Contribución de la empresa') ?></li>
              <li data-target="#step4"><span class="badge ">4</span><span class="chevron"></span><?= __('Descripción') ?></li>
              <li data-target="#step5"><span class="badge ">5</span><span class="chevron"></span><?= __('Visualización') ?></li>
          </ul>
          <div class="actions">
              <button type="button" id="butant" class="btn btn-prev hidden"> <i class="fa fa-arrow-left"></i><?php echo __('Ant').'.'?></button>
              <button type="button" id="butnext" class="btn btn-success btn-next hidden" data-last="<?php echo __('Finalizar');?>"><?php echo __('Sig').'.'?><i class="fa fa-arrow-right"></i></button>
          </div>
      </div>

      <div class="step-content">

        <?php echo $this->Form->create('CompanyProject', array('action' => 'add', 'class' => 'form-horizontal rellenar-campos', 'type' => 'file')); ?>

          <div class="step-pane active" id="step1">

              <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php 
                echo __('Ha participado en éste proyecto');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <input type="radio" id="hasParticipedSi" name="CompanyProject[has_participed]" value="S" checked style="margin-right:6px;"><?php echo __('Si');?>
                <input type="radio" id="hasParticipedNo"  name="CompanyProject[has_participed]" value="N" style="  margin-left: 6px;margin-right:6px;"><?php echo __('No');?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Nombre del proyecto*:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php echo $this->Form->input('name',array('label' => false,'class' => 'ui-autocomplete-input span8'));?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Fecha Inicio/ Fin - Desde*:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <input type="text" id="CompanyProjectInitDate" name="CompanyProject[init_date]" class="datepicker" placeholder="mm/yyyy" />
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Hasta*:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <input type="text" id="CompanyProjectEndDate" name="CompanyProject[end_date]" class="datepicker" placeholder="mm/yyyy" />
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
              </div>
              <div class="col-sm-6 col-md-6">
                <?php
                  echo $this->Form->input('on_going',array('type' => 'checkbox','label' => __('Proyecto en curso')));

                ?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Nombre del cliente:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php echo $this->Form->input('custome',array('label' => false,'class' => 'ui-autocomplete-input span8'));?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Sectores del proyecto*:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <ul id="mySectors">
            <!-- Existing list items will be pre-added to the tags -->
                </ul>
              </div>
            </div>
          </div>

          <div class="step-pane" id="step2">
              <div class="form-group add-event">
                  <div class="row">
                      <input id="pac-input" class="controls" type="text" placeholder="<?php echo __('Busca la ubicación de tu proyecto');?>">
                      <div class="col-sm-12 col-md-12 col-xs-12">
                        <p><?php echo __('Haga una búsqueda en el mapa y en caso de no encontrar dirección exacta arrastre el puntero hasta la zona dónde es de su proyecto')?></p>
                        <div id="map-canvas" class="col-md-9 col-xs-9 -col-sm-9"></div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                          <?php 
                          echo $this->Form->input('country_namex', array('label'=>__('País de ejecución:'),'readonly' => 'readonly'));
                          echo $this->Form->input('country_id', array('label'=>false, 'class'=>'hidden'));
                          echo $this->Form->input('cit',array('label'=>__('Ciudad de ejecución:'),'class' => 'ui-autocomplete-input ok_input span8','readonly' => 'readonly'));
                          echo $this->Form->input('cit_id', array('label'=>false, 'type'=>'hidden'));
                          echo $this->Form->input('address',array('label'=>__('Dirección:'),'class' => 'hidden', 'label' => false));
                          echo $this->Form->input('complet_address',array('label'=>__('Dirección:'),'class' => 'hidden', 'label' => false));
                          echo $this->Form->input('longitude',array('class' =>'hidden','label' => false));
                          echo $this->Form->input('latitude',array('class' => 'hidden','label' => false));
                          ?>
                        </div>
                      </div>
                  </div>
              </div>
          </div>

          <div class="step-pane" id="step3">
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                    <label><?php echo __('Línea de negocio:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php 
                    echo $this->Form->input('sector_servic',array('label'=>false,'class' => 'ui-autocomplete-input ok_input span8'));
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                    <label><?php echo __('Función:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php 
                    echo $this->Form->input('functionalit',array('label'=>false, 'class' => 'ui-autocomplete-input ok_input span8'));
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                    <label><?php echo __('Subfunción:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php 
                  echo $this->Form->input('departmen',array('label'=>false,'class' => 'ui-autocomplete-input ok_input span8'));
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                    <label><?php echo __('Duración de su servicio:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <input type="text" name="data[CompanyProject][duration_year]" class="tiempo" placeholder="<?=__('Años');?>">
                  <input type="text" name="data[CompanyProject][duration_month]" class="tiempo" placeholder="<?=__('Meses');?>">
                  <input type="text" name="data[CompanyProject][duration_day]" class="tiempo" placeholder="<?=__('Días');?>">
                  <input type="text" name="data[CompanyProject][duration_hours]" class="tiempo" placeholder="<?=__('Horas');?>">
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Presupuesto de adjudicación*:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php 
                  echo $this->Form->input('budge',array('label' => false,'class' => 'money'));
                  echo $this->Form->input('budget_currency_id',array('label' => false,'div' => false,'style' => 'width:200px;height:26px;'));
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Facturación final*:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php 
                  echo $this->Form->input('billing',array('label' => false,'class' => 'money'));
                  echo $this->Form->input('billing_currency_id',array('label' => false,'div' => false,'style' => 'width:200px;height:26px;'));
                  ?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Volúmenes ejecutados:');?></label>
                </div>
                <div class="col-sm-6 col-md-6">
                  <?php echo $this->Form->textarea('volumes',array('id'=>'area1','style'=>'width:450px;'));?>
                </div>
              </div>
          </div>
          <div class="step-pane" id="step4">
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Descripción:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php echo $this->Form->textarea('description',array('id'=>'area2','style'=>'width:450px;height:140px;'));?>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header">
                    <h2><i class="fa fa-picture-o"></i><?php echo __('Imágen por defecto');?></h2>
                  </div>
                  <div class="box-content">
                    <div class="row">
                      <div class="col-md-4">&nbsp;</div>
                      <div class="col-md-4">
                          <?php 
                          $urlImage = (!empty($images)) ? $user['User']['photo_dir'].$user['User']['photo'] : 'countries/noimage.png';
                          if ($urlImage == 'countries/noimage.png') {
                            $nameImage = $this->webroot.'img'.DS.$urlImage;  
                          } else {
                            $nameImage = $this->webroot.$urlImage;
                          }
                          ?>
                          <img src="<?php echo  $nameImage?>" width="100%" id="image1" type ='file' alt="<?php echo __('Imágen de perfil');?>" > 
                          <?php
                              echo $this->Form->input('CompanyProject.image1',array('id'=> 'imgInp1','label' => false,'class'=>'hidden','type' => 'file'));
                          ?>
                      </div>
                      <div class="col-md-4">&nbsp;</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="box">
                  <div class="box-header">
                    <h2><i class="fa fa-picture-o"></i><?php echo __('Añadir más imágenes');?></h2>
                  </div>
                  <div class="box-content">
                    <div class="row">
                      <div class="col-md-4 col-xs-4 col-sm-4">
                  <?php 
                  $urlImage = (!empty($images)) ? $user['User']['photo_dir'].$user['User']['photo'] : 'countries/noimage.png';
                  if ($urlImage == 'countries/noimage.png') {
                    $nameImage = $this->webroot.'img'.DS.$urlImage;  
                  } else {
                    $nameImage = $this->webroot.$urlImage;
                  }
                  ?>
                  <img src="<?php echo  $nameImage?>" width="100%" id="image2" type ='file' alt="<?php echo __('Imágen de perfil');?>" > 
                  <?php
                      echo $this->Form->input('CompanyProject.image2',array('id'=> 'imgInp2','label' => false,'class'=>'hidden','type' => 'file'));
                  ?>
              </div>
              <div class="col-md-4 col-xs-4 col-sm-4">
                  <?php 
                  $urlImage = (!empty($images)) ? $user['User']['photo_dir'].$user['User']['photo'] : 'countries/noimage.png';
                  if ($urlImage == 'countries/noimage.png') {
                    $nameImage = $this->webroot.'img'.DS.$urlImage;  
                  } else {
                    $nameImage = $this->webroot.$urlImage;
                  }
                  ?>
                  <img src="<?php echo  $nameImage?>" width="100%" id="image3" type ='file' alt="<?php echo __('Imágen de perfil');?>" > 
                  <?php
                      echo $this->Form->input('CompanyProject.image3',array('id'=> 'imgInp3','label' => false,'class'=>'hidden','type' => 'file'));
                  ?>
              </div>
              <div class="col-md-4 col-xs-4 col-sm-4">
                  <?php 
                  $urlImage = (!empty($images)) ? $user['User']['photo_dir'].$user['User']['photo'] : 'countries/noimage.png';
                  if ($urlImage == 'countries/noimage.png') {
                    $nameImage = $this->webroot.'img'.DS.$urlImage;  
                  } else {
                    $nameImage = $this->webroot.$urlImage;
                  }
                  ?>
                  <img src="<?php echo  $nameImage?>" width="100%" id="image4" type ='file' alt="<?php echo __('Imagen de perfil');?>" > 
                  <?php
                      echo $this->Form->input('CompanyProject.image4',array('id'=> 'imgInp4','label' => false,'class'=>'hidden','type' => 'file'));
                  ?>
              </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>                          
          </div>
          <div class="step-pane" id="step5">
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Dónde se verá su CV:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                
                <input type="radio" id="wheretosee1"  name="data[CompanyProject][where_to_see]" value="M" style="margin-right:6px;"><?php echo __('Mundial');?>
                <input type="radio"  id="wheretosee2" name="data[CompanyProject][where_to_see]" value="N" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Nacional');?>
                <input type="radio"  id="wheretosee3"  name="data[CompanyProject][where_to_see]" value="I" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Interno');?>


              </div>
            </div>   
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Quién lo verá:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                
                <input type="radio" id="whocansee1" name="data[CompanyProject][who_can_see]" value="T" style="margin-right:6px;"><?php echo __('Todos');?>
                <input type="radio" id="whocansee2"  name="data[CompanyProject][who_can_see]" value="C" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Colegas');?>
                <input type="radio" id="whocansee3"  name="data[CompanyProject][who_can_see]" value="U" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Usuarios');?>
                <input type="radio" id="whocansee4"  name="data[CompanyProject][who_can_see]" value="E" style="  margin-left: 6px;margin-right:6px;"><?php echo __('Empresas');?>

              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Quién será contactado:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php echo $this->Form->input('contact_responsable_id',array('options' => $contactResponsableList,'label' => false,'style' => 'margin-right:6px;'))?>
                
                </div>
            </div>
          </div>
  
          <div class="actions">
              <button type="button" id="but1" class="btn btn-prev"> <i class="fa fa-arrow-left"></i><?php echo __('Ant').'.'?></button>
              <button type="button" id="but2" class="btn btn-success2 btn-next pull-right2" data-last="<?php echo __('Finalizar');?>"><?php echo __('Sig').'.'?><i class="fa fa-arrow-right"></i></button>
          </div>

        <?= $this->Form->end(); ?>

      </div>
    </div>
</div>

<style>
.rellenar-campos input, textarea, select {
  margin: 3px;
  padding: 2px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border: 1px solid #999999;
  border-color: #47789f;
}
#map-canvas {
  height: 400px;
  width: 70%;
  margin: 0px;
  border-color: #47789f;
}
#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.ui-datepicker-calendar {
    display: none;
    }

.pac-container {
  font-family: Roboto;
  font-color: #47789f
}
.tiempo {
  width:60px;
}
.btn-success2 {
  background-color: #47789f;
  color:#fff !important;
}

.myTags {
  border: 1px solid #799cb9;
  border-radius: 5px;
  margin-bottom: 5px;
}
.mySectors{
  border: 1px solid #799cb9;
  border-radius: 5px;
  margin-bottom: 5px;
}
.pull-right2 {
  text-align: right;
  float: right !important;
}
.step-content .step-pane {
    width: 850px;
    margin: auto;
}
</style>