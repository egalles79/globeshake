<?php

$this->extend('/Common/panel');
echo $this->Html->script(array(
    'jquery-ui-1.10.3.custom.min',
    'jquery.sparkline.min',
    'jquery.chosen.min',
    'jquery.autosize.min',
    'jquery.placeholder.min',
    'daterangepicker.min',
    'moment.min',
    'jquery.steps.min',
    'custom.min',
    'bootbox.min',
    'core.min',
    'jquery.tooltipster.min',
    'jquery.mask.min',
    'tag-it'
), array('inline' => false));

echo $this->Html->css('tagit');

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
    $('#menu1').addClass('active2');
    $('#menu2').removeClass('active2');
   

  $('.money').mask('000.000.000.000.000,00', {reverse: true});
  var departments   = [ <?php echo $departmentsText;?> ];
  var functionalities = [ <?php echo $functionalitiesText;?> ];
  var companies = [ <?php echo $companiesText;?> ];
  var sectors = [ <?php echo $sectorProjectText;?> ];
  var cities = [ <?php echo $citiesText;?> ];
    var accentMap = {
      "á": "a",
      "ö": "o",
      "é": "e",
      "í": "i",
    };
    var normalize = function( term ) {
      var ret = "";
      for ( var i = 0; i < term.length; i++ ) {
        ret += accentMap[ term.charAt(i) ] || term.charAt(i);
      }
      return ret;
    };
 
    
    // For convenience...
    Date.prototype.format = function (mask, utc) {
        return dateFormat(this, mask, utc);
    };

  /* ---------- Datapicker ---------- */
  $('.datepicker').datepicker();

  /* ---------- Choosen ---------- */
  $('[data-rel="chosen"],[rel="chosen"]').chosen();

  /* ---------- Placeholder Fix for IE ---------- */
  $('input, textarea').placeholder();

  /* ---------- Auto Height texarea ---------- */
  $('textarea').autosize();   
});
<?php $this->Html->scriptEnd();
?>
<div class="box">
<div class="box-content">

<div class="row" style="">
	<br>
	<h1><?php echo __('Proyecto %s', $project['CompanyProject']['name']);?></h1>
	<br><br>
  <div class="col-md-12 marg2">
    <div class="col-md-4">
      <img src="../../img/icon_03p.png" class="pull-left" style="">
      <p class="blue" style="margin-bottom: 0px;"><?php echo __('CONTACTO');?>:&nbsp;<span style="color:#47789f"><?php echo $staticUserNames[$project['CompanyProject']['contact_responsable_id']];?></span></p>
    </div>
    <div class="col-md-4">
      <img src="../../img/logo_hombre_casco.png" class="pull-left" style="margin:4px;">
      <p class="blue" style="margin-bottom: 0px;"><?php echo __('ÚLTIMO REVISOR');?>:&nbsp;<span style="color:#47789f"><?php echo $staticUserNames[$project['CompanyProject']['last_reviewer_id']];?></span></p>
    </div>
    <div class="col-md-4">
      <img src="../../img/icon_05p.png" class="pull-left" style="">
      <p class="blue" style="margin-bottom: 0px;"><?php echo __('PROPIETARIO');?>:&nbsp;<span style="color:#47789f"><?php echo $staticUserNames[$project['CompanyProject']['contact_responsable_id']];?></span></p>
    </div>
    <div class="col-md-12">
      <img src="../../img/icon_07p.png" class="pull-left" style="">
      <p class="blue" style="margin-bottom: 0px;"><?php echo __('EQUIPO');?>:&nbsp;<span style="color:#47789f">
        <?php 
        $nameUsersProjects = '';
        $numusers = 0;
        foreach($usersProjects as $userProject) {
          if ($numusers > 9) {
            $nameUsersProjects .=',...  ';
            break;
          }
          $numusers++;
          $nameUsersProjects .= $staticUserNames[$userProject].', ';
        }
        echo substr($nameUsersProjects,0,strlen($nameUsersProjects)-2);
        ?>

      </span></p>
    </div>
    <div class="col-md-12">
      <?php 
                $selected = (!empty($project['ContactResponsable']['id'])) ? $project['ContactResponsable']['id'] : 0;
                if ($selected !=0) {?>
                <div class="box_one round row-fluid" id="conact_who" style="">
                <div class="abso_mini_tit blue">
                  <?php echo __('¿QUIEN SERÁ CONTACTADO?').':';?></div>
                  <br>
                  <div class="cold-md-12">
                    <p class="col-md-4"><?php echo __('Responsable de contacto').':';?></p>
                    <div class="col-md-7 new_font"><?php echo $staticUserNames[$project['ContactResponsable']['id']];?></div>
                    <div class="clearfix"></div>
                  </div>
                </div>
              <?php } ?>
    </div>
  </div>
</div>
<div class="row box">
    <div class="box-header">
	    <h2 class="title " style="color:#fff">
                  <i class="fa fa-edit"></i>
                  <a href="#" class="btn-minimize" style="text-decoration:none;color:#fff"><?php echo __('Ver proyecto %s', $project['CompanyProject']['name']);?></a>
                </h2>
    </div>
    
    <div class="box-content">
	    <div class="row marg2 ">
		    
        <?php echo $this->Form->create('Companies', array('action' => 'ver_cv', 'class' => 'form-horizontal rellenar-campos box_one round')); ?>
          <div class="row">
	          <div class="abso_mini_tit blue " style="margin-left: 16px;">INFORMACIÓN GENERAL DEL PROYECTO:</div>
	          <div class="clearfix"></div>
            <div class="col-sm-4 col-md-4 text-right">
              <label><?php 
              echo __('Ha participado en éste proyecto');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php if ($bUserInProject) {
                  echo __('Sí');
                } else {
                  echo __('No');
                }
                ?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
              <label><?php echo __('Nombre del proyecto*:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php echo $this->Form->input('name',array('label' => false,'class' => 'ui-autocomplete-input span8','value' => $project['CompanyProject']['name'],'readonly' => 'readonly',));?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
              <label><?php echo __('Fecha Inicio/ Fin - Desde*:');?></label>
            </div>
            <?php 
            $dateInit = explode('-',$project['CompanyProject']['start_date']);
            $dateInit = $dateInit[1].'/'.$dateInit[0];
            ?>
            <div class="col-sm-6 col-md-6">
              <input type="text" id="CompanyProjectInitDate" name="CompanyProject[start_date]"  readonly placeholder="mm/yyyy" value="<?php echo $dateInit;?>" />
            </div>
          </div>
          <?php if (!empty($dateEnd)) {?>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
              <label><?php echo __('Hasta*:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <input type="text" id="CompanyProjectEndDate" name="CompanyProject[end_date]" class="" readonly placeholder="mm/yyyy" value="<?php echo $dateEnd;?>" />
            </div>
          </div>
          <?php } ?>
          <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
              </div>
              <div class="col-sm-6 col-md-6">
                <?php
                  if ($project['CompanyProject']['on_going']) {
                    echo  __('Proyecto en curso');
                  }
                ?>
              </div>
          </div>

            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Nombre del cliente:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php echo $this->Form->input('custome',array('label' => false,'class' => 'ui-autocomplete-input span8', 'readonly' => 'readonly', 'value' => $project['Customer']['name']));?>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Sectores del proyecto*:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <ul id="mySectors">
                  <?php
                  if (!empty($sectors_companyproject))
                  {
                    foreach ($sectors_companyproject as $cprojectsSector) {
                      echo "<li>".$staticSectorsNames[$cprojectsSector]."</li>";
                    }
                  }
                ?>
                </ul>
              </div>

            </div>
    </div>
    <div class="row marg2">
	    <div class="col-md-12 box_one round">
		    <div class="abso_mini_tit blue ">LOCALIZACIÓN:</div>
	          <div class="clearfix"></div>
          <div class="col-sm-12 col-md-12 col-xs-12">
            <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Localización:')?></label>
              </div>
            
            <div class="col-md-6" style="height:300px;background-image:url(https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:Sede%7C<?php echo $project['Geolocalitzation']['latitude']?>,<?php echo $project['Geolocalitzation']['longitude']?>);">
               <?php 
              echo $this->Form->input('country_namex', array('label'=>__('País de ejecución:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'),'readonly' => 'readonly', 'value' => $staticCountriesNames[$project['CompanyProject']['country_id']]));
              echo $this->Form->input('country_id', array('label'=>false, 'class'=>'hidden', 'value' => $project['Country']['id']));
              echo $this->Form->input('cit',array('label'=>__('Ciudad de ejecución:'),'class' => 'ui-autocomplete-input ok_input span8','readonly' => 'readonly', 'value' => $staticCitiesNames[$project['CompanyProject']['city_id']]));
              echo $this->Form->input('cit_id', array('label'=>false, 'type'=>'hidden', 'value' => $project['CompanyProject']['city_id']));
              echo $this->Form->input('id', array('label'=>false, 'type'=>'hidden', 'value' => $project['CompanyProject']['id']));
              echo $this->Form->input('longitude',array('class' =>'hidden','label' => false,'value' =>$project['Geolocalitzation']['longitude']));
              echo $this->Form->input('latitude',array('class' => 'hidden','label' => false,'value' => $project['Geolocalitzation']['latitude']));
              ?>
            </div>
            <br><br>
          </div>
      </div>
      </div>
      <div class="clearfix visible-xs"></div>
      
      <div class="row marg2">
	      <div class="col-md-12 box_three round">
		      <div class="abso_mini_tit blue ">DESCRIPCIÓN:</div>
	          <div class="clearfix"></div>
              <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Descripción:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php echo $this->Form->textarea('description',array('id'=>'area2','style'=>'width:450px;height:140px;','readonly' => 'readonly','placeholder' => __('Rellene la descripción').'...','value' => $project['CompanyProject']['description']));?>
              </div>
            </div>
            </div>
            <div class="row marg2">
              <div class="col-md-12 box_three round">
	              <div class="abso_mini_tit blue "><?php echo __('IMÁGEN POR DEFECTO');?>:</div>
                <div class="clearfix"></div>
                <div class="row">
                  <div class="col-md-4">&nbsp;</div>
                  <div class="col-md-4">
                      <?php 
                      $images = $project['CompanyProjectsImages'];
                      foreach ($images as $image) {
                        if ($image['default']) {
                           $urlImage = (!empty($images)) ? 'companies/company_'.$project['Company']['id'].'/company_project_'.$project['CompanyProject']['id'].'/'.$image['name'] : 'countries/noimage.png';
                          $nameImage = ($urlImage == 'countries/noimage.png') ? $this->webroot.'img'.DS.$urlImage : $this->webroot.'files'.DS.$urlImage;
                          break;
                        }
                        
                      }
                      
                      ?>
                      <img src="<?php echo  $nameImage?>" width="100%" id="image1" type ='file' alt="<?php echo __('Imágen de perfil');?>" > 
                      <?php
                          echo $this->Form->input('CompanyProject.image1',array('id'=> 'imgInp1','label' => false,'class'=>'hidden','type' => 'file'));
                      ?>
                  </div>
                  <div class="col-md-4">&nbsp;</div>
              </div>
              <div class="col-md-12">
                <h2><?php echo __('Añadir más imágenes');?></h2>
                <div class="row">
                  <?php 
                  $images = $project['CompanyProjectsImages'];
                  $counter = 1;
                  foreach ($images as $image) { 
                     if (!$image['default']) { ?>
                          <div class="col-md-4 col-xs-4 col-sm-4">
                     <?php
                      $counter++;
                       $urlImage = (!empty($images)) ? 'companies/company_'.$project['Company']['id'].'/company_project_'.$project['CompanyProject']['id'].'/'.$image['name'] : 'countries/noimage.png';
                      if ($urlImage == 'countries/noimage.png') {
                        $nameImage = $this->webroot.'img'.DS.$urlImage;  
                      } else {
                        $nameImage = $this->webroot.'files'.DS.$urlImage;
                      }?>
                       <img src="<?php echo  $nameImage?>" width="100%" id="image<?php echo $counter?>" type ='file' alt="<?php echo __('Imágen de perfil');?>" >
                       <?php
                          echo $this->Form->input('CompanyProject.image'.$counter,array('id'=> 'imgInp'.$counter,'label' => false,'class'=>'hidden','type' => 'file'));
                    ?>
                    </div>
                    <?php } 
                  }
                  ?>
              
              <?php 
              if ($counter < 4) {
                for($i=$counter+1;$i<=4;$i++) {
                  $nameImage = $this->webroot.'img'.DS.'countries/noimage.png';
                  $aprint = '<div class="col-md-4 col-xs-4 col-sm-4">';
                  $aprint .= '<img src="'.$nameImage.'" width="100%" id="image'.$i.'" type ="file" alt="'.__("Imagen").'">';
                  $aprint .= $this->Form->input('CompanyProject.image'.$i,array('id'=> 'imgInp'.$i,'label' => false,'class'=>'hidden','type' => 'file'));
                  $aprint .= '</div>';
                  echo $aprint;
                }
              }
              echo '</div>';
               ?>
                </div></div></div>
                <div class="row marg2 rellenar-campos">
	                <div class="col-md-12 box_two round">
		                <div class="abso_mini_tit blue "><?php echo __('CONTRIBUCIÓN DE LA EMPRESA');?>:</div>
                <div class="clearfix"></div>
                <div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Línea de negocio:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php 
                  echo $this->Form->input('sector_servic',array('label'=>false,'class' => 'ui-autocomplete-input ok_input span8','readonly' => 'readonly','value' => $project['Sector']['name']));
                ?>
              </div>
              </div><div class="row">
               <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Función:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php 
                  echo $this->Form->input('functionalit',array('label'=>false, 'class' => 'ui-autocomplete-input ok_input span8', 'readonly' => 'readonly','value' => $project['Functionality']['name']));
                ?>
              </div>
              </div><div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Subfunción:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php 
                echo $this->Form->input('departmen',array('label'=>false,'class' => 'ui-autocomplete-input ok_input span8','readonly' => 'readonly','value' => $project['Department']['name']));
                ?>
              </div>
              </div><div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Duración de su servicio:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <input type="text" name="data[CompanyProject][duration_year]" class="tiempo" placeholder="<?=__('Años');?>" readonly value="<?php echo $project['CompanyProject']['duration_year']?>">
                <input type="text" name="data[CompanyProject][duration_month]" class="tiempo" placeholder="<?=__('Meses');?>" readonly value="<?php echo $project['CompanyProject']['duration_month']?>">
                <input type="text" name="data[CompanyProject][duration_day]" class="tiempo" placeholder="<?=__('Días');?>" readonly value="<?php echo $project['CompanyProject']['duration_day']?>">
                <input type="text" readonly name="data[CompanyProject][duration_hours]" class="tiempo" placeholder="<?=__('Horas');?>" value="<?php echo $project['CompanyProject']['duration_hours']?>">
              </div>
              </div><div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Presupuesto de adjudicación*:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php 
                echo $this->Form->input('budge',array('label' => false,'class' => 'money', 'readonly' => 'readonly','value' => $project['CompanyProject']['budget']));
                
                echo $project['BudgetCurrency']['symbol'].' ('.$project['BudgetCurrency']['name'].')';
                ?>
              </div>
              </div><div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Facturación final*:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php 
                echo $this->Form->input('billing',array('label' => false,'class' => 'money','readonly' => 'readonly','value' => $project['CompanyProject']['billing']));
                echo $project['BillingCurrency']['symbol'].' ('.$project['BillingCurrency']['name'].')';
                ?>
              </div>
              </div><div class="row">
              <div class="col-sm-4 col-md-4 text-right">
                <label><?php echo __('Volúmenes ejecutados:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <?php echo $this->Form->textarea('volumes',array('id'=>'area1','style'=>'width:450px;','value'=>$project['CompanyProject']['volumes'],'readonly' => 'readonly'));?>
              </div>
              </div></div></div>
              <div class="row marg2 rellenar-campos">
	              
	              <div class="col-md-12 box_four round">
		              <div class="abso_mini_tit blue "><?php echo __('VISUALIZACIÓN');?>:</div>
                <div class="clearfix"></div>
              <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Quién lo verá:');?></label>
              </div>
              <div class="col-sm-6 col-md-6">
                <div class="input text"><input name="" class="" readonly="readonly" value="<?php 
                if ($project['CompanyProject']['who_can_see'] == 'T') {
                  echo __('Todos');
                } else if ($project['CompanyProject']['who_can_see'] == 'C') {
                  echo __('Colegas');
                } else if ($project['CompanyProject']['who_can_see'] == 'U') {
                  echo __('Usuarios');
                } else if ($project['CompanyProject']['who_can_see'] == 'E') {
                  echo __('Empresas');
                }
                ?>">
              </div></div>
              <div class="col-sm-4 col-md-4 text-right">
                  <label><?php echo __('Dónde se verá su CV:');?></label>
              </div>
              
              <div class="col-sm-6 col-md-6">
	              <div class="input text"><input name="" class="" readonly="readonly" value="<?php 
                if ($project['CompanyProject']['where_to_see'] == 'M') {
                  echo __('Mundial');
                } else if ($project['CompanyProject']['where_to_see'] == 'N') {
                  echo __('Nacional');
                } else if ($project['CompanyProject']['where_to_see'] == 'I') {
                  echo __('Interno');
                }
                ?>" type="text" id="" autocomplete="off"></div>
                

              </div>
                </div>
              </div>
            </div>
            <?php echo $this->Form->end('Volver'); ?>
      </div>

    </div>
</div>

<style>
.rellenar-campos input, textarea, select {
  margin: 3px;
  padding: 2px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border: 1px solid #999999;
  border-color: #47789f;
}

#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.ui-datepicker-calendar {
    display: none;
    }

.pac-container {
  font-family: Roboto;
  font-color: #47789f
}
.tiempo {
  width:60px;
}
.btn-success2 {
  background-color: #47789f;
  color:#fff !important;
}

.myTags {
  border: 1px solid #799cb9;
  border-radius: 5px;
  margin-bottom: 5px;
}
.mySectors{
  border: 1px solid #799cb9;
  border-radius: 5px;
  margin-bottom: 5px;
}
.pull-right2 {
  text-align: right;
  float: right !important;
}
.box_one {
    padding: 20px;
    margin-bottom: 25px;
    background-color: #e6ebf1;
    margin-top: 20px;
}
.round {
  border-radius: 15px;
  box-shadow: 7px 7px 6px -3px #cbcbcb;
}
.abso_mini_tit {
  background-color: white;
  float: left;
  position: relative;
  bottom: 30px;
  font-weight: bold;
}
.blue {
  color: #47789f;
}
.span4 {
  width: 31.914893617021278%;
}
.new_font {
    border: 1px solid #799ebe;
    color: #666666;
    line-height: 1.5em;
    width: 57.44680851063829%;
}
.row.marg2{
	margin-bottom: 30px;
}
.box_three{
	    padding: 20px;
    margin-bottom: 25px;
    background-color: #e5cd98;
    margin-top: 20px;
}
.box_two {
    background-color: #cdcdcd;
    padding: 20px;
    margin-bottom: 25px;
    margin-top: 20px;
}
.box_four {
    background-color: #abc4dd!important;
    padding: 20px;
    margin-bottom: 25px;
    margin-top: 20px;
}
</style>