<?php

$this->extend('/Common/panel');
echo $this->Html->script(array(
    'jquery-ui-1.10.3.custom.min',
    'jquery.sparkline.min',
    'jquery.chosen.min',
    'jquery.autosize.min',
    'jquery.placeholder.min',
    'daterangepicker.min',
    'moment.min',
    'jquery.steps.min',
    'custom.min',
    'bootbox.min',
    'jquery.tooltipster.min',
    'jquery.mask.min',
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){

  $('#menu1').addClass('active2');
  $('#menu2').removeClass('active2');

  $('.submit').on('click', function() {
    if ($('#OfficeCit').val() == '') {
      bootbox.alert('Debe seleccionar una ciudad');
      return false;
    }
    if ($('#OfficeName').val() == '') {
      bootbox.alert('Debe seleccionar un nombre de empresa');
      return false;
    }
    if ($('#OfficeContactResponsableId').val() == 0) {
      bootbox.alert('Debe seleccionar una persona de contacto');
      return false;
    }
    
  });
  $('[id^="image"]').click(function(){
        var str = this.id;
        var index = str.replace("image", "");
        $('#imgInp'+index).click();
    });
    $('[id^="imgInp"]').click(function(){
        var str = this.id;
        var index = str.replace("imgInp", "");
        $('#imgInp'+index).change(function() {
            readURL(this, index);
        });
    });

    function readURL(input,value) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                isImage = $.inArray( input.files[0].type, [ "image/gif", "image/jpeg", "image/png","image/vnd.microsoft.icon"] );
                if (isImage != -1) {
                    var img = new Image;
                    img.onload = function() {
                        $('#image'+value).attr('src', e.target.result);
                          var img = new Image;
                          img.onload = function() {
                            max_ancho = 300;
                            max_alto = 250;
                            x_ratio = max_ancho / img.width;
                            y_ratio = max_alto / img.height;
                            if ((img.width <= max_ancho) && (img.height <= max_alto)) {
                            //Si es más pequeña que el máximo no redimensionamos
                            ancho_final = img.width;
                            alto_final = img.height;
                            } else if ((x_ratio * img.height) < max_alto) {
                              alto_final = x_ratio * img.height;
                              ancho_final = max_ancho;
                            } else{
                              ancho_final = y_ratio * img.width;
                              alto_final = max_alto;
                            }
                            $('#image'+value).attr('style', 'width:'+ancho_final+'px;height:'+alto_final+'px');
                          };
                        img.src = reader.result;
                    };
                img.src = reader.result;
                } else {
                    bootbox.alert(I18nJs.t('Sólo se aceptan formatos gif / jpg / png o ico'));
                    return false;
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


  $('#OfficeCountryId').change(function() {
    value = $("#OfficeCountryId option:selected").text();
    $('#OfficeCountryNamex').val(value);
  });
    var accentMap = {
      "á": "a",
      "ö": "o",
      "é": "e",
      "í": "i",
    };
    var normalize = function( term ) {
      var ret = "";
      for ( var i = 0; i < term.length; i++ ) {
        ret += accentMap[ term.charAt(i) ] || term.charAt(i);
      }
      return ret;
    };

  /* ---------- Placeholder Fix for IE ---------- */
  $('input, textarea').placeholder();

  /* ---------- Auto Height texarea ---------- */
  $('textarea').autosize();   
});
var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 1,
    center: new google.maps.LatLng(35.137879, -82.836914),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var myMarker = new google.maps.Marker({
    zoom: 18,
    position: new google.maps.LatLng($('#OfficeLatitude').val(), $('#OfficeLongitude').val()),
    draggable: true
});

var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

    google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    $('#OfficeAddresss').val('');
    $('#OfficeCitId').val('');
    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+evt.latLng.lat().toFixed(3)+","+evt.latLng.lng().toFixed(3)+"&sensor=true";
    locc = evt.latLng;
    map.setCenter(locc);
    
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(locc);
    latitudeLongitude = JSON.stringify(bounds);
    
    latitudeLongitude = latitudeLongitude.split(',');
    latitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
    console.log(latitudeLongitude);
    longitude = latitudeLongitude[2].substring(9,latitudeLongitude[2].length);

    $('#OfficeLongitude').val(longitude);
    $('#OfficeLatitude').val(latitude);

    map.fitBounds(bounds);
    map.setZoom(20);
    $.ajax({
      url: url,
    }).success(function(result) {
      if (result['status'] == 'OK') {
        if (typeof result != 'undefined') {
          $('#pac-input').val(result['results'][0]['formatted_address']);
          $('#OfficeCompletAddress').val($('#pac-input').val());
          var haslocality = false;
          var hasroute = false;
          var hasadministrativearea = false;
          $.each(result['results'][0]['address_components'], function(index, resultType) {
            if (resultType['types'][0] == 'country') {
                var url = '/Countries/get_country_from_code_2/'+resultType['short_name'];
                console.log(url); 
                $.ajax({
                  url:  url,
                  type: 'get',
                  success:  function (response) {
                      if (response != false) {
                        $('#OfficeCountryId').val(response);
                      } else {
                        $('#OfficeCountryId').val(0);
                      }
                      $('#OfficeCountryId').change();
                  }
                });
            }
            if (resultType['types'][0] == 'street_number') {
                $('#OfficeAddress').val(','+resultType['short_name']);
            }
            if (resultType['types'][0] == 'postal_code') {
                $('#OfficeAddress').val($('#OfficeAddress').val()+'('+resultType['short_name']+')');
            }
            if (resultType['types'][0] == 'locality') {
                haslocality = true;
                searchplace = searchplace = resultType['long_name'].trim();
            }
            if (!haslocality) {
              if (resultType['types'][0] == 'administrative_area_level_1') {
                  hasadministrativearea = true;
                  searchplace = resultType['long_name'].trim();
              }
            }
            if ((!haslocality)&&(!hasadministrativearea)) {
              if (resultType['types'][0] == 'route') {
                  hasroute = true;
                  searchplace = resultType['long_name'].trim();
                  $('#OfficeAddress').val(searchplace+$('#OfficeAddress').val());
              }
            }
          });
          url = '/Cities/get_city_from_name/'+searchplace;
          $.ajax({
            url:  url,
            type: 'get',
            success:  function (response) {
                console.log(response);
                if (response != false) {
                  response = JSON.parse(response);
                  $('#OfficeCit').val(response['name']);
                  $('#OfficeCitId').val(response['id']);
                } else {
                  $('#OfficeCit').val(searchplace);
                }
            }
          });
          $('#OfficeAddresss').val(result['results'][0]['formatted_address']);
        } 
      }
    });
});


adddress = $('#OfficeAddresss').val();
$('#pac-input').val(adddress);
google.maps.event.trigger(map, 'resize');

google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
    $('#pac-input').val('Asignando punto');
});
google.maps.event.addListener(map, 'bounds_changed', function() {
  var bounds = map.getBounds();
  searchBox.setBounds(bounds);
});

map.setCenter(myMarker.position);
myMarker.setMap(map);
markers = [];

google.maps.event.addListener(searchBox, 'places_changed', function() {
      $('#OfficeAddress').val('');
      $('#OfficeCitId').val('');
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }
      for (var i = 0, marker; marker = markers[i]; i++) {
        marker.setMap(null);
      }

      // For each place, get the icon, place name, and location.
      markers = [];
      var bounds = new google.maps.LatLngBounds();
      var myPlaces = [];
      var results  = [];
      for (var i = 0, place; place = places[i]; i++) {
        var image = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        myMarker.setPosition(place.geometry.location);

        map.setCenter(place.geometry.location);
        //markers.push(marker);

        bounds.extend(place.geometry.location);
        var hasCompleteAddress = false;
        if (place.types[0] == 'street_address') {
          latitudeLongitude = JSON.stringify(place.geometry.location);
          
          latitudeLongitude = JSON.stringify(bounds);
    
          latitudeLongitude = latitudeLongitude.split(',');
          latitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
          console.log(latitudeLongitude);
          longitude = latitudeLongitude[2].substring(9,latitudeLongitude[2].length);

          $('#OfficeLongitude').val(longitude);
          $('#OfficeLatitude').val(latitude);
          var hasCompleteAddress = true;
        } else if (place.types[0] == 'route') {
          var hasCompleteAddress = true;
        } else {
          $('#OfficeLongitude').val('');
          $('#OfficeLatitude').val('');
        }
        results.push(place['address_components']);
        myPlaces.push(place.types[0]);
      }
      map.fitBounds(bounds);
      var listener = google.maps.event.addListener(map, "idle", function() { 
        val = map.getZoom();
        switch (myPlaces[0]) {
          case 'continent' :
            val = 4;
            break;
          case 'country' :
            val = 6;
            break;
          case 'administrative_area_level_1':
          case 'locality' :
            val = 13;
            break;
          case 'lodging' :
          case 'route' :
            val = 17;
            break;
          case 'neighborhood' :
            val = 16;
            break;
        }
          if (hasCompleteAddress) {
          $('#OfficeAddress').val('');
          $('#OfficeCompletAddress').val($('#pac-input').val());
          var haslocality = false;
          var hasroute = false;
          var hasadministrativearea = false;
          var i = 0;
          $.each(results[0], function(index, resultType) {
            if (resultType['types'][0] == 'country') {
              var url = '/Countries/get_country_from_code_2/'+resultType['short_name'];

                $.ajax({
                  url:  url,
                  type: 'get',
                  success:  function (response) {
                      if (response != false) {
                        $('#OfficeCountryId').val(response);
                      } else {
                        $('#OfficeCountryId').val(0);
                      }
                      $('#OfficeCountryId').change();
                  }
                });
            }
            if (resultType['types'][0] == 'postal_code') {
                $('#OfficeAddress').val($('#OfficeAddress').val()+'('+resultType['short_name']+')');
            }
            if (resultType['types'][0] == 'street_number') {
                $('#OfficeAddress').val(','+resultType['short_name'].trim());
            }
            if (resultType['types'][0] == 'locality') {
                haslocality = true;
                searchplace = resultType['short_name'].trim();
            }
            if (!haslocality) {
              if (resultType['types'][0] == 'administrative_area_level_1') {
                  hasadministrativearea = true;
                  searchplace = resultType['long_name'].trim();
              }
            }
            if ((!haslocality)&&(!hasadministrativearea)) {
              if (resultType['types'] == 'route') {
                  hasroute = true;
                  searchplace = resultType['long_name'].trim();
                  $('#OfficeAddress').val(searchplace+$('#OfficeAddress').val());
              }
            }
            i++;
          });
          $.ajax({
            url:  '/Cities/get_city_from_name/'+searchplace,
            type: 'get',
            success:  function (response) {
                if (response != false) {
                  console.log(response);
                  response = JSON.parse(response);
                  $('#OfficeCit').val(response['name']);
                  $('#OfficeCitId').val(response['id']);
                } else {
                  $('#OfficeCit').val(searchplace);
                }
            }
          });
        }
        map.setZoom(val);
        google.maps.event.removeListener('bounds_changed'); 
      });
});

google.maps.event.trigger('places_changed');
<?php $this->Html->scriptEnd();
?>
<div class="box">
    <div class="box-header">
      <?php 
      $textTitle = __('Editar oficina %s',$data['Office']['name']);
      ?>
      <h2><i class="fa fa-edit"></i><?php echo $textTitle;?></h2>

    </div>
    <div class="box-content">

            <?= $this->Form->create('Office', array('action' => 'edit', 'class' => 'form-horizontal rellenar-campos', 'type' => 'file')); ?>
          <div class="row" style="margin:20px">
            <div class="col-md-4">
              <?php 
              $urlImage = (!empty($data['Office']['image_dir'])) ? $data['Office']['image'] : 'noimagecompany.png';
              if ($urlImage == 'noimagecompany.png') {
                $nameImage = $this->webroot.'img'.DS.'company'.DS.$urlImage;  
              } else {
                $nameImage = $this->webroot.'files'.DS.'offices'.DS.'office_'.$data['Office']['id'].DS.$urlImage;
              }
              ?>
              <p><?php echo __('Añada una imagen representativa de su oficina');?></p>
              <img src="<?php echo  $nameImage?>" id="image1" type ='file' alt="<?php echo __('Imágen de oficina');?>" > 
              <?php
                  echo $this->Form->input('Office.image1',array('id'=> 'imgInp1','label' => false,'class'=>'hidden','type' => 'file'));
                  echo $this->Form->input('name',array('label' => __('Nombre :')));
                  echo $this->Form->input('email',array('label'=> __('Email:')));
                  echo $this->Form->input('phone',array('label'=> __('Teléfono:')));
                  echo $this->Form->input('description',array('label'=> false,'placeholder' => __('Descripción')));
              ?>
            </div>
            <div class="col-md-8">
              <input id="pac-input" class="controls" type="text" placeholder="<?php echo __('Busca la ubicación de tu oficina');?>" value="<?php echo $data['Office']['address'] ?>">
              <div class="col-sm-12 col-md-12 col-xs-12">
                <?php // debug($data);?>
                <p><?php echo __('Haga una búsqueda en el mapa y en caso de no encontrar dirección exacta de la compañía arrastre el puntero hacia el punto exacto')?></p>
                <div id="map-canvas" class="col-md-9 col-xs-9 -col-sm-9"></div>

                <div class="col-md-3 col-sm-3 col-xs-3">
                  <?php 
                  echo $this->Form->input('country_namex', array('label'=>__('País:'),'value' => $data['Country']['name'],'readonly' => 'readonly'));
                  echo $this->Form->input('id', array('label'=>false, 'class'=>'hidden', 'value' => $data['Office']['id']));
                  echo $this->Form->input('company_id', array('label'=>false, 'class'=>'hidden', 'value' => $data['Office']['company_id']));
                  echo $this->Form->input('Office.id', array('label'=>false, 'class'=>'hidden', 'value' => $data['Office']['id']));
                  echo $this->Form->input('country_id', array('label'=>false, 'selected' => $data['Country']['id'],'class'=>'hidden', 'options' => $countryList));
                  echo $this->Form->input('cit',array('label'=>__('Ciudad:'),'class' => 'ok_input','value' => $data['City']['name'],'readonly' => 'readonly'));
                  echo $this->Form->input('addresss',array('label'=>__('Dirección:'),'type' => 'textarea','readonly' => 'readonly','value' => $data['Office']['address']));
                  echo $this->Form->input('cit_id', array('label'=>false, 'value' => $data['City']['id'],'type'=>'hidden'));
                  echo $this->Form->input('address',array('label'=>__('Dirección:'),'class' => 'hidden', 'label' => false,'value' => $data['Office']['address']));
                  echo $this->Form->input('complet_address',array('label'=>__('Dirección:'),'class' => 'hidden', 'label' => false));
                  echo $this->Form->input('longitude',array('class' =>'hidden','label' => false,'value' =>$data['Geolocalitzation']['longitude']));
                  echo $this->Form->input('latitude',array('class' => 'hidden','label' => false,'value' => $data['Geolocalitzation']['latitude']));
                  ?>
                </div>
              </div>
            </div>
            <div class="col-md-12" style="text-align:center;margin-top:30px">
                  <?= $this->Form->button(__('Editar'), array('type' => 'submit','class' => 'btn submit')) ?>
                  <?= $this->Form->end(); ?>
            </div>
          </div>
          <?= $this->Form->end(); ?>
    </div>
</div>

<style>
.rellenar-campos input, textarea, select {
  margin: 3px;
  padding: 2px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border: 1px solid #999999;
  border-color: #47789f;
}
#map-canvas {
  height: 400px;
  width: 70%;
  margin: 0px;
  border-color: #47789f;
}
#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.ui-datepicker-calendar {
    display: none;
    }

.pac-container {
  font-family: Roboto;
  font-color: #47789f
}
.tiempo {
  width:60px;
}
.btn-success2 {
  background-color: #47789f;
  color:#fff !important;
}


.submit {
  text-shadow: 0 1px 1px rgba(0,0,0,.3);
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  box-shadow: 0 1px 2px rgba(0,0,0,.2);
  background: #f78d1d;
  background: -webkit-gradient(linear, left top, left bottom, from(#ddc079), to(#c5a653));
  background: -moz-linear-gradient(top, #ddc079, #c5a653);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ddc079', endColorstr='#c5a653');
  background-color: #C2A34E;
  border: medium none;
  color: #eee;
  margin-top: 2% !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 5px;
  padding-bottom: 5px;
}
.submit:hover {
  color: #fff;
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.4);
  box-shadow: 0 1px 2px rgba(0,0,0,.4);
}
</style>