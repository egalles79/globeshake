<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'jquery.mockjax.min',
  'bootstrap-editable.min',
  'moment.min',
  'typeahead.min',
  'typeaheadjs.min',
  'address.min',
  'select2.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));
  

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){

var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 1,
    center: new google.maps.LatLng(35.137879, -82.836914),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng(47.651968, 9.478485),
    draggable: true
});

var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+evt.latLng.lat().toFixed(3)+","+evt.latLng.lng().toFixed(3)+"&sensor=true";
    locc = evt.latLng;
    map.setCenter(locc);
    console.log(locc);
    $.ajax({
      url: url,
    }).success(function(result) {
      if (result['status'] == 'OK') {
        if (typeof result != 'undefined') {
          $('#pac-input').val(result['results'][0]['formatted_address']);
        }
      }
    });
});

google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
    $('#pac-input').val('Asignando punto');
});
google.maps.event.addListener(map, 'bounds_changed', function() {
      var bounds = map.getBounds();
      searchBox.setBounds(bounds);
    });
map.setCenter(myMarker.position);
myMarker.setMap(map);
markers = [];
google.maps.event.addListener(searchBox, 'places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }
      for (var i = 0, marker; marker = markers[i]; i++) {
        marker.setMap(null);
      }

      // For each place, get the icon, place name, and location.
      markers = [];
      var bounds = new google.maps.LatLngBounds();
      var myPlaces = [];
      for (var i = 0, place; place = places[i]; i++) {
        var image = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        console.log(place);
        myMarker.setPosition(place.geometry.location);

        map.setCenter(place.geometry.location);
        //markers.push(marker);

        bounds.extend(place.geometry.location);
        console.log(place);
        myPlaces.push(place.types[0]);
      }

      map.fitBounds(bounds);
      var listener = google.maps.event.addListener(map, "idle", function() { 
        console.log(myPlaces[0]);
        val = map.getZoom();
        switch (myPlaces[0]) {
          case 'continent' :
            val = 4;
            break;
          case 'country' :
            val = 6;
            break;
          case 'administrative_area_level_1':
          case 'locality' :
            val = 13;
            break;
          case 'lodging' :
          case 'route' :
            val = 17;
            break;
          case 'neighborhood' :
            val = 16;
            break;
        }
        map.setZoom(val);
        google.maps.event.removeListener(listener); 
      });
    });
});
<?php $this->Html->scriptEnd();
?>


<div class="projects form">
	<input id="pac-input" class="controls" type="text" placeholder="Search Box">
<div id="map-canvas" class="col-md-6"></div>

</div>
<?php echo $this->Form->create('Project'); ?>
	<fieldset>
		<legend><?php echo __('Add Project'); ?></legend>
	<?php
		echo $this->Form->input('actual_company');
		echo $this->Form->input('name');
		echo $this->Form->input('start_date');
		echo $this->Form->input('end_date');
		echo $this->Form->input('on_going');
		echo $this->Form->input('budget');
		echo $this->Form->input('budget_currency');
		echo $this->Form->input('billing');
		echo $this->Form->input('billing_currency');
		echo $this->Form->input('volumes');
		echo $this->Form->input('customer_name');
		echo $this->Form->input('tags');
		echo $this->Form->input('description');
		echo $this->Form->input('geolocalitzation_id');
		echo $this->Form->input('country_id');
		echo $this->Form->input('currency_id');
		echo $this->Form->input('sector_id');
		echo $this->Form->input('city_id');
		echo $this->Form->input('function_id');
		echo $this->Form->input('id_service_sector');
		echo $this->Form->input('company_id');
		echo $this->Form->input('duration_year');
		echo $this->Form->input('duration_month');
		echo $this->Form->input('duration_day');
		echo $this->Form->input('duration_hours');
		echo $this->Form->input('who_can_see');
		echo $this->Form->input('where_to_see');
		echo $this->Form->input('subfunction');
		echo $this->Form->input('User');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Projects'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Geolocalitzations'), array('controller' => 'geolocalitzations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Geolocalitzation'), array('controller' => 'geolocalitzations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Currencies'), array('controller' => 'currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Currency'), array('controller' => 'currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sectors'), array('controller' => 'sectors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sector'), array('controller' => 'sectors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Functions'), array('controller' => 'functions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Function'), array('controller' => 'functions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Companies'), array('controller' => 'companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Company'), array('controller' => 'companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<style>
#map-canvas {
  height: 500px;
  width: 40%;
  margin: 0px;
  padding: 0px
}
#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.pac-container {
  font-family: Roboto;
}
</style>