<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
'jquery-ui-1.10.3.custom.min',
'jquery.mockjax.min',
'bootstrap-editable.min',
'moment.min',
'typeahead.bundle',
'address.min',
'select2.min',
'custom.min',
'form-x-editable-demo',
'core.min',
'jquery.mockjax.min',
'bootbox.min'

), array('inline' => false));
/*

'typeaheadjs.10x.js',

'form-x-editable',






*/

$this->Html->scriptStart(array('inline' => false));

?>
$(document).ready(function(){
  $('#cardImage').hide();
  $(function() {
    $('#menu1').addClass('active2');
    $('#menu2').removeClass('active2');
    $('.general').css('cursor', 'pointer');
    $('.general').click(function() {
      url = this.getAttribute('url');
      window.location.href = url;
    });
    $('.has_participed').on('click', function() {
        var idProjectValue = $(this).attr('idproject');
        var idCountry = $(this).attr('idcountry');
        $('#card-image').html('<div class="row tarjeta" style="color: #333333;font-size: 16px;"><div style="position:relative;float:right;margin-top:-5px"><a href="/Companies/proyectos_pais/'+idCountry+'">X</a></div><div class="col-md-12">Has participado en éste proyecto ?</div><div class="row"><a href="/CompanyProjects/has_participed/1/'+idProjectValue+'/'+idCountry+'">Si</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="/CompanyProjects/has_participed/0/'+idProjectValue+'/'+idCountry+'">No</a></div>');
        $('#cardImage').show();
    });
  });
  $('.general').hover(function(){
      $(this).find('.over_tot').css('display', 'block');
      $(this).find('.over_tot_green').css('display', 'block');
  },function(){
      $(this).find('.over_tot').css('display', 'none');
      $(this).find('.over_tot_green').css('display', 'none');
  });
});
<?php
$this->Html->scriptEnd();
?><!-- start: Content -->
<div id="cardImage">
  <div id="card-image"></div>
</div>
<div class="box">
  <div class="box-header">
    <?php $countryId = $countryName['Country']['id'];?>
    <h2><i class="fa fa-map-marker"></i><?php echo __('Proyectos en %s',$countryName['Country']['name']);?></h2>
     <div class="box-icon">
        <a href="/Companies/resumen_cv" class=" pull-left" style="margin-right:25px;">
          <?php echo $this->Html->image('resumen_cv.png');?>
          <?php echo __('Ir a resumen CV');?>
        </a>
        <a href="/Companies/cvmap" class=" pull-left" style="margin-right:25px;">
          <?php echo $this->Html->image('mapamundi_mini.png');?>
          <?php echo __('Ver CV en mapa');?>
        </a>
    </div>
  </div>
  <div class="box-content">
    <div class="row">
      <div class="col-md-12">
        <p><h2 class="title text-center"><?php echo __('CV EMPRESA %s',strtoupper(str_replace(array('á','é','í','ó','ú','ñ'),array('Á','É','Í','Ó','Ú','Ñ'),$countryName['Country']['name'])));?></h2></p>
        <hr />
        <div class="col-md-12">
          <article>
            <div class="sombra" style="background-color: #e6ebf1; border-radius: 10px;margin-bottom: 30px;">
              <div class="row">
                  <div class="col-md-12">
	                  <br>
                      <p class="text-center verd" style="font-size:2em">
                        <?php echo strtoupper(str_replace(array('á','é','í','ó','ú','ñ'),array('Á','É','Í','Ó','Ú','Ñ'),$countryName['Country']['name']));?></p>
                        <div class="col-md-12 marg2">
                        <?php 
                        $imageExists = file_exists(APP.'webroot'.DS.'img'.DS.'countries'.DS.$countryName['Country']['code'].'.jpg');
                        $countryImage = ($imageExists) ? 'countries/'.$countryName['Country']['code'].'.jpg' : 'countries/noimage.png';
                        echo $this->Html->image($countryImage,array('style' => 'padding-left: 10px;'));?>
                        </div>
                        <div class="col-md-12 marg2" style="text-align: center;margin-top:20px">
                          <div id="myCarousel_lin" class="carousel slide">
                            <div class="carousel-inner" style="  min-height: 150px;">
                              <?php 
                              foreach($sectorsShow as $intValue => $currentSector) {
                                if ($intValue == 0) {?>
                                  <div class="item active">
                                <?php }?>
                                  <div class="col-md-2" style="position: relative;">
                                      <?php 
                                      $img = (!empty($currentSector['image'])) ? $currentSector['image'] : 'otro.png';
                                      echo $this->Html->image('sectors/'.$img,array('style' => 'width:100%;'));?>
                                    <!--  Si no te imatge afegim la clase abso_sector -->
                                    <div style="font-size:0.8 em;color:#a7a7a7"><?php echo $currentSector['name'];?></div>
                                  </div>
                                <?php if ((($intValue%3 ==0) &&($intValue != 0))||($intValue == count($sectorsShow)-1)) { ?>
                                  </div>
                                  <?php if ($intValue != count($sectorsShow)-1) {?>
                                  <div class="item">
                                    <?php }
                                  }
                              } ?>
                            </div>
                            <?php if (count($sectorsShow) > 4) { ?>
                              <a class="carousel-control left" href="#myCarousel_lin" data-slide="prev" style="background: none;border: none;">
                              <?php echo $this->Html->image('flecha_proyectos_2.png',array('style' => 'padding:4px'));?>
                              </a>
                              <a class="carousel-control right" href="#myCarousel_lin" data-slide="next" style="background: none;border: none;">
                                <?php echo $this->Html->image('flecha_proyectos.png');?>
                              </a>
                            <?php }?>
                          </div>
                        </div>
                  </div>
              </div>
            </div>
          </article>
          <div style="margin-top:10px"></div>
          <div class="box">
              <div class="box-header" style="background-color:#4778a0">
                <h2 class="title " style="color:#fff">
                  <i class="fa fa-globe"></i>
                  <a href="#" class="btn-minimize" style="text-decoration:none;color:#fff">
                    <?php echo __('PROYECTOS');?>
                  </a>
                </h2>
                <div class="box-icon">
                  <a href="#" class="btn-minimize">
                    <i class="fa fa-chevron-up"></i>
                  </a>
                </div>
              </div>
              <div class="box-content">
                <div class="row">
                  <div class="col-md-12 marg2">
                    <div style="text-align:center">
                      <div id="myCarousel_lin2" class="carousel slide">
                          <div class="carousel-inner row">
                            <?php 
                            foreach($projects as $intValue => $project) {
                              if ($intValue == 0) {?>
                                <div class="item active col-md-4">
                                  <?php }?>
                                    <div class="" style="position: relative;">
                                      <div class="tres__box">
                                        <?php 
                                          $userInProject = false;
                                          foreach ($companyProjectUser as $cProjectUser) {
                                            if ($cProjectUser['CompanyProjectsUser']['company_project_id'] == $project['CompanyProject']['id']) {
                                              $userInProject = true;
                                              break;
                                            }
                                          }
                                        if (!$userInProject) {
                                        ?>
                                        <a href="#" class="has_participed" idproject="<?php echo $project['CompanyProject']['id']?>" idcountry="<?php echo $countryId?>"><?php echo __('¿Has participado?');?></a>
                                        <?php } else { ?>
                                           <a href="#" class="has_participed" idproject="<?php echo $project['CompanyProject']['id']?>" idcountry="<?php echo $countryId?>"><?php echo __('Participa en éste proyecto');?></a>
                                        <?php } ?>
                                        <div class="pull-right editso">
                                          <a href="/CompanyProjects/edit/<?php echo $project['CompanyProject']['id']?>">
                                            <?php echo $this->Html->image('logo_editar.png')?>
                                          </a>
                                        </div>
                                        <div class="general" url="/CompanyProjects/view/<?php echo $project['CompanyProject']['id']?>" style="cursor:hand">
                                          <?php
                                            $isCompleted = ($project['CompanyProject']['is_completed']) ? 'over_tot_green' : 'over_tot';
                                          ?>
                                          <div class="<?php echo $isCompleted?>" style="display: none;">
                                            <a href="#" class="detail_proj"><?php echo __('VER DETALLE DE PROYECTO');?></a>
                                          </div>
                                          <?php 
                                          $defaultImageProject = array();
                                          if (!empty($project['CompanyProjectsImages'])) {
                                            foreach($project['CompanyProjectsImages'] as $projImage) {
                                              if ($projImage['default']) {
                                                $defaultImageProject = '/files/companies/company_'.$project['CompanyProject']['company_id'].'/company_project_'.$project['CompanyProject']['id'].'/'.$projImage['name'];
                                              }
                                            }
                                          }
                                          
                                          $imageExists = (!empty($defaultImageProject)) ? $defaultImageProject : 'noimage.png';
                                          echo $this->Html->image($imageExists,array('style' => 'width:100%;height:150px'));?>
                                          
                                          <div class="blue" style="padding: 5px 15px;font-size: 15px;">
                                            <strong><?php echo $project['CompanyProject']['name']?></strong>
                                          </div>
                                          <hr>
                                          <div class="infos">
                                              <table>
                                                <tr>
                                                  <td style="vertical-align:top"><?php echo $this->Html->image('logo_hombre_casco.png');?></td>
                                                  <td>
                                                    <div style="margin-left:10px">
                                                      <p class="blue"><?php echo __('CONTACTO:');?></p>
                                                      <p style="font-size:0.8em;color:#7a7a7a;"><?php echo $project['ContactResponsable']['firstname'].' '.$project['ContactResponsable']['lastname'];?></p>
                                                    </div>
                                                  </td>
                                                </tr>
                                                <tr>
                                                  <td style="vertical-align:top"><?php echo $this->Html->image('logo_hombre_casco.png');?></td>
                                                  <td>
                                                    <div style="margin-left:10px">
                                                      <p class="blue"><?php echo __('ÚLTIMO REVISOR:');?></p>
                                                      <p style="font-size:0.8em;color:#7a7a7a;"><?php echo $project['LastReviewer']['firstname'].' '.$project['LastReviewer']['lastname'];?></p>
                                                    </div>
                                                  </td>
                                                </tr>
                                                 <tr>
                                                  <td style="vertical-align:top"><?php echo $this->Html->image('logo_equipo.png');?></td>
                                                  <td>
                                                    <div style="margin-left:10px">
                                                      <p class="blue"><?php echo __('EQUIPO:');?></p>
                                                        <?php 
                                                        $numusers = 0;
                                                        echo '<p style="font-size:0.8em;color:#7a7a7a;">';
                                                        $textToWrite = '';
                                                        foreach ($project['User'] as $projectuser) {
                                                          if ($numusers > 9) {
                                                            $textToWrite .= ',... ';
                                                            break;
                                                          }
                                                          $numusers++;
                                                          $textToWrite .= $projectuser['firstname'].' '.$projectuser['lastname'].',';
                                                        }
                                                        echo substr($textToWrite,0,strlen($textToWrite)-1);
                                                        echo '</p>';
                                                        ?>
                                                    </div>
                                                  </td>
                                                </tr>
                                             </table>
                                          </div>
                                          <div style="float:left;margin-left:10px">
                                          <?php echo __('ÚLTIMA REVISIÓN:')?>
                                            <span class="grey">
                                            <?php 
                                            $dateShow = new DateTime($project['CompanyProject']['modified']);
                                            echo $dateShow->format('d/m/Y');?>
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php if ((($intValue%2 ==0) &&($intValue != 0))||($intValue == count($projects)-1)) { ?>
                                </div>
                                <?php if ($intValue != count($projects)-1) {?>
                                <div class="item">
                                  <?php }
                                }
                            } ?>
                          </div>
                          <?php if (count($projects) > 3) { ?>
                            <a class="carousel-control2 left" href="#myCarousel_lin2" data-slide="prev" style="background: none;border: none;">
                            <?php echo $this->Html->image('flecha_proyectos_2.png',array('style' => 'padding:4px'));?>
                            </a>
                            <a class="carousel-control carousel-control3 right" href="#myCarousel_lin2" data-slide="next" style="background: none;border: none;">
                              <?php echo $this->Html->image('flecha_proyectos.png');?>
                            </a>
                          <?php }?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<style>


.verd {
  color: rgb(203, 153, 106);
  margin-left: 5px;
}

.row-fluid .span2 {
  width: 14.893617021276595%;
}

.row-fluid [class*="span"] {
  float: left;
  text-align: center;
  min-height: 30px;
  margin-left: 2.127659574468085%;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
.white {
  color: #fff !important;
}
/*
.carousel-inner {
    width: 100%;  margin-top: -50px;  margin-left: 15%;
}
*/
.carousel-inner2 {
  /* min-height: 150px; */
  width: 100%;
  margin-top:2%;
  margin-left:20px;
}
.carousel-control.left {
   background-image: none !important;
}
.carousel-control.right {
   background-image: none !important;
}
.verd2 {
  color: rgb(203, 153, 106) !important;
  margin-left: 0px;
}
.infos {
  width: 90%;
  margin: 20px auto;
  background-color: #e6ebf1;
  border: 2px solid #47789f;
  padding: 10px;
  line-height: 15px;
  text-align: left;
}
.blue {
  color: #47789f;
}
.general {
  border: 2px solid rgb(199, 199, 199);
  position: relative;
  height: 560px;
  margin-top:20px;
  border-radius: 5px;
}
.over_tot_green {
  width: 100%;
  background: rgba(0, 128, 0, 0.8);
  height: 560px;
  position: absolute;
  z-index: 1;
  display: none;
}
.over_tot {
    width: 100%;
    background: rgba(251, 0, 0, 0.8);
    height: 560px;
    position: absolute;
    z-index: 1;
    display: none;
}
.carousel-control2 {
  position: absolute;
  top: 250px;
  left: 0;
  bottom: 0;
  opacity: .5;
  filter: alpha(opacity=50);
  font-size: 20px;
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0,0,0,.6);
}
.detail_proj {
    width: 100%;
    height: 100vh;
    display: block;
    color: white;
    font-size: 25px;
    padding-top: 55px;
    text-align: center;
}
.detail_proj:hover {
    width: 100%;
    height: 100vh;
    display: block;
    color: white;
    font-size: 25px;
    padding-top: 55px;
    text-align: center;
}
.carousel-control3 {
  position: absolute;
  top: 250px !important;
}
.to-left {
  margin-left:40px
}
#cardImage {
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   position: fixed;
   display: block;
   opacity: 0.9;
   background-color: #fff;
   z-index: 99;
   text-align: center;
}
#card-image {
  position: absolute;
  top: 100px;
  left: 35%;
  z-index: 100;
}
@media (max-width: 480px) {
  .carousel-inner2 {
    margin-left:0px;
  }
  .to-left {
    margin-left:5px;
  }
}
</style>
