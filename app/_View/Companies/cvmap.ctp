<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));
?>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#menu1').addClass('active2');
  $('#menu2').removeClass('active2');

  //bootbox.alert('Bon dia');
});
<?php $this->Html->scriptEnd();
?>
<?php // echo $this->Element('card');

?>

<div id="mobile">
<div id="map" style="position:absolute;"></div>
<div id="buttons">
  <div class="tbuttons active"><?php echo __('CV Empresa');?></div>
  <div class="tbuttons"><a href="/Companies/projectmap"><?php echo __('Proyectos');?></a></div>
  <div class="tbuttons"><a href="/Companies/officemap"><?php echo __('Oficinas');?></a></div>
</div>
</div>
<script>
// Provide your access token
L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
// Create a map in the div #map
// Create array of lat,lon points.

// Define polyline options

  //REHACER !!!!!! 
// http://leafletjs.com/reference.html#polyline
var map = L.mapbox.map('map', 'egalles79.mcf2id2p')
  .setView([40.731, -6.438],3);
        var countryNames = <?php echo $namesForMap?>;
        
        var countries = <?php echo $countryPoints?>;
        var countryWithProjects = <?php echo $countryWithProjects?>;


        // Define polyline options
        // http://leafletjs.com/reference.html#polyline
        
        // Defining a polygon here instead of a polyline will connect the
        // endpoints and fill the path.
        // http://leafletjs.com/reference.html#polygon
        var layerPoints = [];
        var myPoints = [];
        var clickpoints = [];
        map.featureLayer.on('ready', function(e) {
          var i = 0;
          $.each(countries, function(index, country) {
            hasProjects = false;
            myCountryProjects = [];
            if (countryWithProjects[index] != undefined) {
              hasProjects = true;
              myCountryProjects.push(index);
              var hasProjects = true;
            } else {
              var hasProjects = false;
            }
            
            var valueToPush = {};
            valueToPush.hasprojects = hasProjects;
            if (hasProjects) {
              myColor = '#'+Math.floor(Math.random()*16777215).toString(16);
              myOpacity = 0.6;
            } else {
              myColor = '#ddd';
              myOpacity = 0;

            }
            
            var polyline_options = {
              color: myColor,
              fill: myColor,
              fillOpacity: myOpacity,
              stroke: 0
            };
            myPoints = [];
            cprepeated = [];
            $.each(country.countrypoints, function( index2, countrypointline ) {
              var a = cprepeated.indexOf(countrypointline);
              if (a == -1) {
                if (typeof(countrypointline) == 'string') {
                  linepointsCountry = "["+countrypointline+"]";
                  linepointsCountry = JSON.parse(linepointsCountry);
                  myPoints.push(linepointsCountry);
                }
              }
              
            });
            layerPoints = [];
            valuesLayerPoints = [];
            $.each(myPoints, function( index, value ) {
              layerPoints.push(L.polyline(value, polyline_options).addTo(map));
            });
            valueToPush.name       = countryNames[index];
            valueToPush.country_id = index;
            valueToPush.points = layerPoints;
            if (countryWithProjects[index] != undefined) {
              valueToPush.general                = countryWithProjects[index];
              valueToPush.company_name           = countryWithProjects[index]['company_name'];
              valueToPush.company_id             = countryWithProjects[index]['company_id'];
              valueToPush.projectsongoing        = countryWithProjects[index]['projectsongoing'];
              valueToPush.projectsfinished        = countryWithProjects[index]['projectsfinished'];
              valueToPush.projectsincomplete     = countryWithProjects[index]['projectsincomplete'];
              valueToPush.projects_in_country    = countryWithProjects[index]['projects_in_country'];
              valueToPush.total_company_projects = countryWithProjects[index]['total_company_projects'];
              valueToPush.users_in_project       = countryWithProjects[index]['users_in_project'];
            }
            clickpoints.push(valueToPush);
            layerPoints = [];
          }); 
          
          $.each(clickpoints, function( indexes, values ) {
             btn = values.points;
             $.each(btn, function (index2,buttons) {
                buttons.on('click',function() {
                  if (values.hasprojects) {
                    if (values.projectsincomplete > 0) {
                      textIncomplete = '<br><form action="/Companies/proyectos_pais/'+values.country_id+'/incomplete"><button  type="submit" id="" class="accept button_blue">'+I18nJs.t('Completar info incompleta de')+' <span>'+values.projectsincomplete+'</span> '+I18nJs.t('proyectos')+'</button></form>';
                    } else {
                      textIncomplete = '';
                    }
                    text = '<div class="row"><div class="col-md-12"><a href="/Companies/ver_cv"><img src="/img/lupaicon.png"></a></div><div class="col-md-12 text-center"><a href="/Companies/proyectos_pais/'+values.country_id+'"><p class="title">'+values.name+'</p></a></div><div class="col-md-12 text-center"><a href="#">'+I18nJs.t('Potencial')+' '+values.name+' <span>30%</span></a></div></div><br><div class="row"><div class="col-md-6 "><span class="glyphicon glyphicon-briefcase"></span> Proyectos '+values.name+':</div><a class="col-md-6" href="/Companies/ver_cv"><div class="new_font">'+values.projects_in_country+'</div><small>('+I18nJs.t('De')+' '+values.total_company_projects+' '+I18nJs.t('Proyectos totales')+' '+values.company_name+')</small></a><div class="col-md-6 "><span class="glyphicon glyphicon-briefcase"></span> '+I18nJs.t('Proyectos acabados')+':</div><a class="col-md-6" href="/Companies/proyectos_pais/'+values.country_id+'/finished/"><div class="new_font">'+values.projectsfinished+'</div></a><div class="clearfix"></div><div class="col-md-6 "><span class="glyphicon glyphicon-briefcase"></span> '+I18nJs.t('Proyectos en curso')+':</div><a class="col-md-6" href="/Companies/proyectos_pais/'+values.country_id+'/ongoing/"><div class="new_font">'+values.projectsongoing+'</div></a><div class="clearfix"></div><div class="col-md-6"><span class="glyphicon glyphicon-briefcase"></span> '+I18nJs.t('Empleados participantes')+'</div><a class="col-md-6" href=""><div class=" new_font">'+values.users_in_project+'</div></a></div></div><br><div class="row buts"><button id="forta" class="accept button_blue">'+I18nJs.t('Fortalezca Potencial')+'</button><br><form action="/CompanyProjects/add/"><button type="submit" id="" class="accept button_blue">'+I18nJs.t('Añadir proyecto')+'</button></form>'+textIncomplete+'</div></div></div></div>';
                  } else {
                    console.log(values);
                      text = '<div class="pop-up-map" style=""><div class="row"><div class="col-md-12" style="text-align:center; min-height: 10px;"><a href="/Companies/proyectos_pais/'+values.country_id+'"><p class="title">'+values.name+'</p></a><a href="#">'+I18nJs.t('No hay proyectos en')+' '+values.name+'.</a><a class="accept" href="/CompanyProjects/add/"> '+I18nJs.t('Añadir proyectos')+'</a></div></div>';
                  }
                  bootbox.alert(text);
                }); 
             });
          });
        });
// Defining a polygon here instead of a polyline will connect the
// endpoints and fill the path.
// http://leafletjs.com/reference.html#polygon
//map.addLayer(line_points);
</script>
<style>
.modal-dialog{
	width: 300px;
	height: 90%;
}
	p.title{
		padding-bottom: 0px;
		    text-align: center;
    margin-bottom: 0px;
    font-size: 16px;
    text-transform: uppercase;
	}
#buttons {
  z-index: 100;
  position: relative;
  float: right;
  margin-right: 20px;
}
.tbuttons {
    background-color: #47789F;
    font-size: 0.9em;    
    color: #fff;
    text-align: center;
    -webkit-box-shadow: 7px 4px 6px -3px #333333;
    -moz-box-shadow: 7px 4px 6px -3px #333333;
    box-shadow: 7px 4px 6px -3px #333333;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    float: left;
    margin-left: 10px;
    padding: 5px;
    padding-left: 20px;
    padding-right: 20px;
}

.tbuttons a {
  text-decoration: none;
  color: #fff;
}
.active {
  background-color: #66A0C6
}
.modal-content{
	top: 20%;
	    background: #e6ebf1;
    color: #47789f;
    font-size: 13px;
    line-height: 24px;
    width: 300px;
    -webkit-box-shadow: 14px 8px 12px -6px #333333;
    -moz-box-shadow: 14px 8px 12px -6px #333333;
    box-shadow: 14px 8px 12px -6px #333333;
        border-radius: 3px;
}
.modal-footer{
	display: none;
}
.pop-up-map{
	width: 400px;
	max-width: 100%;
}
.pop-up-map small{
	    font-size: 10px;
    line-height: 12px;
    margin: 5px 0px;
    display: block;
}


.pop-up-map .span12 button,a.accept{
	font-size: 12px!important;
  padding: 5px!important;
  width: 100%!important;
  margin-top: 10px;
}
.pop-up-map .title{
	text-align: center;
	margin-bottom: 0px;
}
.pop-up-map .brum{
	    position: absolute;
    top: 35px;
    right: 16px;
    width: 15px;
}
.pop-up-map .brumdos{
	  position: absolute;
  top: 2px;
  left: 20px;
  width: 28px;
}

.icon{
	background-image: url(../img/icon_03.png);
}
.icon2{
	background-image: url(../img/icon_07.png);
}
.iconsol{
	background-image: url(../img/logo_hombre_casco.png);
}
.iconteam{
	background-image: url(../img/icon_07p.png);
}
.iconcasc{
	background-image: url(../img/logo_hombre_casco.png);
	background-repeat: no-repeat;
}
.icon3{
	background-image: url(../img/icon_10.png);
}
.icon4{
	background-image: url(../img/icon_14.png);
}
.icon5{
	background-image: url(../img/icon_17.png);
}
.icon6{
	background-image: url(../img/icon_20.png);
}
</style>