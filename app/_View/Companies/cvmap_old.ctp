<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));
?>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#menu1').addClass('active2');
  $('#menu2').removeClass('active2');

  //bootbox.alert('Bon dia');
});
<?php $this->Html->scriptEnd();
?>
<?php // echo $this->Element('card');

?>

<div id="mobile">
<div id="map" style="position:absolute;"></div>
<div id="buttons">
  <div class="tbuttons active"><?php echo __('CV Empresa');?></div>
  <div class="tbuttons"><a href="/Companies/projectmap"><?php echo __('Proyectos');?></a></div>
  <div class="tbuttons"><a href="/Companies/officemap"><?php echo __('Oficinas');?></a></div>
</div>
</div>
<script>
// Provide your access token
L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
// Create a map in the div #map
// Create array of lat,lon points.

// Define polyline options
// http://leafletjs.com/reference.html#polyline
var map = L.mapbox.map('map', 'egalles79.mcf2id2p')
  .setView([40.731, -6.438],3);
var companyprojects = JSON.parse(JSON.stringify(<?php echo json_encode($companyprojects)?>));
$.each(companyprojects, function(index, companyproject) {
    L.mapbox.featureLayer({
      // this feature is in the GeoJSON format: see geojson.org
      // for the full specification
      type: 'Feature',
      geometry: {
          type: 'Point',
          // coordinates here are in longitude, latitude order because
          // x, y is the standard for GeoJSON and many formats
          coordinates: [
            companyproject.Geolocalitzation.longitude,
            companyproject.Geolocalitzation.latitude
          ]
      },
      properties: {
          title: '<strong>'+companyproject.CompanyProject.name+'</strong>',
          description: '<br />Persona responsable del proyecto: '+companyproject.ContactResponsable.firstname+' '+companyproject.ContactResponsable.lastname,
          // one can customize markers by adding simplestyle properties
          // https://www.mapbox.com/guides/an-open-platform/#simplestyle
          'marker-size': 'large',
          'marker-color': '#66A0C6',
          'marker-symbol': 'marker-stroked'
      }
  }).addTo(map);
});
</script>
<style>
#buttons {
  z-index: 100;
  position: relative;
  float: right;
  margin-right: 20px;
}
.tbuttons {
    background-color: #47789F;
    font-size: 0.9em;    
    color: #fff;
    text-align: center;
    -webkit-box-shadow: 7px 4px 6px -3px #333333;
    -moz-box-shadow: 7px 4px 6px -3px #333333;
    box-shadow: 7px 4px 6px -3px #333333;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    float: left;
    margin-left: 10px;
    padding: 5px;
    padding-left: 20px;
    padding-right: 20px;
}
.tbuttons a {
  text-decoration: none;
  color: #fff;
}
.active {
  background-color: #66A0C6
}
</style>