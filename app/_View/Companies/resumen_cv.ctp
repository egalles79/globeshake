<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
'jquery-ui-1.10.3.custom.min',
'jquery.mockjax.min',
'bootstrap-editable.min',
'moment.min',
'address.min',
'select2.min',
'custom.min',
'core.min',
'bootbox.min',
'jquery.mask.min'
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
	$('.money').mask('000.000.000.000.000,00', {reverse: true});
	$('#menu2').removeClass('active2');
  	$('#menu1').addClass('active2');
});
<?php $this->Html->scriptEnd();
?>
<div clas="row">
    		<p>
    			<strong><h2 class="title text-center" style="font-size:1.6em"><?php echo __('CIFRAS TOTALES EMPRESA');?></h2></strong>
    		</p>
    	</div>
    	
<div class="box" style="border:0px !important;width: 850px;margin:auto;">
	<div class="mine" style="position:absolute;margin-left:-10px;margin-top:10px"><?php echo $this->Html->image('logo.png')?></span></a></div>

	<div class="box-content" style="border: 7px solid #45759b;">
	  <div class="box-header ">
	  	<p class="tit_box"><?php echo $staticCompanyNames[$user['User']['company_id']];?></p>
	  </div>
    	<div class="box-content">
	    	<div class="row">
	    		<div class="col-md-12">
				        <!-- INFORMACIÓN PROYECTOS -->
				    <div class="box">
					    <div class="box-header" style="background-color:#4778a0">
                <h2 class="title " style="color:#fff">
                  <i class="fa fa-info"></i>
                  <a href="#" class="btn-minimize" style="text-decoration:none;color:#fff"><?php echo __('INFORMACIÓN GENERAL PROYECTOS');?></a>
                </h2>
                <div class="box-icon">
                  <a href="#" class="btn-minimize">
                    <i class="fa fa-chevron-up"></i>
                  </a>
                </div>
              </div>
				        <!--
<div class="box-header" style="">
		            		<h2 class="title " style="color:#fff">
		            			<i style="color:#7a7a7a" class="fa fa-info"></i>
		            			<a href="#" class="btn-minimize target" style="text-decoration:none;"><?php echo __('INFORMACIÓN GENERAL PROYECTOS');?></a></h2>
		            			<div class="box-icon">
					              <a href="#" class="btn-minimize target"><i class="fa fa-chevron-up" style="color:#47789f"></i></a>
					            </div>
					    </div>
-->
						<div class="box-content">
							<div class="row row_round2">
							<div class="col-md-12">
				                    <?php
				                    $projectsNameString = '';
				                    $projectsCount = 0;
				                    foreach($projectsNames as $projectsName) {
				                    	$projectsCount++;
				                    	$projectsNameString .= $projectsName.'<br />';
				                    }
				                    $countryProjectsCountryName = '';
				                    $countryProjectsCountryCount = 0;
				                    foreach($countryProjectIds as $countryProjectId) {
				                    	$countryProjectsCountryCount++;
				                    	$countryProjectsCountryName .= $countryNames[$countryProjectId].'<br />';
				                    }
				                    $customerNamesString = '';
				                    $customerNamesCount = 0;
				                    foreach($customerNames as $customerName) {
				                    	$customerNamesCount++;
				                    	$customerNamesString .= $customerName.'<br />';
				                	}
				                	$numSectors = 0;
				                	$sectorProjectString = '';
				                	foreach($sectorsProjectNames as $sectorsProjectName) {
				                		$numSectors++;
				                		$sectorProjectString .= $sectorsProjectName.'<br />';
				                	}
				                    ?>
				                    <table class="table table-responsive">
				                      <thead></thead>
				                      <tbody>
				                        <tr>
				                          <td>
				                            <div class="text-right colour_grey">
				                              <?php echo __('Nombre de Proyectos').':';?>&nbsp;&nbsp;<i class="fa fa-briefcase"></i>
				                            </div>
				                          </td>
				                          <td>
				                          	<div class="contador"><?php echo $projectsCount;?></div>
				                            <div class="text-left resumen"><?php echo $projectsNameString;?>
				                          </td>
				                          <td>
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey">
				                              <?php echo __('Países').':';?>&nbsp;&nbsp;<i class="fa fa-globe"></i>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                            <div class="contador"><?php echo $countryProjectsCountryCount;?></div>
				                            <div class="text-left resumen"><?php echo $countryProjectsCountryName;?>
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey">
				                              <?php echo __('Clientes').':';?>&nbsp;&nbsp;<i class="fa fa-folder-open"></i>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                          	<div class="contador"><?php echo $customerNamesCount;?></div>
				                            <div class="text-left resumen" ><?php echo $customerNamesString;?>
				                          </td>
				                          
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey">
				                              <?php echo __('Sectores').':';?>&nbsp;&nbsp;<i class="fa fa-circle-o"></i>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                          	<div class="contador"><?php echo $numSectors;?></div>
				                          	<div id="myCarousel_lin" class="carousel slide">
	                  							<br><br>
					                          	<div class="carousel-inner" style="  min-height: 150px;">
							                    <?php 
							                    if (empty($sectorsProjectNames)) {
							                      echo '<div class="span6" style="position: relative;">'.__('Añada proyectos a su empresa para ver los sectores.').'</div>';
							                    } else {
							                    if (count($sectorsProjectNames) > 1) {?>
							                      <div class="item active">
							                    <?php 
							                    }
							                      $countValue = 0;
							                      foreach($sectorsProjectNames as $intValue => $currentSector) {
							                        $img = (!empty($currentSector['image'])) ? $currentSector['image'] : 'otro.png'; ?>
							                        <div class="col-md-3 col-sm-3 col-xs-3" style="position: relative;">
							                          <?php echo $this->Html->image('sectors/'.$img,array('style' => 'width:100%;'));?>
							                          <div style="font-size:0.8 em;color:#a7a7a7;text-align:center"><?php echo __($currentSector['name']);?></div>
							                        </div>
							                        <?php 
							                        if ($intValue != count($sectorsProjectNames)-1) {
							                          if (($countValue%3 == 0) && ($countValue != 0)) {
							                            echo '</div><div class="item">';
							                            $countValue=0;
							                          } else {
							                            $countValue++;  
							                          }
							                        }
							                      }
							                    } ?>
							                    </div>
						                    <?php if (count($sectorsProjectNames) > 4) { ?>
						                    <a class="carousel-control left" href="#myCarousel_lin" data-slide="prev" style="background: none;border: none;left:-32px;top:50px">
						                    <?php echo $this->Html->image('flecha_proyectos_2.png',array('style' => 'padding:4px'));?>
						                    </a>
						                    <a class="carousel-control right" href="#myCarousel_lin" data-slide="next" style="background: none;border: none;right:-32px;top:50px">
						                      <?php echo $this->Html->image('flecha_proyectos.png');?>
						                    </a>
						                  <?php }?>
						              </div>
						                  </div>
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey">
				                              <?php echo __('Presupuesto total').':';?>&nbsp;&nbsp;<i class="fa fa-money"></i>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                          	<div class="contador"><?php echo $projectsCount;?></div>
				                            <div class="text-left resumen" ><span class="money"><?php echo round($projectsSumBudget,2)?></span> €
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                      </tbody>
				                    </table>
				                  </div>
							</div>
						</div>
		    		</div>
	    		</div>
	    	</div>
	    </div>
	   </div>

</box>



<style>

.table th, .table td { 
     border-top: none !important;
     margin: 0px !important;
     padding: 0px !important;
 }
 .resumen {
 	border: 1px solid #799ebe;
  color: #666666;
  padding:10px;
  border-radius: 10px;
  margin-bottom:10px;
  background-color: #ffffff;
 }
 .row_round {
 	background-color:#abc4dd;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
 .row_round2 {
 	background-color:#e6ebf1;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
 
  .row_round3 {
 	background-color:#cdcdcd;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
  .row_round4 {
 	background-color:#E5CD98;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
 
 .colour_grey {
 	color:#666666;
 	padding:10px;
 	  font-family: sourcesansprobold;
  font-size: 12pt;
 }
 .contador {
  position: relative;
  z-index: 0;
  background-color: white;
  border: 2px solid #47789f;
  border-radius: 10px;
  width: 25px;
  text-align: center;
  left: -6px;
  color: #ff3600;
  top: 11px;
}
.box {
	border: 0px;
}

.tit_box {
  color: #c4a34e;
  padding: 20px;
  font-size: 24px;
  line-height: 5px;
  font-weight: bold;
  -webkit-box-shadow: 0px 6px 30px 0px rgba(50, 50, 50, 0.75);
  -moz-box-shadow: 0px 6px 30px 0px rgba(50, 50, 50, 0.75);
  box-shadow: 0px 6px 30px 0px rgba(50, 50, 50, 0.75);
}
</style>