<?php
App::uses('AppModel', 'Model');
/**
 * Department Model
 *
 * @property CompanyProject $CompanyProject
 * @property UserProject $UserProject
 * @property User $User
 */
class Department extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public function check($name) {
		$this->recursive = -1;
		$resourceId= $this->find('first',array(
			'conditions' => array(
				'name' => $name,
				)
			)
		);
		if (!empty($resourceId)) {
			$resourceId = $resourceId['Department']['id'];
		} else {
			$saveArray['name'] = $name;
			if ($this->save($saveArray)) {
				 $resourceId = $this->getLastInsertID();
			}	
		}
		return $resourceId;
	}
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CompanyProject' => array(
			'className' => 'CompanyProject',
			'foreignKey' => 'department_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserProject' => array(
			'className' => 'UserProject',
			'foreignKey' => 'department_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'department_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
