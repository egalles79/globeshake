<?php
App::uses('AppModel', 'Model');
/**
 * Functionality Model
 *
 * @property CompanyProject $CompanyProject
 * @property UserProject $UserProject
 * @property User $User
 */
class Functionality extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';


	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public function check($name) {
		$this->recursive = -1;
		$resourceId= $this->find('first',array(
			'conditions' => array(
				'name' => $name,
				)
			)
		);
		if (!empty($resourceId)) {
			$resourceId = $resourceId['Functionality']['id'];
		} else {
			$saveArray['name'] = $name;
			if ($this->save($saveArray)) {
				 $resourceId = $this->getLastInsertID();
			}	
		}
		return $resourceId;
	}
/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CompanyProject' => array(
			'className' => 'CompanyProject',
			'foreignKey' => 'functionality_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserProject' => array(
			'className' => 'UserProject',
			'foreignKey' => 'functionality_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'functionality_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
