<?php
App::uses('AppModel', 'Model');
/**
 * CompanyProjectsUser Model
 *
 * @property CompanyProject $CompanyProject
 * @property User $User
 */
class CompanyProjectsUser extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CompanyProject' => array(
			'className' => 'CompanyProject',
			'foreignKey' => 'company_project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
