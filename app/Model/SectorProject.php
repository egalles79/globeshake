<?php
App::uses('AppModel', 'Model');
/**
 * SectorProject Model
 *
 * @property CompanyProject $CompanyProject
 * @property User $User
 * @property CompanyProject $CompanyProject
 */
class SectorProject extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $useTable     = 'sectors';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CompanyProject' => array(
			'className' => 'CompanyProject',
			'foreignKey' => 'sector_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'sector_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'CompanyProject' => array(
			'className' => 'CompanyProject',
			'joinTable' => 'company_projects_sectors',
			'foreignKey' => 'sector_id',
			'associationForeignKey' => 'company_project_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);

}
