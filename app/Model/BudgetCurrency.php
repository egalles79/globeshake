<?php
App::uses('AppModel', 'Model');
/**
 * Currency Model
 *
 * @property CompanyProject $CompanyProject
 * @property Project $Project
 */
class BudgetCurrency extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $useTable = 'currencies';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'ISO' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $belongsTo = array(
		'UserProject' => array(
			'className' => 'UserProject',
			'foreignKey' => 'budget_currency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CompanyProject' => array(
			'className' => 'CompanyProject',
			'foreignKey' => 'budget_currency_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
