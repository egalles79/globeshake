<?php
App::uses('AppModel', 'Model');
/**
 * UsersUserProject Model
 *
 * @property UserProject $UserProject
 * @property User $User
 */
class UsersUserProject extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'UserProject' => array(
			'className' => 'UserProject',
			'foreignKey' => 'user_project_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
