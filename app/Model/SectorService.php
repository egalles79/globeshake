<?php
App::uses('AppModel', 'Model');
/**
 * SectorProject Model
 *
 * @property CompanyProject $CompanyProject
 * @property User $User
 * @property CompanyProject $CompanyProject
 */
class SectorService extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $useTable     = 'sectors';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $belonsTo = array(
		'UserProject' => array(
			'className' => 'UserProject',
			'foreignKey' => 'sector_service_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

}
