<?php
App::uses('AppModel', 'Model');
/**
 * City Model
 *
 * @property Country $Country
 * @property CompanyProject $CompanyProject
 * @property UserProject $UserProject
 * @property User $User
 */
class City extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'country_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	public function check($cityName = null,$countryId) {
		$this->recursive = -1;
		$resourceId= $this->find('first',array(
			'conditions' => array(
				'name' => $cityName
				)
			)
		);
		if (!empty($resourceId)) {
			$resourceId = $resourceId['City']['id'];
		} else {
			$saveArray['country_id'] = $countryId;
			$saveArray['name']       = $cityName;
			if ($this->save($saveArray)) {
				 $resourceId = $this->getLastInsertID();
			}	
		}
		return $resourceId;
	}
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Country' => array(
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CompanyProject' => array(
			'className' => 'CompanyProject',
			'foreignKey' => 'city_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserProject' => array(
			'className' => 'UserProject',
			'foreignKey' => 'city_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'city_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
