<?php
App::uses('AppModel', 'Model');
/**
 * Function Model
 *
 * @property Project $Project
 * @property User $User
 */
class Customer extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
	public $useTable = 'companies';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $belonsTo = array(
		'UserProject' => array(
			'className' => 'UserProject',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CompanyProject' => array(
			'className' => 'CompanyProject',
			'foreignKey' => 'customer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
