<?php
App::uses('AppModel', 'Model');
/**
 * Function Model
 *
 * @property Project $Project
 * @property User $User
 */
class LastReviewer extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'username';
	public $useTable = 'users';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $belonsTo = array(
		'CompanyProject' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
