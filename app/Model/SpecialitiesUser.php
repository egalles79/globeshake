<?php
App::uses('AppModel', 'Model');
/**
 * SpecialitiesUser Model
 *
 * @property User $User
 * @property Speciality $Speciality
 */
class SpecialitiesUser extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'speciality_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'speciality_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Speciality' => array(
			'className' => 'Speciality',
			'foreignKey' => 'speciality_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
