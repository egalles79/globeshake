<?php
App::uses('AppModel', 'Model');
/**
 * Sector Model
 *
 * @property CompanyProject $CompanyProject
 * @property User $User
 * @property CompanyProject $CompanyProject
 */
class Sector extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public function check($name) {
		$this->recursive = -1;
		$resourceId= $this->find('first',array(
			'conditions' => array(
				'name' => $name,
				)
			)
		);
		if (!empty($resourceId)) {
			$resourceId = $resourceId['Sector']['id'];
		} else {
			$saveArray['name'] = $name;
			if ($this->save($saveArray)) {
				 $resourceId = $this->getLastInsertID();
			}	
		}
		return $resourceId;
	}

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CompanyProject' => array(
			'className' => 'CompanyProject',
			'foreignKey' => 'sector_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'sector_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
}
