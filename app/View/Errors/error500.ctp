<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'jquery.mockjax.min',
  'bootstrap-editable.min',
  'moment.min',
  'typeahead.min',
  'typeaheadjs.min',
  'address.min',
  'select2.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));
?>
<h2><?php echo $message; ?></h2>
<p class="error">
	<strong><?php echo __d('cake', 'Error'); ?>: </strong>
	<?php echo __d('cake', 'Ha ocurrido un error interno del servidor, por favor contacte con el administrador'); ?>
</p>
<?php
if (Configure::read('debug') > 0):
	echo $this->element('exception_stack_trace');
endif;
?>
