<?php
/**
 * User: Miquel Ramon Ortega i Tido
 * Date: 12/05/13
 * Time: 12:28
 * @file CustomInputsHelper.php
 */
App::uses('AppHelper', 'View/Helper');

class CustomInputsHelper extends AppHelper{
  public $helpers = array('Form');

  /**
   * Función para ampliar el funcionamiento de la función input de FormHelper
   * @param $field
   * @param null $options
   * @return mixed
   */
  public function input($field, $options = NULL, $checkbox = FALSE){
    // Opciones de visualización comunes a todos los inputs
    $inputOptions = array(
      'div' => 'control-group',
      'label' => array('class' => 'control-label'),
      'between' => '<div class="controls">',
      'after' => '</div>'
    );

    if ($checkbox){
      ob_start();
      ?>
    <div class="control-group">
        <div class="controls">
            <label class="checkbox">
              <?
              $opt_array=array('hiddenField' => false);
              if(isset($options['required'])){
                $opt_array['required']='required';
              }
              if (isset($options['text'])){
                echo $this->Form->checkbox($field, $opt_array) . $options['text'];
              }
              else{
                echo $this->Form->checkbox($field, $opt_array) . $this->_textToCheckbox($field);
              }
              ?>
            </label>
        </div>
    </div>
    <?php
      return ob_get_clean();
    }
    // En caso de añadir opciones especificas se añaden al array de opciones por defecto
    if(isset($options)){
        $inputOptions = array_merge($inputOptions, $options);
    }
    return $this->Form->input($field, $inputOptions);
  }

  public function userInputs($username = false){
    ob_start();
    if (!$username || $username==''){
      echo $this->input('User.username', array('required' => 'required'));
    }
    else{
      echo $this->input('User.username', array('readonly' => TRUE, 'value'=>$username));
    }

    echo $this->input('User.email', array('type' => 'email'));
    echo $this->input('User.password', array('required' => 'required'));
    echo $this->input('User.password2', array('type' => 'password', 'label' => array('text' => __('Repeat password'))));

    return ob_get_clean();
  }

  public function userInputsEdit(){
    ob_start();

    echo $this->input('User.username', array('readonly' => TRUE));
    echo $this->input('User.email', array('type' => 'email'));
    echo $this->input('User.password', array('value'=>'', 'required' => ''));
    echo $this->input('User.password2', array('type' => 'password', 'label' => array('text' => __('Repeat password'))));

    return ob_get_clean();
  }

  public function userInputsEditMe(){
    ob_start();

    echo $this->input('User.username', array('readonly' => TRUE));
    echo $this->input('User.email', array('type' => 'email'));
    echo $this->input('User.check_old_password', array('type' => 'password', 'label' => array('text' => __('Old password'))));
    echo $this->input('User.password', array('value'=>''));
    echo $this->input('User.password2', array('type' => 'password', 'label' => array('text' => __('Repeat password'))));

    return ob_get_clean();
  }

  public function _textToCheckbox($fieldName){
    if (strpos($fieldName, '.') !== false) {
      $fieldElements = explode('.', $fieldName);
      $text = array_pop($fieldElements);
    } else {
      $text = $fieldName;
    }
    if (substr($text, -3) == '_id') {
      $text = substr($text, 0, -3);
    }
    return __(Inflector::humanize(Inflector::underscore($text)));
  }
}