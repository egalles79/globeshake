<?php
App::uses('LanguageHelper', 'View/Helper');

class LanguageHelper extends HtmlHelper {

    public function selectIdiom($language = null) {
        $idiom['locale'] = 'es';
        $idiom['name'] = 'Castellano';
        $idiom['name_curt'] = 'es';
        $idiom['speciallocale'] = 'es';

        if ($language == 'eng') {
            $idiom['locale'] = 'gb';
            $idiom['speciallocale'] = 'gb';
            $idiom['name']   = 'English';
            $idiom['name_curt'] = 'en';
        }
        return $idiom;
    }
}
