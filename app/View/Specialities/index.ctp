<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
'jquery-ui-1.10.3.custom.min',
'jquery.mockjax.min',
'bootstrap-editable.min',
'moment.min',
'address.min',
'select2.min',
'custom.min',
'core.min',
'bootbox.min',
'jquery.mask.min'
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
    $('#menu1').removeClass('active2');
    $('#menu2').addClass('active2');

    $('.speciality_delete').click(function(){
        var href = $(this).attr('url');
        console.log(href);
        bootbox.confirm(I18nJs.t("¿Está seguro que quiere borrar esta especialidad?"), function(result) {
        if (result){
          window.location.href = href;
        }
        });
        return false;
    });

    $('.specialities_add').on('click', function() {
      bootbox.dialog({
        title: I18nJs.t('Añada especialidad'),
        message : '<?php echo $this->Form->create('Speciality', array('action' => 'add')).$this->Form->CustomInput('name',array('class'=> 'form-control','label' =>__('Nombre de la especialidad'),'placeholder' => __('Nombre'))).$this->Form->input('description',array('class'=> 'form-control','label' =>__('Descripción'),'placeholder' => __('Descripción'),'type' => 'textarea'));?><?php $this->Form->end() ?>',
      buttons: {
        success: {
          label: I18nJs.t("Guardar"),
          className: "btn-success",
          callback: function() {
            console.log($('#SpecialityName').val());
            if ($('#SpecialityName').val().length != 0) {
              dataserialize = $('#SpecialityAddForm').serialize();
               $.ajax({
                  url:  '/Specialities/add/',
                  type: 'post',
                  data: dataserialize,
                  success:  function (response) {
                      if (response ) {
                        location.reload();
                      } 
                  }
                });
            }
            
          }
        },
        danger: {
          label: I18nJs.t("Cancelar"),
          className: "btn-danger",
          callback: function() {
            return true;
          }
        }
      },
    });
  });
  $('.speciality_edit').on('click', function() {
      var name = $(this).attr('url_data_name');
      var id   = $(this).attr('url_data_id');
      var description = ($(this).attr('url_data_description'));
      bootbox.dialog({
        title: I18nJs.t('Añada especialidad'),
        message : '<form action="/specialities/add" id="SpecialityAddForm" method="post" accept-charset="utf-8"><div style="display:none;"><input type="hidden" name="_method" value="POST"><input type="hidden" name="id" value="'+id+'"></div><input name="data[Speciality][name]" class="form-control" label='+I18nJs.t("Nombre de la especialidad")+' placeholder="'+I18nJs.t("Nombre")+'" value="'+name+'" type="CustomInput" id="SpecialityName" required="required"><div class="input textarea"><label for="SpecialityDescription">'+I18nJs.t('Descripción')+'</label><textarea name="data[Speciality][description]" class="form-control" placeholder="'+I18nJs.t('Descripción')+'" cols="30" rows="6" id="SpecialityDescription">'+description+'</textarea></div></form>',
      buttons: {
        success: {
          label: I18nJs.t("Editar"),
          className: "btn-success",
          callback: function() {
            if ($('#SpecialityName').val().length != 0) {
              dataserialize = $('#SpecialityAddForm').serialize();
               $.ajax({
                  url:  '/Specialities/add/',
                  type: 'post',
                  data: dataserialize,
                  success:  function (response) {
                      if (response ) {
                        location.reload();
                      } 
                  }
                });
            }
            
          }
        },
        danger: {
          label: I18nJs.t("Cancelar"),
          className: "btn-danger",
          callback: function() {
            return true;
          }
        }
      },
    });
  });
});
<?php $this->Html->scriptEnd();
?>
<div class="mine" style="position:absolute;margin-left:-10px;margin-top:10px"><?php echo $this->Html->image('logo.png')?></span></a></div>
<div clas="row">
    		<p>
    			<strong>
    				<h2 class="title text-center" style="font-size:1.6em"><?php echo __('ESPECIALISTA');?></h2>
    			</strong>
    			<div style="  position: absolute;margin-left: 55%;margin-top: -2.7%;"><?php echo $this->Html->image('star_03.png');?></div>
    		</p>
    	</div>
<div class="box" style="border:0px !important">
	<div class="box-content" style="  background-color: #FFFFFF;
  border-radius: 11px;
  -moz-border-radius: 11px;
  -webkit-border-radius: 11px;
  border: 7px solid #45759b;">
	  <div class="box-header ">
	  	<h2 class="title text-center">
	  		<i style="color:#7a7a7a" class="fa fa-star"></i>
	  		<?php echo __('Registrate como especialista y destaca dentro y fuera de la empresa.');?>
	  	</h2>
	  </div>
    	<div class="box-content">
	    	<div class="row">
	    		<div class="col-md-12">
	    			<div style="float:right">
	    			 <a href="#" class="btn btn-success specialities_add">
            			<i class="fa fa-plus-circle" style="width:180px;text-align:left"><?php echo '  '.	__('Añadir especialidad');?></i></a>
        		</div>
            	<div class="clear:both"></div>
          
		    		<?php 
                    $urlImage = (!empty($user['User']['photo_dir'])) ? $user['User']['photo_dir'].$user['User']['photo'] : 'camarafot.png';
                    if ($urlImage == 'camarafot.png') {
                      $nameImage = $this->webroot.'img'.DS.$urlImage;  
                    } else {
                      $nameImage = '/'.$urlImage;
                    }
                    ?>
                    <img src="<?php echo  $nameImage?>" width="100px" heigth="100px" id="image1" type ='file' alt="<?php echo __('Imágen de perfil');?>" > 
                    <?php  if (empty($specialitiesUser)) {
                    	echo '<div style="margin-top:20px"><p>'.__('Su usuario no ha añadido ninguna especialidad. Pulse en añadir nueva especialidad para destacar su usuario').'</p></div>';
                    } else {
                      foreach ($specialitiesUser as $speciality) {
                    	?>
                    	 <div style="clear:both;"></div>
                    		<div class="box" style="margin-top:20px">
      					          <div class="box-header" style="background-color:#4778a0">
      					            <h2 class="title text-center">
      						  		      <i style="color:#fff" class="fa fa-star"></i>
      					              <a href="#" class="btn-minimize" style="text-decoration:none;color:#fff">
      					              	<?php echo $speciality['Speciality']['name'];?></a></h2>
      					            <div class="box-icon">
                              <a href="#" url="/specialities/delete/<?php echo $speciality['Speciality']['id'];?>" class="speciality_delete" style="color:#fff">
                                <i class="fa fa-trash-o" style="color:#fff"></i>
                              </a>
                            </div>
                            <div class="box-icon">

      					              <a href="#" class="speciality_edit" url_data_name="<?php echo $speciality['Speciality']['name'];?>" url_data_id="<?php echo $speciality['Speciality']['id'];?>" url_data_description="<?php echo $speciality['Speciality']['description'];?>" style="color:#fff">
      					              	<i class="fa fa-edit" style="color:#fff"></i>
      					              </a>
      					            </div>
                            
      					          </div>
      					          <div class="box-content">
      					            <div class="row">
      					                  <div class="col-md-12">
      					                    <table class="table table-responsive">
      					                      <thead></thead>
      					                      <tbody>
      					                        <tr>
      					                          <td class="col-md-1">
      					                           &nbsp;
      					                          </td>
      					                          <td class="col-md-9">
      					                            <div class="text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:20px;border-radius: 10px 10px 10px 10px;"><?php echo $speciality['Speciality']['description'];?>
      					                          </td>
      					                          <td class="col-md-1">
      					                            &nbsp;
      					                          </td>
      					                        </tr>
      					                      </tbody>
      					                    </table>
      					                  </div>
      					              </div>
      					          </div>
        				</div>
					<?php }
                    }?>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>



<style>

.table th, .table td { 
     border-top: none !important;
     margin: 0px !important;
     padding: 0px !important;
 }
 .resumen {
 	border: 1px solid #799ebe;
  color: #666666;
  padding:10px;
  border-radius: 10px;
  margin-bottom:10px;
  background-color: #ffffff;
 }
 .row_round {
 	background-color:#abc4dd;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
 .row_round2 {
 	background-color:#e6ebf1;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
 
  .row_round3 {
 	background-color:#cdcdcd;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
  .row_round4 {
 	background-color:#E5CD98;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
 
 .colour_grey {
 	color:#666666;
 	padding:10px;
 	  font-family: sourcesansprobold;
  font-size: 12pt;
 }
 .contador {
  position: relative;
  z-index: 0;
  background-color: white;
  border: 2px solid #47789f;
  border-radius: 10px;
  width: 25px;
  text-align: center;
  left: -6px;
  color: #ff3600;
  top: 11px;
}
.box {
	border: 0px;
}
.tit_box {
  color: #c4a34e;
  padding: 20px;
  font-size: 24px;
  line-height: 5px;
  font-weight: bold;
  -webkit-box-shadow: 0px 6px 30px 0px rgba(50, 50, 50, 0.75);
  -moz-box-shadow: 0px 6px 30px 0px rgba(50, 50, 50, 0.75);
  box-shadow: 0px 6px 30px 0px rgba(50, 50, 50, 0.75);
}
</style>