<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));

?>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#menu1').addClass('active2');
  $('#menu2').removeClass('active2');
  //bootbox.alert('Bon dia');
  $('.closes').on('click', function() {
    $(this).parent().hide();
  });
});
<?php $this->Html->scriptEnd();
?>
<?php // echo $this->Element('card');

?>

<div id="mobile">
<div class='custom-popup' id="map" style="position:absolute;"></div>
<div id="buttons">
  <div class="tbuttons"><a href="/Companies/cvmap"><?php echo __('CV Empresa');?></a></div>
  <div class="tbuttons"><a href="/Companies/projectmap"><?php echo __('Proyectos');?></a></div>
  <div class="tbuttons active"><?php echo __('Oficinas');?></div>
</div>
</div>
<script>
// Provide your access token
L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
// Create a map in the div #map
// Create array of lat,lon points.

// Define polyline options
// http://leafletjs.com/reference.html#polyline
var map = L.mapbox.map('map', 'egalles79.mcf2id2p')
  .setView([40.731, -6.438],3);
var offices = JSON.parse(JSON.stringify(<?php echo json_encode($offices)?>));
$.each(offices, function(index, office) {
    
    var color = '';
    if (office.principal == 1) {
      map.setView([office.latitude,office.longitude],3);
    }
    color= '#66A0C6';
    urldomain = "http://<?php echo $_SERVER["SERVER_NAME"]?>";
    
    L.mapbox.featureLayer({
      // this feature is in the GeoJSON format: see geojson.org
      // for the full specification
      type: 'Feature',
      geometry: {
          type: 'Point',
          // coordinates here are in longitude, latitude order because
          // x, y is the standard for GeoJSON and many formats
          coordinates: [
            office.longitude,
            office.latitude
          ]
      },
      properties: {
          title: '<div style="float:left"><a href="'+urldomain+'/Companies/ver_cv"><img src="'+urldomain+'/img/lupaicon.png"></a></div><div style="float:right"><a href="'+urldomain+'/Offices/edit/'+office.id+'"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;</div><strong>'+office.name.toUpperCase()+'</strong>',
          description: '<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-4"><i class="fa fa-home">&nbsp;&nbsp;<?php echo __("Dirección")?>: </i></div><div class="col-md-8"><div class="new_font">'+office.address+'</div></div><div class="col-md-12"><div class="row"><div class="col-md-4"><i class="fa fa-phone">&nbsp;&nbsp;<?php echo __("Teléfono")?>: </i></div><div class="col-md-8"><div class="new_font">'+office.phone+'</div></div><div class="col-md-12"><div class="row"><div class="col-md-4"><i class="fa fa-envelope">&nbsp;&nbsp;<?php echo __("Dirección de email")?>: </i></div><div class="col-md-8"><div class="new_font">'+office.email+'</div></div></div></div><div class="col-md-12"><div class="buttonadd"><a href="'+urldomain+'/Offices/add">Añadir nueva oficina</a></div></div></div>',
          // one can customize markers by adding simplestyle properties
          // https://www.mapbox.com/guides/an-open-platform/#simplestyle
          'marker-size': 'large',
          'marker-color': color,
          'marker-symbol': 'city'
      }
  }).addTo(map);
});
</script>
<style>
#buttons {
  z-index: 100;
  position: relative;
  float: right;
  margin-right: 20px;
}
.tbuttons {
    background-color: #47789F;
    font-size: 0.9em;    
    color: #fff;
    text-align: center;
    -webkit-box-shadow: 7px 4px 6px -3px #333333;
    -moz-box-shadow: 7px 4px 6px -3px #333333;
    box-shadow: 7px 4px 6px -3px #333333;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    float: left;
    margin-left: 10px;
    padding: 5px;
    padding-left: 20px;
    padding-right: 20px;
}
.tbuttons a {
  text-decoration: none;
  color: #fff;
}
.active {
  background-color: #66A0C6
}
.custom-popup .leaflet-popup-content-wrapper {
  background:#e6ebf1;
  color:#47789f;
  font-size: 13px;
  line-height:24px;
  width:300px;
  -webkit-box-shadow: 14px 8px 12px -6px #333333;
    -moz-box-shadow: 14px 8px 12px -6px #333333;
    box-shadow: 14px 8px 12px -6px #333333;
  }

.custom-popup .leaflet-popup-tip-container {
  width:10px;
  height:15px;
  }
.custom-popup .leaflet-popup-tip {
  border-left:15px solid transparent;
  border-right:15px solid transparent;
  border-top:15px solid #2c3e50;
  }
  .custom-popup .brumdos {
    position: absolute;
    top: 2px;
    left: 20px;
  }
  .custom-popup .marker-title {
    text-align: center;
    margin-bottom: 0px;
  }
  .new_font {
      padding: 3px;
      border: 1px solid #517fa4;
      min-height: 20px; 
      text-align: center; 
      margin-bottom: 8px; 
    font-size: 13px;
        background: white;
    border-radius: 4px;
  }
  .marker-description {
    padding: 20px;
  }
  .buttonadd {
    background-color: #47789f;
    color:#fff;
    text-align:center;
    margin-top: 10px;
        border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
  }
  .buttonadd a {
    text-decoration: none;
    color: #fff;
  }
  .closes {
    cursor: hand;
    float: right;
  }
#main-menu-min{
	display:none!important;
}

</style>