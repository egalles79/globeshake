<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
'jquery-ui-1.10.3.custom.min',
//'jquery.mockjax.min',
'bootstrap-editable.min',
'moment.min',
'ver_cv_company',
'address.min',
'select2.min',
'custom.min',
//'form-x-editable-demo',
'core.min',
//'jquery.mockjax.min',
'bootbox.min'

), array('inline' => false));
/*
'typeaheadjs.10x.js',
'form-x-editable',
*/

?>

<!-- start: Content -->
<div id="cardImage">
  <div id="card-image"></div>
</div>
<div class="box">
  <?php
   $data    = serialize($staticFunctionalitiesNames); 
   $encoded = htmlentities($data);
   $encoded = substr($encoded,5,strlen($encoded));

   echo '<input type="hidden" id="staticFunctionalitiesNames" value="'.$encoded.'">';
  ?>
  <div class="box-header">
    <h2><i class="fa fa-briefcase"></i><?php echo __('CV Empresa');?></h2>
     <div class="box-icon">
        <a href="/Companies/resumen_cv" class=" pull-left" style="margin-right:25px;">
          <?php echo $this->Html->image('resumen_cv.png');?>
          <?php echo __('Ir a resumen CV');?>
        </a>
        <a href="/Companies/cvmap" class=" pull-left" style="margin-right:25px;">
          <?php echo $this->Html->image('mapamundi_mini.png');?>
          <?php echo __('Ver CV en mapa');?>
        </a>
    </div>
  </div>
  <div class="box-content"><br><br>
    <div class="row borde_max">
	    <br>
      <div class="col-md-12"><h2 class="title text-center" style="font-size: 20px;position: relative;"><?php echo __('CV EMPRESA GS');?><div class="back_his" style="">
                  <a href="javascript:history.back()"><?php echo $this->Html->image('/img/flecha_volver.png');?>VOLVER</a>
                </div></h2>
        <hr />
        <div class="text-center">
            <a href="/CompanyProjects/add" style="font-size: 20px;"><i class="fa fa-plus-circle green"></i> <?php echo __('PROYECTOS');?></a>
            
        </div>
        <br>
        <div class="">
          <article id="tabs-to-right" class="tabs-to-right tabs ui-tabs ui-widget ui-widget-content ui-corner-all">
            <ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all">
              <li class="ui-state-default ui-corner-top ui-tabs-selected ui-state-active tabs-to-right"><a href="#tabs-to-right-1"><?php  echo __('Estado en %s',$dataCompany['Company']['name']);?></a></li>
              <li class="ui-state-default ui-corner-top tabs-to-right"><a href="#tabs-to-right-2"><?php echo __('Información de contacto');?></a></li>
              <li class="ui-state-default ui-corner-top tabs-to-right"><a href="#tabs-to-right-3"><?php echo __('Sectores');?></a></li>
            </ul>
            <div class="tabs-background"></div>
            <div id="tabs-to-right-1" style="background-color:#e6ebf1;-webkit-box-shadow: 6px 6px 22px -5px rgba(50, 50, 50, 0.35);-moz-box-shadow: 6px 6px 22px -5px rgba(50, 50, 50, 0.35);    box-shadow: 6px 6px 22px -5px rgba(50, 50, 50, 0.35);" class="ui-tabs-panel ui-widget-content ui-corner-bottom">
              <div class="row">
                  <div class="col-md-12">
                      <div style="float:right">
                        <button type="button" class="btn btn-default btn-lg">
                          <a href="/Companies/edit/<?php echo $dataCompany['Company']['id']?>">
                            <span class="fa fa-edit" aria-hidden="true"></span>
                          </a>
                        </button>
                      </div>
                      <p class="text-center verd" style="font-size:2em"><?php echo strtoupper($dataCompany['Company']['name']);?></p>
                      <div class="col-md-12">
                        <table>
                          <tr>
                            <td style="vertical-align:top;padding:10px">
                              <?php 
                              $urlImage = (!empty($dataCompany['Company']['logo_dir'])) ? $dataCompany['Company']['logo'] : 'camarafot.png';
                              if ($urlImage == 'camarafot.png') {
                                $nameImage = $this->webroot.'img'.DS.'company'.DS.$urlImage;  
                              } else {
                                $nameImage = $this->webroot.'files'.DS.'companies'.DS.'company_'.$dataCompany['Company']['id'].DS.$urlImage;
                              }
                              ?>
                              <img src="<?php echo  $nameImage?>" id="image1" alt="<?php echo __('Imágen de perfil');?>" > 
                            </td>
                            <td style="vertical-align:top;padding:10px">
                              <p style="margin-bottom:10px;color:#7a7a7a;font-size:0.8em">
                              <?php echo $dataCompany['Company']['description']?>
                              </p>
                              <div class="clear:both"></div>
                              <span style="color:#7a7a7a"><?php echo __('Último revisor:')?></span>
                              <a href="#" class="viewCard" datauser="<?php 
                              echo $last_reviewer;?>"><?php echo $staticUserNames[$last_reviewer];?></a>
                            </td>
                          </tr>
                        </table>
                      </div>
                      <div class="col-md-12" style="margin-top:5%;color:#7a7a7a">
                          <div class="col-md-6">
                            <?php echo $this->Html->image('numero_1.png',array('class' => 'pull-left'));?>
                            <div class="progress progress-success progress-striped" style="  width: 80%;margin-left: 31px;margin-top: 5px;  margin-bottom: 0;">
                              <div class="bar" style="width: <?php echo $percentComplete?>%">&nbsp;</div>
                            </div>
                            <p style="margin:10px;margin-left: 10%;margin-bottom: 10px;padding: 0;"><?php echo __('CV Empresa al ');?><span class="verd"><?php echo $percentComplete?>%</span> 
                            </p>
                            <?php echo $this->Html->image('numero_2.png',array('class' => 'pull-left'));?>
                            <p><?php echo __('Proyectos incompletos').':';?><span class="verd"><?php echo $incompleteProjects?></span></p>
                          
                            <?php echo $this->Html->image('numero_3.png',array('class' => 'pull-left'));?>
                            <p><?php echo __('Nº de proyectos').':';?><span class="verd"><?php echo $numProjects?></span></p>
                          </div>
                          <div class="col-md-6">
                              <?php echo $this->Html->image('numero_4.png',array('class' => 'pull-left'));?>
                              <p><?php echo __('Nº de paises').':';?><span class="verd"><?php echo $numCountries?></span></p>
                          
                              <?php echo $this->Html->image('numero_5.png',array('class' => 'pull-left'));?>
                              <p><?php echo __('Nº de empleados').':';?><span class="verd"><?php echo $numempleados?></span></p>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
            <div id="tabs-to-right-2" style="background-color:#e6ebf1" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
              <div class="row">
                        <button type="button"  class="btn btn-default btn-success btn-lg" style="position: absolute;right: 24px;">
                          <a href="/Offices/add/">
                            <span class="fa fa-plus" style="color:#fff" aria-hidden="true">&nbsp;<?php echo __('Añadir oficina');?></span>
                          </a>
                        </button>
                    <p class="text-center verd" style="font-size:2em"><?php echo strtoupper(__('Oficinas'))?></p>
                    <div class="col-md-12">
                    <?php if (empty($offices)) { 
                      $linkUrl = '<a href="/Companies/add/" class="verd2">'.__('aquí').'</a>';
                      echo __('No ha añadido oficinas. Pulse %s para añadir una.',$linkUrl);
                    } else {?>
                      
                      <?php 
                      $i=1;
                      foreach($offices as $office) {
                        $styleCss = ($i % 2 == 0) ? 'margin-bottom: 10px;border-left:1px solid;' : 'margin-bottom: 10px;';

                        ?>
                        <div class="col-md-6" style="<?=$styleCss;?>">
                          <div class="col-md-12">
	                          <div style="float:right">
                            <button type="button" class="btn btn-default btn-lg">
                              <a href="/Offices/edit/<?php echo $office['Office']['id'];?>">
                                <span class="fa fa-edit" aria-hidden="true"></span>
                              </a>
                            </button>
                          </div>
                            <?php echo '<p>'.$office['Office']['name'].'</p><p>'.$office['Office']['description'].'</p><p>'.$office['Office']['address'].'</p><p>'.$office['Office']['name'].'-'.$staticCountriesNames[$office['Country']['id']].'</p>';?>
                            <div class="row">
                              <?php 
                                if (!empty($office['Office']['image_dir'])) {
                                  echo '<img class="col-md-6" src="'.$office['Office']['image_dir'].DS.$office['Office']['image'].'" />';
                                }

                              ?>
                            <div class="col-md-6" style="height:120px;background-size:cover;background-image:url(https://maps.googleapis.com/maps/api/staticmap?&zoom=13&size=185x120&maptype=roadmap&markers=color:blue%7Clabel:Sede%7C<?php echo $office['Geolocalitzation']['latitude']?>,<?php echo $office['Geolocalitzation']['longitude']?>);">

                          </div></div>
                          </div>
                        </div>
                        <?php if($i % 2 == 0) echo('<div class="col-md-12" style="border-top: 1px solid;margin-bottom:20px;"></div>'); ?>
                        
                      <?php $i++; }
                    }?>
                    </div>

              </div>
              
            </div>
            <div id="tabs-to-right-3" style="background-color:#e6ebf1" class="ui-tabs-panel ui-widget-content ui-corner-bottom ui-tabs-hide">
              <div class="row-fluid" style="text-align: center;">
                <div id="myCarousel_lin" class="carousel slide">
                  <br><br>
                  <div class="carousel-inner" style="min-height: 150px;width: 600px;margin: auto;">
                    <?php 
                    if (empty($sectorsShow)) {
                      echo '<div class="span6" style="position: relative;">'.__('Añada proyectos a su empresa para ver los sectores.').'</div>';
                    } else {
                    if (count($sectorsShow) > 1) {?>
                      <div class="item active">
                    <?php 
                    }
                      $countValue = 0;
                      foreach($sectorsShow as $intValue => $currentSector) {
                        $img = (!empty($currentSector['image'])) ? $currentSector['image'] : 'otro.png'; ?>
                        <div class="col-md-3" style="position: relative;">
                          <?php echo $this->Html->image('sectors/'.$img,array('style' => 'width:100%;'));?>
                          <div style="font-size:0.8 em;color:#a7a7a7"><?php echo __($currentSector['name']);?></div>
                        </div>
                        <?php 
                        if ($intValue != count($sectorsShow)-1) {
                          if (($countValue%3 == 0) && ($countValue != 0)) {
                            echo '</div><div class="item">';
                            $countValue=0;
                          } else {
                            $countValue++;  
                          }
                        }
                      }
                    } ?>
                    </div>
                  </div>
                  <?php if (count($sectorsShow) > 4) { ?>
                    <a class="carousel-control left" href="#myCarousel_lin" data-slide="prev" style="background: none;border: none;">
                    <?php echo $this->Html->image('flecha_proyectos_2.png',array('style' => 'padding-top:4px'));?>
                    </a>
                    <a class="carousel-control right" href="#myCarousel_lin" data-slide="next" style="background: none;border: none;">
                      <?php echo $this->Html->image('flecha_proyectos.png');?>
                    </a>
                  <?php }?>
                </div>
              </div>
            </div>
          </article>
          &nbsp;
          <div style="margin-top:10px"></div>
          <div class="box">
            <div class="box-header" style="background-color:#4778a0">
              <h2 class="title" style="color:#fff">
                <i class="fa fa-users"></i>
                <a href="#" class="btn-minimize formation" style="text-decoration:none;color:#fff">
                  <?php echo __('PERSONAL PARTICIPANTE EN PROYECTOS');?>
                </a>
              </h2>
              <div class="box-icon">
                <a href="#" class="btn-minimize formation"><i class="fa fa-chevron-up"></i></a>
              </div>
            </div>
            <div class="box-content">
              <div class="row">
                <div class="marg2 col-md-12">
                  <?php

                   if (empty($usersCompany)) {
                    echo __('No tiene compañeros con proyectos asignados a ésta compañía');
                   } else {
                   foreach($usersCompany as $userCompany) {?>
                    <div class="usercompany col-md-5 col-sm-5 col-xs-5">
                      <?php echo __('NOMBRE').': &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#7a7a7a">'.$staticUserNames[$userCompany['id']].'</span>';?><br />
                      <?php echo __('PROYECTOS').': <span style="color:#7a7a7a;font-size:0.8em">';
                        if (!empty($userCompany['personalprojects'])) {
                          $projectNames = '';
                          foreach($userCompany['personalprojects'] as $personalproject) {
                            $projectNames .= $staticUserProjectNames[$personalproject].', ';
                          }
                          echo substr($projectNames,0,strlen($projectNames)-2);
                        } else {
                          $projectNames = '';
                          foreach($userCompany['companyprojects'] as $personalproject) {
                            $projectNames .= $staticProjectCompanyNames[$personalproject].', ';
                          }
                          echo substr($projectNames,0,strlen($projectNames)-2);
                        }
                        echo '</span>';?><br />
                      <a href="#" class="viewCard" dataUser="<?php echo $userCompany['id'];?>" ><?php echo __('Ver tarjeta de visita');?></a>
                    </div>
                   <?php }} ?>
                </div>
              </div>
            </div>
          </div>
          <div style="margin-top:10px"></div>
          <div class="box">
	          <div class="pull-right">
	          <?php
                  if (!empty($staticUserNames[$last_reviewer])) : ?>
                  <div class="pull-left" style="    margin-top: 4px;"><?php  echo $this->Html->image('logo_hombre_casco.png')?></div>
                  <div class="pull-right" style="margin-left: 10px;font-size: 12px;margin-bottom: 5px;"> 
                  <p style="margin: 0px;padding: 0px;text-align: left;">
                    <?php echo __('ÚLTIMO REVISOR:').' '?>
                  </p>
                  <a href="#" class="viewCard" dataUser="<?php echo $last_reviewer;?>" ><?php echo $staticUserNames[$last_reviewer];?></a>
                  </div>
                  <?php   
                      
                  endif; ?>
              </div>
              <div class="clearfix"></div>
            <div class="box-header" style="background-color:#4778a0">
              <h2 class="title" style="color:#fff">
                <i class="fa fa-question"></i>
                <a href="#" class="btn-minimize formation" style="text-decoration:none;color:#fff">
                  <?php echo __('EN QUÉ NOS DIFERENCIAMOS');?>
                </a>
              </h2>
              <div class="box-icon">
                  <a style="color:#fff;" href="/Companies/edit/<?php echo $dataCompany['Company']['id']?>/true"  class=""><i class="fa fa-edit white"></i></a>
                <a href="#" class="btn-minimize">
                  <i class="fa fa-chevron-up"></i>
                </a>
              </div>
            </div>
            <div class="box-content">
              <div class="row">
                <div class="col-md-12 marg2">

                          <?php if (empty($dataCompany['Company']['differences'])) { 
                            echo __('Añada las diferencias de su empresa'); 
                          } else {
                            echo $dataCompany['Company']['differences'];
                          }?></td>
                </div>
              </div>
            </div>
          </div>
          <p class="text-center verd" style="font-size:2em"><?php echo __('PROYECTOS EN %s',strtoupper($dataCompany['Company']['name']))?></p>
          <?php $divCounter = 1;?>

          <?php 
          
          foreach ($continents as $continentId => $continent) {?>
            <div class="box">
              <div class="box-header" style="background-color:#4778a0">
                <h2 class="title " style="color:#fff">
                  <i class="fa fa-globe"></i>
                  <a href="#" class="btn-minimize" style="text-decoration:none;color:#fff">
                    <?php echo strtoupper(str_replace(array('á','é','í','ó','ú','ñ'),array('Á','É','Í','Ó','Ú','Ñ'),$continent['name']));?></a>
                  </a>
                </h2>
                <div class="box-icon">
                  <a href="#" class="btn-minimize">
                    <i class="fa fa-chevron-up"></i>
                  </a>
                </div>
              </div>
              <div class="box-content continets">
                <div class="row">
                  <div class="col-md-12 marg2">
	                  <?php echo $this->Html->image('continents/'.$continentId.'.jpg',array('style' => 'width:100%;'));?>
                    </div>
                    <div class="col-md-12 marg2">
	                        
                          <div id="myCarousel_lin<?php echo $continentId?>" class="carousel slide">
                            <div class="carousel-inner" style="width: 600px;margin: auto;">
                              <?php 
                                if ($continents[$continentId]['countries'] > 0) {?>
                                  <div class="item active">
                                <?php }
                                $numCountries = count($continents[$continentId]['countries'])-1;
                                $countValue = $counter = 0;
                                $continentCountries = $continents[$continentId]['countries'];
                                foreach($continentCountries as $idCountry => $country) { ?>
                                    <div class="col-md-3 col-sm-3 pais_slide">
                                      <a href="/Companies/proyectos_pais/<?php echo $idCountry?>">
                                        <?php 
                                        echo strtoupper(str_replace(array('á','é','í','ó','ú','ñ'),array('Á','É','Í','Ó','Ú','Ñ'),$country));?></a>
                                    </div>
                                  <?php 
                                  if ($counter != count($continentCountries)-1) {
                                    if (($countValue%3 == 0) && ($countValue != 0)) {
                                      echo '</div><div class="item">';
                                      $countValue=0;
                                    } else {
                                      $countValue++;  
                                    }
                                  }
                                   $counter++;
                                }
                            ?>
                            </div></div>
                            <?php if ($numCountries > 3) { ?>
                              <a class="carousel-control left" href="#myCarousel_lin<?php echo $continentId?>" data-slide="prev" style="background: none;border: none;">
                              <?php echo $this->Html->image('flecha_proyectos_2.png',array('style' => 'padding:4px'));?>
                              </a>
                              <a class="carousel-control right" href="#myCarousel_lin<?php echo $continentId?>" data-slide="next" style="background: none;border: none;">
                                <?php echo $this->Html->image('flecha_proyectos.png');?>
                              </a>
                            <?php }?>
                          </div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                  </div>
               
              </div>
            

            <?php $divCounter++; ?>
          <?php } 
          if (empty($continents)) { ?>
          <div class="box">
              <div class="box-header" style="background-color:#4778a0">
                <h2 class="title " style="color:#fff">
                  <i class="fa fa-globe"></i>
                  <a href="#" class="btn-minimize" style="text-decoration:none;color:#fff">
                    <?php echo __('Sin proyectos');?>
                  </a>
                </h2>
                <div class="box-icon">
                  <a href="#" class="btn-minimize">
                    <i class="fa fa-chevron-up"></i>
                  </a>
                </div>
              </div>
              <div class="box-content">
                <div class="row">
                  <div class="col-md-12">
                    <div style="text-align:center"><?php echo __('No tiene proyectos añadidos.Pulse arriba en (+) Proyectos para añadir nuevos proyectos a su cv');?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<style>
.tabs-to-right .ui-tabs-nav { 
    float: right; 
    border-left: none; 
    background-color: white;
} 
.tabs-to-right .tabs-background { 
    height: 31px; 
    background-color: white;
    border-radius: 10px 10px 0px 0px;
}
.ui-widget-content {
  border: 0px;
  border-radius: 0px 0px 10px 10px;
}
.ui-widget-header {
  background: #e6ebf1;
  border: 0px;
  font-size: 0.8em;
}
.pais_slide{
	font-size:1.4em;margin-top:20px;border: 1px solid;border-color:#fff;border-right-color:#4778a0;position: relative;text-align: center;
}
.pais_slide:last-child{
	border:none;
}
.ui-widget {
  font-family: inherit;
}
.ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
  color: #7A7A7A;
}
.ui-tabs .ui-tabs-nav li.ui-tabs-active .ui-tabs-anchor, .ui-tabs .ui-tabs-nav li.ui-state-disabled .ui-tabs-anchor, .ui-tabs .ui-tabs-nav li.ui-tabs-loading .ui-tabs-anchor {
  background-color : rgb(230, 235, 241);
}
.ui-tabs .ui-tabs-nav li{
	border:none;
}
.verd {
  color: rgb(203, 153, 106);
  margin-left: 5px;
}
.progress {
  height: 20px;
  margin-bottom: 20px;
  overflow: hidden;
  background-color: #f7f7f7;
  background-image: -moz-linear-gradient(top,#f5f5f5,#f9f9f9);
  background-image: -webkit-gradient(linear,0 0,0 100%,from(#f5f5f5),to(#f9f9f9));
  background-image: -webkit-linear-gradient(top,#f5f5f5,#f9f9f9);
  background-image: -o-linear-gradient(top,#f5f5f5,#f9f9f9);
  background-image: linear-gradient(to bottom,#f5f5f5,#f9f9f9);
  background-repeat: repeat-x;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fff5f5f5',endColorstr='#fff9f9f9',GradientType=0);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
}
.progress-success.progress-striped .bar, .progress-striped .bar-success {
  background-color: #62c462;
  background-image: -webkit-gradient(linear,0 100%,100% 0,color-stop(0.25,rgba(255,255,255,0.15)),color-stop(0.25,transparent),color-stop(0.5,transparent),color-stop(0.5,rgba(255,255,255,0.15)),color-stop(0.75,rgba(255,255,255,0.15)),color-stop(0.75,transparent),to(transparent));
  background-image: -webkit-linear-gradient(45deg,rgba(255,255,255,0.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,0.15) 50%,rgba(255,255,255,0.15) 75%,transparent 75%,transparent);
  background-image: -moz-linear-gradient(45deg,rgba(255,255,255,0.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,0.15) 50%,rgba(255,255,255,0.15) 75%,transparent 75%,transparent);
  background-image: -o-linear-gradient(45deg,rgba(255,255,255,0.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,0.15) 50%,rgba(255,255,255,0.15) 75%,transparent 75%,transparent);
  background-image: linear-gradient(45deg,rgba(255,255,255,0.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,0.15) 50%,rgba(255,255,255,0.15) 75%,transparent 75%,transparent);
}
.progress-success .bar, .progress .bar-success {
  background-color: #5eb95e;
  background-image: -moz-linear-gradient(top,#62c462,#57a957);
  background-image: -webkit-gradient(linear,0 0,0 100%,from(#62c462),to(#57a957));
  background-image: -webkit-linear-gradient(top,#62c462,#57a957);
  background-image: -o-linear-gradient(top,#62c462,#57a957);
  background-image: linear-gradient(to bottom,#62c462,#57a957);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462',endColorstr='#ff57a957',GradientType=0);
}

.white {
  color: #fff !important;
}
/*
.continets .carousel-inner {
 margin-top: -50px;
}
*/
.carousel-inner2 {
  /* min-height: 150px; */
  width: 100%;
  margin-top:2%;
  margin-left: 15%;
}
.carousel-control.left {
   background-image: none !important;
}
.carousel-control.right {
   background-image: none !important;
}

.verd2 {
  color: rgb(203, 153, 106) !important;
  margin-left: 0px;
}
.usercompany {
  padding:2%;
  margin:1%;
  background-color:#e6ebf1;
  border-radius: 10px;
  -webkit-box-shadow: 6px 6px 22px -5px rgba(50, 50, 50, 0.35);
    -moz-box-shadow: 6px 6px 22px -5px rgba(50, 50, 50, 0.35);
    box-shadow: 6px 6px 22px -5px rgba(50, 50, 50, 0.35);
}
#map-canvas {
  height: 400px;
  width: 70%;
  margin: 0px;
  border-color: #47789f;
}
@media (max-width: 480px) {
  .formation {
    font-size: 0.8em;
    margin-left: -10px !important;
  }
}
.carousel-control img{
	margin-top: 20px;
}
.abso_tito{
  background: #4778a0;
width: 560px;
display: inline-block;
height: 20px;
cursor: pointer;
vertical-align: top;
}
.abso_titos h3{
  color: #4778a0;
background-color: white;
padding: 0px 30px 0 8px;
font-family: sourcesansprobold!important;
background-image: url(../../images/close_03.png);
background-repeat: no-repeat;
background-position: 98% 2px;
background-size: 20px;
}
.collapsed h3{
  background-image: url(../../images/down_03.png);
}
.cv_personal{
padding: 20px 0 20px 20px;
border-top: 2px solid #d9d9d9;
}
.bot .span8,.tarjeta .span8 .span11{
  border-bottom: 1px dashed #82c7f8;
  padding-top: 10px;
  margin-left: 0px;
  font-size: 16px;
}
.tarjeta{
  border: 2mm solid #DDC079;
  padding: 10px;
  font-family: sourcesansprobold;
font-size: 10pt;
}
.abso_titos{
  cursor: pointer;
}

.back{
  background-image: url(../../images/fondo_tarjeta_03.png);
background-color: white;
background-position: right 80px;
position: relative;
background-repeat: no-repeat;
background-size: 165px;
}
.forma{
  background-color: #e6ebf1;
  background-image: url(../../images/grua_mini.png);
  background-repeat: no-repeat;
  background-position: right 10px;
  padding: 10px;
  font-size: 14px;
  border-radius: 10px;
  margin-bottom: 20px;
}
.forma.fon{
  background-image: url(../../images/doument_formacion.png);
}
.sans{
  font-family: sourcesansprobold;
  font-size: 11pt;
  color: #3fa9f5;
  text-align: right;
}
.edit{
  margin-right: 10px;
font-size: 14px;
margin-top: 1px;
cursor: pointer;
}
.edit img{
  display: inline-block;
  position: relative;
top: 2px;
  
}

#cv_personal .row-fluid .span11:first-child,.forma .span9:first-child{
  margin-left: 2.127659574468085%!important;
}
.open_new{
  background-image: url(../../images/close_03.png);
  width: 20px;
  height: 17px;
  background-repeat: no-repeat;
  float: left;
  margin: 6px 0 0 6px;
}
.edis{
min-height: 26px!important;
padding-top: 7px!important; 
}
.tit_proj.collapsed{
margin-bottom: 20px;
}
.table th, .table td { 
     border-top: none !important;
     margin: 0px !important;
     padding: 0px !important;
 }
.submittarjet {
    text-shadow: 0 1px 1px rgba(0,0,0,.3);
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  box-shadow: 0 1px 2px rgba(0,0,0,.2);
  background: #f78d1d;
  background: -webkit-gradient(linear, left top, left bottom, from(#ddc079), to(#c5a653));
  background: -moz-linear-gradient(top, #ddc079, #c5a653);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ddc079', endColorstr='#c5a653');
  background-color: #C2A34E;
  border: medium none;
  color: #FFFFFF;
  margin-top: 2% !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  font-size: 1.5em !important;
  padding: 5px;
}
a.viewCard {
  color : rgb(203, 153, 106);
  text-decoration: none;
}
a.viewCard:hover {
  text-decoration: underline;
  color: #ACACAC;
}
#cardImage {
   width: 100%;
   height: 100%;
   top: 0px;
   left: 0px;
   position: fixed;
   display: block;
   opacity: 0.9;
   background-color: #fff;
   z-index: 99;
   text-align: center;
}
#card-image {
  position: absolute;
  top: 100px;
  left: 35%;
  z-index: 100;
}
/*
#tabs-to-right-2 .row > .col-md-12 > .col-md-6:nth-child(odd){
	border-right: 1px solid;
}
*/
.item{
	    min-height: 55px;
}
.back_his{
	position: absolute;
	left: 90%;
    top: 0;
}
#myCarousel_lin .carousel-control img{
	padding-top: 80px!important;
}
#myCarousel_lin .carousel-control.right img{
	 padding-top: 74px!important;

}
</style>
