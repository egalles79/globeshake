<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));
?>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#menu1').addClass('active2');
  $('#menu2').removeClass('active2');

  //bootbox.alert('Bon dia');
});
<?php $this->Html->scriptEnd();
?>
<?php // echo $this->Element('card');

?>
<script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet' />
<link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet' />

<div id="mobile">
<div id="map" style="position:absolute;"></div>
<div id="buttons">
  <div class="tbuttons"><a href="/Companies/cvmap"><?php echo __('CV empresa');?></a></div>
  <div class="tbuttons active"><?php echo __('Proyectos');?></div>
  <div class="tbuttons"><a href="/Companies/officemap"><?php echo __('Oficinas');?></a></div>
</div>
</div>
<script>
L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
// Here we don't use the second argument to map, since that would automatically
// load in non-clustered markers from the layer. Instead we add just the
// backing tileLayer, and then use the featureLayer only for its data.
var map = L.mapbox.map('map')
    .setView([38.9, -77], 13)
    .addLayer(L.mapbox.tileLayer('mapbox.streets'));

var testMarkers = new L.markerClusterGroup();
var coords = [
    [37.4219985, -122.08395439999998],
    [37.4219985, -122.08395439999998],
    [37.4219985, -122.08395439999998],
    [37.4219985, -122.08395439999998],
    [33.4219985, -122.08395439999998],
    [37.4219985, -122.08395439999998],
    [37.4219985, -122.08395439999998],
    [37.4219985, -122.08395439999998],
    [37.4219985, -122.08395439999998],
    [37.4219985, -122.08395439999998]
];

for (var i = 0; i < coords.length; i++) {
    var marker = L.marker(new L.LatLng(coords[i][0], coords[i][1]), {
        icon: L.mapbox.marker.icon({
            'marker-symbol': 'a',
            'marker-color': '#ccc'
        })
    });

    testMarkers.addLayer(marker);
}

var bounds = testMarkers.getBounds();
//map.fitBounds(bounds);

map.addLayer(testMarkers);
</script>
<style>
#buttons {
  z-index: 100;
  position: relative;
  float: right;
  margin-right: 20px;
}
.tbuttons {
    background-color: #47789F;
    font-size: 0.9em;    
    color: #fff;
    text-align: center;
    -webkit-box-shadow: 7px 4px 6px -3px #333333;
    -moz-box-shadow: 7px 4px 6px -3px #333333;
    box-shadow: 7px 4px 6px -3px #333333;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    float: left;
    margin-left: 10px;
    padding: 5px;
    padding-left: 20px;
    padding-right: 20px;
}
.tbuttons a {
  text-decoration: none;
  color: #fff;
}
.active {
  background-color: #66A0C6
}
</style>