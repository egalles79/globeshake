<?php
$this->extend('/Common/panel');
echo $this->Html->script(array(
    'jquery-ui-1.10.3.custom.min',
    'jquery.sparkline.min',
    'jquery.chosen.min',
    'jquery.autosize.min',
    'jquery.placeholder.min',
    'daterangepicker.min',
    'moment.min',
    'jquery.steps.min',
    'custom.min',
    'bootbox.min',
    'jquery.tooltipster.min',
    'jquery.mask.min',
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){

  $('#menu1').addClass('active2');
  $('#menu2').removeClass('active2');

  $('.submit').on('click', function() {
    if ($('#CompanyCit').val() == '') {
      bootbox.alert('Debe seleccionar una ciudad');
      return false;
    }
    if ($('#CompanyName').val() == '') {
      bootbox.alert('Debe seleccionar un nombre de empresa');
      return false;
    }
    if ($('#CompanyContactResponsableId').val() == 0) {
      bootbox.alert('Debe seleccionar una persona de contacto');
      return false;
    }
    
  });
  $('[id^="image"]').click(function(){
        var str = this.id;
        var index = str.replace("image", "");
        $('#imgInp'+index).click();
    });
    $('[id^="imgInp"]').click(function(){
        var str = this.id;
        var index = str.replace("imgInp", "");
        $('#imgInp'+index).change(function() {
            readURL(this, index);
        });
    });

    function readURL(input,value) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                isImage = $.inArray( input.files[0].type, [ "image/gif", "image/jpeg", "image/png","image/vnd.microsoft.icon"] );
                if (isImage != -1) {
                    var img = new Image;
                    img.onload = function() {
                        $('#image'+value).attr('src', e.target.result);
                          var img = new Image;
                          img.onload = function() {
                            max_ancho = 300;
                            max_alto = 250;
                            x_ratio = max_ancho / img.width;
                            y_ratio = max_alto / img.height;
                            if ((img.width <= max_ancho) && (img.height <= max_alto)) {
                            //Si es más pequeña que el máximo no redimensionamos
                            ancho_final = img.width;
                            alto_final = img.height;
                            } else if ((x_ratio * img.height) < max_alto) {
                              alto_final = x_ratio * img.height;
                              ancho_final = max_ancho;
                            } else{
                              ancho_final = y_ratio * img.width;
                              alto_final = max_alto;
                            }
                            $('#image'+value).attr('style', 'width:'+ancho_final+'px;height:'+alto_final+'px');
                          };
                        img.src = reader.result;
                    };
                img.src = reader.result;
                } else {
                    bootbox.alert(I18nJs.t('Sólo se aceptan formatos gif / jpg / png o ico'));
                    return false;
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


  $('#CompanyCountryId').change(function() {
    value = $("#CompanyCountryId option:selected").text();
    $('#CompanyCountryNamex').val(value);
  });
    var accentMap = {
      "á": "a",
      "ö": "o",
      "é": "e",
      "í": "i",
    };
    var normalize = function( term ) {
      var ret = "";
      for ( var i = 0; i < term.length; i++ ) {
        ret += accentMap[ term.charAt(i) ] || term.charAt(i);
      }
      return ret;
    };

  /* ---------- Placeholder Fix for IE ---------- */
  $('input, textarea').placeholder();

  /* ---------- Auto Height texarea ---------- */
  $('textarea').autosize();   
});

<?php $this->Html->scriptEnd();
?>

<div class="box">
    <div class="box-header">
      <?php 
      if ($onlyDifferences) {
        $textTitle = __('Editar diferencias de la compañía %s',$data['Company']['name']);
      } else {
        $textTitle = __('Editar Compañía %s',$data['Company']['name']);
      }
      ?>
      <h2><i class="fa fa-edit"></i><?php echo $textTitle;?></h2>

    </div>
    <div class="box-content"><br><br>
	    <div class="row ">
		    <div class="col-md-12 borde_max marg2" style="    padding-right: 37px;">
<!-- 			    <h2 class="title text-center" style="font-size: 20px;"><?php echo $textTitle;?><div class="back_his" style="left: 33%;"> -->

			    <h2 class="title text-center" style="font-size: 20px;"><?php echo $textTitle;?><div class="back_his" style="left: 23%;">
                  <a href="javascript:history.back()"><?php echo $this->Html->image('/img/flecha_volver.png');?>VOLVER</a>
                </div></h2><hr>
                <div class="box_one round ">
    <?php 
      if ($onlyDifferences) { 
        echo $this->Form->create('Company', array('action' => 'edit', 'class' => 'form-horizontal rellenar-campos'));
        echo $this->Form->input('onlyDifferences',array('type'=>'hidden','value' => true));
        echo $this->Form->input('id', array('label'=>false, 'class'=>'hidden', 'value' => $data['Company']['id']));
      ?>
      <div class="row">
        <div class="col-sm-4 col-md-4 text-right">
          <label><?php echo __('Nuestras diferencias:');?></label>
        </div>
        <div class="col-sm-8 col-md-8">
          <?php echo $this->Form->input('differences',array('label' => false));?>
        </div>
      </div>
      <div class="row" style="margin:20px">
    <?php 
      } else { ?>
            <?= $this->Form->create('Company', array('action' => 'edit', 'class' => 'form-horizontal rellenar-campos', 'type' => 'file')); ?>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
              <label><?php echo __('Nombre de la compañía*:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php echo $this->Form->input('name',array('label' => false,'class' => 'ui-autocomplete-input span8'));?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
              <label><?php echo __('Descripción:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php echo $this->Form->input('description',array('label' => false,'class' => 'span8'));?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
              <label><?php echo __('Nuestras diferencias:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php echo $this->Form->input('differences',array('label' => false,'class' => 'span8'));?>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 col-md-4 text-right">
              <label><?php echo __('Persona responsable de contacto:');?></label>
            </div>
            <div class="col-sm-6 col-md-6">
              <?php echo $this->Form->input('contact_responsable_id',array('label' => false,'options' => $userNames,'class' => 'span8'));?>
            </div>
          </div>
          
          <div class="row" style="margin:20px">
            <div class="col-md-4">
              <?php 
              $urlImage = (!empty($data['Company']['logo_dir'])) ? $data['Company']['logo'] : 'noimagecompany.png';
              if ($urlImage == 'noimagecompany.png') {
                $nameImage = $this->webroot.'img'.DS.'company'.DS.$urlImage;  
              } else {
                $nameImage = $this->webroot.'files'.DS.'companies'.DS.'company_'.$data['Company']['id'].DS.$urlImage;
              }
              ?>
              <p><?php echo __('Añada una imagen representativa de su empresa');?></p>
              <img src="<?php echo  $nameImage?>" id="image1" type ='file' alt="<?php echo __('Imágen de compañía');?>" > 
              <?php
                  echo $this->Form->input('Company.image1',array('id'=> 'imgInp1','label' => false,'class'=>'hidden','type' => 'file'));
              ?>
            </div>

            <div class="col-md-8">
              <input id="pac-input" class="controls" type="text" placeholder="<?php echo __('Busca la ubicación de tu compañía');?>" value="<?php echo $data['Company']['address'] ?>">
              <div class="col-sm-12 col-md-12 col-xs-12">
                <p><?php echo __('Haga una búsqueda en el mapa y en caso de no encontrar dirección exacta de la compañía arrastre el puntero hacia el punto exacto')?></p>
                <div id="map-canvas" class="col-md-9 col-xs-9 -col-sm-9"></div>
                <div class="col-md-3 col-sm-3 col-xs-3 hidden">
                  <?php 
                  foreach ($data['Office'] as $offices) {
                    if ($offices['principal'] == 1) {
                      $officeSelected = $data['Office'];
                      break;
                    }
                  }
                  echo $this->Form->input('country_namex', array('label'=>__('País:'),'value' => $data['Office']['Country']['name'],'readonly' => 'readonly'));
                  echo $this->Form->input('id', array('label'=>false, 'class'=>'hidden', 'value' => $data['Company']['id']));
                  echo $this->Form->input('Office.id', array('label'=>false, 'class'=>'hidden', 'value' => $officeSelected['Office']['id']));
                  echo $this->Form->input('country_id', array('label'=>false, 'selected' => $officeSelected['Country']['id'],'class'=>'hidden', 'options' => $countryList));
                  echo $this->Form->input('cit',array('label'=>__('Ciudad:'),'class' => 'ok_input','value' => $data['Office']['City']['name'],'readonly' => 'readonly'));
                  echo $this->Form->input('cit_id', array('label'=>false, 'value' => $data['Office']['City']['id'],'type'=>'hidden'));
                  echo $this->Form->input('address',array('label'=>__('Dirección:'),'class' => 'hidden', 'label' => false, 'value' => $officeSelected['Office']['address']));
                  echo $this->Form->input('complet_address',array('label'=>__('Dirección:'),'class' => 'hidden', 'label' => false, 'value' => $officeSelected['Office']['address']));
                  echo $this->Form->input('longitude',array('class' =>'hidden','label' => false,'value' => $officeSelected['Geolocalitzation']['longitude']));
                  echo $this->Form->input('latitude',array('class' => 'hidden','label' => false,'value' => $officeSelected['Geolocalitzation']['latitude']));
                  ?>
                </div>
              </div>
            </div>
    <?php } ?>
            <div class="col-md-12" style="text-align:center;margin-top:30px">
                  <?= $this->Form->button(__('Guardar'), array('type' => 'submit','class' => 'btn submit')) ?>
                  <?= $this->Form->end(); ?>
            </div>
          </div>
          <?= $this->Form->end(); ?>
    </div>
</div>

<style>
.rellenar-campos input, textarea, select {
  margin: 3px;
  padding: 2px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border: 1px solid #999999;
  border-color: #47789f;
}
#map-canvas {
  height: 400px;
  width: 100%;
  margin: 0px;
 
}
#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 45px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 200px;
  height: 40px;
/*
position: absolute;
    z-index: 9999999;
    left: 100px;
    top: 0px;
    background-color: #fff;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 200px;
    height: 40px;
*/
}
#pac-input:focus {
  border-color: #4d90fe;
}

.ui-datepicker-calendar {
    display: none;
    }

.pac-container {
  font-family: Roboto;
  font-color: #47789f
}
.tiempo {
  width:60px;
}
.btn-success2 {
  background-color: #47789f;
  color:#fff !important;
}


.submit {
  text-shadow: 0 1px 1px rgba(0,0,0,.3);
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  box-shadow: 0 1px 2px rgba(0,0,0,.2);
  background: #f78d1d;
  background: -webkit-gradient(linear, left top, left bottom, from(#ddc079), to(#c5a653));
  background: -moz-linear-gradient(top, #ddc079, #c5a653);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ddc079', endColorstr='#c5a653');
  background-color: #C2A34E;
  border: medium none;
  color: #eee;
  margin-top: 2% !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 5px;
  padding-bottom: 5px;
}
.submit:hover {
  color: #fff;
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.4);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.4);
  box-shadow: 0 1px 2px rgba(0,0,0,.4);
}
.box_one {
    padding: 20px;
    margin-bottom: 25px;
    background-color: #e6ebf1;
    margin-top: 20px;
}
.round {
  border-radius: 15px;
  box-shadow: 7px 7px 6px -3px #cbcbcb;
}
.abso_mini_tit {
  background-color: white;
  float: left;
  position: relative;
  bottom: 30px;
  font-weight: bold;
}

.box_three{
	    padding: 20px;
    margin-bottom: 25px;
    background-color: #e5cd98;
    margin-top: 20px;
}
.box_two {
    background-color: #cdcdcd;
    padding: 20px;
    margin-bottom: 25px;
    margin-top: 20px;
}
.box_four {
    background-color: #abc4dd!important;
    padding: 20px;
    margin-bottom: 25px;
    margin-top: 20px;
}
</style>