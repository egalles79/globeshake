<div class="projects index">
	<h2><?php echo __('Projects'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('actual_company'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('start_date'); ?></th>
			<th><?php echo $this->Paginator->sort('end_date'); ?></th>
			<th><?php echo $this->Paginator->sort('on_going'); ?></th>
			<th><?php echo $this->Paginator->sort('budget'); ?></th>
			<th><?php echo $this->Paginator->sort('budget_currency'); ?></th>
			<th><?php echo $this->Paginator->sort('billing'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_currency'); ?></th>
			<th><?php echo $this->Paginator->sort('volumes'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_name'); ?></th>
			<th><?php echo $this->Paginator->sort('tags'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('geolocalitzation_id'); ?></th>
			<th><?php echo $this->Paginator->sort('country_id'); ?></th>
			<th><?php echo $this->Paginator->sort('currency_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sector_id'); ?></th>
			<th><?php echo $this->Paginator->sort('city_id'); ?></th>
			<th><?php echo $this->Paginator->sort('function_id'); ?></th>
			<th><?php echo $this->Paginator->sort('id_service_sector'); ?></th>
			<th><?php echo $this->Paginator->sort('company_id'); ?></th>
			<th><?php echo $this->Paginator->sort('duration_year'); ?></th>
			<th><?php echo $this->Paginator->sort('duration_month'); ?></th>
			<th><?php echo $this->Paginator->sort('duration_day'); ?></th>
			<th><?php echo $this->Paginator->sort('duration_hours'); ?></th>
			<th><?php echo $this->Paginator->sort('who_can_see'); ?></th>
			<th><?php echo $this->Paginator->sort('where_to_see'); ?></th>
			<th><?php echo $this->Paginator->sort('subfunction'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($projects as $project): ?>
	<tr>
		<td><?php echo h($project['Project']['id']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['created']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['modified']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['actual_company']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['name']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['end_date']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['on_going']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['budget']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['budget_currency']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['billing']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['billing_currency']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['volumes']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['customer_name']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['tags']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['description']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($project['Geolocalitzation']['id'], array('controller' => 'geolocalitzations', 'action' => 'view', $project['Geolocalitzation']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($project['Country']['name'], array('controller' => 'countries', 'action' => 'view', $project['Country']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($project['Currency']['name'], array('controller' => 'currencies', 'action' => 'view', $project['Currency']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($project['Sector']['name'], array('controller' => 'sectors', 'action' => 'view', $project['Sector']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($project['City']['name'], array('controller' => 'cities', 'action' => 'view', $project['City']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($project['Function']['name'], array('controller' => 'functions', 'action' => 'view', $project['Function']['id'])); ?>
		</td>
		<td><?php echo h($project['Project']['id_service_sector']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($project['Company']['name'], array('controller' => 'companies', 'action' => 'view', $project['Company']['id'])); ?>
		</td>
		<td><?php echo h($project['Project']['duration_year']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['duration_month']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['duration_day']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['duration_hours']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['who_can_see']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['where_to_see']); ?>&nbsp;</td>
		<td><?php echo h($project['Project']['subfunction']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $project['Project']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $project['Project']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $project['Project']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $project['Project']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Project'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Geolocalitzations'), array('controller' => 'geolocalitzations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Geolocalitzation'), array('controller' => 'geolocalitzations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Currencies'), array('controller' => 'currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Currency'), array('controller' => 'currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sectors'), array('controller' => 'sectors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sector'), array('controller' => 'sectors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Functions'), array('controller' => 'functions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Function'), array('controller' => 'functions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Companies'), array('controller' => 'companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Company'), array('controller' => 'companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
