<div class="projects form">
<?php echo $this->Form->create('Project'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Project'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('actual_company');
		echo $this->Form->input('name');
		echo $this->Form->input('start_date');
		echo $this->Form->input('end_date');
		echo $this->Form->input('on_going');
		echo $this->Form->input('budget');
		echo $this->Form->input('budget_currency');
		echo $this->Form->input('billing');
		echo $this->Form->input('billing_currency');
		echo $this->Form->input('volumes');
		echo $this->Form->input('customer_name');
		echo $this->Form->input('tags');
		echo $this->Form->input('description');
		echo $this->Form->input('geolocalitzation_id');
		echo $this->Form->input('country_id');
		echo $this->Form->input('currency_id');
		echo $this->Form->input('sector_id');
		echo $this->Form->input('city_id');
		echo $this->Form->input('function_id');
		echo $this->Form->input('id_service_sector');
		echo $this->Form->input('company_id');
		echo $this->Form->input('duration_year');
		echo $this->Form->input('duration_month');
		echo $this->Form->input('duration_day');
		echo $this->Form->input('duration_hours');
		echo $this->Form->input('who_can_see');
		echo $this->Form->input('where_to_see');
		echo $this->Form->input('subfunction');
		echo $this->Form->input('User');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Project.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Project.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Projects'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Geolocalitzations'), array('controller' => 'geolocalitzations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Geolocalitzation'), array('controller' => 'geolocalitzations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Currencies'), array('controller' => 'currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Currency'), array('controller' => 'currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sectors'), array('controller' => 'sectors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sector'), array('controller' => 'sectors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Functions'), array('controller' => 'functions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Function'), array('controller' => 'functions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Companies'), array('controller' => 'companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Company'), array('controller' => 'companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
