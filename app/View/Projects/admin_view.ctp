<div class="projects view">
<h2><?php echo __('Project'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($project['Project']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($project['Project']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($project['Project']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Actual Company'); ?></dt>
		<dd>
			<?php echo h($project['Project']['actual_company']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($project['Project']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo h($project['Project']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End Date'); ?></dt>
		<dd>
			<?php echo h($project['Project']['end_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('On Going'); ?></dt>
		<dd>
			<?php echo h($project['Project']['on_going']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget'); ?></dt>
		<dd>
			<?php echo h($project['Project']['budget']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Currency'); ?></dt>
		<dd>
			<?php echo h($project['Project']['budget_currency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing'); ?></dt>
		<dd>
			<?php echo h($project['Project']['billing']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Currency'); ?></dt>
		<dd>
			<?php echo h($project['Project']['billing_currency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Volumes'); ?></dt>
		<dd>
			<?php echo h($project['Project']['volumes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Name'); ?></dt>
		<dd>
			<?php echo h($project['Project']['customer_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tags'); ?></dt>
		<dd>
			<?php echo h($project['Project']['tags']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($project['Project']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Geolocalitzation'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['Geolocalitzation']['id'], array('controller' => 'geolocalitzations', 'action' => 'view', $project['Geolocalitzation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['Country']['name'], array('controller' => 'countries', 'action' => 'view', $project['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Currency'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['Currency']['name'], array('controller' => 'currencies', 'action' => 'view', $project['Currency']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sector'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['Sector']['name'], array('controller' => 'sectors', 'action' => 'view', $project['Sector']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['City']['name'], array('controller' => 'cities', 'action' => 'view', $project['City']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Function'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['Function']['name'], array('controller' => 'functions', 'action' => 'view', $project['Function']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Id Service Sector'); ?></dt>
		<dd>
			<?php echo h($project['Project']['id_service_sector']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company'); ?></dt>
		<dd>
			<?php echo $this->Html->link($project['Company']['name'], array('controller' => 'companies', 'action' => 'view', $project['Company']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Year'); ?></dt>
		<dd>
			<?php echo h($project['Project']['duration_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Month'); ?></dt>
		<dd>
			<?php echo h($project['Project']['duration_month']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Day'); ?></dt>
		<dd>
			<?php echo h($project['Project']['duration_day']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Hours'); ?></dt>
		<dd>
			<?php echo h($project['Project']['duration_hours']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Who Can See'); ?></dt>
		<dd>
			<?php echo h($project['Project']['who_can_see']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Where To See'); ?></dt>
		<dd>
			<?php echo h($project['Project']['where_to_see']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Subfunction'); ?></dt>
		<dd>
			<?php echo h($project['Project']['subfunction']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Project'), array('action' => 'edit', $project['Project']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Project'), array('action' => 'delete', $project['Project']['id']), array(), __('Are you sure you want to delete # %s?', $project['Project']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Geolocalitzations'), array('controller' => 'geolocalitzations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Geolocalitzation'), array('controller' => 'geolocalitzations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Currencies'), array('controller' => 'currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Currency'), array('controller' => 'currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sectors'), array('controller' => 'sectors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sector'), array('controller' => 'sectors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Functions'), array('controller' => 'functions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Function'), array('controller' => 'functions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Companies'), array('controller' => 'companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Company'), array('controller' => 'companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Users'); ?></h3>
	<?php if (!empty($project['User'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Firstname'); ?></th>
		<th><?php echo __('Lastname'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('Formation Completed'); ?></th>
		<th><?php echo __('Photo'); ?></th>
		<th><?php echo __('Photo Dir'); ?></th>
		<th><?php echo __('Notification Date'); ?></th>
		<th><?php echo __('Notification Msg'); ?></th>
		<th><?php echo __('Notification News'); ?></th>
		<th><?php echo __('Notification Task'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Code Deactivation'); ?></th>
		<th><?php echo __('Alias Email'); ?></th>
		<th><?php echo __('Birthdate'); ?></th>
		<th><?php echo __('Formation'); ?></th>
		<th><?php echo __('Gender'); ?></th>
		<th><?php echo __('Courses'); ?></th>
		<th><?php echo __('Idioms'); ?></th>
		<th><?php echo __('Other Interests'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th><?php echo __('Company Id'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Function Id'); ?></th>
		<th><?php echo __('Sector Id'); ?></th>
		<th><?php echo __('Department Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($project['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['created']; ?></td>
			<td><?php echo $user['modified']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['firstname']; ?></td>
			<td><?php echo $user['lastname']; ?></td>
			<td><?php echo $user['email']; ?></td>
			<td><?php echo $user['phone']; ?></td>
			<td><?php echo $user['password']; ?></td>
			<td><?php echo $user['address']; ?></td>
			<td><?php echo $user['formation_completed']; ?></td>
			<td><?php echo $user['photo']; ?></td>
			<td><?php echo $user['photo_dir']; ?></td>
			<td><?php echo $user['notification_date']; ?></td>
			<td><?php echo $user['notification_msg']; ?></td>
			<td><?php echo $user['notification_news']; ?></td>
			<td><?php echo $user['notification_task']; ?></td>
			<td><?php echo $user['code']; ?></td>
			<td><?php echo $user['code_deactivation']; ?></td>
			<td><?php echo $user['alias_email']; ?></td>
			<td><?php echo $user['birthdate']; ?></td>
			<td><?php echo $user['formation']; ?></td>
			<td><?php echo $user['gender']; ?></td>
			<td><?php echo $user['courses']; ?></td>
			<td><?php echo $user['idioms']; ?></td>
			<td><?php echo $user['other_interests']; ?></td>
			<td><?php echo $user['country_id']; ?></td>
			<td><?php echo $user['group_id']; ?></td>
			<td><?php echo $user['company_id']; ?></td>
			<td><?php echo $user['city_id']; ?></td>
			<td><?php echo $user['function_id']; ?></td>
			<td><?php echo $user['sector_id']; ?></td>
			<td><?php echo $user['department_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), array(), __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
