<?php
/**
 * User: Eloi Gallés Villaplana
 * Date: 30/04/14
 * Time: 01:49
 * @file panel.ctp
 */
echo $this->Html->css(array('glyphicons', 'estilo'));
?>
<div class="container">
    <div class="row">
        <div id="content" style="min-height:400px" class="col-lg-12 col-md-12 col-sm-12">
            <?php echo $this->Session->flash(); ?>
            <?= $this->fetch('content');?>
        </div>
    </div>
</div>

<?= $this->fetch('modal'); ?>

<div class="clearfix"></div>
