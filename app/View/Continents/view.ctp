<div class="continents view">
<h2><?php echo __('Continent'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($continent['Continent']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($continent['Continent']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($continent['Continent']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($continent['Continent']['name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Continent'), array('action' => 'edit', $continent['Continent']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Continent'), array('action' => 'delete', $continent['Continent']['id']), array(), __('Are you sure you want to delete # %s?', $continent['Continent']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Continents'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Continent'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Countries'); ?></h3>
	<?php if (!empty($continent['Country'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Code 2'); ?></th>
		<th><?php echo __('Latitude'); ?></th>
		<th><?php echo __('Altitude'); ?></th>
		<th><?php echo __('Continent Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($continent['Country'] as $country): ?>
		<tr>
			<td><?php echo $country['id']; ?></td>
			<td><?php echo $country['created']; ?></td>
			<td><?php echo $country['modified']; ?></td>
			<td><?php echo $country['name']; ?></td>
			<td><?php echo $country['code']; ?></td>
			<td><?php echo $country['code_2']; ?></td>
			<td><?php echo $country['latitude']; ?></td>
			<td><?php echo $country['altitude']; ?></td>
			<td><?php echo $country['continent_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'countries', 'action' => 'view', $country['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'countries', 'action' => 'edit', $country['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'countries', 'action' => 'delete', $country['id']), array(), __('Are you sure you want to delete # %s?', $country['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
