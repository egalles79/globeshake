<?php
/**
 * User: Eloi Gallés Villaplana
 * Date: 30/04/14
 * Time: 00:06
 * @file default.ctp
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?= $this->Html->charset(); ?>
  <title><?= $title_for_layout;?></title>

  <?php
  echo $this->Html->meta('icon');
  echo $this->fetch('meta');
  ?>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <?= $this->Html->css(array('bootstrap.min', 'style.min', 'retina.min')); ?>
  <?= $this->Html->css('print', 'stylesheet', array('media' => 'print')); ?>
  <?= $this->Html->css('estilo'); ?>
  <?= $this->fetch('css');?>
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.css" />
	<script src="http://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>	
	<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.js'></script>
  	<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.9/mapbox.css' rel='stylesheet' />
</head>
<body>
<?= $this->fetch('content');?>

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <?= $this->Html->script(array('respond.min')); ?>
<![endif]-->
<!--[if !IE]>-->
    <?= $this->Html->script(array('jquery-2.1.0.min')); ?>
<!--<![endif]-->

<!--[if IE]>
    <?= $this->Html->script(array('jquery-1.11.0.min')); ?>
<![endif]-->
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

<?= $this->Html->script(array('jquery-2.1.0.min','jquery-migrate-1.2.1.min', 'bootstrap.min','i18n_js/js/i18n_js')); ?>
<?= $this->fetch('script');?>

</body>
</html>