<?php
/**
 * User: Eloi Gallés Villaplana
 * Date: 30/04/14
 * Time: 00:06
 * @file default.ctp
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?= $this->Html->charset(); ?>
  <title><?= $title_for_layout;?></title>

  <?php
  echo $this->Html->meta('icon');
  echo $this->fetch('meta');
  ?>

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <?= $this->Html->css(array('bootstrap.min','retina.min'));  //  'style.min', ?>
  <?= $this->Html->css('icons'); ?>
  <?= $this->Html->css('estilo'); ?>
  <?= $this->fetch('css');?>
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.css" />
	<!--
  añadidoesto
  -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript" src="http://www.globeshake.com/test/js/jquery.flexslider-min.js"></script>
  <link rel="stylesheet" href="http://www.globeshake.com/test/js/rs-plugin/css/settings.css">
  
  <!--
  fin añadido
  -->
</head>
<body>

<?= $this->fetch('content');?>

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <?= $this->Html->script(array('respond.min')); ?>
<![endif]-->
<!--[if !IE]>-->
    <?= $this->Html->script(array('jquery-2.1.0.min')); ?>
<!--<![endif]-->

<!--[if IE]>
    <?= $this->Html->script(array('jquery-1.11.0.min')); ?>
<![endif]-->

<?= $this->Html->script(array('jquery-migrate-1.2.1.min', 'bootstrap.min','i18n_js/js/i18n_js')); ?>
<?= $this->fetch('script');?>

</body>

</html>