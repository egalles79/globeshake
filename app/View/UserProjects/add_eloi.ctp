<?php

$this->extend('/Common/panel');
echo $this->Html->script(array(
    'jquery-ui-1.10.3.custom.min',
    'jquery.sparkline.min',
    'jquery.chosen.min',
    'jquery.autosize.min',
    'jquery.placeholder.min',
    'daterangepicker.min',
    'moment.min',
    'jquery.steps.min',
    'wizard.min',
    'custom.min',
    'bootbox.min',
    'core.min',
    'jquery.tooltipster.min'
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){

  var departments   = [ <?php echo $departmentsText;?> ];
  var functionalities = [ <?php echo $functionalitiesText;?> ];
  var companies = [ <?php echo $companiesText;?> ];
  var sectors = [ <?php echo $sectorProjectText;?> ];
  var cities = [ <?php echo $citiesText;?> ];
    var accentMap = {
      "á": "a",
      "ö": "o",
      "é": "e",
      "í": "i",
    };
    var normalize = function( term ) {
      var ret = "";
      for ( var i = 0; i < term.length; i++ ) {
        ret += accentMap[ term.charAt(i) ] || term.charAt(i);
      }
      return ret;
    };
 
    $( "#UserProjectDepartmen" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( departments, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectFunctionalit" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( functionalities, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectSectorServic" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectSectorProj" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
     $( "#UserProjectCit" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( cities, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectCustome" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( companies, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#UserProjectCompanyName" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( companies, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    /*$( "#lineaNegocio" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#lineaNegocio_personal" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( sectors, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });
    $( "#ciudad" ).autocomplete({
      source: function( request, response ) {
        var matcher = new RegExp( $.ui.autocomplete.escapeRegex( request.term ), "i" );
        response( $.grep( cities, function( value ) {
          value = value.label || value.value || value;
          return matcher.test( value ) || matcher.test( normalize( value ) );
        }) );
      }
    });*/
  $( ".datepicker" ).datepicker({
    dateFormat: "mm/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: '1925:2015',
    showButtonPanel: true,
    onClose: function(dateText, inst) {


        function isDonePressed(){
            return ($('#ui-datepicker-div').html().indexOf('ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all ui-state-hover') > -1);
        }

        if (isDonePressed()){
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1)).trigger('change');
            
             $('.date-picker').focusout()//Added to remove focus from datepicker input box on selecting date
        }
    },
    beforeShow : function(input, inst) {

        inst.dpDiv.addClass('month_year_datepicker')

        if ((datestr = $(this).val()).length > 0) {
            year = datestr.substring(datestr.length-4, datestr.length);
            month = datestr.substring(0, 2);
            $(this).datepicker('option', 'defaultDate', new Date(year, month-1, 1));
            $(this).datepicker('setDate', new Date(year, month-1, 1));
            $(".ui-datepicker-calendar").hide();
        }
    }

  });

  $.datepicker.regional['es'] = {
      closeText: I18nJs.t('Escoger fecha'),
      prevText: '< Anterior',
      nextText: 'Siguiente >',
      currentText: 'Hoy',
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
      dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
      weekHeader: 'Sm',
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''
  };
  $.datepicker.setDefaults($.datepicker.regional['es']);

  /* ---------- FuelUX Wizard ---------- */
    var wizard = $('#MyWizard');

    wizard.on('finished', function(e, data) {
        $('#UserProjectAddForm').submit();
    });
    function volverAtras() {
        $('#but1').click();
    }
    $('#but1').click(function(){
        $('#butant').click();
        if ($('#step1').hasClass('active')) {
            $('#but1').removeClass('btn-success');
        }
    });
    $('#but2').click(function(){
        $('#butnext').click();
    });
    $('.btn-next').on('click', function() {
        //logica de los errores paso a paso

        if ($('#step2').hasClass('active')) {
            google.maps.event.trigger(map, 'resize');
            $('#but1').addClass('btn-success');
            /*if (($('#UserProjectInitDate').val() == '') || ($('#UserProjectEnDate').val() == '')) {
                bootbox.alert(I18nJs.t('Debe seleccionar una fecha incio/fin de proyecto para continuar '),volverAtras);
            } */
        }
        if ($('#step3').hasClass('active')) {
            if ($('#GamificationName').val() == '') {
                bootbox.alert(I18nJs.t('El nombre de la aplicación no puede estar vacío'),volverAtras);
            }
        }
    });
    wizard.on('change', function(e, data) {
      
    });
    wizard.wizard();

  /* ---------- Datapicker ---------- */
  $('.datepicker').datepicker();

  /* ---------- Choosen ---------- */
  $('[data-rel="chosen"],[rel="chosen"]').chosen();

  /* ---------- Placeholder Fix for IE ---------- */
  $('input, textarea').placeholder();

  /* ---------- Auto Height texarea ---------- */
  $('textarea').autosize();   
});
var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 1,
    center: new google.maps.LatLng(35.137879, -82.836914),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng(47.651968, 9.478485),
    draggable: true
});

var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+evt.latLng.lat().toFixed(3)+","+evt.latLng.lng().toFixed(3)+"&sensor=true";
    locc = evt.latLng;
    map.setCenter(locc);
    
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(locc);
    latitudeLongitude = JSON.stringify(locc);
    latitudeLongitude = latitudeLongitude.split(',');
    latitude = latitudeLongitude[0].substring(5);
    longitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
    $('#UserProjectLongitude').val(longitude);
    $('#UserProjectLatitude').val(latitude);

    map.fitBounds(bounds);
    map.setZoom(20);
    $.ajax({
      url: url,
    }).success(function(result) {
      if (result['status'] == 'OK') {
        if (typeof result != 'undefined') {
          $('#pac-input').val(result['results'][0]['formatted_address']);
        } 
      }
    });
});

google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
    $('#pac-input').val('Asignando punto');
});
google.maps.event.addListener(map, 'bounds_changed', function() {
      var bounds = map.getBounds();
      searchBox.setBounds(bounds);
    });
map.setCenter(myMarker.position);
myMarker.setMap(map);
markers = [];
google.maps.event.addListener(searchBox, 'places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }
      for (var i = 0, marker; marker = markers[i]; i++) {
        marker.setMap(null);
      }

      // For each place, get the icon, place name, and location.
      markers = [];
      var bounds = new google.maps.LatLngBounds();
      var myPlaces = [];
      for (var i = 0, place; place = places[i]; i++) {
        var image = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        myMarker.setPosition(place.geometry.location);

        map.setCenter(place.geometry.location);
        //markers.push(marker);

        bounds.extend(place.geometry.location);
        if (place.types[0] == 'street_address') {
          latitudeLongitude = JSON.stringify(place.geometry.location);
          latitudeLongitude = latitudeLongitude.split(',');
          latitude = latitudeLongitude[0].substring(5);
          longitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
          $('#UserProjectLongitude').val(longitude);
          $('#UserProjectLatitude').val(latitude);
        } else {
          $('#UserProjectLongitude').val('');
          $('#UserProjectLatitude').val('');
        }
        myPlaces.push(place.types[0]);
      }
      map.fitBounds(bounds);
      var listener = google.maps.event.addListener(map, "idle", function() { 
        val = map.getZoom();
        switch (myPlaces[0]) {
          case 'continent' :
            val = 4;
            break;
          case 'country' :
            val = 6;
            break;
          case 'administrative_area_level_1':
          case 'locality' :
            val = 13;
            break;
          case 'lodging' :
          case 'route' :
            val = 17;
            break;
          case 'neighborhood' :
            val = 16;
            break;
        }
        map.setZoom(val);
        google.maps.event.removeListener(listener); 
      });
});

<?php $this->Html->scriptEnd();
?>

<div class="box">
    <div class="box-header">
      <h2><i class="fa fa-edit"></i><?php echo __('Añadir proyecto');?></h2>
    </div>
    <div class="box-content">
        <div id="MyWizard" class="wizard">
          <ul class="steps">
              <li data-target="#step1" class="active"><span class="badge badge-info">1</span><span class="chevron"></span><?= __('Información general del proyecto') ?></li>
              <li data-target="#step2"><span class="badge">2</span><span class="chevron"></span><?= __('Localización') ?></li>
              <li data-target="#step3"><span class="badge ">3</span><span class="chevron"></span><?= __('Tu contribución') ?></li>
              <li data-target="#step4"><span class="badge ">4</span><span class="chevron"></span><?= __('Descripción') ?></li>
              <li data-target="#step5"><span class="badge ">5</span><span class="chevron"></span><?= __('Visualización') ?></li>
          </ul>
          <div class="actions">
              <button type="button" id="butant" class="btn btn-prev hidden"> <i class="fa fa-arrow-left"></i><?php echo __('Ant').'.'?></button>
              <button type="button" id="butnext" class="btn btn-success btn-next hidden" data-last="<?php echo __('Finalizar');?>"><?php echo __('Sig').'.'?><i class="fa fa-arrow-right"></i></button>
          </div>
      </div>

      <div class="step-content">

        <?= $this->Form->create('UserProject', array('action' => 'add', 'class' => 'form-horizontal rellenar-campos', 'type' => 'file')); ?>

          <div class="step-pane active" id="step1">

              <div class="row">
                  <div class="col-sm-offset-2 col-sm-8 col-m-offset-2">
                    <p><?php echo __('El proyecto pertenece a la compañía actual');?></p>
                    <?php
                      echo $this->Form->input('actual_project', array(
                      'type' => 'radio',
                        'separator' => '<br />',
                        'before' => '',
                        'after' => '',
                        'legend' => false,
                        'options' => array('S' => __('Si'), 'N' => __('No'))
                    ));
                    echo $this->Form->input('companyName',array('label' => __('Nombre de la empresa:').'&nbsp;&nbsp;','class' => 'ui-autocomplete-input ok_input span8'));
                    echo $this->Form->input('name',array('label' => __('Nombre del proyecto:').'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','class' => 'ui-autocomplete-input ok_input span8'));
                    echo $this->Form->input('UserProject[init_date]',array('placeholder' => 'mm/yyyy','class'=>'datepicker','label' => __('Desde* :')));
                    echo $this->Form->input('UserProject[end_date]',array('placeholder' => 'mm/yyyy','class'=>'datepicker','label' => __('Hasta* :')));
                    echo $this->Form->input('on_going',array('type' => 'checkbox','label' => __('Proyecto en curso')));
                    echo $this->Form->input('custome',array('label' => __('Nombre del cliente:').'&nbsp;&nbsp;&nbsp;','class' => 'ui-autocomplete-input ok_input span8'));
                    echo $this->Form->input('sector_proj',array('label' => __('Sector del proyecto: ').'&nbsp;&nbsp;','class' => 'ui-autocomplete-input span8','style' => 'height: 24px!important;'));
                    echo $this->Form->input('budget',array('label' => __('Presupuesto del proyecto*:').'&nbsp;&nbsp;','div' => false));
                    echo $this->Form->input('budget_currency_id',array('label' => false,'div' => false,'style' => 'width:100px'));
                    // echo $this->Form->input('billing');
                    // echo $this->Form->input('billing_currency_id');
                    echo $this->Form->input('volumes');
                    ?>
                  </div>
              </div>
          </div>

          <div class="step-pane" id="step2">

              <div class="form-group add-event">
                  <div class="row">
                      <input id="pac-input" class="controls" type="text" placeholder="<?php echo __('Busca la ubicación de tu proyecto');?>">
                      <div class="col-sm-12 col-md-12 col-xs-12">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <?php 
                          echo $this->Form->input('country_id');
                          echo $this->Form->input('cit',array('class' => 'ui-autocomplete-input ok_input span8'));
                          echo $this->Form->input('longitude',array('disabled' =>'disabled'));
                          echo $this->Form->input('latitude',array('disabled' => 'disabled'));
                          ?>
                        </div>
                        <div id="map-canvas" class="col-md-6"></div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="step-pane" id="step3">
              <div class="row">
                  <div class="col-md-12">
                    <?php
                      echo $this->Form->input('functionalit',array('class' => 'ui-autocomplete-input ok_input span8'));
                    echo $this->Form->input('sector_servic',array('class' => 'ui-autocomplete-input ok_input span8'));
                    echo $this->Form->input('departmen',array('class' => 'ui-autocomplete-input ok_input span8'));
                    
                    echo $this->Form->input('duration_year');
                    echo $this->Form->input('duration_month');
                    echo $this->Form->input('duration_day');
                    echo $this->Form->input('duration_hours');
                    ?>
                  </div>
              </div>
          </div>
          <div class="step-pane" id="step4">
              <div class="row">
                  <div class="col-md-12">
                      <?php 
                      echo $this->Form->input('tags');
                    echo $this->Form->input('description');
                    ?>
                  </div>
              </div>
          </div>
          <div class="step-pane" id="step5">
              <div class="row">
                  <div class="col-md-12">
                      <?php
                        echo $this->Form->input('who_can_see');
                        echo $this->Form->input('where_to_see');
                      ?>
                  </div>
              </div>
          </div>
  
          <div class="actions">
              <button type="button" id="but1" class="btn btn-prev"> <i class="fa fa-arrow-left"></i><?php echo __('Ant').'.'?></button>
              <button type="button" id="but2" class="btn btn-success btn-next pull-right" data-last="<?php echo __('Finalizar');?>"><?php echo __('Sig').'.'?><i class="fa fa-arrow-right"></i></button>
          </div>

        <?= $this->Form->end(); ?>

      </div>
    </div>
</div>

<div class="userProjects form">

<?php echo $this->Form->create('UserProject'); ?>
  
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<style>
#map-canvas {
  height: 500px;
  width: 50%;
  margin: 0px;
}
#pac-input {
  background-color: #fff;
  font-family: Roboto;
  font-size: 15px;
  font-weight: 300;
  margin-left: 12px;
  padding: 0 11px 0 13px;
  text-overflow: ellipsis;
  width: 400px;
}

#pac-input:focus {
  border-color: #4d90fe;
}

.ui-datepicker-calendar {
    display: none;
    }

.pac-container {
  font-family: Roboto;
}
</style>