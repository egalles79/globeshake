<div class="userProjects index">
	<h2><?php echo __('User Projects'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('start_date'); ?></th>
			<th><?php echo $this->Paginator->sort('end_date'); ?></th>
			<th><?php echo $this->Paginator->sort('on_going'); ?></th>
			<th><?php echo $this->Paginator->sort('budget'); ?></th>
			<th><?php echo $this->Paginator->sort('budget_currency_id'); ?></th>
			<th><?php echo $this->Paginator->sort('billing'); ?></th>
			<th><?php echo $this->Paginator->sort('billing_currency_id'); ?></th>
			<th><?php echo $this->Paginator->sort('volumes'); ?></th>
			<th><?php echo $this->Paginator->sort('tags'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th><?php echo $this->Paginator->sort('geolocalitzation_id'); ?></th>
			<th><?php echo $this->Paginator->sort('country_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sector_project_id'); ?></th>
			<th><?php echo $this->Paginator->sort('city_id'); ?></th>
			<th><?php echo $this->Paginator->sort('functionality_id'); ?></th>
			<th><?php echo $this->Paginator->sort('sector_service_id'); ?></th>
			<th><?php echo $this->Paginator->sort('department_id'); ?></th>
			<th><?php echo $this->Paginator->sort('company_id'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_id'); ?></th>
			<th><?php echo $this->Paginator->sort('duration_year'); ?></th>
			<th><?php echo $this->Paginator->sort('duration_month'); ?></th>
			<th><?php echo $this->Paginator->sort('duration_day'); ?></th>
			<th><?php echo $this->Paginator->sort('duration_hours'); ?></th>
			<th><?php echo $this->Paginator->sort('who_can_see'); ?></th>
			<th><?php echo $this->Paginator->sort('where_to_see'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($userProjects as $userProject): ?>
	<tr>
		<td><?php echo h($userProject['UserProject']['id']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['created']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['modified']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['name']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['end_date']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['on_going']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['budget']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($userProject['BudgetCurrency']['name'], array('controller' => 'budget_currencies', 'action' => 'view', $userProject['BudgetCurrency']['id'])); ?>
		</td>
		<td><?php echo h($userProject['UserProject']['billing']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($userProject['BillingCurrency']['name'], array('controller' => 'billing_currencies', 'action' => 'view', $userProject['BillingCurrency']['id'])); ?>
		</td>
		<td><?php echo h($userProject['UserProject']['volumes']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['tags']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['description']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($userProject['Geolocalitzation']['id'], array('controller' => 'geolocalitzations', 'action' => 'view', $userProject['Geolocalitzation']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userProject['Country']['name'], array('controller' => 'countries', 'action' => 'view', $userProject['Country']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userProject['SectorProject']['name'], array('controller' => 'sector_projects', 'action' => 'view', $userProject['SectorProject']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userProject['City']['name'], array('controller' => 'cities', 'action' => 'view', $userProject['City']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userProject['Functionality']['name'], array('controller' => 'functionalities', 'action' => 'view', $userProject['Functionality']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userProject['SectorService']['name'], array('controller' => 'sector_services', 'action' => 'view', $userProject['SectorService']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userProject['Department']['name'], array('controller' => 'departments', 'action' => 'view', $userProject['Department']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userProject['Company']['name'], array('controller' => 'companies', 'action' => 'view', $userProject['Company']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userProject['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $userProject['Customer']['id'])); ?>
		</td>
		<td><?php echo h($userProject['UserProject']['duration_year']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['duration_month']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['duration_day']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['duration_hours']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['who_can_see']); ?>&nbsp;</td>
		<td><?php echo h($userProject['UserProject']['where_to_see']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $userProject['UserProject']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userProject['UserProject']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $userProject['UserProject']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userProject['UserProject']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New User Project'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Budget Currencies'), array('controller' => 'budget_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Budget Currency'), array('controller' => 'budget_currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Billing Currencies'), array('controller' => 'billing_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing Currency'), array('controller' => 'billing_currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Geolocalitzations'), array('controller' => 'geolocalitzations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Geolocalitzation'), array('controller' => 'geolocalitzations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sector Projects'), array('controller' => 'sector_projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sector Project'), array('controller' => 'sector_projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Functionalities'), array('controller' => 'functionalities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Functionality'), array('controller' => 'functionalities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sector Services'), array('controller' => 'sector_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sector Service'), array('controller' => 'sector_services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departments'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Department'), array('controller' => 'departments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Companies'), array('controller' => 'companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Company'), array('controller' => 'companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>
