<div class="userProjects view">
<h2><?php echo __('User Project'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Start Date'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['start_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('End Date'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['end_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('On Going'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['on_going']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['budget']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Budget Currency'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['BudgetCurrency']['name'], array('controller' => 'budget_currencies', 'action' => 'view', $userProject['BudgetCurrency']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['billing']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Billing Currency'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['BillingCurrency']['name'], array('controller' => 'billing_currencies', 'action' => 'view', $userProject['BillingCurrency']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Volumes'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['volumes']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tags'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['tags']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Geolocalitzation'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['Geolocalitzation']['id'], array('controller' => 'geolocalitzations', 'action' => 'view', $userProject['Geolocalitzation']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Country'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['Country']['name'], array('controller' => 'countries', 'action' => 'view', $userProject['Country']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sector Project'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['SectorProject']['name'], array('controller' => 'sector_projects', 'action' => 'view', $userProject['SectorProject']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['City']['name'], array('controller' => 'cities', 'action' => 'view', $userProject['City']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Functionality'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['Functionality']['name'], array('controller' => 'functionalities', 'action' => 'view', $userProject['Functionality']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sector Service'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['SectorService']['name'], array('controller' => 'sector_services', 'action' => 'view', $userProject['SectorService']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Department'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['Department']['name'], array('controller' => 'departments', 'action' => 'view', $userProject['Department']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Company'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['Company']['name'], array('controller' => 'companies', 'action' => 'view', $userProject['Company']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userProject['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $userProject['Customer']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Year'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['duration_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Month'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['duration_month']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Day'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['duration_day']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Duration Hours'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['duration_hours']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Who Can See'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['who_can_see']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Where To See'); ?></dt>
		<dd>
			<?php echo h($userProject['UserProject']['where_to_see']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User Project'), array('action' => 'edit', $userProject['UserProject']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User Project'), array('action' => 'delete', $userProject['UserProject']['id']), array(), __('Are you sure you want to delete # %s?', $userProject['UserProject']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List User Projects'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Project'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Budget Currencies'), array('controller' => 'budget_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Budget Currency'), array('controller' => 'budget_currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Billing Currencies'), array('controller' => 'billing_currencies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing Currency'), array('controller' => 'billing_currencies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Geolocalitzations'), array('controller' => 'geolocalitzations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Geolocalitzation'), array('controller' => 'geolocalitzations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sector Projects'), array('controller' => 'sector_projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sector Project'), array('controller' => 'sector_projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Functionalities'), array('controller' => 'functionalities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Functionality'), array('controller' => 'functionalities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sector Services'), array('controller' => 'sector_services', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sector Service'), array('controller' => 'sector_services', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Departments'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Department'), array('controller' => 'departments', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Companies'), array('controller' => 'companies', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Company'), array('controller' => 'companies', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customers'), array('controller' => 'customers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer'), array('controller' => 'customers', 'action' => 'add')); ?> </li>
	</ul>
</div>
