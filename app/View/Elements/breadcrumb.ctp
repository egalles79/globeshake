<?php
/**
 * User: Eloi Gallés Villaplana
 * Date: 30/04/14
 * Time: 18:50
 * @file breadcrumb.ctp
 */

if(!isset($breadcrumb)){
    $cName = Inflector::humanize($this->request->params['controller']);
    $aName = Inflector::humanize($this->request->params['action']);
    $breadcrumb = array(
        __($cName) => array(
                'url' => '/'.$this->request->params['controller'],
                'active' => false
            ),
        __($aName) => array(
                'url' => '/'.$this->request->params['controller'].'/'.$this->request->params['action'],
                'active' => true
            )
        );
}
?>
<!-- Breadcrumb -->
<div class="row">
  <div class="col-sm-12 col-md-12">
    <ol class="breadcrumb">
      <li><a href="/Companies/cvmap"><?php echo __('Inicio')?></a></li>
      <?php
      foreach($breadcrumb as $name => $crumb){
        $active = $crumb['active']?'active':'';
        $url = $crumb['url'];
        echo "<li class='$active'><a href='$url'>$name</a></li>";
      }
      ?>
    </ol>
  </div>
</div>
<!-- Breadcrumb-->