<?php
/**
 * User: eloi Gallés villaplana
 * Date: 31/05/13
 * Time: 23:22
 * @file flash_message.ctp
 */
?>
<div class="alert alert-<?php echo $type; ?>" style="z-index: 25;position: absolute;width: 90%;" id="flashMessage">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <?php echo $message; ?>
</div>