<!-- TU CONTRIBUCIÓN -->
<div class="box">
    <div class="box-header" style="background-color:#cdcdcd">
		<h2 class="title text-center" style="color:#fff">
			<i style="color:#7a7a7a" class="fa fa-user"></i>
			<a href="#" class="btn-minimize target" style="text-decoration:none;"><?php echo __('TU CONTRIBUCIÓN');?></a></h2>
			<div class="box-icon">
              <a href="#" class="btn-minimize target"><i class="fa fa-chevron-up" style="color:#47789f"></i></a>
            </div>
    </div>
	<div class="box-content">
		<div class="row row_round3">
			<div class="col-md-12">
	            <?php
	            $sectorServiceString = '';
	            $functionalitiesString = '';
	            $departmentNameString = '';
	            foreach($sectorServices as $sectorService) {
	            	$sectorServiceString .= $sectorNames[$sectorService].'<br />';
	            }
	            foreach($departmentNames as $departmentName) {
	            	$departmentNameString .= $departmentName.'<br />';
	            }
	            foreach($functionalitiesNames as $functionalitiesName) {
	            	$functionalitiesString .= $functionalitiesName.'<br />';
	            }
	            ?>
	            <table class="table table-responsive">
	              <thead></thead>
	              <tbody>
	                <tr>
	                  <td class="col-md-2">
	                    <div class="text-right colour_grey" >
	                      <?php echo __('Sectores de sus servicios').':';?>
	                    </div>
	                  </td>
	                  <td class="col-md-9">
	                    <div class="text-left resumen"><?php echo $sectorServiceString;?>
	                  </td>
	                  <td class="col-md-1">
	                    &nbsp;
	                  </td>
	                </tr>
	                <tr>
	                  <td>
	                    <div class="text-right colour_grey">
	                      <?php echo __('Funciones durante sus servicios').':';?>
	                    </div>
	                  </td>
	                  <td>
	                    <div class="text-left resumen"><?php echo $functionalitiesString;?>
	                  </td>
	                  <td>
	                    &nbsp;
	                  </td>
	                </tr>
	                <tr>
	                  <td class="col-md-2">
	                    <div class="text-right colour_grey">
	                      <?php echo __('Subfunciones').':';?>
	                    </div>
	                  </td>
	                  <td class="col-md-9">
	                    <div class="text-left resumen"><?php echo $departmentNameString;?>
	                  </td>
	                  <td class="col-md-1">
	                    &nbsp;
	                  </td>
	                </tr>
	                <tr>
	                  <td class="col-md-2">
	                    <div class="text-right colour_grey">
	                      <?php echo __('Tiempo acumulado de experiencia').':';?>
	                    </div>
	                  </td>
	                  <td class="col-md-9">
	                    <div class="text-left resumen" ><?php echo $duration;?>
	                  </td>
	                  
	                  <td class="col-md-1">
	                    &nbsp;
	                  </td>
	                </tr>
	              </tbody>
	            </table>
			</div>
		</div>
	</div>
</div>