<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */
echo $this->Html->script(array(
  'jquery-1.11.3.min'
), array('inline' => false));

if (!Configure::read('debug')):
  throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>

<input id="pac-input" class="controls" type="text" placeholder="Search Box">
<div id="map-canvas" class="col-md-6"></div>

</div>
       
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#window_area .login').click(function(){
    if($(this).hasClass('active')) $('.header_window').hide();
    else $('.header_window').show(100);
    $(this).toggleClass('active');
  });

var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 1,
    center: new google.maps.LatLng(35.137879, -82.836914),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng(47.651968, 9.478485),
    draggable: true
});

var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+evt.latLng.lat().toFixed(3)+","+evt.latLng.lng().toFixed(3)+"&sensor=true";
    locc = evt.latLng;
    map.setCenter(locc);
    console.log(locc);
    $.ajax({
      url: url,
    }).success(function(result) {
      if (result['status'] == 'OK') {
        if (typeof result != 'undefined') {
          $('#pac-input').val(result['results'][0]['formatted_address']);
        }
      }
    });
});

google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
    $('#pac-input').val('Asignando punto');
});
google.maps.event.addListener(map, 'bounds_changed', function() {
      var bounds = map.getBounds();
      searchBox.setBounds(bounds);
    });
map.setCenter(myMarker.position);
myMarker.setMap(map);
markers = [];
google.maps.event.addListener(searchBox, 'places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }
      for (var i = 0, marker; marker = markers[i]; i++) {
        marker.setMap(null);
      }

      // For each place, get the icon, place name, and location.
      markers = [];
      var bounds = new google.maps.LatLngBounds();
      var myPlaces = [];
      for (var i = 0, place; place = places[i]; i++) {
        var image = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        console.log(place);
        myMarker.setPosition(place.geometry.location);

        map.setCenter(place.geometry.location);
        //markers.push(marker);

        bounds.extend(place.geometry.location);
        console.log(place);
        myPlaces.push(place.types[0]);
      }

      map.fitBounds(bounds);
      var listener = google.maps.event.addListener(map, "idle", function() { 
        console.log(myPlaces[0]);
        val = map.getZoom();
        switch (myPlaces[0]) {
          case 'continent' :
            val = 4;
            break;
          case 'country' :
            val = 6;
            break;
          case 'administrative_area_level_1':
          case 'locality' :
            val = 13;
            break;
          case 'lodging' :
          case 'route' :
            val = 17;
            break;
          case 'neighborhood' :
            val = 16;
            break;
        }
        map.setZoom(val);
        google.maps.event.removeListener(listener); 
      });
    });
});
<?php $this->Html->scriptEnd();
?>

