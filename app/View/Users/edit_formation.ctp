<?php

$this->extend('/Common/panel');
echo $this->Html->css('chosen.min');
echo $this->Html->script(array(
'jquery-ui-1.10.3.custom.min',
'jquery.mockjax.min',
'bootstrap-editable.min',
'moment.min',
'address.min',
'select2.min',
'custom.min',
'daterangepicker.min',
'chosen.jquery.min',
'core.min',
'bootbox.min'
), array('inline' => false));



$this->Html->scriptStart(array('inline' => false));

?>
$(document).ready(function(){
  $( "#datepicker" ).datepicker({
    changeMonth: true,
    changeYear: true,
    defaultDate: "-40y",
    maxDate: "-15y",
    dateFormat: "dd/mm/yy",
    yearRange: '1925:2014',
  });
  $.datepicker.regional['es'] = {
      closeText: 'Cerrar',
      prevText: '< Anterior',
      nextText: 'Siguiente >',
      currentText: 'Hoy',
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
      dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
      weekHeader: 'Sm',
      dateFormat: 'dd/mm/yy',
      firstDay: 1,
      isRTL: false,
      showMonthAfterYear: false,
      yearSuffix: ''
  };
  $.datepicker.setDefaults($.datepicker.regional['es']);
  $(".chosen-select").chosen();
});
<?php 
$this->Html->scriptEnd();
?><!-- start: Content -->
<div class="box">
  <div class="box-header">
    <h2><i class="fa fa-edit"></i><?php echo __('Editar Formación');?></h2>
     <div class="box-icon">
    </div>
  </div>
  <?php echo $this->Form->create('User',array('class' =>'edit_formacion'));?>
  <div class="box-content"><br><br>	
    <div class="row borde_max" style="padding-bottom: 30px;">
      <div class="col-md-12">
	      <h2 class="title text-center" style="font-size: 20px;"><?php echo __('Editar Formación');?><div class="back_his" style="left: 38%;">
                  <a href="javascript:history.back()"><?php echo $this->Html->image('/img/flecha_volver.png');?>VOLVER</a>
                </div></h2><hr>
        <div class="col-md-1 col-xs-1 col-sm-1">&nbsp;</div>
        <div class="col-md-10 col-xs-10 col-sm-10">
          <div class="col-xs-2 col-sm-2 col-md-2 text-right" style="color:#47789f;padding:10px">
            <?php echo __('Fecha nacimiento');?>*:
          </div>
          <div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;border-radius: 10px 10px 0 0;">
             <?php
                  if ($user['User']['birthdate'] != null) {
                    $datemine = new DateTime($user['User']['birthdate']);
                    $dateString = $datemine->format('d/m/Y'); 
                  } else {
                    $dateString ='';
                  }
                  ?>
                  <input type="text" name="User[birthdate]" id="datepicker" placeholder="dd/mm/yyyy" value="<?php echo $dateString ?>">
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 text-right" style="color:#47789f;padding:10px;clear:both">
            <?php echo __('Género').':';?>
          </div>
          <div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
            <input type="radio" <?php  if (($user['User']['gender'] == 0) || (empty($user['User']['gender']))) echo 'checked';?> name="User[gender]" value="0" style="margin-right:6px;"><?php echo __('Hombre');?>
                  <input type="radio"  <?php if ($user['User']['gender'] == 1) echo 'checked';?>  name="User[gender]" value="1" style="margin-right:6px;"><?php echo __('Mujer');?>
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 text-right" style="color:#47789f;padding:10px">
            <?php echo __('Formación').':';?>
          </div>
          <div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
            <?php echo $this->Form->textarea('formation',array('id'=>'area1','style'=>'width:450px;'));?>
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 text-right" style="color:#47789f;padding:10px">
            <?php echo __('Idiomas').':';?>
          </div>
          <div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
            <select class="chosen-select span8" name="User[idioms][]" tabindex="8" multiple="" data-placeholder="<?php echo __('Seleccione idiomas');?>">
              <option value=""></option>
                <?php 
                
                $idiomas_selected = json_decode($user['User']['idioms']);
                if (empty($idiomas_selected)) {
                  foreach ($languageName as $langId => $lang) {
                    echo '<option value='.$langId.'>'.$lang.'</option>';
                  }
                } else {
                  foreach ($languageName as $langId => $lang) {
                    if (is_array($idiomas_selected)) {
                      if (in_array($langId, $idiomas_selected)) {
                        echo '<option value='.$langId.' selected>'.$lang.'</option>';
                      } else {
                        echo '<option value='.$langId.'>'.$lang.'</option>';
                      }
                    } else {
                      if ($lang == $idiomas_selected) {
                        echo '<option value='.$langId.' selected>'.$lang.'</option>';
                      } else {
                        echo '<option value='.$langId.'>'.$lang.'</option>';
                      }
                    }
                  }
                }
                ?>
            </select>
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 text-right" style="color:#47789f;padding:10px">
            <?php echo __('Cursos').':';?>
          </div>
          <div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;">
            <?php echo $this->Form->textarea('courses',array('id'=>'area2','style'=>'width:450px;'));?>
          </div>
          <div class="col-xs-2 col-sm-2 col-md-2 text-right" style="color:#47789f;padding:10px">
            <?php echo __('Intereses').':';?>
          </div>
          <div class="col-xs-offset-2 col-sm-offset-2 col-md-offset-2 text-left" style="color:#7A7A7A;background-color:#e6ebf1;padding:10px;border-radius: 0px 0px 10px 10px;">
            <?php echo $this->Form->textarea('other_interests',array('id'=>'area3','style'=>'width:450px'));?><br>
          </div>
          </form>
        </div>
        <center><div style="margin-top: 15px;"></div>
          <?php echo $this->Form->submit(__('Guardar'),array('type' => 'submit','class' => 'accept text-center','style'=>'height: auto!important;color: #fff !important;margin-top: 30px;display: initial;'));?>
        </center>
        	<br><br>
      </div>
    </div>
  </div>
  <?php echo $this->Form->end();?>
<!-- <center><h2 class="blue">Formación</h2></center> -->
      </div>
    </div>
  </div>
</div>

<style>
.ui-datepicker .ui-datepicker-header{display: block!important;}
.edit_formacion input {
  height: 20px!important;
  padding: 4px 6px!important;
  border-radius: 4px!important;
  border: 1px solid #799cb9;
  color: #4d4d4d;
}
.accept {
  text-shadow: 0 1px 1px rgba(0,0,0,.3);
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  box-shadow: 0 1px 2px rgba(0,0,0,.2);
  background: #f78d1d;
  background: -webkit-gradient(linear, left top, left bottom, from(#c5a653), to(#ddc079));
  background: -moz-linear-gradient(top, #c5a653, #ddc079);
  background-color: #C2A34E;
  color: white!important;
  /* margin-top: 20px; */
  background-color: #C5A653;
  text-shadow: 0 1px 1px rgba(0,0,0,.3);
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
  box-shadow: 0 1px 2px rgba(0,0,0,.2);
  background: #f78d1d;
  background: -webkit-gradient(linear, left top, left bottom, from(#ddc079), to(#c5a653));
  background: -moz-linear-gradient(top, #ddc079, #c5a653);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ddc079', endColorstr='#c5a653');
  background-color: #C2A34E;
  border: medium none;
  width: 150px !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  font-size: 1.4em !important;
  padding: 6px;
  -webkit-box-shadow: 5px 6px 7px -3px #adadad;
  -moz-box-shadow: 5px 6px 7px -3px #adadad;
  box-shadow: 5px 6px 7px -3px #adadad;
  margin: 20px auto 0;
  display: block;
}


.edit_formacion p{
  margin: 0px;
  text-align: right;
  padding-right: 18px;
}

.edit_formacion input, textarea, select {
  margin: 3px;
  padding: 0px;
  border-radius: 5px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border: 1px solid #999999;
  border-color: #47789f;
}
.edit_formacion input{
  height: 30px !important;
  padding: 4px 6px!important;
  border-radius: 4px!important;
  border: 1px solid #799cb9 !important;
  color: #4d4d4d !important;
}
.nicEdit-main {
    background-color: #fff !important;
}
</style>
<script type="text/javascript" src="/js/nicedit/nicEdit.js"></script>
<script type="text/javascript">
//<![CDATA[
  bkLib.onDomLoaded(function() {
        //new nicEditor().panelInstance('area1');
        new nicEditor({fullPanel : true}).panelInstance('area1');
        new nicEditor({fullPanel : true}).panelInstance('area2');
        new nicEditor({fullPanel : true}).panelInstance('area3');
        // new nicEditor({iconsPath : '/js/nicedit/nicEditorIcons.gif'}).panelInstance('area1');
        // new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area1');
        // new nicEditor({maxHeight : 100}).panelInstance('area1');
  });
  //]]>
</script>