<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'jquery.mockjax.min',
  'bootstrap-editable.min',
  'moment.min',
  'typeahead.min',
  'typeaheadjs.min',
  'address.min',
  'select2.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));
?>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#menu2').addClass('active2');
  $('#menu1').removeClass('active2');

  //bootbox.alert('Bon dia');
});
<?php $this->Html->scriptEnd();
?>
<?php // echo $this->Element('card');

?>

<div id="mobile">
<div id="map" style="position:absolute;"></div>
</div>
<script>
// Provide your access token
L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
// Create a map in the div #map
// Create array of lat,lon points.

// Define polyline options
// http://leafletjs.com/reference.html#polyline
var map = L.mapbox.map('map', 'egalles79.mcf2id2p')
  .setView([40.731, -6.438],3);
        var countryNames = JSON.parse(JSON.stringify(<?php echo $namesForMap?>));
        
        var countries = JSON.parse(JSON.stringify(<?php echo $countryPoints?>));
        var countryWithProjects = JSON.parse(JSON.stringify(<?php echo $countryWithProjects?>));


        // Define polyline options
        // http://leafletjs.com/reference.html#polyline
        
        // Defining a polygon here instead of a polyline will connect the
        // endpoints and fill the path.
        // http://leafletjs.com/reference.html#polygon
        var layerPoints = [];
        var myPoints = [];
        var clickpoints = [];
        map.featureLayer.on('ready', function(e) {
          
          var i = 0;
          $.each(countries, function(index, country) {
            hasProjects = false;
            myCountryProjects = [];
            if (countryWithProjects[index] != undefined) {
              hasProjects = true;
              myCountryProjects.push(index);
              var hasProjects = true;
            } else {
              var hasProjects = false;
            }
            
            var valueToPush = {};
            valueToPush.hasprojects = hasProjects;
            if (hasProjects) {
              myColor = '#'+Math.floor(Math.random()*16777215).toString(16);
              myOpacity = 0.6;
            } else {
              myColor = '#ddd';
              myOpacity = 0;

            }
            
            var polyline_options = {
              color: myColor,
              fill: myColor,
              fillOpacity: myOpacity,
              stroke: 0
            };
            myPoints = [];
            cprepeated = [];
            $.each(country.countrypoints, function( index2, countrypointline ) {
              var a = cprepeated.indexOf(countrypointline);
              if (a == -1) {
                if (typeof(countrypointline) == 'string') {
                  linepointsCountry = "["+countrypointline+"]";
                  linepointsCountry = JSON.parse(linepointsCountry);
                  myPoints.push(linepointsCountry);
                }
              }
              
            });
            layerPoints = [];
            valuesLayerPoints = [];
            $.each(myPoints, function( index, value ) {
              layerPoints.push(L.polyline(value, polyline_options).addTo(map));
            });
            valueToPush.name = countryNames[index];
            valueToPush.points = layerPoints;
            if (countryWithProjects[index] != undefined) {
              valueToPush.general      = countryWithProjects[index];
              valueToPush.projects_in_country = countryWithProjects[index]['projects_in_country'];
              valueToPush.total_company_projects = countryWithProjects[index]['total_company_projects'];
              valueToPush.company_name = countryWithProjects[index]['company_name'];
            }
            clickpoints.push(valueToPush);
            layerPoints = [];
          }); 
          
          $.each(clickpoints, function( indexes, values ) {
             btn = values.points;
             $.each(btn, function (index2,buttons) {
                buttons.on('click',function() {
                  if (values.hasprojects) {
                    text = '<div class="pop-up-map" style=""><div class="row-fluid"><div class="span12" style="  min-height: 10px;"><a href="#"><p class="title">'+values.name+'</p></a><a href="http://www.globeshake.com/test/es/auth_public/edit_cv_empresa/30/false"><img src="http://www.globeshake.com/test/images/logo_editar.png" class="brum"></a></div><div class="row-fluid"><div class="span12"><a href="#"><p class="title">Potencial '+values.name+' <span>30%</span></p></a></div></div><div class="row-fluid"><div class="span12"><div class="span6 icon2">Proyectos '+values.name+':<a class="span6" href="#"><span class="new_font">'+values.projects_in_country+'</span><small>(De '+values.total_company_projects+' Proyectos totales '+values.company_name+')</small></a></div></div><div class="row-fluid"><div class="span12"><div class="span6 icon2">Proyectos acabados:</div><a class="span6" href=""><div class="new_font">12</div></a></div></div><div class="row-fluid"><div class="span12"><div class="span6 icon2">Proyectos en curso:</div><a class="span6" href=""><div class="new_font">9</div></a></div></div><div class="row-fluid"><div class="span12"><div class="span6 iconcasc">Empleados participantes</div><a class="span6" href=""><div class=" new_font">156</div></a></div></div><div class="row-fluid"><div class="span12"><button id="forta" class="accept">Fortalezca Potencial</button></div><div class="row-fluid nope_seen" style=""><div class="span12"><form action="http://google.com"><button type="submit" id="" class="accept">Añadir proyecto</button></form><form action="http://www.globeshake.com/test/es/auth_public/resumen_cv_empresa"><button  type="submit" id="" class="accept">Completar info incompleta de <span>2</span> proyectos</button></form></div></div></div></div>';
                  } else {
                      text = '<div class="pop-up-map" style=""><div class="row-fluid"><div class="span12" style="  min-height: 10px;"><a href="#"><p class="title">'+values.name+'</p></a><a href="#">No hay proyectos en '+values.name+'. Añadir proyectos</a></div></div></div>';
                  }
                  bootbox.alert(text);
                }); 
             });
          });
        });
// Defining a polygon here instead of a polyline will connect the
// endpoints and fill the path.
// http://leafletjs.com/reference.html#polygon
//map.addLayer(line_points);
</script>