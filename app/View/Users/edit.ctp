<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username');
		echo $this->Form->input('firstname');
		echo $this->Form->input('lastname');
		echo $this->Form->input('email');
		echo $this->Form->input('phone');
		echo $this->Form->input('password');
		echo $this->Form->input('country_id');
		echo $this->Form->input('city_id');
		echo $this->Form->input('function_id');
		echo $this->Form->input('sector_id');
		echo $this->Form->input('group_id');
		echo $this->Form->input('photo');
		echo $this->Form->input('photo_dir');
		echo $this->Form->input('notification_date');
		echo $this->Form->input('notification_msg');
		echo $this->Form->input('notification_news');
		echo $this->Form->input('notification_task');
		echo $this->Form->input('code');
		echo $this->Form->input('code_deactivation');
		echo $this->Form->input('alias_email');
		echo $this->Form->input('birthdate');
		echo $this->Form->input('gender');
		echo $this->Form->input('courses');
		echo $this->Form->input('idioms');
		echo $this->Form->input('other_interests');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Functions'), array('controller' => 'functions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Function'), array('controller' => 'functions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Sectors'), array('controller' => 'sectors', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Sector'), array('controller' => 'sectors', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
	</ul>
</div>
