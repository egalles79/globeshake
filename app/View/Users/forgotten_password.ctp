<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */
echo $this->Html->script(array(
  'jquery-1.11.3.min',
  'bootstrap.min',
  'bootbox.min'
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('.link_button').prop('disabled',true);
  $('.link_button').addClass('inactive');
  //bootbox.alert('Bon dia');
  $('#change_password').keyup(function() {
    if ($(this).val().length >= 6) {
      $('#groupclass1').addClass('has-success');
      $('#groupclass1').removeClass('has-error');
      $('#mine').val($(this).val());
    } else {
      $('#groupclass1').removeClass('has-success');
      $('#groupclass1').addClass('has-error');
    }
  });
  $('#change_password2').keyup(function() {
    if ($(this).val().length >= 6) {
      if ($(this).val() == $('#mine').val()) {
        $('#groupclass2').addClass('has-success');
        $('#groupclass2').removeClass('has-error');
        $('.link_button').prop('disabled',false);
        $('.link_button').removeClass('inactive');
        $('.link_button').addClass('submit');
      } else {
        $('#groupclass2').removeClass('has-success');
        $('#groupclass2').addClass('has-error');
        $('.link_button').prop('disabled',true);
        $('.link_button').addClass('inactive');
        $('.link_button').removeClass('submit');
      }
    } else {
      $('#groupclass2').removeClass('has-success');
      $('#groupclass2').addClass('has-error');
      $('.link_button').prop('disabled',true);
      $('.link_button').addClass('inactive');
      $('.link_button').removeClass('submit');
    }
  });
});
<?php $this->Html->scriptEnd();
?>
<div class="row containgener">
	<div id='map'></div>
	<script>
	// Provide your access token
	L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
	// Create a map in the div #map
	// Create array of lat,lon points.

	// Define polyline options
	// http://leafletjs.com/reference.html#polyline
	var map = L.mapbox.map('map', 'egalles79.m2k17jkm')
	  .setView([40.731, -6.438],2);
	// Defining a polygon here instead of a polyline will connect the
	// endpoints and fill the path.
	// http://leafletjs.com/reference.html#polygon
	//map.addLayer(line_points);
	</script>
  
    <!-- Inicio columna de la izda la del logo --> 
    <div class="col-sm-12 col-md-12 col-xs-12">
      <div class="row">
        <div class="col-sm-3 col-md-3 col-xs-3">
          <div class="grid_2 logo globes">
          <a href="/">
          	<?php echo $this->Html->image('logo_Globeshake.png',array('title' => __('GlobeShake'),'alt' => __('GlobeShake')));?>
          </a>
          </div>
        </div>
        <div class="col-sm-9 col-xs-9 col-sm-9 text-center">
          &nbsp;
        </div>
      </div>
    </div>
	<?php echo $this->Session->flash(); ?>
  <?php echo $this->Form->create('User'); 
  echo $this->Form->input('mine',array('id' => 'mine','type'=>'hidden'))?>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="col-sm-offset-1 col-md-offset-3 col-sm-offset-3 col-xs-offset-1 col-md-6 col-xs-11 col-sm-11 ">
  			<div class="h1 rsitya_inic"><?php echo __('Reestableciendo su contraseña');?></div>
  			<div class="cajabox caja">
          <div class="form-group" id="groupclass1">
            <label class="control-label" for="inputError"><?php echo __('Introduzca su nueva contraseña (mínimo 6 caracteres)');?></label>
            <input type="password" class="form-control" id="change_password">
          </div> 
          <div class="form-group" id="groupclass2">
            <label class="control-label" for="inputError"><?php echo __('Repita su nueva contraseña');?></label>
            <input type="password" class="form-control" id="change_password2">
          </div>
          <input type="submit"  name="send_forgotten_password" value="Enviar" class="link_button large reheol">
      </div>
		</div>
	</div>
	<div class="espacio">&nbsp;</div>
<style>
.rsitya_inic {
  font-family: Arial, Helvetica, sans-serif;
  clear: both;
  color: #47789F;
  font-size: 2em;
}
.caja {
  /* height: 8em; */
  padding-top: 2%;
}
.grey {
  color: #666;
}
.cajabox {
  background: #e5e3e4 !important;
  border-radius: 12px !important;
  -moz-border-radius: 12px !important;
  -webkit-border-radius: 12px !important;
  -webkit-box-shadow: 7px 4px 6px -3px #adadad;
  -moz-box-shadow: 7px 4px 6px -3px #adadad;
  box-shadow: 7px 4px 6px -3px #adadad;
  padding: 5%;
}
.submit {
    text-shadow: 0 1px 1px rgba(0,0,0,.3);
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);
    background: #f78d1d !important;
    background: -webkit-gradient(linear, left top, left bottom, from(#ddc079), to(#c5a653)) !important;
    background: -moz-linear-gradient(top, #ddc079, #c5a653) !important;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ddc079', endColorstr='#c5a653');
    background-color: #C2A34E !important;
    border: medium none;
    color: #FFFFFF !important;
    margin-left: 32% !important;
    margin-top: 2% !important;
    width: 34% !important;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    padding: 5px;
}
.inactive {
    text-shadow: 0 1px 1px rgba(0,0,0,.3);
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    -moz-box-shadow: 0 1px 2px rgba(0,0,0,.2);
    box-shadow: 0 1px 2px rgba(0,0,0,.2);
    background: #ccc !important;
    background: -webkit-gradient(linear, left top, left bottom, from(#ccc), to(#ddd)) !important;
    background: -moz-linear-gradient(top, #ccc, #ddd) !important;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ccc', endColorstr='#ddd');
    color: #FFFFFF !important;
    margin-left: 32% !important;
    margin-top: 2% !important;
    width: 34% !important;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    padding: 5px;
    
}
.espacio {
	margin-bottom: 9.5%;
}

.rsitya_dircc.recupcondo_entr {
  font-size: 1.1em;
  line-height: 1em;
  color: #7A7A7A;
  padding-bottom: 10px;
}
</style>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#window_area .login').click(function(){
    if($(this).hasClass('active')) $('.header_window').hide();
    else $('.header_window').show(100);
    $(this).toggleClass('active');
  });
});
<?php $this->Html->scriptEnd();
?>