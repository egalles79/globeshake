<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
'jquery-ui-1.10.3.custom.min',
'jquery.mockjax.min',
'bootstrap-editable.min',
'moment.min',
'address.min',
'select2.min',
'custom.min',
'core.min',
'bootbox.min',
'jquery.mask.min'
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
	$('.money').mask('000.000.000.000.000,00', {reverse: true});
	$('#menu1').removeClass('active2');
  	$('#menu2').addClass('active2');
});
<?php $this->Html->scriptEnd();
?>
<div class="box" style="border:0px !important;width: 850px;margin:auto;">
<!-- <div class="mine" style="position:absolute;margin-left:-10px;margin-top:10px"><?php echo $this->Html->image('logo.png')?></span></a></div> -->

<div class="box" style="border:0px !important">
	<div class="box-content" style="border: 7px solid #45759b;">

		<h2 class="title text-center" style="font-size:20px;position:relative;"><?php echo __('RESUMEN CV PERSONAL GLOBESHAKE ');?><div class="back_his" style="">
      <a href="javascript:history.back()"><?php echo $this->Html->image('/img/flecha_volver.png');?>VOLVER</a>
    </div></h2><hr>
		
	  <div class=" ">
	  	<p class="	" style="text-align: center;font-size: 20px;"><?php echo $user['User']['firstname'].' '.$user['User']['lastname']?></p>
	  </div>
    	<div class="box-content">
	    	<div class="row">
	    		<div class="col-md-12">
		    		<div class="box">
			    		
		    			<div class="box-header" style="">
		            		<h2 class="title " style="color:#fff">
		            			<i class="fa fa-book"></i>
		            			<a href="#" class="btn-minimize " style="text-decoration:none;color: white;"><?php echo __('FORMACIÓN');?></a></h2>
		            			<div class="box-icon">
					              <a href="#" class="btn-minimize target"><i class="fa fa-chevron-up" style=""></i></a>
					            </div>
					    </div>
					    <!-- FORMACIÓN -->
					    <div class="box-content">
				            <div class="row row_round" >
				                <?php 			                
				                if (!empty($user['User']['formation_completed'])) { ?>
				                  <div class="col-md-12">
				                    <?php
				                    $fecha    = empty($user['User']['birthdate']) ? time() : date($user['User']['birthdate']) ;
				                    if (is_string($fecha)) {
				                        $fecha_de_nacimiento = strtotime($fecha);
				                        $diferencia_de_fechas = time() - $fecha_de_nacimiento;
				                        $edad = round($diferencia_de_fechas / (60 * 60 * 24 * 365));
				                    } else {
				                      $edad = __('Fecha de nacimiento no asignada');
				                    }
				                    
				                    $languages =  json_decode($user['User']['idioms']);
				                    $languagesText = '';
				                        if (!empty($languages)) {
				                          foreach ($languages as $language) {
				                            $languagesText .= $languageName[$language].' - ';
				                          }
				                        } else {
				                          $languagesText = __('No ha añadido idiomas');
				                        }
				                        $otherInterests = (!empty($user['User']['other_interests'])) ? $user['User']['other_interests'] : __('Sin otros intereses añadidos');
				                    switch ($user['User']['gender']) {
				                      case 0: $sSex = __('Hombre'); break;
				                      case 1: $sSex = __('Mujer'); break;
				                      default: $sSex = __('Sin sexo asignado');
				                    }
				                    $courses = (!empty($user['User']['courses'])) ? $user['User']['courses'] : __('Sin cursos añadidos');
				                      $formation = (!empty($user['User']['formation'])) ? $user['User']['formation'] : __('Sin formación añadida');
				                    ?>
				                    <br>
				                    <table class="table table-responsive">
				                      <thead></thead>
				                      <tbody>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey nopad" >
				                              <?php echo __('Edad').':';?>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                            <div class="text-left resumen"><?php echo $edad;?>
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td>
				                            <div class="text-right colour_grey nopad">
				                              <?php echo __('Sexo').':';?>
				                            </div>
				                          </td>
				                          <td>
				                            <div class="text-left resumen"><?php echo $sSex;?>
				                          </td>
				                          <td>
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey nopad">
				                              <?php echo __('Formación').':';?>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                            <div class="text-left resumen"><?php echo $formation;?>
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey">
				                              <?php echo __('Idiomas').':';?>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                          	<div class="contador"><?php echo count($languages);?></div>
				                            <div class="text-left resumen" ><?php echo $languagesText;?>
				                          </td>
				                          
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey nopad">
				                              <?php echo __('Cursos').':';?>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                            <div class="text-left resumen"><?php echo $courses;?>
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey nopad">
				                              <?php echo __('Intereses').':';?>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                            <div class="text-left resumen" ><?php echo $otherInterests;?>
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                      </tbody>
				                    </table>
				                  </div>
				                <?php } else {?>
				                  <div class="col-md-12">
				                    <div class="row">
				                      <?php echo $this->Element('formation_not_completed');?>
				                    </div>
				                  </div>
				                <?php }?>
				              
				            </div>
				          </div>
						<br />
					<!-- ESPECIALISTA -->
			        <?php
			        if (!empty($specialitiesFromUser)) {
			        ?>
						<div class="box">
					        <div class="box-header">
			            		<h2 class="title text-center" style="color:#fff">
			            			<i  class="fa fa-star"></i>
			            			<a href="#" class="btn-minimize " style="text-decoration:none;color:white;"><?php echo __('ESPECIALISTA');?></a></h2>
			            			<div class="box-icon">
			            				<?php

						              echo $this->Html->image('star_03.png');
						              ?>
						              <a href="#" class="btn-minimize target"><i class="fa fa-chevron-up" style=""></i></a>
						            </div>
						    </div>
						    <div class="box-content">
								<div class="row row_round4">
									<div class="col-md-12">
										<?php foreach ($specialitiesFromUser as $specialityFromUser) {
											echo '<span color:#7a7a7a">'.$specialityFromUser['name'].':</span> '.$specialityFromUser['description'].'<br />';
										}?>
									</div>
								</div>
							</div>
						</div>

					<?php
					}
					?>
				        <!-- INFORMACIÓN PROYECTOS -->
				    <div class="box">
				        <div class="box-header" style="    background-color: #4778a0;">
		            		<h2 class="title text-center" style="color:#fff">
		            			<i class="fa fa-info"></i>
		            			<a href="#" class="btn-minimize " style="text-decoration:none;color: white;"><?php echo __('INFORMACIÓN GENERAL PROYECTOS');?></a></h2>
		            			<div class="box-icon">
					              <a href="#" class="btn-minimize target"><i class="fa fa-chevron-up" style=""></i></a>
					            </div>
					    </div>
						<div class="box-content">
							<div class="row row_round2">
							<div class="col-md-12">
				                    <?php
				                    $companyNameString = '';
				                    $companiesCount = 0;
				                    foreach($companyNames as $companyName) {
				                    	$companiesCount++;
				                    	$companyNameString .= $companyName.'<br />';
				                    }
				                    $projectsNameString = '';
				                    $projectsCount = 0;
				                    foreach($projectsNames as $projectsName) {
				                    	$projectsCount++;
				                    	$projectsNameString .= $projectsName.'<br />';
				                    }
				                    $countryProjectsCountryName = '';
				                    $countryProjectsCountryCount = 0;
				                    foreach($countryProjectIds as $countryProjectId) {
				                    	$countryProjectsCountryCount++;
				                    	$countryProjectsCountryName .= $countryNames[$countryProjectId].'<br />';
				                    }
				                    $customerNamesString = '';
				                    $customerNamesCount = 0;
				                    foreach($customerNames as $customerName) {
				                    	$customerNamesCount++;
				                    	$customerNamesString .= $customerName.'<br />';
				                	}
				                	$numSectors = 0;
				                	$sectorProjectString = '';
				                	foreach($sectorsProjectNames as $sectorsProjectName) {
				                		$numSectors++;
				                		$sectorProjectString .= $sectorsProjectName.'<br />';
				                	}
				                    ?>
				                    <table class="table table-responsive ">
				                      <thead></thead>
				                      <tbody>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey" >
				                              <?php echo __('Empresas').':';?>&nbsp;&nbsp;<i class="fa fa-users"></i>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                          	<div class="contador"><?php echo $companiesCount;?></div>
				                            <div class="text-left resumen"><?php echo $companyNameString;?>
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td>
				                            <div class="text-right colour_grey">
				                              <?php echo __('Proyectos').':';?>&nbsp;&nbsp;<i class="fa fa-briefcase"></i>
				                            </div>
				                          </td>
				                          <td>
				                          	<div class="contador"><?php echo $projectsCount;?></div>
				                            <div class="text-left resumen"><?php echo $projectsNameString;?>
				                          </td>
				                          <td>
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey">
				                              <?php echo __('Países').':';?>&nbsp;&nbsp;<i class="fa fa-globe"></i>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                            <div class="contador"><?php echo $countryProjectsCountryCount;?></div>
				                            <div class="text-left resumen"><?php echo $countryProjectsCountryName;?>
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey">
				                              <?php echo __('Clientes').':';?>&nbsp;&nbsp;<i class="fa fa-folder-open"></i>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                          	<div class="contador"><?php echo $customerNamesCount;?></div>
				                            <div class="text-left resumen" ><?php echo $customerNamesString;?>
				                          </td>
				                          
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey">
				                              <?php echo __('Sectores').':';?>&nbsp;&nbsp;<i class="fa fa-circle-o"></i>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                          	<div class="contador"><?php echo $numSectors;?></div>
				                            <div class="text-left resumen"><?php echo $sectorProjectString;?>
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                        <tr>
				                          <td class="col-md-2">
				                            <div class="text-right colour_grey">
				                              <?php echo __('Presupuesto total').':';?>&nbsp;&nbsp;<i class="fa fa-money"></i>
				                            </div>
				                          </td>
				                          <td class="col-md-9">
				                          	<div class="contador"><?php echo $projectsCount;?></div>
				                            <div class="text-left resumen" ><span class="money"><?php echo round($projectsSumBudget,2)?></span> €
				                          </td>
				                          <td class="col-md-1">
				                            &nbsp;
				                          </td>
				                        </tr>
				                      </tbody>
				                    </table>
				                  </div>
							</div>
						</div>
		    		</div>
		    		<?php echo $this->Element('tucontribucion',array('sectorServices' => $sectorServices,'departmentNames'=>$departmentNames,'functionalitiesNames'=>$functionalitiesNames));?>
	    		</div>
	    	</div>
	    </div>
	   </div>

</box>



<style>

.table th, .table td { 
     border-top: none !important;
     margin: 0px !important;
     padding: 0px !important;
 }
 .resumen {
 	border: 1px solid #799ebe;
  
  padding:10px;
  border-radius: 10px;
  margin-bottom:10px;
  background-color: #ffffff;
 }
 .row_round {
 	background-color:#abc4dd;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
 .row_round2 {
 	background-color:#e6ebf1;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
 
  .row_round3 {
 	background-color:#cdcdcd;width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
  .row_round4 {
 	width:80%;margin-left:10%;margin-right:10%;
 	border-radius: 10px;
 }
 
 .colour_grey {
 	color:#47789f;
 	padding:10px;
 	padding-top: 20px;
 	  font-family: sourcesansprobold;
  font-size: 12pt;
 }
 .contador {
  position: relative;
  z-index: 0;
  background-color: white;
  border: 2px solid #47789f;
  border-radius: 10px;
  width: 25px;
  text-align: center;
  left: -6px;
  color: #ff3600;
  top: 11px;
}
.box {
	border: 0px;
}
.tit_box {
  color: #c4a34e;
  padding: 20px;
  font-size: 24px;
  line-height: 5px;
  font-weight: bold;
  -webkit-box-shadow: 0px 6px 30px 0px rgba(50, 50, 50, 0.75);
  -moz-box-shadow: 0px 6px 30px 0px rgba(50, 50, 50, 0.75);
  box-shadow: 0px 6px 30px 0px rgba(50, 50, 50, 0.75);
}
.back_his{
	position: absolute;
	left: 90%;
    top: 0;
}
.nopad{
	padding-top: 10px;
}
</style>