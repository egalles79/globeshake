<?php
/**
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Pages
 * @since         CakePHP(tm) v 0.10.0.1076
 */
echo $this->Html->script(array(
  'jquery-1.11.3.min',
  'bootstrap.min',
  'bootbox.min'
), array('inline' => false));

if (!Configure::read('debug')):
  throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>
<div class="row containgener">
	<div id='map'></div>
	<script>
	// Provide your access token
	L.mapbox.accessToken = 'pk.eyJ1IjoiZWdhbGxlczc5IiwiYSI6IklSSEhONkkifQ.IVV6qrpfmZx8jj0jSn7ZtA';
	// Create a map in the div #map
	// Create array of lat,lon points.

	// Define polyline options
	// http://leafletjs.com/reference.html#polyline
	var map = L.mapbox.map('map', 'egalles79.m2k17jkm')
	  .setView([40.731, -6.438],2);
	// Defining a polygon here instead of a polyline will connect the
	// endpoints and fill the path.
	// http://leafletjs.com/reference.html#polygon
	//map.addLayer(line_points);
	</script>
  
    <!-- Inicio columna de la izda la del logo --> 
    <div class="col-sm-12 col-md-12 col-xs-12">
      <div class="row">
        <div class="col-sm-3 col-md-3 col-xs-3">
          <div class="grid_2 logo globes">
          <a href="/">
          	<?php echo $this->Html->image('logo_Globeshake.png',array('title' => __('GlobeShake'),'alt' => __('GlobeShake')));?>
          </a>
          </div>
        </div>
        <div class="col-sm-9 col-xs-9 col-sm-9 text-center">
          &nbsp;
        </div>
      </div>
    </div>
	<?php echo $this->Session->flash(); ?>
  <?php echo $this->Form->create('User'); ?>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="col-md-offset-3 col-sm-offset-3 col-xs-offset-3 col-md-6 col-xs-6 col-sm-6 ">
			<div class="h1 rsitya_inic"><?php echo __('Reenviar clave de activación');?></div>
			<div class="rsitya_inicdc recupcondo_box">
				<div class="rsitya_dircc recupcondo_entr">
					<?php echo __('Pon tu email para que te enviemos la clave de reactivación.');?>
				</div>
        <?php echo $this->Form->input('forgot_password_identity',array('class' => 'tooltoip_trigger', 'id' => 'rsitya_di', 'type' => 'text', 'value' => '', 'label' => ''));?>
				<input type="submit" name="send_forgotten_password" id="submit" value="Enviar" class="link_button large reheol">
			</div>
		</div>
	</div>
	<div class="espacio">&nbsp;</div>
<style>
.rsitya_inic {
  font-family: Arial, Helvetica, sans-serif;
  clear: both;
  color: #47789F;
  font-size: 2em;
}
.recupcondo_box {
  /* height: 8em; */
  padding-top: 2%;
}
.rsitya_inicdc {
  background: #e5e3e4 !important;
  border-radius: 12px !important;
  -moz-border-radius: 12px !important;
  -webkit-border-radius: 12px !important;
  -webkit-box-shadow: 7px 4px 6px -3px #adadad;
  -moz-box-shadow: 7px 4px 6px -3px #adadad;
  box-shadow: 7px 4px 6px -3px #adadad;
  padding: 5%;
}
.espacio {
	margin-bottom: 9.5%;
}
.rsitya_dircc.recupcondo_entr {
  font-size: 1.1em;
  line-height: 1em;
  color: #7A7A7A;
  padding-bottom: 10px;
}
</style>
<?php
$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
  $('#window_area .login').click(function(){
    if($(this).hasClass('active')) $('.header_window').hide();
    else $('.header_window').show(100);
    $(this).toggleClass('active');
  });
});
<?php $this->Html->scriptEnd();
?>