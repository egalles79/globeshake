
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  <title>Cards</title>

  <link href="/favicon.ico" type="image/x-icon" rel="icon" /><link href="/favicon.ico" type="image/x-icon" rel="shortcut icon" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">

  
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/style.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/retina.min.css" />
  <link rel="stylesheet" type="text/css" href="/css/icons.css" />  <link rel="stylesheet" type="text/css" href="/css/estilo.css" />  	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.css" />
	<!--
  añadidoesto
  -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
	$("#checkall").toggle(   
    	function () {
        	$(".checkis").attr('checked', this.checked);
    	},
 	);
  </script>
  <!--
  fin añadido
  -->
  <style>
    #map { position:absolute;}
    .minbox {
    	min-height:70px;
    	margin-bottom:2px
    }
    .isuser {
    	background-color : rgba(71, 120, 159, 0.36);
    }
  </style>
</head>
<body>


	<link rel="stylesheet" type="text/css" href="/css/glyphicons.css" />
	<link rel="stylesheet" type="text/css" href="/css/estilo.css" />

<!-- start: Header -->
<header class="navbar">
  <div class="container">
    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".sidebar-nav.nav-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a id="main-menu-toggle" class="hidden-xs open"><i class="fa fa-bars"></i></a>
    <a class="navbar-brand col-md-2 col-sm-1 col-xs-2" href="/Companies/cvmap"><span>&nbsp;&nbsp;&nbsp;<img src="/img/logo.png" alt="" /> Globeshake</span></a>
      <nav>
          <ul class="menu">
           <li><a id="menu1" href="/Companies/cvmap"><i class="fa fa-building-o"></i>&nbsp;&nbsp;EMPRESA</a>
           <ul class="sub-menu">
             <li>
                <a href="#"><i class="fa fa-globe">&nbsp;Dar a conocer empresa</i></a>
                <ul class="sub-menu">
                  <li><a href="/Companies/ver_cv"><i class="fa fa-columns">&nbsp;Ver CV</i></a></li>
                  <li><a href="/Companies/resumen_cv"><i class="fa fa-dashboard">&nbsp;Cifras totales de empresa</i></a></li>
                  <li><a href="#"><i class="fa fa-external-link">&nbsp;Crea portal empresa</i></a></li>
                </ul>
             </li>
             <li>
                <a href="#"><i class="fa fa-users">&nbsp;Colaboradores</i></a>
                <ul class="sub-menu">
                  <li><a href="#"><i class="fa fa-list">&nbsp;Listas internas</i></a></li>
                  <li><a href="#"><i class="fa fa-edit">&nbsp;Crear lista</i></a></li>
                  <li><a href="#"><i class="fa fa-list-ul">&nbsp;Mis listas</i></a></li>
                </ul>
             </li>
             <li>
                <a href="#"><i class="fa fa-paperclip">&nbsp;Licitaciones</i></a>
                <ul class="sub-menu">
                  <li><a href="#"><i class="fa fa-edit">&nbsp;Publicar licitación</i></a></li>
                  <li><a href="#"><i class="fa fa-file-text-o">&nbsp;Petición de oferta</i></a></li>
                  <li><a href="#"><i class="fa fa-list">&nbsp;Listas internas</i></a></li>
                  <li><a href="#"><i class="fa fa-search">&nbsp;Market Survey</i></a></li>
                </ul>
             </li>
           </ul>
         </li>
          <li><a id="menu2"  href="/Users/cvmap"><i class="fa fa-briefcase"></i>&nbsp;&nbsp;PERFIL PROFESIONAL</a>
          <ul class="sub-menu">
           <li>
              <a href="#"><i class="fa fa-info">&nbsp;Dar a conocer mi perfil</i></a>
              <ul>
              <li><a href="/Users/ver_cv"><i class="fa fa-columns">&nbsp;Ver CV</i></a></li>
              <li><a href="/Users/resumen_cv"><i class="fa fa-file-text">&nbsp;Resumen CV</i></a></li>
              <li><a href="/Specialities/"><i class="fa fa-star">&nbsp;Soy especialista</i></a></li>
              <li><a href="#"><i class="fa fa-sun-o">&nbsp;Sé visible en la empresa</i></a></li>
            </ul>
           </li>
           <li><a href="#"><i class="fa fa-search">&nbsp;Buscar (general)</i></a>
            <ul>
              <li><a href="#"><i class="fa fa-users">&nbsp;Personas</i></a></li>
              <li><a href="#"><i class="fa fa-book">&nbsp;Contactos</i></a></li>
              <li><a href="#"><i class="fa fa-briefcase">&nbsp;Empresas</i></a></li>
            </ul>
          </li>
         </ul>

        </li>
        </ul>
        </nav>
    <!-- start: Header Menu -->
    <div class="nav-no-collapse header-nav">
      <ul class="nav navbar-nav pull-right">
          <link rel="stylesheet" type="text/css" href="/css/msdropdown/dd.css" /><link rel="stylesheet" type="text/css" href="/css/msdropdown/flags.css" /><!-- <select name="countries" id="countries" style="cursor:pointer;width:150px;">-->
<select id="countries" style="width:150px;cursor:pointer;margin-top:4px"><option value="es"selected = "es"data-title="Castellano">Castellano</option><option value="gb"data-title="English">English</option></select><!--</select>-->

        <li class="dropdown hidden-xs">
          <!--<a class="btn dropdown-toggle"  href="#">
            <i class="glyphicons wifi_alt" style="color:#fff"></i>
          </a>-->
          <ul class="dropdown-menu notifications" id="hotspotslist">
            <li class="dropdown-menu-title" style="text-align: center;">
              <span>Puntos de acceso</span>
            </li>
          </ul>
        </li>

        <!-- start: User Dropdown -->
        <li class="dropdown">
          <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
            <div class="avatar"></div>
            <div class="user">
              <span class="hello">Bienvenido</span>
              <span class="name">__</span>
            </div>
          </a>
          <ul class="dropdown-menu menumine" style="text-align: left">
            <li><a href="/Users/my"><i class="fa fa-user"></i>Perfil</a></li>
            <!-- <li><a href="/Users/my"><i class="fa fa-cog"></i>Configuration</a></li> -->
            <li><a href="/Users/logout"><i class="fa fa-off"></i>Salir</a></li>
          </ul>
        </li>
        <!-- end: User Dropdown -->
      </ul>
    </div>
    <div style="float: right;
  margin-right: 20px;
  margin-top: 5px;">
        <div class="social social-head"><a href="#" title="Facebook"><i class="icon-facebook"></i></a><a href="#" title="Twitter"><i class="icon-twitter"></i></a><a href="#" title="Rss"><i class="icon-rss"></i></a><a href="#" title="Linkedin"><i class="icon-linkedin"></i></a><a href="#" title="Google Plus"><i class="icon-google-plus"></i></a></div>    </div>
    <!-- end: Header Menu -->

  </div>
</header>
<style>
/* ---------- Google Font ---------- */
@import url(http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,700,800);

/* RESET STYLES */
*, html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, form, label, fieldset, input, p, blockquote, th, td { margin:0; padding:0 }
table { border-collapse:collapse; border-spacing:0 }
fieldset, img { border:0 }
address, caption, cite, code, dfn, em, strong, th, var { font-style:normal; font-weight:normal }
ol, ul, li { list-style:none }
caption, th { text-align:left }
h1, h2, h3, h4, h5, h6 {font-weight:normal;}
q:before, q:after { content:''}
strong { font-weight: bold }
em { font-style: italic }
.italic { font-style: italic }
.aligncenter { display:block; margin:0 auto; }
.alignleft { float:left; margin:10px; }
.alignright { float:right;margin:10px; }
.no-margin{margin:0px;}
.no-bottom{margin-bottom:0px;}
.no-padding{padding:0px;}
.margin-t{margin-top:22px;}

a{text-decoration:none;}
a:hover{text-decoration:underline;}
a:active,a:focus{outline: none;}

img.alignleft, img.alignright, img.aligncenter {
    margin-bottom: 11px;
}

.alignleft, img.alignleft{
display: inline;
    float: left;
    margin-right: 22px;
}

.alignright, img.alignright {
    display: inline;
    float: right;
    margin-left: 22px;
}

.aligncenter, img.aligncenter {
    clear: both;
    display: block;
    margin-left: auto;
    margin-right: auto;
}

article, aside, figure, footer, header, hgroup, nav, section {display: block;}


*{ 
  -webkit-box-sizing: border-box; /* Safari/Chrome, other WebKit */
  -moz-box-sizing: border-box;    /* Firefox, other Gecko */
  box-sizing: border-box;         /* Opera/IE 8+ */
}


p{padding-bottom:11px;}
p,div, span{line-height:1.5em;}
.column-clear{clear:both;}
.clear{overflow: hidden;}


nav{display:block;
margin-left:170px;
float:left;
}

.menu{display:block;}

.menu li{
display: inline-block;
position: relative;
z-index:100;}

.menu li:first-child{margin-left:0;}

.menu li a {font-weight:600;
text-decoration:none;
padding:11px;
display:block;
color:#ffffff;

-webkit-transition: all 0.2s ease-in-out 0s;
   -moz-transition: all 0.2s ease-in-out 0s;
   -o-transition: all 0.2s ease-in-out 0s;
   -ms-transition: all 0.2s ease-in-out 0s;
   transition: all 0.2s ease-in-out 0s;
}
.active2 {
  font-weight:600;
  text-decoration:none;
  padding:11px;
  display:block;
  color:#47789f !important;
  background:#e9ebec;
  border-radius: 5px 5px 0px 0px;
  -moz-border-radius: 5px 5px 0px 0px;
  -webkit-border-radius: 5px 5px 0px 0px;
}
.menu li a:hover, .menu li:hover > a{
  color:#47789f;
  background:#e9ebec;
  border-radius: 5px 5px 0px 0px;
  -moz-border-radius: 5px 5px 0px 0px;
  -webkit-border-radius: 5px 5px 0px 0px;
}

.menu ul {display: none;
margin: 0;
padding: 0;
width: 150px;
position: absolute;
top: 43px;
left: 0px;
background: #ffffff;
}

.menu ul li {display:block;
float: none;
background:none;
margin:0;
padding:0;
}
.menu ul li a {font-size:12px;
font-weight:normal;
display:block;
color:#47789f;
border-left:3px solid #ffffff;
background:#ffffff;}

.menu ul li a:hover, .menu ul li:hover > a{
background:#47789f;
border-left:3px solid #fff;
color:#fff;
}

.menu li:hover > ul{ display: block;}
.menu ul ul {left: 149px;
  top: 0px;
}

.mobile-menu{display:none;
width:100%;
padding:11px;
background:#3E4156;
color:#ffffff;
text-transform:uppercase;
font-weight:600;
}
.mobile-menu:hover{background:#3E4156;
color:#ffffff;
text-decoration:none;
}


@media (min-width: 768px) and (max-width: 979px) {
.menu ul {top:37px;}
.menu li a{font-size:12px;
padding:8px;}
}

@media (max-width: 767px) {

.mainWrap{width:auto;padding:50px 20px;}

.menu{display:none;}

.mobile-menu{display:block;
margin-top:100px;}

nav{margin:0;
background:none;}

.menu li{display:block;
margin:0;}

.menu li a {background:#ffffff;
color:#797979;
border-top:1px solid #e0e0e0;
border-left:3px solid #ffffff;}

.menu li a:hover, .menu li:hover > a{
background:#f0f0f0;
color:#797979;
border-left:3px solid #9CA3DA;}

.menu ul {display:block;
position:relative;
top:0;
left:0;
width:100%;}

.menu ul ul {left:0;}

}

@media (max-width: 480px) {

}


@media (max-width: 320px) {
}
</style>
<!-- end: Header --><div class="container">
    <div class="row">
        <!-- start: Main Menu -->
<div id="sidebar-left" class="col-lg-2 col-sm-1 ">

    <div class="sidebar-nav nav-collapse collapse navbar-collapse">
        <ul class="nav main-menu">
            <li>
                <a href="/Companies/projectmap"><i class="fa fa-home"></i><span class="hidden-sm text"> Inicio</span></a>            </li>
            <li>
                <a href="/Cards"><i class="fa fa-shopping-cart"></i><span class="hidden-sm text submenu"> Comprar tarjeta de visita</span></a>            </li>
            <li>
                <a href="#"><i class="fa fa-shopping-cart"></i><span class="hidden-sm text submenu"> Tiene 27 tarjetas de visita</span></a>            </li>
            <li>
                <a class="dropmenu hidden2" href="#"><i class="fa fa-building-o"></i><span class="hidden-sm text"> Empresa</span> <span class="chevron closed"></span></a>
                <ul class="hidden2">
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-globe"></i><span class="hidden-sm text"> Dar a conocer empresa</span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-info"></i><span class="hidden-sm text"> Presenta a tu empresa</span></a>
                            </li>
                            <li>
                                <a href="/Companies/ver_cv"><i class="fa fa-book"></i><span class="hidden-sm text submenu"> Ver CV</span></a>                            </li>
                             <li>
                                <a class="submenu" href="#"><i class="fa fa-dashboard"></i><span class="hidden-sm text"> Cifras totales empresa</span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-external-link"></i><span class="hidden-sm text"> Crea portal empresa</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-users"></i><span class="hidden-sm text"> Colaboradores</span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-list"></i><span class="hidden-sm text"> Listas internas</span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-edit"></i><span class="hidden-sm text"> Crear lista</span></a>
                            </li>
                             <li>
                                <a class="submenu" href="#"><i class="fa fa-list-ul"></i><span class="hidden-sm text"> Mis listas</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-paperclip"></i><span class="hidden-sm text"> Licitaciones</span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-edit"></i><span class="hidden-sm text"> Publicar licitación</span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-file-text-o"></i><span class="hidden-sm text"> Petición oferta</span></a>
                            </li>
                             <li>
                                <a class="submenu" href="#"><i class="fa fa-list"></i><span class="hidden-sm text"> Listas internas</span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-search"></i><span class="hidden-sm text"> Market Survey</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a class="dropmenu hidden2" href="#"><i class="fa fa-briefcase"></i><span class="hidden-sm text"> Perfil profesional</span><span class="chevron closed"></span></a>
                <ul class="hidden2">
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-info"></i><span class="hidden-sm text"> Dar a conocer mi perfil</span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-info"></i><span class="hidden-sm text"> Preséntate profesionalmente</span></a>
                            </li>
                            <li>
                                <a href="/Users/ver_cv"><i class="fa fa-columns"></i><span class="hidden-sm text submenu"> Ver CV</span></a>                            </li>
                            <li>

                                <a href="/Users/resumen_cv"><i class="fa fa-file-text"></i><span class="hidden-sm text submenu"> Resumen CV</span></a>

                                
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-star"></i><span class="hidden-sm text"> Soy especialista</span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-sun-o"></i><span class="hidden-sm text"> Sé visible en la empresa</span></a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="dropmenu sub" href="#"><i class="fa fa-search"></i><span class="hidden-sm text"> Buscar (general)</span><span class="chevron closed"></span></a>
                        <ul>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-users"></i><span class="hidden-sm text"> Personas</span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-book"></i><span class="hidden-sm text"> Contactos</span></a>
                            </li>
                            <li>
                                <a class="submenu" href="#"><i class="fa fa-building-o"></i><span class="hidden-sm text"> Empresas</span></a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="/Users/ver_cv"><i class="fa fa-user"></i><span class="hidden-sm text submenu"> Ver perfil</span></a>            </li>
            <li>
            <a href="#" id="main-menu-min" class="visible-md visible-lg full" style="backgorund-color:#fff !important"><i class="fa fa-angle-double-left"></i></a>

            <!-- ACL User Secction -->
            <!--                            <li>
                                    </li>
            
                            <li>
                                    </li>
            
                            <li>
                                    </li>
            
                            <li>
                                    </li>
            
                            <li>
                                    </li>
                                        <li>
                                    </li>
            -->        </ul>
    </div>
    </div>
<!-- end: Main Menu -->
        <div id="content" class="col-lg-10 col-md-10 col-sm-11">
            <!-- Breadcrumb -->
<div class="row">
  <div class="col-sm-12 col-md-12">
    <ol class="breadcrumb">
      <li><a href="/Companies/projectmap">Inicio</a></li>
      <li class=''><a href='#'>Compra</a></li>    </ol>
  </div>
</div>
<!-- Breadcrumb-->                        <input type="hidden" id="bootstrapmessage" value="" />
<div style="  background-color: #FFFFFF;
  border-radius: 11px;
  -moz-border-radius: 11px;
  -webkit-border-radius: 11px;
  border: 1px solid #999999;
  padding: 30px;
  margin: -13px;">
	<div class="row">
		<div class="col-sm-1">&nbsp;</div>
		<div class="col-sm-10">
			<div class="row">
				 <h1><?php echo __('IMPORTA DE FORMA FÁCIL TUS CONTACTOS DE GMAIL');?></h1>
				<p>
					<label for="checkall"><input type="checkbox" id="checkall">&nbsp;<?php echo __('Seleccionar todos');?></label>
				</p>
			</div>
			<div class="row">
				<?php
				echo "<div class='row'>";
				$counter = 0;
				$passedContacts = array();
				foreach($contacts as $id => $contact) {
					if (!in_array($contact['email'],$passedContacts)) {
						if ($contact['is_globeshake_user']) {
							echo '<div class="col-md-3 box minbox isuser">';
						} else {
							echo '<div class="col-md-3 box minbox">';
						}
						if ($contact['is_globeshake_user']) {
							echo $contact['user'].' ('.$contact['email'].') :: USUARIO YA REGISTRADO EN GLOBESHAKE';
						} else {
							echo '<label for="check_list'.$id.'">';
							if ($contact['user'] != $contact['email']) {
								$completeName = $contact['user'].' ('.$contact['email'].')';
							} else {
								$completeName = $contact['user'];
							}
							if ((stristr($string, '-') === FALSE) && (stristr($string, ' ') === FALSE)) {
								$contChar = 50;
							} else { 
								$contChar = 30;
							}
							$completeName = (strlen($completeName) > $contChar) ? substr($completeName,0,$contChar-5).'...' : $completeName;
							echo '<input type="checkbox" class="checkis" id="check_list'.$id.'" name="check_list[]" value="'.$contact['email'].'">&nbsp;'.$completeName.'</label>';
						}
						echo '</div>';
						$counter++;
						if ($counter == 4) {
							echo "</div><div class='row'>";
							$counter = 0;
						}
						$passedContacts[] = $contact['email'];
					}
				} ?>
			</div>
			<hr />
		</div>
		<div class="col-sm-1">&nbsp;</div>
	</div>
</div>

<script>

	/*
$('input:radio[value=C]').click(function() {
		if($('input:radio[value=C]').is(':checked')) {
			$('.tv_panttienda_boxinf').css('outline','5px solid red'); 
		 }
   });
*/
</script>
</div>
        </div>
    </div>
</div>


<div class="clearfix"></div>
<footer>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-3">
                &copy; GlobeShake BETA Copyright @ 2015            </div>
            <div class="col-sm-2">
                <a href="#">Copyright</a>            </div>
            <div class="col-sm-2">
                <a href="#">Condiciones de uso</a>            </div>
            <div class="col-sm-2">
                <a href="#">Política de Privacidad</a>            </div>
            <div class="col-sm-2">
                <a href="#">Directrices comunitarias</a>            </div>
            <div class="col-sm-1">
                <a href="#">Blog</a>            </div>
        </div><!--/.col-->
    </div><!--/.row-->
</footer>

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    
	<script type="text/javascript" src="/js/respond.min.js"></script>
<![endif]-->
<!--[if !IE]>-->
    
	<script type="text/javascript" src="/js/jquery-2.1.0.min.js"></script>
<!--<![endif]-->

<!--[if IE]>
    
	<script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>
<![endif]-->


	<script type="text/javascript" src="/js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/i18n_js/js/i18n_js.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.min.js"></script><script type="text/javascript" src="/js/jquery.mockjax.min.js"></script><script type="text/javascript" src="/js/bootstrap-editable.min.js"></script><script type="text/javascript" src="/js/moment.min.js"></script><script type="text/javascript" src="/js/typeahead.min.js"></script><script type="text/javascript" src="/js/typeaheadjs.min.js"></script><script type="text/javascript" src="/js/address.min.js"></script><script type="text/javascript" src="/js/select2.min.js"></script><script type="text/javascript" src="/js/custom.min.js"></script><script type="text/javascript" src="/js/core.min.js"></script><script type="text/javascript" src="/js/bootbox.min.js"></script><script type="text/javascript">
//<![CDATA[
$(document).ready(function(){
	$('.solicitar').hide();
	$('.ceder').hide();
	$("input[id^='CardsOpTarjeta']").click(function() {
	if (this.value == 'C') {
	  $('.compra').show();
	  $('.solicitar').hide();
	  $('.ceder').hide();
	}
	if (this.value == 'S') {
	  $('.compra').hide();
	  $('.solicitar').show();
	  $('.ceder').hide();
	}
	if (this.value == 'D') {
	  $('.compra').hide();
	  $('.solicitar').hide();
	  $('.ceder').show();
	}
	});
	$('#hregistr_nom').bind('keydown', function(e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        } 
  });
  $('#num_cards').bind('keyup', function(e) {
    var value = 0.99;
    var multiplier = $('#num_cards').val();
    $('span.price').html(round(multiplier*value,2));
  });
   
  $.each( $("[name='radio-buttons']" ), function( i, val ) {
     $(this).bind('click', function() {
      $('span.price').html(round(this.value*9.9,2));
      $('#num_cards').val(this.value*10);
     });
  });
  function round(value, precision, mode) {
    var m, f, isHalf, sgn; // helper variables
    precision |= 0; // making sure precision is integer
    m = Math.pow(10, precision);
    value *= m;
    sgn = (value > 0) | -(value < 0); // sign of the number
    isHalf = value % 1 === 0.5 * sgn;
    f = Math.floor(value);

    if (isHalf) {
    switch (mode) {
      case 'PHP_ROUND_HALF_DOWN':
        value = f + (sgn < 0); // rounds .5 toward zero
        break;
      case 'PHP_ROUND_HALF_EVEN':
        value = f + (f % 2 * sgn); // rouds .5 towards the next even integer
        break;
      case 'PHP_ROUND_HALF_ODD':
        value = f + !(f % 2); // rounds .5 towards the next odd integer
        break;
      default:
        value = f + (sgn > 0); // rounds .5 away from zero
    }
    }

return (isHalf ? value : Math.round(value)) /  m;
  }

  $(".hregistr_nom").bind("change",function(){
    $(".tv_panttienda_precioss").revertFlip();
    return false;
  });
	});

//]]>
</script><script type="text/javascript" src="/js/msdropdown/jquery.dd.min.js"></script><script type="text/javascript" src="/I18nJs/js/i18n_js.js"></script><script type="text/javascript" src="/js/Locale/i18n_js.esp.js"></script><script type="text/javascript">
//<![CDATA[

$(document).ready(function() {
$("#countries").msDropdown();
$( "#countries" ).change(function(e) {
var al = $( "#countries option:selected" ).val();
var url = "/Idiom/changeidiom/"+al; // the script where you handle the form input.
e.preventDefault();
$.ajax({
type: "POST",
url: url,
data: al, // serializes the form's elements.
success: function(data)
{
console.log(data);
window.location.href = data;

},
error : function() {
console.log(e);
}
});
});
});


//]]>
</script>
</body>

</html>