<?php 
$valueBootstrap = (!empty($bootstrap_message)) ? $bootstrap_message : '';

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'jquery.mockjax.min',
  'bootstrap-editable.min',
  'moment.min',
  'typeahead.min',
  'typeaheadjs.min',
  'address.min',
  'select2.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));

$this->Html->scriptStart(array('inline' => false));
?>
$(document).ready(function(){
	$('.solicitar').hide();
	$('.ceder').hide();
	$("input[id^='CardsOpTarjeta']").click(function() {
	if (this.value == 'C') {
	  $('.compra').show();
	  $('.solicitar').hide();
	  $('.ceder').hide();
	}
	if (this.value == 'S') {
	  $('.compra').hide();
	  $('.solicitar').show();
	  $('.ceder').hide();
	}
	if (this.value == 'D') {
	  $('.compra').hide();
	  $('.solicitar').hide();
	  $('.ceder').show();
	}
	});
	$('#hregistr_nom').bind('keydown', function(e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        } 
  });
  $('#num_cards').bind('keyup', function(e) {
    var value = 0.99;
    var multiplier = $('#num_cards').val();
    $('span.price').html(round(multiplier*value,2));
  });
   
  $.each( $("[name='radio-buttons']" ), function( i, val ) {
     $(this).bind('click', function() {
      $('span.price').html(round(this.value*9.9,2));
      $('#num_cards').val(this.value*10);
     });
  });
  function round(value, precision, mode) {
    var m, f, isHalf, sgn; // helper variables
    precision |= 0; // making sure precision is integer
    m = Math.pow(10, precision);
    value *= m;
    sgn = (value > 0) | -(value < 0); // sign of the number
    isHalf = value % 1 === 0.5 * sgn;
    f = Math.floor(value);

    if (isHalf) {
    switch (mode) {
      case 'PHP_ROUND_HALF_DOWN':
        value = f + (sgn < 0); // rounds .5 toward zero
        break;
      case 'PHP_ROUND_HALF_EVEN':
        value = f + (f % 2 * sgn); // rouds .5 towards the next even integer
        break;
      case 'PHP_ROUND_HALF_ODD':
        value = f + !(f % 2); // rounds .5 towards the next odd integer
        break;
      default:
        value = f + (sgn > 0); // rounds .5 away from zero
    }
    }

return (isHalf ? value : Math.round(value)) /  m;
  }

  $(".hregistr_nom").bind("change",function(){
    $(".tv_panttienda_precioss").revertFlip();
    return false;
  });
	});
<?php
$this->Html->scriptEnd();
?>
<input type="hidden" id="bootstrapmessage" value="<?php echo $valueBootstrap?>" />
<div style="  background-color: #FFFFFF;
  border-radius: 11px;
  -moz-border-radius: 11px;
  -webkit-border-radius: 11px;
  border: 1px solid #999999;
  padding: 30px;
  margin: -13px;">
	<div class="row">
		<div class="col-sm-1">&nbsp;</div>
		<div class="col-sm-10">
			<div class="row">
				<div class="col-sm-12 text-center tienda_titulo" style="margin-bottom:20px"><?php echo __('TIENDA DE TARJETAS DE VISITA VIRTUALES GLOBESHAKE');?></div>
				
			</div>
			<hr />
			<div class="row">
				<div class="col-sm-5 text-left tienda_subtitulo">
					<?php echo __('Imprescindible tenerlas para darse a conocer y obtener nuevos contactos');?>
				</div>
				<div class="col-sm-5 text-center">
					
					<img src="/img/img_tarjetavisita.png" alt="<?php echo __('Tarjetas de visita');?>" title="<?php echo __('Tarjetas de visita');?>" width="65%">
				</div>
			</div>
			<hr />
		</div>
		<div class="col-sm-1">&nbsp;</div>
		<div class="col-sm-12">
			<?php echo $this->Form->create('Cards', array('action' => 'add'));?>
				<div class="row col-sm-offset-1 col-md-offset-1 col-xs-offset-1 col-md-10 col-sm-10 col-xs-10 col-sm-offset-1 col-md-offset-1 col-xs-offset-1 " style="margin-bottom:20px">
					<table class="table table-bordered table-striped">
						<thead></thead>
						<tbody>
							<tr>
								<td>
									<input type="radio" value="C" id="CardsOpTarjetaC" name="data[Cards][opTarjeta]" checked /><span style="margin-left:10px"><?php echo __('COMPRAR');?></span>
								</td>
								<td>
									<input type="radio" value="S" id="CardsOpTarjetaS" name="data[Cards][opTarjeta]" /><span style="margin-left:10px"><?php echo __('SOLICITAR');?></span>
								</td>
            					<td>
            						<input type="radio" value="D" id="CardsOpTarjetaD" name="data[Cards][opTarjeta]"  /><span style="margin-left:10px"><?php echo __('CEDER');?></span>
            					</td>
            				</tr>
            			</tbody>
            		</table>
			</div>
			<!-- <div class="selCompra">
		      <div class="tv_panttienda_tarjevirtcuatr">
		      <input type="radio" class="tv_panttienda_C_but" name="opTarjeta" value="C" title="Compre tarjetas y no pase desapercibido entre nuevos contactos" required>&nbsp;<?php echo __('COMPRAR');?>
		      </div>
		      <div class="tv_panttienda_tarjevirtcuatr variospantiendados">
		      <input type="radio" class="tv_panttienda_C_but" name="opTarjeta" value="S" title="Solicite tarjetas a un superior" required>&nbsp;<?php echo __('SOLICITAR');?>
		      </div>
		      <div class="tv_panttienda_tarjevirtcuatr variospantienda">
		      <input type="radio" class="tv_panttienda_C_but" name="opTarjeta" value="D" title="Ceda tarjetas y haga que su equipo multiplique las posibilidades de dar a conocer la empresa" required>&nbsp;<?php echo __('CEDER');?>
		      </div>
		    </div> -->
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-1">&nbsp;</div>
					<div class="col-sm-10" style="background-color: #DDC079;">
						<div class="row">
							<div class="col-sm-6 text-left tienda_box">
								<p>
								<?php echo __('Por cada contacto nuevo en GLOBESHAKE, recibirá gratis 1 tarjeta de visita virtual.');?>
								</p>
							</div>
							<div class="col-sm-6 text-justify">
								<div class="tienda_box_boton">
									<?php $direccion = 'https://accounts.google.com/o/oauth2/auth?client_id=' . $clientId . '&redirect_uri=' . $redirectUri . '&scope=https://www.google.com/m8/feeds/&response_type=code';?>
									<a href="<?php echo $direccion?>">
											<?php echo __('Descarga nuevos contactos');?>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-1">&nbsp;</div>
				</div>
			</div>
			<div class="solicitar">
		    	<div class="col-md-offset-1 col-sd-offset-1 col-xs-offset-1 col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sd-offset-1 col-xs-offset-1 ">
			    	<table class="table">
						<thead></thead>
						<tbody>
							<tr>
								<td>
									<span class="strong" style="font-size:1.5em;"><?php echo __('SOLICITAR');?></span>
								</td>
	        				</tr>
	        			</tbody>
	        		</table>
				</div>
		    </div>

		    <div class="ceder">
		    	<div class="col-md-offset-1 col-sd-offset-1 col-xs-offset-1 col-md-10 col-sm-10 col-xs-10 col-md-offset-1 col-sd-offset-1 col-xs-offset-1 ">
			    	<table class="table">
						<thead></thead>
						<tbody>
							<tr>
								<td>
									<span class="strong" style="font-size:1.5em"><?php echo __('CEDER');?></span>
								</td>
	        				</tr>
	        			</tbody>
	        		</table>
				</div>
		    </div>

		    <div class="compra">
		    	<div class="col-sm-offset-1 col-sm-10 col-sm-offset-1">
		    		<div class="row" style="margin-top:10px;">
			    		<div class="col-sm-12 tienda_titulo_precios" >
			    		<?php echo __('PRECIOS Y CANTIDADES');?>
			    		</div>
			    	</div>
			    	<div class="row" style="border-bottom: 1px dotted #666666;">
		    			<div class="col-sm-4 tienda_box_1">
			    			<img src="/img/tvirtu_azul.png">
							<p><?php echo __('1 tarjeta');?> </p>
							<span class="tienda_box_precio"><?php echo __('0,99 €');?></span>
			    		</div>
			    		<div class="col-sm-4 tienda_box_2">
			    			<p><?php echo __('Número de tarjetas deseado');?>...</p>
							<input id="num_cards" type="text" name="num_cards" required>
							<p class="tv_panttienda_tarjjc"><?php echo __('tarjetas');?></p>
							<p class="tv_panttienda_precioss"><?php echo __('Precio');?> 
							<span class="price">XXX,XX</span> 
							<span class="tv_panttienda_eur">€</span><p>
			    		</div>
			    		<div class="col-sm-5 tienda_box_3">
			    			<p>...<?php echo __('o escoja entre uno de nuestros');?> <span class="tv_panttienda_pack"><?php echo __('PACKS');?>:</span></p>
					    <ul> 
					        <li class="tarjetas">
					        	<input type="radio" class="radio tarjetas" name="radio-buttons" value="1" id="one" />
					            <label for="one">
					            	<img src="/img/packtvirtu01.png" alt="<?php echo __('10 tarjetas');?>" title="<?php echo __('10 tarjetas');?>">
					            	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo __('10 tarjetas');?>................<span>9,90 €</span></p>
								</label>
							</li>
					        <li class="tarjetas">
					        	<input type="radio" class="radio tarjetas" name="radio-buttons" value="2" id="two" />
					            <label for="two">
					            	<img src="/img/packtvirtu02.png" alt="<?php echo __('20 tarjetas');?>" title="<?php echo __('20 tarjetas');?>">
					            	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo __('20 tarjetas');?>................<span>9,90 €</span></p>
								</label>
							</li>
					        <li class="tarjetas">
					        	<input type="radio" class="radio tarjetas" name="radio-buttons" value="3" id="three" />
					            <label for="three">
					            	<img src="/img/packtvirtu03.png" alt="<?php echo __('30 tarjetas');?>" title="<?php echo __('30 tarjetas');?>">
					            	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo __('30 tarjetas');?>................<span>9,90 €</span></p>
								</label>
							</li>
					        <li class="tarjetas">
					        	<input type="radio" class="radio" name="radio-buttons" value="4" id="four" />
					            <label for="four">
					            	<img src="/img/packtvirtu04.png" alt="<?php echo __('40 tarjetas');?>" title="<?php echo __('40 tarjetas');?>">
					            	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo __('40 tarjetas');?>................<span>9,90 €</span></p>
								</label>
							</li>
					    </ul>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 tienda_condiciones">
					<p>
	            		<input class="tv_panttienda_tarjvitr_tarjevirtse" type="checkbox" name="t 30" checked="checked" value="si" required="">He leido y acepto las <a href="#">Condiciones de pago</a> y la <a href="#">Política de compra</a> 
	            	</p>
	            	<input type="submit" id="tarjvitr_aceptartres" name="buy_visit_card" value="Continuar">
	        	</div>
			</div>

			
			



			<?php echo $this->Form->end();?>
		</div>
	</div>
</div>

<script>

	/*
$('input:radio[value=C]').click(function() {
		if($('input:radio[value=C]').is(':checked')) {
			$('.tv_panttienda_boxinf').css('outline','5px solid red'); 
		 }
   });
*/
</script>
</div>
