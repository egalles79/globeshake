<div class="countries view">
<h2><?php echo __('Country'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($country['Country']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($country['Country']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($country['Country']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($country['Country']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($country['Country']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code 2'); ?></dt>
		<dd>
			<?php echo h($country['Country']['code_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Latitude'); ?></dt>
		<dd>
			<?php echo h($country['Country']['latitude']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Altitude'); ?></dt>
		<dd>
			<?php echo h($country['Country']['altitude']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Continent'); ?></dt>
		<dd>
			<?php echo $this->Html->link($country['Continent']['name'], array('controller' => 'continents', 'action' => 'view', $country['Continent']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Country'), array('action' => 'edit', $country['Country']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Country'), array('action' => 'delete', $country['Country']['id']), array(), __('Are you sure you want to delete # %s?', $country['Country']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Continents'), array('controller' => 'continents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Continent'), array('controller' => 'continents', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countrypoints'), array('controller' => 'countrypoints', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Countrypoint'), array('controller' => 'countrypoints', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Cities'), array('controller' => 'cities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List User Projects'), array('controller' => 'user_projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Project'), array('controller' => 'user_projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Countrypoints'); ?></h3>
	<?php if (!empty($country['Countrypoint'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Country'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('Countrypoint'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($country['Countrypoint'] as $countrypoint): ?>
		<tr>
			<td><?php echo $countrypoint['id']; ?></td>
			<td><?php echo $countrypoint['country']; ?></td>
			<td><?php echo $countrypoint['country_id']; ?></td>
			<td><?php echo $countrypoint['countrypoint']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'countrypoints', 'action' => 'view', $countrypoint['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'countrypoints', 'action' => 'edit', $countrypoint['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'countrypoints', 'action' => 'delete', $countrypoint['id']), array(), __('Are you sure you want to delete # %s?', $countrypoint['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Countrypoint'), array('controller' => 'countrypoints', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Cities'); ?></h3>
	<?php if (!empty($country['City'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Latitude'); ?></th>
		<th><?php echo __('Longitude'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($country['City'] as $city): ?>
		<tr>
			<td><?php echo $city['id']; ?></td>
			<td><?php echo $city['name']; ?></td>
			<td><?php echo $city['latitude']; ?></td>
			<td><?php echo $city['longitude']; ?></td>
			<td><?php echo $city['country_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'cities', 'action' => 'view', $city['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'cities', 'action' => 'edit', $city['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'cities', 'action' => 'delete', $city['id']), array(), __('Are you sure you want to delete # %s?', $city['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New City'), array('controller' => 'cities', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related User Projects'); ?></h3>
	<?php if (!empty($country['UserProject'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Actual Company'); ?></th>
		<th><?php echo __('Name'); ?></th>
		<th><?php echo __('Init Date'); ?></th>
		<th><?php echo __('End Date'); ?></th>
		<th><?php echo __('On Going'); ?></th>
		<th><?php echo __('Budget'); ?></th>
		<th><?php echo __('Tags'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('Sector Id'); ?></th>
		<th><?php echo __('Company Id'); ?></th>
		<th><?php echo __('Currency Id'); ?></th>
		<th><?php echo __('Companies Users Id'); ?></th>
		<th><?php echo __('Geolocalitzation Project Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($country['UserProject'] as $userProject): ?>
		<tr>
			<td><?php echo $userProject['id']; ?></td>
			<td><?php echo $userProject['created']; ?></td>
			<td><?php echo $userProject['modified']; ?></td>
			<td><?php echo $userProject['actual_company']; ?></td>
			<td><?php echo $userProject['name']; ?></td>
			<td><?php echo $userProject['init_date']; ?></td>
			<td><?php echo $userProject['end_date']; ?></td>
			<td><?php echo $userProject['on_going']; ?></td>
			<td><?php echo $userProject['budget']; ?></td>
			<td><?php echo $userProject['tags']; ?></td>
			<td><?php echo $userProject['description']; ?></td>
			<td><?php echo $userProject['country_id']; ?></td>
			<td><?php echo $userProject['sector_id']; ?></td>
			<td><?php echo $userProject['company_id']; ?></td>
			<td><?php echo $userProject['currency_id']; ?></td>
			<td><?php echo $userProject['companies_users_id']; ?></td>
			<td><?php echo $userProject['geolocalitzation_project_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'user_projects', 'action' => 'view', $userProject['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'user_projects', 'action' => 'edit', $userProject['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'user_projects', 'action' => 'delete', $userProject['id']), array(), __('Are you sure you want to delete # %s?', $userProject['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User Project'), array('controller' => 'user_projects', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Users'); ?></h3>
	<?php if (!empty($country['User'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Username'); ?></th>
		<th><?php echo __('Firstname'); ?></th>
		<th><?php echo __('Lastname'); ?></th>
		<th><?php echo __('Email'); ?></th>
		<th><?php echo __('Phone'); ?></th>
		<th><?php echo __('Password'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('City Id'); ?></th>
		<th><?php echo __('Function Id'); ?></th>
		<th><?php echo __('Sector Id'); ?></th>
		<th><?php echo __('Group Id'); ?></th>
		<th><?php echo __('Photo'); ?></th>
		<th><?php echo __('Photo Dir'); ?></th>
		<th><?php echo __('Notification Date'); ?></th>
		<th><?php echo __('Notification Msg'); ?></th>
		<th><?php echo __('Notification News'); ?></th>
		<th><?php echo __('Notification Task'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Code Deactivation'); ?></th>
		<th><?php echo __('Alias Email'); ?></th>
		<th><?php echo __('Birthdate'); ?></th>
		<th><?php echo __('Gender'); ?></th>
		<th><?php echo __('Courses'); ?></th>
		<th><?php echo __('Idioms'); ?></th>
		<th><?php echo __('Other Interests'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($country['User'] as $user): ?>
		<tr>
			<td><?php echo $user['id']; ?></td>
			<td><?php echo $user['created']; ?></td>
			<td><?php echo $user['modified']; ?></td>
			<td><?php echo $user['username']; ?></td>
			<td><?php echo $user['firstname']; ?></td>
			<td><?php echo $user['lastname']; ?></td>
			<td><?php echo $user['email']; ?></td>
			<td><?php echo $user['phone']; ?></td>
			<td><?php echo $user['password']; ?></td>
			<td><?php echo $user['country_id']; ?></td>
			<td><?php echo $user['city_id']; ?></td>
			<td><?php echo $user['function_id']; ?></td>
			<td><?php echo $user['sector_id']; ?></td>
			<td><?php echo $user['group_id']; ?></td>
			<td><?php echo $user['photo']; ?></td>
			<td><?php echo $user['photo_dir']; ?></td>
			<td><?php echo $user['notification_date']; ?></td>
			<td><?php echo $user['notification_msg']; ?></td>
			<td><?php echo $user['notification_news']; ?></td>
			<td><?php echo $user['notification_task']; ?></td>
			<td><?php echo $user['code']; ?></td>
			<td><?php echo $user['code_deactivation']; ?></td>
			<td><?php echo $user['alias_email']; ?></td>
			<td><?php echo $user['birthdate']; ?></td>
			<td><?php echo $user['gender']; ?></td>
			<td><?php echo $user['courses']; ?></td>
			<td><?php echo $user['idioms']; ?></td>
			<td><?php echo $user['other_interests']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), array(), __('Are you sure you want to delete # %s?', $user['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
