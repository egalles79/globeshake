
var visibilityGestor = visibilityGestor || { toggleInput : {} };

visibilityGestor.init = function(){
    this.toggleInput.init();
}

visibilityGestor.toggleInput = {

    surveySection : null,
    appsSection : null,
    urlSection : null,
    gamificationType : null,

    init : function(){
        this.surveySection = $('iframe#surveySection');
        this.appsSection = $('div#appsSection');
        this.urlSection = $('div#urlSection');
        this.gamificationType = $('#GamificationGamificationTypeId');

        this.toggle( this.gamificationType.val() );

        this.gamificationType.change(function(){
            visibilityGestor.toggleInput.toggle( $(this).val() );
        });
    },
    toggle : function( value ){
        if( value == 8 ){
            this.appsSection.show();
            this.urlSection.hide();
            this.surveySection.hide();
        }else if( value == 7 ){
            this.surveySection.show();
            this.appsSection.hide();
            this.urlSection.hide();
        }else{
            this.urlSection.show();
            this.surveySection.hide();
            this.appsSection.hide();
        }
    }
}

$(document).ready(function(){
    visibilityGestor.init();
});