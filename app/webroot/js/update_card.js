$(document).ready(function(e){
  var projects     = JSON.parse($('#userprojects').val());
  var venimage     = $('#venimage').val();
  var imagen_verde = $('#imagenverde').val();
  var imagen_gris  = $('#imagengris').val();
  var projectLines = '';
  $.each(projects, function(index) {
  	projectLines += "<li>"+(projects[index]['on_going'] == "1") ? imagen_verde : imagen_gris;
  	projectLines += ' xx '+projects[index]['name']+': '+projects[index]['description']+"</li>";
  });

  $(".right").on("click",function(){
    var $this = $(this);
    console.log($this.attr("rel"));
    $(".tarjvitr_contres").flip({
      direction: $this.attr("rel"),
      content: '<a href="#" class="revert">'+venimage+'</a><a class="num_pad_up">up</a><br /><a class="num_pad_down">down</a><br /><div class="tarjvitr_concuatro"><div class="tarjvitr_mcv">'+I18nJs.t('MI CV')+'</div><h6 class="sub_title">'+I18nJs.t('Su CV on-line, proyecto a proyecto le hará visible')+'.</h6><a href="/UserProjects/add" target="_parent">'+I18nJs.t('Anadir proyecto')+'</a><div id="pane-target">'+projectLines+'</div><div class="tarjvitr_cvempresdos"><a href="#" class="tarjvitr_en">RESUMEN MI CV</a></div><div class="tarjvitr_accredes"><p class="tarjvitr_ress" style="float:left;">Acceso a mis redes sociales</p><label style="margin-left:25px" for="redessocialessi"><input type="radio" id="redessocialessi" name="redessociales" value="1" />Si</label><label style="margin-left:25px" for="redessocialesno"><input type="radio" id="redessocialesno" name="redessociales" value="2" />No</label></div><div class="socialb social-headb" style="float:right;margin-top: -30px;margin-right: 10px;"><a href="#" title="Facebook"><i class="icon-facebook"></i></a><a href="#" title="Twitter"><i class="icon-twitter"></i></a><a href="#" title="Rss"><i class="icon-rss"></i></a></div></div>',
      onBefore: function(){
        $("a.revert").show();
        $('a.right').hide();
      },
    })
    return false;
  });

  var mk = 0;
  var test = $('#pane-target')[0].scrollHeight;
  var limiting = test - 150;
  console.log(test);
  console.log(limiting);
  $('.num_pad_down').click(function() {
    if(test <= mk){
       mk += 31;
       console.log(mk);
      $('#pane-target').animate({ scrollTop: mk }, 600);
    } else{};
  });
  $('.num_pad_up').click(function() {
    if(mk >= 31){
      mk -= 31;
      console.log(mk);
      $('#pane-target').animate({ scrollTop: mk }, 600);
    } else{};
  });
  $(".continuar").on("click",function(){
    $(".abso_tarjeta").flip({
      direction: 'tb',
      onEnd: function(){
        $('.abso_tarjeta').css("display","none");
        $('.tarjvitr_contres').css("display","block");
        $('#flipPad').css("display","block");
        $('#tarjvitr_aceptar').css("display","inline-block");
      },
    })
    return false;
  });
  
  $(function() {
    $( "#slider" ).slider({
      range: "min",
      value: $('#percent').val(),
      min:1,
      max: 100,
    });
    var percent = $( "#slider" ).slider( "value" );
    if (percent != 1) {
      $('.amount').append(percent);  
    }
    
  });
});