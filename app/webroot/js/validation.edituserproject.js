$('#but1').click(function(){
  alertError = '<ul>';
  if (($('#UserProjectInitDate').val() == '')) {
      alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar una fecha incio/fin de proyecto para poder editar')+'</li>';
  }
  if (($('#UserProjectsSectorProj').val() == '')) {
      alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar un sector del proyecto para poder editar')+'</li>';
  }
  
  if (($('#UserProjectCompanyName').val() == '')) {
      alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar un nombre de la compañía para poder editar')+'</li>';
  }
  if (($('#UserProjectName').val() == '')) {
      alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar un nombre para el proyecto para poder editar')+'</li>';
  }
  if (($('#UserProjectsEndDate').val() == '') && (!$('#UserProjectOnGoing').prop('checked'))) {
      alertError = alertError + '<li>'+I18nJs.t('Debe seleccionar una fecha final para el proyecto o pulsar en curso')+'</li>';
  }
  alertError = alertError+'</ul>';

  valorLocalizacion = $('#UserProjectLatitude').val();
  if (valorLocalizacion.length == 0) {
    bootbox.alert(I18nJs.t('Debe tener una localización exacta para poder editar. En caso de no tenerla arrastre el puntero hacia el lugar que le parezca más cercano'));
  }
  
  if (alertError != '<ul></ul>') {
    console.log('entro')
    bootbox.alert(alertError);
    return false;
  }
  $('#UserProjectEditForm').submit();
});