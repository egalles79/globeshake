/* ---------- functions used to demonsatration ---------- */

$('#mainCharts a:last').tab('show');
$('#mainCharts a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
});


function randNum(){
    return ((Math.floor( Math.random()* (1+40-20) ) ) + 20)* 1200;
}

function randNum2(){
    return ((Math.floor( Math.random()* (1+40-20) ) ) + 20) * 500;
}

function randNum3(){
    return ((Math.floor( Math.random()* (1+40-20) ) ) + 20) * 300;
}

function randNum4(){
    return ((Math.floor( Math.random()* (1+40-20) ) ) + 20) * 100;
}

function randNum5(){
    return ((Math.floor( Math.random()* (1+40-20) ) ) + 1) * 1;
}

function chart24() {

    var tickets = [[1, 5+randNum3()], [2, 10+randNum3()], [3, 15+randNum3()], [4, 20+randNum3()],[5, 25+randNum3()],[6, 30+randNum3()],[7, 35+randNum3()],[8, 40+randNum3()],[9, 45+randNum3()],[10, 50+randNum3()],[11, 55+randNum3()],[12, 60+randNum3()],[13, 65+randNum3()],[14, 70+randNum3()],[15, 75+randNum3()],[16, 80+randNum3()],[17, 85+randNum3()],[18, 90+randNum3()],[19, 85+randNum3()],[20, 80+randNum3()],[21, 75+randNum3()],[22, 80+randNum3()],[23, 75+randNum3()],[24, 70+randNum3()]];
    var solved = [[1, 5+randNum3()], [2, 10+randNum3()], [3, 15+randNum3()], [4, 20+randNum3()],[5, 25+randNum3()],[6, 30+randNum3()],[7, 35+randNum3()],[8, 40+randNum3()],[9, 45+randNum3()],[10, 50+randNum3()],[11, 55+randNum3()],[12, 60+randNum3()],[13, 65+randNum3()],[14, 70+randNum3()],[15, 75+randNum3()],[16, 80+randNum3()],[17, 85+randNum3()],[18, 90+randNum3()],[19, 85+randNum3()],[20, 80+randNum3()],[21, 75+randNum3()],[22, 80+randNum3()],[23, 75+randNum3()],[24, 70+randNum3()]];

    var plot = $.plot($("#chart-24h"),
        [ { data: tickets, label: "Tickets" },
            { data: solved, label: "Solved Tickets"} ], {
            series: {
                lines: {
                    show: true,
                    lineWidth: 2,
                    fill: true,
                    fillColor: { colors: [ { opacity: 0.1 }, { opacity: 0.1 } ] }
                },
                points: {
                    show: true,
                    lineWidth: 2
                },
                shadowSize: 0
            },
            grid: {
                hoverable: true,
                clickable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            colors: ["#bdea74", "#2FABE9"],
            xaxis: {ticks:10, tickDecimals: 0, tickColor: "#fff"},
            yaxis: {ticks:5, tickDecimals: 0, tickColor: "#e9ebec"}
        });

    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#dfeffc',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#chart-24h").bind("plothover", function (event, pos, item) {

        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                showTooltip(item.pageX, item.pageY,item.series.label + " of " + x + " = " + y);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });
}

function chartWeek(){

    function gd(year, month, day) {
        return new Date(year, month - 1, day).getTime();
    }

    var data1 = [
        [gd(2013, 1, 2), 1690.25], [gd(2013, 1, 3), 1696.3], [gd(2013, 1, 4), 1659.65], [gd(2013, 1, 5), 1668.15], [gd(2013, 1, 6), 1656.1], [gd(2013, 1, 7), 1668.65], [gd(2013, 1, 8), 1668.15]
    ];

    var dayOfWeek = ["Sun", "Mon", "Tue", "Wed", "Thr", "Fri", "Sat"];

    var plot = $.plot($("#chart-week"),
        [ { data: data1} ], {
            series: {
                lines: {
                    show: true,
                    lineWidth: 3,
                    fill: false
                },
                points: {
                    show: true,
                    lineWidth: 3,
                    fill: true,
                    fillColor: "#fff"
                },
                shadowSize: 0
            },
            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#e9ebec",
                borderWidth: 0
            },
            colors: ["#b2b8bd"],
            xaxis: {
                mode: "time",
                tickFormatter: function (val, axis) {
                    return dayOfWeek[new Date(val).getDay()];
                },
                font: {
                    color: "#b2b8bd"
                },
                ticks:7,
                tickColor: "#fff",
                alignTicksWithAxis: 1,
                autoscaleMargin: 0.02
            },
            yaxis: {
                font: {
                    color: "#b2b8bd"
                },
                ticks:4,
                tickDecimals: 0
            }
        });

    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#dfeffc',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#chart-week").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                showTooltip(item.pageX, item.pageY,
                    item.series.label + " of " + x + " = " + y);
            }
        }
        else {
            $("#tooltip").remove();
            previousPoint = null;
        }

    });

}

function chartMonth(){

    var data1 = [[0, 0.5], [1, 1], [2, 1.5], [3, 2],[4, 2.5], [5, 2], [6, 1.5], [7, 1.5],[8, 2], [9, 2.5], [10, 2.5], [11, 3],[12, 3], [13, 2.5], [14, 2.5], [15, 2],[16, 3], [17, 2.5], [18, 2], [19, 1.5],[20, 1], [21, 0.5], [22, 1], [23, 1],[24, 1.5], [25, 2], [26, 2.5], [27, 3],[28, 2.5], [29, 2], [30, 1.5], [31, 1]];
    var data2 = [[0, 1], [1, 2], [2, 3], [3, 4],[4, 5], [5, 4], [6, 3], [7, 3],[8, 4], [9, 5], [10, 5], [11, 6],[12, 6], [13, 5], [14, 5], [15, 4],[16, 6], [17, 5], [18, 4], [19, 3],[20, 2], [21, 1], [22, 2], [23, 2],[24, 3], [25, 4], [26, 5], [27, 6],[28, 5], [29, 4], [30, 3], [31, 2]];

    var plot = $.plot($("#chart-month"),

        [ {
            data: data2,
            bars: {
                show: true,
                fill: false,
                barWidth: 0.1,
                align: "center",
                lineWidth: 16
            }
        },{ data: data2,
            label: "Visits",
            lines: {
                show: true,
                lineWidth: 1.5
            },
            points: {
                show: true,
                lineWidth: 2,
                fill: true
            },
            shadowSize: 0
        }	,{ data: data1,
            label: "Visits",
            lines: {
                show: true,
                lineWidth: 0.5
            },
            points: {
                show: true,
                lineWidth: 0.5,
                fill: true
            },
            shadowSize: 0
        }
        ], {

            grid: {
                hoverable: true,
                clickable: true,
                tickColor: "#f7f7f7",
                borderWidth: 0,
                labelMargin: 10,
                margin: {
                    top: 0,
                    left: 5,
                    bottom: 0,
                    right: 0
                }
            },
            legend: {
                show: false
            },
            colors: ["rgba(190,190,190,0.3)","#b2b8bd"],
            xaxis: {ticks:5, tickDecimals: 0, tickColor: "#fff"},
            yaxis: {ticks:3, tickDecimals: 0}
        }
    );

    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css( {
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid #fdd',
            padding: '2px',
            'background-color': '#dfeffc',
            opacity: 0.80
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null;
    $("#chart-month").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;

                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);

                showTooltip(item.pageX, item.pageY,
                    item.series.label + " of " + x + " = " + y);
            }
        } else {
            $("#tooltip").remove();
            previousPoint = null;
        }

    });

}
