$(document).ready(function(){
	/* ---------- FuelUX Wizard ---------- */
    var wizard = $('#MyWizard');

    wizard.on('finished', function(e, data) {
        if( $('input[name="data[Gamification][gamification_type_id]"]:checked').val() == 7 ){
            $.ajax({
                url : '/gamifications/haveSurveyOnSession',
                type : 'post',
                success : function( data, status, xhr ){
                    if( data == true ){
                        $('#GamificationAddForm').submit();
                    }else{
                        alert('Save the survey before send the form');
                    }
                }
            });
        }else{
            $('#GamificationAddForm').submit();
        }
    });

    wizard.on('change', function(e, data) {
        if( data.step == 2 ){
            switch( parseInt( $('input[name="data[Gamification][gamification_type_id]"]:checked').val() ) ){
                case 1:;
                case 2:;
                case 3:;
                case 4:;
                case 5:;
                case 6:
                    $('div#urlSection').show();
                    $('div#surveyWrapper').hide();
                    $('div#appsSection').hide();
                    break;
                case 7:
                    $('div#surveyWrapper').show();
                    $('div#urlSection').hide();
                    $('div#appsSection').hide();
                    break;
                case 8:
                    $('div#appsSection').show();
                    $('div#surveyWrapper').hide();
                    $('div#urlSection').hide();
                    break;
            }
        }
    });

    wizard.wizard();

	/* ---------- Datapicker ---------- */
	$('.datepicker').datepicker();

	/* ---------- Choosen ---------- */
	$('[data-rel="chosen"],[rel="chosen"]').chosen();

	/* ---------- Placeholder Fix for IE ---------- */
	$('input, textarea').placeholder();

	/* ---------- Auto Height texarea ---------- */
	$('textarea').autosize();   
});