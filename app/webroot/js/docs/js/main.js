(function() {
  var $confirm;

  $confirm = null;

  $(function() {
    function volverAtras() {
      $('.btn-prev').click();
    }
    function stopRKey(evt) {
      var evt = (evt) ? evt : ((event) ? event : null);
      var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
      if ((evt.keyCode == 13)) {
        if ($('#step3').hasClass('active')) {
          $('#addemails').click();
        } 
        if ($('#step4').hasClass('active')) {
          $('.saveform').click();
        }
        return false;
      }
    }
    document.onkeypress = stopRKey;
    //logica show / hide al entrar en wizard
    $('.btn-next').on('click', function() {
      //logica de los errores paso a paso
      if ($('#step1').hasClass('active')) {
        alertMessage = '';
        if ($('#ReportFields').val() == null) {
          alertMessage = alertMessage + I18nJs.t("Debe seleccionar almenos un campo a mostrar")+"<br />"; 
        }
        fechasCorrectas = true;
        if (($('#ReportBirthdateFormat1').val() != '') && ($('#ReportBirthdate1').val() == '')) {
          fechasCorrectas = false;
        }
        if (($('#ReportBirthdateFormat1').val() == '') && ($('#ReportBirthdate1').val() != '')) {
          fechasCorrectas = false;
        }
        if (($('#ReportBirthdateFormat2').val() != '') && ($('#ReportBirthdate2').val() == '')) {
          fechasCorrectas = false;
        }
        if (($('#ReportBirthdateFormat2').val() == '') && ($('#ReportBirthdate2').val() != '')) {
          fechasCorrectas = false;
        }
        if ((($('#ReportBirthdateFormat2').val() != '') && ($('#ReportBirthdate2').val() != ''))&&
           (($('#ReportBirthdateFormat1').val() == '') || ($('#ReportBirthdate1').val() == ''))) {
          fechasCorrectas = false;
          alertMessage = alertMessage + I18nJs.t("Seleccione un filtro para la primera fecha si selecciona un filtro para la segunda fecha o ponga el filtro en la primer input de las fechas")+"<br />"; 
        }
        if (!fechasCorrectas) {
          alertMessage = alertMessage + I18nJs.t("Las fechas seleccionadas no son correctas")+"<br />"; 
        }
        if (alertMessage != '') {
          bootbox.alert(alertMessage,volverAtras);
        }
      }
      if ($('#step2').hasClass('active')) {
        alertMessage = '';
        if ($('#frequencyvalues').val() == '') {
          alertMessage = alertMessage + I18nJs.t("Debe seleccionar una frecuencia de envío")+"<br />"; 
        }
        if ($('#frequencyvalues').val() == 'semana') {
          if ($('#frequencyvaluesday').val() == '' ) {
            alertMessage = alertMessage + I18nJs.t("Si selecciona frecuencia semanal debe seleccionar el día de envío")+"<br />"; 
          }
        }
        if (alertMessage != '') {
          bootbox.alert(alertMessage,volverAtras);
          event.preventDefault();
          return false;
        }
      }

      if ($('#step3').hasClass('active')) {
        if ($('#ReportEmailvalues').val() == '') {
          bootbox.alert(I18nJs.t("Debe seleccionar almenos un email de envío"),volverAtras);
          return false;
        }
      }
      if ($('#step4').hasClass('active')) {
        $('.saveform').click();
      }

    });
    $('#odd0').hide();
    if ($('input[name="data[Report][editValue]"]') && !$('#datafrequency2').attr("checked")) {
      $('input[name="data[weekDay]"]').bootstrapSwitch('disabled', true);  
    }
    if ($('input[name="data[Report][nameform]"]').val() == '') {
      $('#divNameForm').hide();
    }
    $('#datebirthdayplus').hide();
    $('.remove').hide();

    //logica filtro postalcodes
    $('#FiltersPostalCodes').on('change', function() {
      var filters = [];
      $('#FiltersPostalCodes_chosen').find('.chosen-choices').find('li').find('span').each(function (index) {
        filters.push($(this).text());
      });
      if ($('#FiltersPostalCodes_chosen').find('.chosen-choices').find('li').find('span').length == 0) {
          $('#postalcodesvalues').val('');
          return false;    
      }
      $('#postalcodesvalues').val(filters);
    });

    //logica filtro provincias
    $('#FiltersStates').on('change', function() {
      var filters = [];
      $('#FiltersStates_chosen').find('.chosen-choices').find('li').find('span').each(function (index) {
        filters.push($(this).text());
      });
      if ($('#FiltersStates_chosen').find('.chosen-choices').find('li').find('span').length == 0) {
          $('#statesvalues').val('');
          return false;
      }
      $('#statesvalues').val(filters);
    });

    //logica filtro provincias
    $('#FiltersCountries').on('change', function() {
      var filters = [];
      $('#FiltersCountries_chosen').find('.chosen-choices').find('li').find('span').each(function (index) {
        filters.push($(this).text());
      });
      if ($('#FiltersCountries_chosen').find('.chosen-choices').find('li').find('span').length == 0) {
          $('#countryvalues').val('');
          return false;
      }
      $('#countryvalues').val(filters);
    });

    //logica filtro ciudades
    $('#FiltersCities').on('change', function() {
      var filters = [];
      $('#FiltersCities_chosen').find('.chosen-choices').find('li').find('span').each(function (index) {
        filters.push($(this).text());
      });
      if ($('#FiltersCities_chosen').find('.chosen-choices').find('li').find('span').length == 0) {
          $('#cityvalues').val('');
          return false;
      }
      $('#cityvalues').val(filters);
    });

    //logica eliminar emails pestaña emails
    $('.removemail').on('click', function () {
      emailremove = $(this).parent().parent().find('.sorting_1').text();
      str         = $('#ReportEmailvalues').val();
      var inicio = str.indexOf(emailremove);
      var res    = str.substring(0, inicio)+str.substring(inicio+emailremove.length, str.length);
      res = res.replace(',,', ',');
      if (res == ',') {
        res = '';
      }
      $('#ReportEmailvalues').val(res);
      $(this).parent().parent().remove();
    });

    //logica añadir emails pestaña emails
    $('#addemails').on('click', function () {
      if ($('#listEmails').val()) { //check email is not null field
        emailTextVal = $('#listEmails').val();
        var emails = emailTextVal.split(",");
        
        //check valid emails
        var noterrors = true;
        var validEmails = [];
        for (var i=0; i<emails.length; i++) { 
          if (validateEmail(emails[i])) {
            validEmails.push(emails[i]);
          } else {
            noterrors = false;
            break;
          }
        }

        if (noterrors) {
          str = $('#ReportEmailvalues').val();
          if (str == null) {
            str = '';
          }
          for (i in validEmails) {
            if (str.indexOf(validEmails[i]) == -1) {
              $('#ReportEmailvalues').val(validEmails[i]+','+$('#ReportEmailvalues').val());  //add mail to session
              var $tr = $('tr[id^="odd"]:last'); // 
              var id = +$tr[0].id.match(/\d+/g) + 1; // Number out of ID and +1
              $tr.after( $tr.clone( true ).attr('id', 'odd'+id ) ); // Clone and assign new ID
              var $tr = $('tr[id^="odd"]:last'); // get last el which ID starts with "klon".
              $tr.find('.sorting_1').text(validEmails[i]);
              $tr.show();
            }
          }
          $('#listEmails').val('');
        } else {
          bootbox.alert(I18nJs.t("Errores en el campo de email, algún email no es válido"));
          return false;
        }
      } else {
        bootbox.alert("No puede estar vacío");
        return false;
      }
    });

    $('#toggle-state-switch-button-birthday').on('click', function () {
      $('.birthdaytext1').val('');
      $('.birthdaytext2').val('');
      $('#ReportBirthdateFormat1').val('');
      $('#ReportBirthdateFormat2').val('');
      $('#birthdayvalues').val('');
    });


    //logica filtros gender
    $('#toggle-state-switch-button-gender').on('click', function () {
      $('input[name="data[Filters][gender]"]').each(function() {
        $(this).bootstrapSwitch('state', true);
        $(this).bootstrapSwitch('toggleState');
      });
      $('#gendervalue').val($(this).val());
    });
    //logica filtros teléfono
    $('#toggle-state-switch-button-phone').on('click', function () {
      $('input[name="data[Filters][has_phone]"]').each(function() {
        $(this).bootstrapSwitch('state', true);
        $(this).bootstrapSwitch('toggleState');
      });
      $('#phonevalue').val($(this).val());
    });

    //logica freuencia
    $('input[name="data[Frequency]"]').on('switchChange.bootstrapSwitch', function(event, state) {
      if (($(this).val() == 'semana') || ($(this).val() == 'mes')) {
        $('#divNameForm').show();
      } else {
        $('#divNameForm').hide();
        $('#ReportNameForm').val('');
      }
      if ($(this).val() != 'semana') {
        $('input[name="data[weekDay]"]').each(function() {
          $(this).bootstrapSwitch('disabled', false);
          $(this).bootstrapSwitch('state', true);
          $(this).bootstrapSwitch('toggleState');
          $(this).bootstrapSwitch('disabled', true);
          
        });
      } else {
        $('input[name="data[weekDay]"]').each(function() {
          $(this).bootstrapSwitch('state', false);
          $(this).bootstrapSwitch('disabled', false);
        });
      }
      $('#frequencyvalues').val($(this).val());
      $('#frequencyvaluesday').val('');
    });

    $('input[name="data[weekDay]"]').on('switchChange.bootstrapSwitch', function(event, state) {
      $('#frequencyvaluesday').val($(this).val());
    });


    //logica guardar formulario 
    $('.saveform').on('click', function() {
      $(this).attr('disabled','disabled');
      
      fechasCorrectas = true;
      if (($('#ReportBirthdateFormat1').val() != '') && ($('#ReportBirthdate1').val() == '')) {
        fechasCorrectas = false;
      }
      if (($('#ReportBirthdateFormat1').val() == '') && ($('#ReportBirthdate1').val() != '')) {
        fechasCorrectas = false;
      }
      if (($('#ReportBirthdateFormat2').val() != '') && ($('#ReportBirthdate2').val() == '')) {
        fechasCorrectas = false;
      }
      if (($('#ReportBirthdateFormat2').val() == '') && ($('#ReportBirthdate2').val() != '')) {
        fechasCorrectas = false;
      }
      if ((($('#ReportBirthdateFormat2').val() != '') && ($('#ReportBirthdate2').val() != ''))&&
         (($('#ReportBirthdateFormat1').val() == '') || ($('#ReportBirthdate1').val() == ''))) {
        bootbox.alert(I18nJs.t("Seleccione un filtro para la primera fecha si selecciona un filtro para la segunda fecha o ponga el filtro en la primer input de las fechas"));
        $(this).removeAttr('disabled');
        return false;
      }
      if (!fechasCorrectas) {
        bootbox.alert(I18nJs.t("Filtro de las fechas no correctas"));
        $(this).removeAttr('disabled');
        return false;
      }
      
      if ($('#ReportFields').val() == null) {
        bootbox.alert(I18nJs.t("Debe seleccionar almenos un campo a mostrar"));
        $(this).removeAttr('disabled');
        return false;
      }
      if ($('#frequencyvalues').val() == '') {
        bootbox.alert(I18nJs.t("Debe seleccionar una frecuencia de envío"));
        $(this).removeAttr('disabled');
        return false;
      }

      if ($('#frequencyvalues').val() == 'semana') {
        if ($('#frequencyvaluesday').val() == '' ) {
          bootbox.alert(I18nJs.t("Si selecciona frecuencia semanal debe seleccionar el día de envío"));
          $(this).removeAttr('disabled');
          return false;
        }
      }
      if (($('#frequencyvalues').val() == 'semana') || ($('#frequencyvalues').val() == 'mes')) { 
        if ($('#ReportNameForm').val() == '') {
          bootbox.alert(I18nJs.t("Si selecciona frecuencia semanal o mensual debe dar un nombre al informe"));
          $(this).removeAttr('disabled');
          return false;
        }
      }
      if ($('#ReportEmailvalues').val() == '') {
        bootbox.alert(I18nJs.t("Debe seleccionar almenos un email de envío"));
        $(this).removeAttr('disabled');
        return false;
      }
      if ($('#ReportSelectFormat').val() == '') {
        bootbox.alert(I18nJs.t("El formato no puede estar vacío"));
        $(this).removeAttr('disabled');
        return false;
      }
      $("body").addClass("loading");
      $('#formwizard').submit();

    });
    
    var $createDestroy, $window, sectionTop;
    $window = $(window);
    sectionTop = $(".top").outerHeight() + 20;
    $createDestroy = $("#switch-create-destroy");
    hljs.initHighlightingOnLoad();
    $("a[href*=\"#\"]").on("click", function(event) {
      var $target;
      event.preventDefault();
      $target = $($(this).attr("href").slice("#"));
      if ($target.length) {
        return $window.scrollTop($target.offset().top - sectionTop);
      }
    });
    $("input[type=\"checkbox\"], input[type=\"radio\"]").not("[data-switch-no-init]").bootstrapSwitch();
    $("[data-switch-get]").on("click", function() {
      var type;
      type = $(this).data("switch-get");
      return alert($("#switch-" + type).bootstrapSwitch(type));
    });
    $("[data-switch-set]").on("click", function() {
      var type;
      type = $(this).data("switch-set");
      return $("#switch-" + type).bootstrapSwitch(type, $(this).data("switch-value"));
    });
    $("[data-switch-toggle]").on("click", function() {
      var type;
      type = $(this).data("switch-toggle");
      return $("#switch-" + type).bootstrapSwitch("toggle" + type.charAt(0).toUpperCase() + type.slice(1));
    });
    $("[data-switch-set-value]").on("input", function(event) {
      var type, value;
      event.preventDefault();
      type = $(this).data("switch-set-value");
      value = $.trim($(this).val());
      if ($(this).data("value") === value) {
        return;
      }
      return $("#switch-" + type).bootstrapSwitch(type, value);
    });
    $("[data-switch-create-destroy]").on("click", function() {
      var isSwitch;
      isSwitch = $createDestroy.data("bootstrap-switch");
      $createDestroy.bootstrapSwitch((isSwitch ? "destroy" : null));
      return $(this).button((isSwitch ? "reset" : "destroy"));
    });
    return $confirm = $("#confirm").bootstrapSwitch({
      size: "large",
      onSwitchChange: function(event, state) {
        event.preventDefault();
        return console.log(state, event.isDefaultPrevented());
      }
    });
    
  });

}).call(this);
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 
