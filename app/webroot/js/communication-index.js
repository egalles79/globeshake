$(document).ready(function(){

    var wizard = $('div#MyWizard');

    wizard.on('finished', function(e, data) {
        $('form#CommunicationAddCampaignForm').submit();
    });

    wizard.wizard();

});