server  = (window.location.href.substring(0,16) == 'http://localhost') ? 'http://localhost/globecake' : '';
console.log(window.app.variable);
var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 1,
    center: new google.maps.LatLng(35.137879, -82.836914),
    mapTypeId: google.maps.MapTypeId.ROADMAP
});

var myMarker = new google.maps.Marker({
    position: new google.maps.LatLng(47.651968, 9.478485),
    draggable: true
});

var input = /** @type {HTMLInputElement} */(
        document.getElementById('pac-input'));
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));
console.log(searchBox);
google.maps.event.addListener(myMarker, 'dragend', function (evt) {
    $('#OfficeAddresss').val('');
    $('#OfficeCitId').val('');
    var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+evt.latLng.lat().toFixed(3)+","+evt.latLng.lng().toFixed(3)+"&sensor=true";
    locc = evt.latLng;
    map.setCenter(locc);
    
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(locc);
    
    latitudeLongitude = JSON.stringify(bounds);
    console.log(bounds);
    bounds.extend(locc);
    console.log(locc);
    latitudeLongitude = latitudeLongitude.split(',');
    longitude = JSON.parse("{"+latitudeLongitude[1]+"}");
    longitude = longitude["west"];
    latitude = JSON.parse("{"+latitudeLongitude[2]+"}");
    latitude = latitude["north"];

    $('#OfficeLongitude').val(longitude);
    $('#OfficeLatitude').val(latitude);
    map.fitBounds(bounds);
    map.setZoom(20);
    $.ajax({
      url: url,
    }).success(function(result) {
      if (result['status'] == 'OK') {
        if (typeof result != 'undefined') {
          $('#pac-input').val(result['results'][0]['formatted_address']);
          $('#OfficeCompletAddress').val($('#pac-input').val());
          var haslocality = false;
          var hasroute = false;
          var hasadministrativearea = false;
          $.each(result['results'][0]['address_components'], function(index, resultType) {
            if (resultType['types'][0] == 'country') {
                var url = server+'/Countries/get_country_from_code_2/'+resultType['short_name'];
                console.log(url); 
                $.ajax({
                  url:  url,
                  type: 'get',
                  success:  function (response) {
                      if (response != false) {
                        $('#OfficeCountryId').val(response);
                      } else {
                        $('#OfficeCountryId').val(0);
                      }
                      $('#OfficeCountryId').change();
                  }
                });
            }
            if (resultType['types'][0] == 'street_number') {
                $('#OfficeAddress').val(','+resultType['short_name']);
            }
            if (resultType['types'][0] == 'postal_code') {
                $('#OfficeAddress').val($('#OfficeAddress').val()+'('+resultType['short_name']+')');
            }
            if (resultType['types'][0] == 'locality') {
                haslocality = true;
                searchplace = searchplace = resultType['long_name'].trim();
            }
            if (!haslocality) {
              if (resultType['types'][0] == 'administrative_area_level_1') {
                  hasadministrativearea = true;
                  searchplace = resultType['long_name'].trim();
              }
            }
            if ((!haslocality)&&(!hasadministrativearea)) {
              if (resultType['types'][0] == 'route') {
                  hasroute = true;
                  searchplace = resultType['long_name'].trim();
                  $('#OfficeAddress').val(searchplace+$('#OfficeAddress').val());
              }
            }
          });
          url = server+'/Cities/get_city_from_name/'+searchplace;
          $.ajax({
            url:  url,
            type: 'get',
            success:  function (response) {
                if (response != false) {
                  response = JSON.parse(response);
                  $('#OfficeCit').val(response['name']);
                  $('#OfficeCitId').val(response['id']);
                } else {
                  $('#OfficeCit').val(searchplace);
                }
            }
          });
          $('#OfficeAddresss').val(result['results'][0]['formatted_address']);
        } 
      }
    });
});




google.maps.event.addListener(myMarker, 'dragstart', function (evt) {
    $('#pac-input').val('Asignando punto');
});
google.maps.event.addListener(map, 'bounds_changed', function() {
  var bounds = map.getBounds();
  searchBox.setBounds(bounds);
});

map.setCenter(myMarker.position);
myMarker.setMap(map);
markers = [];

google.maps.event.addListener(searchBox, 'places_changed', function() {
      $('#OfficeAddress').val('');
      $('#OfficeCitId').val('');
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }
      for (var i = 0, marker; marker = markers[i]; i++) {
        marker.setMap(null);
      }

      // For each place, get the icon, place name, and location.
      markers = [];
      var bounds = new google.maps.LatLngBounds();
      var myPlaces = [];
      var results  = [];
      for (var i = 0, place; place = places[i]; i++) {
        var image = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        myMarker.setPosition(place.geometry.location);

        map.setCenter(place.geometry.location);
        //markers.push(marker);

        bounds.extend(place.geometry.location);
        var hasCompleteAddress = false;
        if (place.types[0] == 'street_address') {
          //latitudeLongitude = JSON.stringify(place.geometry.location);
          //latitudeLongitude = latitudeLongitude.split(',');
          //latitude = latitudeLongitude[0].substring(5);
          //longitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
          
          latitudeLongitude = JSON.stringify(bounds);
          latitudeLongitude = latitudeLongitude.split(',');
          latitude = latitudeLongitude[1].substring(4,latitudeLongitude[1].length-1);
          longitude = latitudeLongitude[2].substring(9,latitudeLongitude[2].length);

          $('#OfficeLongitude').val(longitude);
          $('#OfficeLatitude').val(latitude);
          var hasCompleteAddress = true;
        } else if (place.types[0] == 'route') {
          var hasCompleteAddress = true;
        } else {
          $('#OfficeLongitude').val('');
          $('#OfficeLatitude').val('');
        }
        results.push(place['address_components']);
        myPlaces.push(place.types[0]);
      }
      map.fitBounds(bounds);
      var listener = google.maps.event.addListener(map, "idle", function() { 
        val = map.getZoom();
        switch (myPlaces[0]) {
          case 'continent' :
            val = 4;
            break;
          case 'country' :
            val = 6;
            break;
          case 'administrative_area_level_1':
          case 'locality' :
            val = 13;
            break;
          case 'lodging' :
          case 'route' :
            val = 17;
            break;
          case 'neighborhood' :
            val = 16;
            break;
        }
          if (hasCompleteAddress) {
          $('#OfficeAddress').val('');
          $('#OfficeCompletAddress').val($('#pac-input').val());
          var haslocality = false;
          var hasroute = false;
          var hasadministrativearea = false;
          var i = 0;
          $.each(results[0], function(index, resultType) {
            if (resultType['types'][0] == 'country') {
              var url = server+'/Countries/get_country_from_code_2/'+resultType['short_name'];

                $.ajax({
                  url:  url,
                  type: 'get',
                  success:  function (response) {
                      if (response != false) {
                        $('#OfficeCountryId').val(response);
                      } else {
                        $('#OfficeCountryId').val(0);
                      }
                      $('#OfficeCountryId').change();
                  }
                });
            }
            if (resultType['types'][0] == 'postal_code') {
                $('#OfficeAddress').val($('#OfficeAddress').val()+'('+resultType['short_name']+')');
            }
            if (resultType['types'][0] == 'street_number') {
                $('#OfficeAddress').val(','+resultType['short_name'].trim());
            }
            if (resultType['types'][0] == 'locality') {
                haslocality = true;
                searchplace = resultType['short_name'].trim();
            }
            if (!haslocality) {
              if (resultType['types'][0] == 'administrative_area_level_1') {
                  hasadministrativearea = true;
                  searchplace = resultType['long_name'].trim();
              }
            }
            if ((!haslocality)&&(!hasadministrativearea)) {
              if (resultType['types'] == 'route') {
                  hasroute = true;
                  searchplace = resultType['long_name'].trim();
                  $('#OfficeAddress').val(searchplace+$('#OfficeAddress').val());
              }
            }
            i++;
          });
          $.ajax({
            url:  server+'/Cities/get_city_from_name/'+searchplace,
            type: 'get',
            success:  function (response) {
                if (response != false) {
                  response = JSON.parse(response);
                  $('#OfficeCit').val(response['name']);
                  $('#OfficeCitId').val(response['id']);
                } else {
                  $('#OfficeCit').val(searchplace);
                }
            }
          });
        }
        map.setZoom(val);
        google.maps.event.removeListener('bounds_changed'); 
      });
});

google.maps.event.trigger('places_changed');