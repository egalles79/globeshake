// Typeaheadjs.10x.js
// https://github.com/btempchin/x-editable/blob/master/dist/inputs-ext/typeaheadjs/typeaheadjs.10x.js
(function ($) {
    "use strict";

    var Constructor = function (options) {
        this.init('typeaheadjs', options, Constructor.defaults);
    };

    $.fn.editableutils.inherit(Constructor, $.fn.editabletypes.text);

    $.extend(Constructor.prototype, {
        render: function() {
            this.renderClear();
            this.setClass();
            this.setAttr('placeholder');
            this.$input.typeahead.apply(this.$input, this.options.typeahead);

            // copy `input-sm | input-lg` classes to placeholder input
            if($.fn.editableform.engine === 'bs3') {
                if(this.$input.hasClass('input-sm')) {
                    this.$input.siblings('input.tt-hint').addClass('input-sm');
                }
                if(this.$input.hasClass('input-lg')) {
                    this.$input.siblings('input.tt-hint').addClass('input-lg');
                }
            }
        }
    });

    Constructor.defaults = $.extend({}, $.fn.editabletypes.list.defaults, {
        /**
        @property tpl
        @default <input type="text">
        **/
        tpl:'<input type="text">',
        /**
        Configuration of typeahead itself.
        [Full list of options](https://github.com/twitter/typeahead.js#dataset).

        @property typeahead
        @type object
        @default null
        **/
        typeahead: null,
        /**
        Whether to show `clear` button

        @property clear
        @type boolean
        @default true
        **/
        clear: true
    });

    $.fn.editabletypes.typeaheadjs = Constructor;

}(window.jQuery));

function toObject(arr) {
  var rv = {};
  str = '';
  for (var i = 0; i < arr.length; ++i) {
    if (arr[i] !== undefined) {
        str += arr[i];
    }
  }
  rv['value'] = str;
  return rv;
}
function toString(arr) {
  return arr.toString();
}
// Our code
$(function () {
    var sectors = JSON.parse($('#sectors').val());    
    var sectorsArr = [];
    $.each(sectors, function(index,value) {
        value = toObject(value);
        sectorsArr.push(value);
    });
    var functionalities = JSON.parse($('#functionalities').val());    
    var functionalitiesArr = [];
    $.each(functionalities, function(index,value) {
        value = toObject(value);
        functionalitiesArr.push(value);
    });
    var companies = JSON.parse($('#companies').val());    
    var companiesArr = [];
    $.each(companies, function(index,value) {
        value = toObject(value);
        companiesArr.push(value);
    });
    var departments = JSON.parse($('#departments').val());    
    var departmentsArr = [];
    $.each(departments, function(index,value) {
        value = toObject(value);
        departmentsArr.push(value);
    });
    var cities = JSON.parse($('#cities').val());    
    var citiesArr = [];
    $.each(cities, function(index,value) {
        value = toObject(value);
        citiesArr.push(value);
    });
    // some = [{ value: "Alabama" }, { value: "Alaska" }, { value: "Arizona" }, { value: "Arkansas" }, { value: "California" }, { value: "Colorado" }, { value: "Connecticut" }, { value: "Delaware" }, { value: "Florida" }, { value: "Georgia" }, { value: "Hawaii" }, { value: "Idaho" }, { value: "Illinois" }, { value: "Indiana" }, { value: "Iowa" }, { value: "Kansas" }, { value: "Kentucky" }, { value: "Louisiana" }, { value: "Maine" }, { value: "Maryland" }, { value: "Massachusetts" }, { value: "Michigan" }, { value: "Minnesota" }, { value: "Mississippi" }, { value: "Missouri" }, { value: "Montana" }, { value: "Nebraska" }, { value: "Nevada" }, { value: "New Hampshire" }, { value: "New Jersey" }, { value: "New Mexico" }, { value: "New York" }, { value: "North Dakota" }, { value: "North Carolina" }, { value: "Ohio" }, { value: "Oklahoma" }, { value: "Oregon" }, { value: "Pennsylvania" }, { value: "Rhode Island" }, { value: "South Carolina" }, { value: "South Dakota" }, { value: "Tennessee" }, { value: "Texas" }, { value: "Utah" }, { value: "Vermont" }, { value: "Virginia" }, { value: "Washington" }, { value: "West Virginia" }, { value: "Wisconsin" }, { value: "Wyoming" }],
    // console.log(some);
    //aconsole.log(some2);
    'use strict';
    var engine = new Bloodhound({
        name: 'functionality',
        //local: [{ value: "Alabama" }, { value: "Alaska" }, { value: "Arizona" }, { value: "Arkansas" }, { value: "California" }, { value: "Colorado" }, { value: "Connecticut" }, { value: "Delaware" }, { value: "Florida" }, { value: "Georgia" }, { value: "Hawaii" }, { value: "Idaho" }, { value: "Illinois" }, { value: "Indiana" }, { value: "Iowa" }, { value: "Kansas" }, { value: "Kentucky" }, { value: "Louisiana" }, { value: "Maine" }, { value: "Maryland" }, { value: "Massachusetts" }, { value: "Michigan" }, { value: "Minnesota" }, { value: "Mississippi" }, { value: "Missouri" }, { value: "Montana" }, { value: "Nebraska" }, { value: "Nevada" }, { value: "New Hampshire" }, { value: "New Jersey" }, { value: "New Mexico" }, { value: "New York" }, { value: "North Dakota" }, { value: "North Carolina" }, { value: "Ohio" }, { value: "Oklahoma" }, { value: "Oregon" }, { value: "Pennsylvania" }, { value: "Rhode Island" }, { value: "South Carolina" }, { value: "South Dakota" }, { value: "Tennessee" }, { value: "Texas" }, { value: "Utah" }, { value: "Vermont" }, { value: "Virginia" }, { value: "Washington" }, { value: "West Virginia" }, { value: "Wisconsin" }, { value: "Wyoming" }],
        local: functionalitiesArr,
        // remote: '/list',
        datumTokenizer: function(d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });
    var engine2 = new Bloodhound({
        name: 'company',
        //local: [{ value: "Alabama" }, { value: "Alaska" }, { value: "Arizona" }, { value: "Arkansas" }, { value: "California" }, { value: "Colorado" }, { value: "Connecticut" }, { value: "Delaware" }, { value: "Florida" }, { value: "Georgia" }, { value: "Hawaii" }, { value: "Idaho" }, { value: "Illinois" }, { value: "Indiana" }, { value: "Iowa" }, { value: "Kansas" }, { value: "Kentucky" }, { value: "Louisiana" }, { value: "Maine" }, { value: "Maryland" }, { value: "Massachusetts" }, { value: "Michigan" }, { value: "Minnesota" }, { value: "Mississippi" }, { value: "Missouri" }, { value: "Montana" }, { value: "Nebraska" }, { value: "Nevada" }, { value: "New Hampshire" }, { value: "New Jersey" }, { value: "New Mexico" }, { value: "New York" }, { value: "North Dakota" }, { value: "North Carolina" }, { value: "Ohio" }, { value: "Oklahoma" }, { value: "Oregon" }, { value: "Pennsylvania" }, { value: "Rhode Island" }, { value: "South Carolina" }, { value: "South Dakota" }, { value: "Tennessee" }, { value: "Texas" }, { value: "Utah" }, { value: "Vermont" }, { value: "Virginia" }, { value: "Washington" }, { value: "West Virginia" }, { value: "Wisconsin" }, { value: "Wyoming" }],
        local: companiesArr,
        // remote: '/list',
        datumTokenizer: function(d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });
    var engine3 = new Bloodhound({
        name: 'department',
        //local: [{ value: "Alabama" }, { value: "Alaska" }, { value: "Arizona" }, { value: "Arkansas" }, { value: "California" }, { value: "Colorado" }, { value: "Connecticut" }, { value: "Delaware" }, { value: "Florida" }, { value: "Georgia" }, { value: "Hawaii" }, { value: "Idaho" }, { value: "Illinois" }, { value: "Indiana" }, { value: "Iowa" }, { value: "Kansas" }, { value: "Kentucky" }, { value: "Louisiana" }, { value: "Maine" }, { value: "Maryland" }, { value: "Massachusetts" }, { value: "Michigan" }, { value: "Minnesota" }, { value: "Mississippi" }, { value: "Missouri" }, { value: "Montana" }, { value: "Nebraska" }, { value: "Nevada" }, { value: "New Hampshire" }, { value: "New Jersey" }, { value: "New Mexico" }, { value: "New York" }, { value: "North Dakota" }, { value: "North Carolina" }, { value: "Ohio" }, { value: "Oklahoma" }, { value: "Oregon" }, { value: "Pennsylvania" }, { value: "Rhode Island" }, { value: "South Carolina" }, { value: "South Dakota" }, { value: "Tennessee" }, { value: "Texas" }, { value: "Utah" }, { value: "Vermont" }, { value: "Virginia" }, { value: "Washington" }, { value: "West Virginia" }, { value: "Wisconsin" }, { value: "Wyoming" }],
        local: departmentsArr,
        // remote: '/list',
        datumTokenizer: function(d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });
    var engine4 = new Bloodhound({
        name: 'sector',
        //local: [{ value: "Alabama" }, { value: "Alaska" }, { value: "Arizona" }, { value: "Arkansas" }, { value: "California" }, { value: "Colorado" }, { value: "Connecticut" }, { value: "Delaware" }, { value: "Florida" }, { value: "Georgia" }, { value: "Hawaii" }, { value: "Idaho" }, { value: "Illinois" }, { value: "Indiana" }, { value: "Iowa" }, { value: "Kansas" }, { value: "Kentucky" }, { value: "Louisiana" }, { value: "Maine" }, { value: "Maryland" }, { value: "Massachusetts" }, { value: "Michigan" }, { value: "Minnesota" }, { value: "Mississippi" }, { value: "Missouri" }, { value: "Montana" }, { value: "Nebraska" }, { value: "Nevada" }, { value: "New Hampshire" }, { value: "New Jersey" }, { value: "New Mexico" }, { value: "New York" }, { value: "North Dakota" }, { value: "North Carolina" }, { value: "Ohio" }, { value: "Oklahoma" }, { value: "Oregon" }, { value: "Pennsylvania" }, { value: "Rhode Island" }, { value: "South Carolina" }, { value: "South Dakota" }, { value: "Tennessee" }, { value: "Texas" }, { value: "Utah" }, { value: "Vermont" }, { value: "Virginia" }, { value: "Washington" }, { value: "West Virginia" }, { value: "Wisconsin" }, { value: "Wyoming" }],
        local: sectorsArr,
        // remote: '/list',
        datumTokenizer: function(d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });
    var engine5 = new Bloodhound({
        name: 'city',
        //local: [{ value: "Alabama" }, { value: "Alaska" }, { value: "Arizona" }, { value: "Arkansas" }, { value: "California" }, { value: "Colorado" }, { value: "Connecticut" }, { value: "Delaware" }, { value: "Florida" }, { value: "Georgia" }, { value: "Hawaii" }, { value: "Idaho" }, { value: "Illinois" }, { value: "Indiana" }, { value: "Iowa" }, { value: "Kansas" }, { value: "Kentucky" }, { value: "Louisiana" }, { value: "Maine" }, { value: "Maryland" }, { value: "Massachusetts" }, { value: "Michigan" }, { value: "Minnesota" }, { value: "Mississippi" }, { value: "Missouri" }, { value: "Montana" }, { value: "Nebraska" }, { value: "Nevada" }, { value: "New Hampshire" }, { value: "New Jersey" }, { value: "New Mexico" }, { value: "New York" }, { value: "North Dakota" }, { value: "North Carolina" }, { value: "Ohio" }, { value: "Oklahoma" }, { value: "Oregon" }, { value: "Pennsylvania" }, { value: "Rhode Island" }, { value: "South Carolina" }, { value: "South Dakota" }, { value: "Tennessee" }, { value: "Texas" }, { value: "Utah" }, { value: "Vermont" }, { value: "Virginia" }, { value: "Washington" }, { value: "West Virginia" }, { value: "Wisconsin" }, { value: "Wyoming" }],
        local: citiesArr,
        // remote: '/list',
        datumTokenizer: function(d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });
    engine.initialize();
    engine2.initialize();
    engine3.initialize();
    engine4.initialize();
    engine5.initialize();

    // editable
    $('#functionality').editable({
        mode: "inline",
        typeahead: [
            {
                minLength: 1,
                highlight: true,
                hint: true
            },
            {
                name: 'functionality',
                source: engine.ttAdapter()
            }
        ],
        validate: function(value) {
           if($.trim(value) == '') return I18nJs.t('Campo requerido');
        }
    });
    $('#city').editable({
        mode: "inline",
        typeahead: [
            {
                minLength: 1,
                highlight: true,
                hint: true
            },
            {
                name: 'city',
                source: engine5.ttAdapter()
            }
        ],
        validate: function(value) {
           if($.trim(value) == '') return I18nJs.t('Campo requerido');
        }
    });
    // editable
    $('#sector').editable({
        mode: "inline",
        typeahead: [
            {
                minLength: 1,
                highlight: true,
                hint: true
            },
            {
                name: 'sector',
                source: engine4.ttAdapter()
            }
        ],
    });

    // editable
    $('#company').editable({
        mode: "inline",
        typeahead: [
            {
                minLength: 1,
                highlight: true,
                hint: true
            },
            {
                name: 'company',
                source: engine2.ttAdapter(),
            }
        ],
        validate: function(value) {
           if($.trim(value) == '') return I18nJs.t('Campo requerido');
        },
        success: function(response, newValue) {
            $('.address').html('Guarde para ver la dirección de la compañía');
        }
    });

    // editable
    $('#department').editable({
        mode: "inline",
        typeahead: [
            {
                minLength: 1,
                highlight: true,
                hint: true
            },
            {
                name: 'departments',
                source: engine3.ttAdapter()
            }
        ]
    });
});