$('#cardImage').hide();
  $('#menu1').addClass('active2');
  $('#menu2').removeClass('active2');
  $(function() {
      $('article.tabs').tabs();
  });
  $('.viewCard').on('click', function() {
    var user = $(this).attr('datauser');
    var url  = '/Users/get_data_card/'+user;
    $.ajax({
      url:  url,
      type: 'post',
      data: null,
      success:  function (response) {
          if (response ) {
            var user = JSON.parse(response);
            var urlImage = user['User']['photo_dir'].length;
            urlImage = (urlImage > 0) ? user['User']['photo'] : 'camarafot.png';
            
            var nameImage = (urlImage == 'camarafot.png') ? '/img/'+urlImage : '/files/users/user_'+user['User']['id']+'/'+urlImage;
            var aStaticFuncionalitiesNames = $('#staticFunctionalitiesNames').val();
            var staticFuncionalitiesNames = aStaticFuncionalitiesNames;
            console.log(user);
            $('#card-image').html('<div class="row tarjeta" style="color: #333333;font-size: 16px;"><div style="position:relative;float:right;margin-top:-5px"><a href="/Companies/ver_cv">X</a></div><div class="row star"><img src="/img/star_03.png" alt="" data-pin-nopin="true"></div><div class="col-md-12">'+user.Sector.name+'<div class="row"><div class="col-md-3"><img src="'+nameImage+'" width="100px" heigth="100px"><div class="form-group"></div></div><div class="col-md-9"><div class="form-inline"><label class="radio" style="cursor:pointer"><input type="radio" name="User[hasCompany]" id="hasCompanyA" value="A" style="cursor:pointer">Soy autónomo&nbsp;&nbsp;&nbsp;&nbsp;</label><label class="radio" style="cursor:pointer"><input type="radio" name="User[hasCompany]" id="hasCompanyE" value="E" style="cursor:pointer" checked="">Empleado</label></div><div class="row">'+user.Company.name+'</div><div class="row">'+user.User.firstname+'&nbsp;&nbsp;'+user.User.lastname+'</div><div class="row">'+user.Functionality.name+'</div><div class="row">'+user.Department.name+'</div><div class="row">'+user.Country.name+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+user.City.name+'</div></div></div></div><div class="row-fluid bot"><div class="span8 edis">'+user.User.email+'</div><div class="span8 edis"><span class="address">'+user.Company.address+'</span></div></div><div class="row-fluid" style="margin-top: 0px;font-size:12px;"><div class="text-center" style="margin-top:10px"></div></div>');
            $('#cardImage').show();
          } 
      }
    });
  });