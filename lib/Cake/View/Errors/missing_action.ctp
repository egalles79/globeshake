<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'jquery.mockjax.min',
  'bootstrap-editable.min',
  'moment.min',
  'typeahead.min',
  'typeaheadjs.min',
  'address.min',
  'select2.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));
?>
<h2><?php echo __d('cake_dev', 'Missing Method in %s', h($controller)); ?></h2> <p class="error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', 'The action %1$s is not defined in controller %2$s', '<em>' . h($action) . '</em>', '<em>' . h($controller) . '</em>'); ?>
</p>
<p class="error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', 'Create %1$s%2$s in file: %3$s.', '<em>' . h($controller) . '::</em>', '<em>' . h($action) . '()</em>', APP_DIR . DS . 'Controller' . DS . h($controller) . '.php'); ?>
</p>
<pre>
&lt;?php
class <?php echo h($controller); ?> extends AppController {

<strong>
	public function <?php echo h($action); ?>() {

	}
</strong>
}
</pre>
<p class="notice">
	<strong><?php echo __d('cake_dev', 'Notice'); ?>: </strong>
	<?php echo __d('cake_dev', 'If you want to customize this error message, create %s', APP_DIR . DS . 'View' . DS . 'Errors' . DS . 'missing_action.ctp'); ?>
</p>
<?php echo $this->element('exception_stack_trace'); ?>
