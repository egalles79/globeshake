<?php

$this->extend('/Common/panel');

echo $this->Html->script(array(
  'jquery-ui-1.10.3.custom.min',
  'jquery.mockjax.min',
  'bootstrap-editable.min',
  'moment.min',
  'typeahead.min',
  'typeaheadjs.min',
  'address.min',
  'select2.min',
  'custom.min',
  'core.min',
  'bootbox.min'
), array('inline' => false));

$pluginDot = empty($plugin) ? null : $plugin . '.';
?>
<h2><?php echo __d('cake_dev', 'Missing Behavior'); ?></h2>
<p class="error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', '%s could not be found.', '<em>' . h($pluginDot . $class) . '</em>'); ?>
</p>
<p class="error">
	<strong><?php echo __d('cake_dev', 'Error'); ?>: </strong>
	<?php echo __d('cake_dev', 'Create the class %s below in file: %s', '<em>' . h($class) . '</em>', (empty($plugin) ? APP_DIR . DS : CakePlugin::path($plugin)) . 'Model' . DS . 'Behavior' . DS . h($class) . '.php'); ?>
</p>
<pre>
&lt;?php
class <?php echo h($class); ?> extends ModelBehavior {

}
</pre>
<p class="notice">
	<strong><?php echo __d('cake_dev', 'Notice'); ?>: </strong>
	<?php echo __d('cake_dev', 'If you want to customize this error message, create %s', APP_DIR . DS . 'View' . DS . 'Errors' . DS . 'missing_behavior.ctp'); ?>
</p>

<?php echo $this->element('exception_stack_trace'); ?>
