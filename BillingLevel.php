<?php
App::uses('AppModel', 'Model');
/**
 * BillingLevel Model
 *
 */
class BillingLevel extends AppModel {
/**
 * Validation rules
 *
 * @var array
 */
  public $validate = array();
/**
 * getLevelsFromUserId method
 *
 * @throws NotFoundException
 * @param integer $iUserId, integer $deep
 * @return array
 */
  public static function getLevelsFromUserId($iUserId, $deep = 1) {
    $this->loadModel('User');
    if (!$this->User->exists($iUserId)) {
      throw new NotFoundException(__('Invalid user'));
    }

    $id = $this->User->find('first', array(
      'recursive' => -1,
      'fields' => array('id,billing_levels_id'),
      'conditions' => array('id' => $iUserId)
    ));

    $deepLevels = array('level1d');
    if ($deep > 1) {
      for($levels = 2;$levels<=$deep;$levels++) {
        $deepLevels[] = 'level'.$levels.'Id';
      }
    }
    $iBillingLevelData = $this->find('first',array(
      'recursive' => -1,
      'fields' => $deepLevels,
      'conditions' => array('id' => $id['User']['billing_levels_id'])
    ));
    return $iBillingLevelData;
  }
}
