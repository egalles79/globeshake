Hi, Thanks for purchase our item Official - Responsive Multipurpose HTML5 Template
---------------------------------------------------------------------------------------------------

Included:
		+120 HTML template
		8 Layered PSD - dark & light
		Help Documentation

---------------------------------------------------------------------------------------------------

Features:
		+120 valid HTML5 template
		9 Different Slideshow
		Ready for Corporate, Magazine, Shop and lot of more websites
		Premium Revolution Slider $12
		Premium Layer Slider $10
		Ajax Contact Form $4
		+15 Homepages Different
		Optional Sticky Header Navigation ON/OFF
		Different header's and footer's
		Dark and Light Version
		Left, Right, Both Sidebar and Both Sidebar for left and right
		8 Layered PSD dark & light
		Seo Optimized for Search Engine
		Fully Responsive pc, phone and tablet
		11 Color included + Unlimited Colors you can use
		Boxed and Fullwide Version
		10 Boxed Frames
		RTL Languages Support like: Hebrew, Arabic, Persian, ...
		Random Style Generator + Tutorial
		Modern Responsive Mobile Navigation Swipe Touch
		11 Shopping Page for convertion to woocommerce
		Products Slider Touchable in touch devices
		26 HD Backgrounds + 24 Web pattern
		20 Breadcrumb Backgrounds
		Unique styles for Headings
		+660 Retina Ready Icon + Metro
		+20 Shortcodes
		3 Contact Page
		1,2,3,4 Columns Portfolio
		10 Blog Pages - Classic and Modern Masonry
		Disqus and facebook comment form
		Blog Single Posts with Social Share buttons
		Event Timer for Under Construction page
		Support forum pages for conversion to bbpress
		10 Different Services blocks
		Landing Page with scrolling
		Twitter API 1.1 widget
		Over +620 Google web fonts you can use
		Googlemap with jquery gmap3
		Support any video Vimeo, Youtube, Dailymotion, Blip TV, Justin tv, Ustream TV, ...
		CSS3 Animated load blocks with scrolling
		jQuery Nicescroll
		Filterable Portfolio with prettyphoto lightbox
		Animated Progress Bars in unlimited Colors
		+5 Animated Knob Progress Bars in unlimited Colors
		New Pricing Tables 2, 3, 4 Columns
		12 Columns Grid System
		
---------------------------------------------------------------------------------------------------

Regards
T20

http://themeforest.net/user/T20

